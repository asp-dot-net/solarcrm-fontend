import { PermissionCheckerService, FeatureCheckerService } from 'abp-ng2-module';
import { AppSessionService } from '@shared/common/session/app-session.service';

import { Injectable } from '@angular/core';
import { AppMenu } from '../../../shared/layout/nav/app-menu';
import { AppMenuItem } from '../../../shared/layout/nav/app-menu-item';

@Injectable()
export class AppNavigationService {
    constructor(
        private _permissionCheckerService: PermissionCheckerService,
        private _appSessionService: AppSessionService,
        private _featureCheckerService: FeatureCheckerService
    ) { }

    getMenu(): AppMenu {
        return new AppMenu('MainMenu', 'MainMenu', [
            new AppMenuItem(
                'Dashboard',
                'Pages.Administration.Host.Dashboard',
                'las la-home',
                '/app/admin/hostDashboard'
            ),
            // new AppMenuItem('Dashboard', 'Pages.Tenant.Dashboard', 'las la-home', '/app/main/dashboard'),
            new AppMenuItem('Dashboard', 'Pages.Tenant.Dashboard', 'las la-home', '/app/main/dashboard'),
            new AppMenuItem('Tenants', 'Pages.Tenants', 'las la-database', '/app/admin/tenants'),
            new AppMenuItem('Editions', 'Pages.Editions', 'las la-box-open', '/app/admin/editions'),

            // new AppMenuItem(
            //     'DemoUiComponents',
            //     null,
            //     'flaticon-shapes',
            //     '/app/admin/demo-ui-components',
            //     [],
            //     [],
            //     // undefined, undefined,
            //     // () => { return this._featureCheckerService.isEnabled('App.Administration.DemoUIComponents'); }
            // ),
            new AppMenuItem(
                'DataVaults',
                'Pages.Tenant.DataVaults',
                'las la-database',
                '',
                [],
                [
                    new AppMenuItem(
                        'Locations',
                        'Pages.Tenant.DataVaults.Locations',
                        'las la-angle-right',
                        '/app/main/datavaults/locations',
                        [],
                        [],
                        undefined, undefined,
                        () => { return this._featureCheckerService.isEnabled('App.DataVaults.Locations'); }
                    ),
                    new AppMenuItem(
                        'Discom',
                        'Pages.Tenant.DataVaults.Discom',
                        'las la-angle-right',
                        '/app/main/datavaults/discom',
                        [],
                        [],
                        undefined, undefined,
                        () => { return this._featureCheckerService.isEnabled('App.DataVaults.Discom'); }
                    ),
                    new AppMenuItem(
                        'Circle',
                        'Pages.Tenant.DataVaults.Circle',
                        //null,
                        'las la-angle-right',
                        '/app/main/datavaults/circle',
                        [],
                        [],
                        undefined, undefined,
                        () => { return this._featureCheckerService.isEnabled('App.DataVaults.Circle'); }
                    ),
                    new AppMenuItem(
                        'Division',
                        'Pages.Tenant.DataVaults.Division',
                        'las la-angle-right',
                        '/app/main/datavaults/division',
                        [],
                        [],
                        undefined, undefined,
                        () => { return this._featureCheckerService.isEnabled('App.DataVaults.Division'); }
                    ),
                    new AppMenuItem(
                        'Sub Division',
                        'Pages.Tenant.DataVaults.SubDivision',
                        'las la-angle-right',
                        '/app/main/datavaults/subdivision',
                        [],
                        [],
                        undefined, undefined,
                        () => { return this._featureCheckerService.isEnabled('App.DataVaults.SubDivision'); }
                    ),
                    new AppMenuItem(
                        'State',
                        'Pages.Tenant.DataVaults.State',
                        'las la-angle-right',
                        '/app/main/datavaults/state',
                        [],
                        [],
                        undefined, undefined,
                        () => { return this._featureCheckerService.isEnabled('App.DataVaults.State'); }
                    ),
                    new AppMenuItem(
                        'District',
                        'Pages.Tenant.DataVaults.District',
                        'las la-angle-right',
                        '/app/main/datavaults/district',
                        [],
                        [],
                        undefined, undefined,
                        () => { return this._featureCheckerService.isEnabled('App.DataVaults.District'); }
                    ),
                    new AppMenuItem(
                        'Taluka',
                        'Pages.Tenant.DataVaults.Taluka',
                        'las la-angle-right',
                        '/app/main/datavaults/taluka',
                        [],
                        [],
                        undefined, undefined,
                        () => { return this._featureCheckerService.isEnabled('App.DataVaults.Taluka'); }
                    ),
                    new AppMenuItem(
                        'City',
                        'Pages.Tenant.DataVaults.City',
                        'las la-angle-right',
                        '/app/main/datavaults/city',
                        [],
                        [],
                        undefined, undefined,
                        () => { return this._featureCheckerService.isEnabled('App.DataVaults.City'); }
                    ),
                    new AppMenuItem(
                        'Lead Type',
                        'Pages.Tenant.DataVaults.LeadType',
                        'las la-angle-right',
                        '/app/main/datavaults/leadtype',
                        [],
                        [],
                        undefined, undefined,
                        () => { return this._featureCheckerService.isEnabled('App.DataVaults.LeadType'); }
                    ),
                    new AppMenuItem(
                        'Lead Source',
                        'Pages.Tenant.DataVaults.LeadSource',
                        'las la-angle-right',
                        '/app/main/datavaults/leadsource',
                        [],
                        [],
                        undefined, undefined,
                        () => { return this._featureCheckerService.isEnabled('App.DataVaults.LeadSource'); }
                    ),
                    new AppMenuItem(
                        'Document List',
                        'Pages.Tenant.DataVaults.DocumentList',
                        'las la-angle-right',
                        '/app/main/datavaults/documentlist',
                        [],
                        [],
                        undefined, undefined,
                        () => { return this._featureCheckerService.isEnabled('App.DataVaults.DocumentList'); }
                    ),
                    new AppMenuItem(
                        'Teams',
                        'Pages.Tenant.DataVaults.Teams',
                        'las la-angle-right',
                        '/app/main/datavaults/teams',
                        [],
                        [],
                        undefined, undefined,
                        () => { return this._featureCheckerService.isEnabled('App.DataVaults.Teams'); }
                    ),
                    new AppMenuItem(
                        'Department',
                        'Pages.Tenant.DataVaults.Department',
                        'las la-angle-right',
                        '/app/main/datavaults/department',
                        [],
                        [],
                        undefined, undefined,
                        () => { return this._featureCheckerService.isEnabled('App.DataVaults.Department'); }
                    ),
                    new AppMenuItem(
                        'Solar Type',
                        'Pages.Tenant.DataVaults.SolarType',
                        'las la-angle-right',
                        '/app/main/datavaults/solartype',
                        [],
                        [],
                        undefined, undefined,
                        () => { return this._featureCheckerService.isEnabled('App.DataVaults.SolarType'); }
                    ),
                    new AppMenuItem(
                        'Project Status',
                        'Pages.Tenant.DataVaults.ProjectStatus',
                        'las la-angle-right',
                        '/app/main/datavaults/projectstatus',
                        [],
                        [],
                        undefined, undefined,
                        () => { return this._featureCheckerService.isEnabled('App.DataVaults.ProjectStatus'); }
                    ),
                    new AppMenuItem(
                        'Cancel Reasons',
                        'Pages.Tenant.DataVaults.CancelReasons',
                        'las la-angle-right',
                        '/app/main/datavaults/cancelreasons',
                        [],
                        [],
                        undefined, undefined,
                        () => { return this._featureCheckerService.isEnabled('App.DataVaults.CancelReasons'); }
                    ),
                    new AppMenuItem(
                        'Hold Reasons',
                        'Pages.Tenant.DataVaults.HoldReasons',
                        'las la-angle-right',
                        '/app/main/datavaults/holdreasons',
                        [],
                        [],
                        undefined, undefined,
                        () => { return this._featureCheckerService.isEnabled('App.DataVaults.HoldReasons'); }
                    ),
                    new AppMenuItem(
                        'Reject Reasons',
                        'Pages.Tenant.DataVaults.RejectReasons',
                        'las la-angle-right',
                        '/app/main/datavaults/rejectreasons',
                        [],
                        [],
                        undefined, undefined,
                        () => { return this._featureCheckerService.isEnabled('App.DataVaults.RejectReasons'); }
                    ),
                    new AppMenuItem(
                        'Refund Reasons',
                        'Pages.Tenant.DataVaults.RefundReasons',
                        'las la-angle-right',
                        '/app/main/datavaults/refundreason',
                        [],
                        [],
                        undefined, undefined,
                        () => { return this._featureCheckerService.isEnabled('App.DataVaults.RefundReasons'); }
                    ),
                    new AppMenuItem(
                        'Job Cancellation',
                        'Pages.Tenant.DataVaults.JobCancellationReason',
                        'las la-angle-right',
                        '/app/main/datavaults/jobcancellationreason',
                        [],
                        [],
                        undefined, undefined,
                        () => { return this._featureCheckerService.isEnabled('App.DataVaults.JobCancellationReason'); }
                    ),
                    new AppMenuItem(
                        'Stock Category',
                        'Pages.Tenant.DataVaults.StockCategory',
                        'las la-angle-right',
                        '/app/main/datavaults/stockcategory',
                        [],
                        [],
                        undefined, undefined,
                        () => { return this._featureCheckerService.isEnabled('App.DataVaults.StockCategory'); }
                    ),
                    new AppMenuItem(
                        'Stock Item',
                        'Pages.Tenant.DataVaults.StockItem',
                        'las la-angle-right',
                        '/app/main/datavaults/stockitem',
                        [],
                        [],
                        undefined, undefined,
                        () => { return this._featureCheckerService.isEnabled('App.DataVaults.StockItem'); }
                    ),
                    new AppMenuItem(
                        'Tender',
                        'Pages.Tenant.DataVaults.Tender',
                        'las la-angle-right',
                        '/app/main/datavaults/tender',
                        [],
                        [],
                        undefined, undefined,
                        () => { return this._featureCheckerService.isEnabled('App.DataVaults.Tender'); }
                    ),
                    new AppMenuItem(
                        'Price',
                        'Pages.Tenant.DataVaults.Prices',
                        'las la-angle-right',
                        '/app/main/datavaults/price',
                        [],
                        [],
                        undefined, undefined,
                        () => {
                            return this._featureCheckerService.isEnabled('App.DataVaults.Prices');
                        }
                    ),
                    new AppMenuItem(
                        'Height Structure',
                        'Pages.Tenant.DataVaults.HeightStructure',
                        'las la-angle-right',
                        '/app/main/datavaults/heightstructure',
                        [],
                        [],
                        undefined, undefined,
                        () => { return this._featureCheckerService.isEnabled('App.DataVaults.HeightStructure'); }
                    ),
                    new AppMenuItem(
                        'Invoice Type',
                        'Pages.Tenant.DataVaults.InvoiceType',
                        'las la-angle-right',
                        '/app/main/datavaults/invoicetype',
                        [],
                        [],
                        undefined, undefined,
                        () => { return this._featureCheckerService.isEnabled('App.DataVaults.InvoiceType'); }
                    ),
                    new AppMenuItem(
                        'Document Library',
                        'Pages.Tenant.DataVaults.DocumentLibrary',
                        'las la-angle-right',
                        '/app/main/datavaults/documentlibrary',
                        [],
                        [],
                        undefined, undefined,
                        () => { return this._featureCheckerService.isEnabled('App.DataVaults.DocumentLibrary'); }
                    ),
                    new AppMenuItem(
                        'Email Template',
                        'Pages.Tenant.DataVaults.EmailTemplates',
                        'las la-angle-right',
                        '/app/main/datavaults/emailtemplate',
                        [],
                        [],
                        undefined, undefined,
                        () => { return this._featureCheckerService.isEnabled('App.DataVaults.EmailTemplates'); }
                    ),
                    new AppMenuItem(
                        'SMS Template',
                        'Pages.Tenant.DataVaults.SmsTemplates',
                        'las la-angle-right',
                        '/app/main/datavaults/smstemplate',
                        [],
                        [],
                        undefined, undefined,
                        () => { return this._featureCheckerService.isEnabled('App.DataVaults.SmsTemplates'); }
                    ),

                    new AppMenuItem(
                        'Activity Log',
                        'Pages.Tenant.DataVaults.ActivityLogMasterVault',
                        'las la-angle-right',
                        '/app/main/datavaults/activitylog',
                        [],
                        [],
                        undefined, undefined,
                        () => {
                            return this._featureCheckerService.isEnabled('App.DataVaults.ActivityLogMasterVault');
                        }
                    ),
                    new AppMenuItem(
                        'Project Type',
                        'Pages.Tenant.DataVaults.ProjectType',
                        'las la-angle-right',
                        '/app/main/datavaults/projecttype',
                        [],
                        [],
                        undefined, undefined,
                        () => { return this._featureCheckerService.isEnabled('App.DataVaults.ProjectType'); }
                    ),

                    new AppMenuItem(
                        'Dispatch Type',
                        'Pages.Tenant.DataVaults.DispatchType',
                        'las la-angle-right',
                        '/app/main/datavaults/dispatchtype',
                        [],
                        [],
                        undefined, undefined,
                        () => { return this._featureCheckerService.isEnabled('App.DataVaults.DispatchType'); }
                    ),


                    new AppMenuItem(
                        'Variation',
                        'Pages.Tenant.DataVaults.Variation',
                        'las la-angle-right',
                        '/app/main/datavaults/variation',
                        [],
                        [],
                        undefined, undefined,
                        () => {
                            return this._featureCheckerService.isEnabled('App.DataVaults.Variation');
                        }
                    ),
                    // new AppMenuItem(
                    //     'Custome Tag',
                    //     'Pages.Tenant.DataVaults.CustomeTag',
                    //     'las la-angle-right',
                    //     '/app/main/datavaults/custometag',
                    //     [],
                    //     [],
                    //     undefined, undefined,
                    //     () => {
                    //         return this._featureCheckerService.isEnabled('App.DataVaults.CustomeTag');
                    //     }
                    // ),
                    new AppMenuItem(
                        'PDF template',
                        'Pages.Tenant.DataVaults.PDFTemplate',
                        'las la-angle-right',
                        '/app/main/datavaults/pdftemplate',
                        [],
                        [],
                        undefined, undefined,
                        () => {
                            return this._featureCheckerService.isEnabled('App.DataVaults.PDFTemplate');
                        }
                    ),
                    new AppMenuItem(
                        'Payment Type',
                        'Pages.Tenant.DataVaults.PaymentType',
                        'las la-angle-right',
                        '/app/main/datavaults/paymenttype',
                        [],
                        [],
                        undefined, undefined,
                        () => {
                            return this._featureCheckerService.isEnabled('App.DataVaults.PaymentType');
                        }
                    ),
                    new AppMenuItem(
                        'Payment Mode',
                        'Pages.Tenant.DataVaults.PaymentMode',
                        'las la-angle-right',
                        '/app/main/datavaults/paymentMode',
                        [],
                        [],
                        undefined, undefined,
                        () => {
                            return this._featureCheckerService.isEnabled('App.DataVaults.PaymentMode');
                        }
                    ),
                    new AppMenuItem(
                        'Bill Of Material',
                        'Pages.Tenant.DataVaults.BillOfMaterial',
                        'las la-angle-right',
                        '/app/main/datavaults/billofmaterial',
                        [],
                        [],
                        undefined, undefined,
                        () => {
                            return this._featureCheckerService.isEnabled('App.DataVaults.BillOfMaterial');
                        }
                    ),
                ],
                undefined, undefined,
                () => { return this._featureCheckerService.isEnabled('App.DataVaults'); }
            ),

            new AppMenuItem(
                'Lead',
                'Pages.Tenant.Leads',
                'las la-crosshairs',
                '',
                [],
                [
                    new AppMenuItem(
                        'Manage Leads',
                        'Pages.Tenant.Leads.ManageLead',
                        'las la-angle-right',
                        '/app/main/leads/manageleads',
                        [],
                        [],
                        undefined, undefined,
                        () => { return this._featureCheckerService.isEnabled('App.Leads.ManageLead'); }
                    ),
                    new AppMenuItem(
                        'Customer',
                        'Pages.Tenant.Leads.Customer',
                        'las la-angle-right',
                        '/app/main/leads/customerleads',
                        [],
                        [],
                        undefined, undefined,
                        () => { return this._featureCheckerService.isEnabled('App.Leads.Customers'); }
                    ),
                    new AppMenuItem(
                        'MIS Report',
                        'Pages.Tenant.Leads.MISReport',
                        'las la-angle-right',
                        '/app/main/leads/misReport',
                        [],
                        [],
                        undefined, undefined,
                        () => { return this._featureCheckerService.isEnabled('App.Leads.MISReport'); }
                    ),
                ],
                undefined, undefined,
                () => { return this._featureCheckerService.isEnabled('App.Leads'); }
            ),
            new AppMenuItem(
                'Jobs',
                'Pages.Tenant.Jobs',
                'las la-briefcase',
                '/app/main/jobs',
                [],
                [],
                undefined, undefined,
                () => { return this._featureCheckerService.isEnabled('App.Jobs'); }
            ),
            new AppMenuItem(
                'Tracker',
                'Pages.Tenant.Tracker',
                'las la-route',
                '',
                [],
                [
                    new AppMenuItem(
                        'Application',
                        'Pages.Tenant.Tracker.Application',
                        'las la-angle-right',
                        '/app/main/tracker/application',
                        [],
                        [],
                        undefined, undefined,
                        () => { return this._featureCheckerService.isEnabled('App.Tracker.Application'); }
                    ),
                    new AppMenuItem(
                        'Estimate Paid',
                        'Pages.Tenant.Tracker.EstimatedPaid',
                        'las la-angle-right',
                        '/app/main/tracker/estimate',
                        [],
                        [],
                        undefined, undefined,
                        () => { return this._featureCheckerService.isEnabled('App.Tracker.EstimatedPaid'); }
                    ),
                    new AppMenuItem(
                        'Refund Tracker',
                        'Pages.Tenant.Tracker.Refund',
                        'las la-angle-right',
                        '/app/main/tracker/refundtracker',
                        [],
                        [],
                        undefined, undefined,
                        () => { return this._featureCheckerService.isEnabled('App.Tracker.Refund'); }
                    ),
                    new AppMenuItem(
                        'Deposit Tracker',
                        'Pages.Tenant.Tracker.Deposit',
                        'las la-angle-right',
                        '/app/main/tracker/deposit-tracker',
                        [],
                        [],
                        undefined, undefined,
                        () => { return this._featureCheckerService.isEnabled('App.Tracker.Deposite'); }
                    ),
                    new AppMenuItem(
                        'Dispatch Tracker',
                        'Pages.Tenant.Tracker.Dispatch',
                        'las la-angle-right',
                        '/app/main/installation/dispatch',
                        [],
                        [],
                        undefined, undefined,
                        () => { return this._featureCheckerService.isEnabled('App.Tracker.Dispatch'); }
                    ),
                    // new AppMenuItem(
                    //     'PickList Tracker',
                    //     'Pages.Tenant.Tracker.PickList',
                    //     'las la-angle-right',
                    //     '/app/main/tracker/picklist',
                    //     [],
                    //     [],
                    //     undefined, undefined,
                    //     () => { return this._featureCheckerService.isEnabled('App.Tracker.PickList'); }
                    // ),              
                    new AppMenuItem(
                        'Transport Tracker',
                        'Pages.Tenant.Tracker.PickList',
                        'las la-angle-right',
                        '/app/main/tracker/transporttracker',
                        [],
                        [],
                        undefined, undefined,
                        () => { return this._featureCheckerService.isEnabled('App.Tracker.PickList'); }
                    ),
                    new AppMenuItem(
                        'Meter Connect',
                        'Pages.Tenant.Tracker.PickList',
                        'las la-angle-right',
                        '/app/main/tracker/meter',
                        [],
                        [],
                        undefined, undefined,
                        () => { return this._featureCheckerService.isEnabled('App.Tracker.PickList'); }
                    ),
                    // new AppMenuItem("Application", 'Pages.Tenant.Tracker.Application', "las la-angle-right", "/app/main/tracker/application"),
                    // new AppMenuItem("Estimate Paid", 'Pages.Tenant.Tracker.EstimatedPaid', "las la-angle-right", "/app/main/tracker/estimate"),
                    // new AppMenuItem("Meter Connect", 'Pages.Tenant.Tracker.Application', "las la-angle-right", "/app/main/tracker/meter"),
                ],
                undefined, undefined,
                () => {
                    return this._featureCheckerService.isEnabled('App.Tracker');
                }
            ),

            new AppMenuItem(
                'Accounts',
                'Pages.Tenant.Accounts',
                'las la-rupee-sign',
                '',
                [],
                [
                    new AppMenuItem(
                        'Payment Issued',
                        'Pages.Tenant.Accounts.PaymentIssued',
                        'las la-angle-right',
                        '/app/main/accounts/payment-issued',
                        [],
                        [],
                        undefined, undefined,
                        () => { return this._featureCheckerService.isEnabled('App.Accounts.PaymentIssued'); }
                    ),
                    new AppMenuItem(
                        'Payment Paid',
                        'Pages.Tenant.Accounts.PaymentPaid',
                        'las la-angle-right',
                        '/app/main/accounts/payment-paid',
                        [],
                        [],
                        undefined, undefined,
                        () => { return this._featureCheckerService.isEnabled('App.Accounts.PaymentPaid'); }
                    ),

                ],
                undefined, undefined,
                () => {
                    return this._featureCheckerService.isEnabled('App.Accounts');
                }
            ),

            new AppMenuItem(
                'Installation',
                null, //'Pages.Tenant.DataVaults',
                'las la-tools',
                '',
                [],
                [

                    //new AppMenuItem("Dispatch", null, "las la-angle-right", "/app/main/installation/dispatch"),
                    new AppMenuItem(
                        'Installation Tracker',
                        'Pages.Tenant.Tracker.Dispatch',
                        'las la-angle-right',
                        '/app/main/tracker/installtiontracker',
                        [],
                        [],
                        undefined, undefined,
                        () => { return this._featureCheckerService.isEnabled('App.Tracker.Dispatch'); }
                    ),
                    new AppMenuItem("Installation Calendar", null, "las la-angle-right", "/app/main/installation/installation-calendar"),
                    new AppMenuItem("Serial No & Photos", null, "las la-angle-right", "/app/main/installation/serial-photos"),
                    new AppMenuItem("Map", null, "las la-angle-right", "/app/main/installation/map"),
                ],
                undefined, undefined,
                () => {
                    return this._featureCheckerService.isEnabled('App.DataVaults');
                }
            ),

            new AppMenuItem(
                'Subsidy',
                'Pages.Tenant.Subsidy',
                'las la-coins',
                '',
                [],
                [
                    new AppMenuItem(
                        "Subsidy Tracker",
                        'Pages.Tenant.Tracker.SubsidyDetail',
                        "las la-angle-right",
                        "/app/main/subsidy/subsidy-tracker",
                        [],
                        [],
                        undefined, undefined,
                        () => { return this._featureCheckerService.isEnabled('App.Subsidy.SubsidyDetail'); }
                    ),
                    new AppMenuItem(
                        "Subsidy Claim",
                        'Pages.Tenant.Tracker.SubsidyClaim',
                        "las la-angle-right",
                        "/app/main/subsidy/subsidy-claim",
                        [],
                        [],
                        undefined, undefined,
                        () => { return this._featureCheckerService.isEnabled('App.Subsidy.SubsidyClaim'); }
                    ),
                ],
                undefined, undefined,
                () => {
                    return this._featureCheckerService.isEnabled('App.DataVaults');
                }
            ),


            new AppMenuItem(
                'Email',
                null, //'Pages.Tenant.DataVaults',
                'las la-envelope',
                '',
                [],
                [
                    new AppMenuItem("Inbox", null, "las la-angle-right", "/app/main/emailbox/inbox"),
                    // new AppMenuItem("Installation Calendar", null, "las la-angle-right", "/app/main/installation/installation-calendar"),
                    // new AppMenuItem("Serial No & Photos", null, "las la-angle-right", "/app/main/installation/serial-photos"),
                    // new AppMenuItem("Map", null, "las la-angle-right", "/app/main/installation/map"),
                ],
                undefined, undefined,
                () => {
                    return this._featureCheckerService.isEnabled('App.DataVaults');
                }
            ),
            new AppMenuItem(
                'Administration',
                '',
                'las la-user',
                '',
                [],
                [
                    new AppMenuItem(
                        'OrganizationUnits',
                        'Pages.Administration.OrganizationUnits',
                        'las la-angle-right',
                        '/app/admin/organization-units'
                    ),
                    new AppMenuItem('Roles', 'Pages.Administration.Roles', 'las la-angle-right', '/app/admin/roles'),
                    new AppMenuItem('Users', 'Pages.Administration.Users', 'las la-angle-right', '/app/admin/users'),
                    // new AppMenuItem(
                    //     'Languages',
                    //     'Pages.Administration.Languages',
                    //     'flaticon-tabs',
                    //     '/app/admin/languages',
                    //     ['/app/admin/languages/{name}/texts']
                    // ),
                    new AppMenuItem(
                        'AuditLogs',
                        'Pages.Administration.AuditLogs',
                        'las la-angle-right',
                        '/app/admin/auditLogs'
                    ),
                    new AppMenuItem(
                        'Chat',
                        null,
                        'las la-angle-right',
                        '/app/admin/chat'
                    ),
                    new AppMenuItem(
                        'Maintenance',
                        'Pages.Administration.Host.Maintenance',
                        'las la-angle-right',
                        '/app/admin/maintenance'
                    ),
                    // new AppMenuItem(
                    //     'Subscription',
                    //     'Pages.Administration.Tenant.SubscriptionManagement',
                    //     'las la-angle-right',
                    //     '/app/admin/subscription-management'
                    // ),
                    new AppMenuItem(
                        'Settings',
                        'Pages.Administration.Host.Settings',
                        'las la-angle-right',
                        '/app/admin/hostSettings'
                    ),
                    new AppMenuItem(
                        'Settings',
                        'Pages.Administration.Tenant.Settings',
                        'las la-angle-right',
                        '/app/admin/tenantSettings'
                    ),
                ],
                // undefined, undefined,
                // () => { return this._featureCheckerService.isEnabled('App.Administration'); }
            ),
        ]);
    }

    checkChildMenuItemPermission(menuItem): boolean {
        for (let i = 0; i < menuItem.items.length; i++) {
            let subMenuItem = menuItem.items[i];

            if (subMenuItem.permissionName === '' || subMenuItem.permissionName === null) {
                if (subMenuItem.route) {
                    return true;
                }
            } else if (this._permissionCheckerService.isGranted(subMenuItem.permissionName)) {
                return true;
            }

            if (subMenuItem.items && subMenuItem.items.length) {
                let isAnyChildItemActive = this.checkChildMenuItemPermission(subMenuItem);
                if (isAnyChildItemActive) {
                    return true;
                }
            }
        }
        return false;
    }

    showMenuItem(menuItem: AppMenuItem): boolean {
        if (
            menuItem.permissionName === 'Pages.Administration.Tenant.SubscriptionManagement' &&
            this._appSessionService.tenant &&
            !this._appSessionService.tenant.edition
        ) {
            return false;
        }

        let hideMenuItem = false;

        if (menuItem.requiresAuthentication && !this._appSessionService.user) {
            hideMenuItem = true;
        }

        if (menuItem.permissionName && !this._permissionCheckerService.isGranted(menuItem.permissionName)) {
            hideMenuItem = true;
        }

        if (this._appSessionService.tenant || !abp.multiTenancy.ignoreFeatureCheckForHostUsers) {
            if (menuItem.hasFeatureDependency() && !menuItem.featureDependencySatisfied()) {
                hideMenuItem = true;
            }
        }

        if (!hideMenuItem && menuItem.items && menuItem.items.length) {
            return this.checkChildMenuItemPermission(menuItem);
        }

        return !hideMenuItem;
    }

    /**
     * Returns all menu items recursively
     */
    getAllMenuItems(): AppMenuItem[] {
        let menu = this.getMenu();
        let allMenuItems: AppMenuItem[] = [];
        menu.items.forEach((menuItem) => {
            allMenuItems = allMenuItems.concat(this.getAllMenuItemsRecursive(menuItem));
        });

        return allMenuItems;
    }

    private getAllMenuItemsRecursive(menuItem: AppMenuItem): AppMenuItem[] {
        if (!menuItem.items) {
            return [menuItem];
        }

        let menuItems = [menuItem];
        menuItem.items.forEach((subMenu) => {
            menuItems = menuItems.concat(this.getAllMenuItemsRecursive(subMenu));
        });

        return menuItems;
    }
}
