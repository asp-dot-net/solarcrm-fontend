import { Injector, Component, OnInit, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { ThemesLayoutBaseComponent } from '@app/shared/layout/themes/themes-layout-base.component';
import { UrlHelper } from '@shared/helpers/UrlHelper';
import { AppConsts } from '@shared/AppConsts';
import { DateTimeService } from '@app/shared/common/timing/date-time.service';

@Component({
    templateUrl: './default-layout.component.html',
    selector: 'default-layout',
    animations: [appModuleAnimation()],
})
export class DefaultLayoutComponent extends ThemesLayoutBaseComponent implements OnInit {
    
    remoteServiceBaseUrl: string = AppConsts.remoteServiceBaseUrl;
    filterText = '';

    constructor(
        injector: Injector, 
        _dateTimeService: DateTimeService,
        @Inject(DOCUMENT) private document: Document) {
        super(injector, _dateTimeService);
    }

    ngOnInit() {
        this.installationMode = UrlHelper.isInstallUrl(location.href);
        if(this.currentTheme.baseSettings.menu.defaultMinimizedAside){
            this.document.body.setAttribute('data-kt-aside-minimize','on');
            // var someElement= document.getElementById("kt_aside");
            // someElement.className += " aside-hoverable";
            
        }
    }
    toggleLeftAside(): void {
        this.document.body.classList.toggle('kt-aside--minimize');
        this.triggerAsideToggleClickEvent();
    }
    triggerAsideToggleClickEvent(): void {
        abp.event.trigger('app.kt_aside_toggler.onClick');
    }
}
