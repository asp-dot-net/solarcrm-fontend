import { Injectable } from '@angular/core';
import { AppConsts } from '@shared/AppConsts';
import { XmlHttpRequestHelper } from '@shared/helpers/XmlHttpRequestHelper';
import { LeadsDto } from '@shared/service-proxies/service-proxies';
import { LocalStorageService } from '@shared/utils/local-storage.service';

@Injectable()
export class DataTransferService {
    private _OrganizationmyVariable: any;
    private _LeadData: LeadsDto;
    constructor() {
        this._OrganizationmyVariable = '';
        this._LeadData = new LeadsDto();
    }
    // get GetOrganizationmyVariable(): void {
    //     return this._OrganizationmyVariable;
    // }

    // set SetOrganizationmyVariable(newValue: any) {
    //     this._OrganizationmyVariable = newValue;
    // }

    getOrgData(): string {
        return this._OrganizationmyVariable;
    }

    setOrgData(value: string) {
        this._OrganizationmyVariable = value;
    }

    setLeadData(value: LeadsDto) {
        this._LeadData = value;
    }
    getLeadData() : LeadsDto{
        return this._LeadData;
    }
}