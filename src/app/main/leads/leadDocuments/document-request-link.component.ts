import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import {  DocumentRequestServiceProxy, SendDocumentRequestInputDto, CommonLookupServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { finalize } from 'rxjs/operators';
import { NgxSpinnerService } from 'ngx-spinner';
import { appModuleAnimation } from '@shared/animations/routerTransition';

@Component({
    selector: 'documentRequestModal',
    templateUrl: './document-request-link.component.html',
    animations: [appModuleAnimation()]
})

export class DocumentRequestLinkModalComponent extends AppComponentBase {

    @ViewChild('documentrequestlinkModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    saving = false;
    DocumentTypes: any;
    leadId:number;
    documentTypeId:number;
    jobNumber: string;
    sectionId: number;

    constructor(
        injector: Injector,
        private _commonLookupServiceProxy: CommonLookupServiceProxy,
        private _documentRequestServiceProxy: DocumentRequestServiceProxy,
        private spinner: NgxSpinnerService,
    ) {
        super(injector);
    }

    show(leadId: number, sectionId: any): void {
        debugger;
        this.spinner.show();
        this.leadId = leadId;
        this.sectionId = sectionId;
        this.DocumentTypes = [];
        this._commonLookupServiceProxy.getAllDocumentLibratyForDropdown(leadId).subscribe(result => {
            this.DocumentTypes = result;
            this.modal.show();
            this.spinner.hide();
        }, error => { this.spinnerService.hide() });
        
        
    }
    openwhatsup(phonenumber:number){
        var link = "https://api.whatsapp.com/send/?phone="+phonenumber+"&text&type=phone_number&app_absent=0";
        window.open(link);
    }
    save(sendMode: any): void {
        this.saving = true;

        let sendDocumentRequestInputDto = new SendDocumentRequestInputDto;
        sendDocumentRequestInputDto.leadId = this.leadId;
        sendDocumentRequestInputDto.docTypeId = this.documentTypeId;
        sendDocumentRequestInputDto.sectionId = this.sectionId;
        sendDocumentRequestInputDto.sendMode = sendMode

        let str = sendMode == 'Email' ? "Email Send Successfully" : "SMS Send Successfully"
        
        this._documentRequestServiceProxy.sendDocumentRequestForm(sendDocumentRequestInputDto).pipe(finalize(() => { this.saving = false;}))
        .subscribe(() => {
            this.notify.info(str);
            this.close();
            this.modalSave.emit(null);
        });  
    }

    close(): void {
        this.modal.hide();
    }
}