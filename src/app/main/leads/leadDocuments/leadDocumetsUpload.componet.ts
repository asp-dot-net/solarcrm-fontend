import { Component, ElementRef, EventEmitter, Injector, Input, OnInit, Output, ViewChild } from '@angular/core';
import {
    LeadsServiceProxy, LeadsActivityLogServiceProxy, CommonLookupDto, EntityDto, CreateOrEditLeadDocumentDto, CommonLookupServiceProxy,
    GetAllLeadDocumentListInput
} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ActivatedRoute, Router } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { NgxSpinnerService } from 'ngx-spinner';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { map as _map, filter as _filter, forEach } from 'lodash-es';
import { AppConsts } from '@shared/AppConsts';
import { finalize } from 'rxjs/operators';
import { FileUploader } from 'ng2-file-upload';
import { LazyLoadEvent } from 'primeng/api';
import { Paginator } from 'primeng/paginator';
import { HttpClient } from '@angular/common/http';
import { DataTransferService } from '@app/shared/common/projectservice/data-transfer.service';
import { DocumentRequestLinkModalComponent } from './document-request-link.component';
@Component({
    selector: 'leadDocumetsUpload',
    templateUrl: './leadDocumetsUpload.component.html',
    animations: [appModuleAnimation()]
})
export class LeadDocumentsUploadComponent extends AppComponentBase {
    @ViewChild('createOrEditDocumentModal', { static: true }) modal: ModalDirective;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild('documentRequestModal', { static: true }) documentRequestModal: DocumentRequestLinkModalComponent;

    FileSizeReq: string = '';
    FileFormate: string = '';
    StorageUnit: string = '';
    FormateList = [];
    @ViewChild('uploadPicture') myFileRef: ElementRef;
    uploadedFiles: any[] = [];
    uploadUrl: string;
    documentListname: any;
    leadDocumentList: any;
    //documentLibrary: CreateOrEditDocumentLibraryDto = new CreateOrEditDocumentLibraryDto();
    filesize: number = 1000000;
    active = false;
    saving = false;
    recordCount: any;
    leadId: any;
    sectionId: number = AppConsts.SectionPage.LeadDocument;
    LeadDocument: CreateOrEditLeadDocumentDto = new CreateOrEditLeadDocumentDto();
    bindDocumentFilter: GetAllLeadDocumentListInput = new GetAllLeadDocumentListInput();

    constructor(
        injector: Injector,
        private _leadsServiceProxy: LeadsServiceProxy,
        private _commonLookup: CommonLookupServiceProxy,
        private _httpClient: HttpClient,
        private _dataTransfer: DataTransferService,
    ) {
        super(injector);
    }

    showLeadDocument(leadId?: number, param?: number, sectionId?: number): void {
        this.leadId = leadId;
        this.LeadDocument = new CreateOrEditLeadDocumentDto();
        this.getLeadDocument();

    }
    getLeadDocument(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            //this.paginator.changePage(0);
            // return;
        }
        this.primengTableHelper.showLoadingIndicator();
        this.bindDocumentFilter.filter = this.leadId
        this._leadsServiceProxy.leadDocumentList(this.bindDocumentFilter).subscribe((result) => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            this.primengTableHelper.hideLoadingIndicator();
            this.recordCount = result.totalCount;
        }, error => { this.spinnerService.hide() });
    }
    addDocumentModal() {
        this.LeadDocument = new CreateOrEditLeadDocumentDto();
        this.LeadDocument.leadId = this.leadId;
        this.documentListname = [];

        this._commonLookup.getAllDocumentLibratyForDropdown(this.leadId).subscribe(result => {
            this.documentListname = result;
        }, error => { this.spinnerService.hide() });

        //setTimeout(function () {
        this.modal.show();

        //}, 1500);

        this.uploadedFiles = [];
        this.active = true;
    }
    deleteDocument(documentLibrary: EntityDto): void {
        this.message.confirm(
            this.l('DocumentDeleteWarningMessage',),
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._leadsServiceProxy.leadDocumentDelete(documentLibrary)
                        .subscribe(() => {
                            this.reloadPage();
                            this.notify.success(this.l('SuccessfullyDeleted'));
                        }, error => { this.spinnerService.hide() });
                }
            }
        );
    }
    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    save() {
        if (this.uploadedFiles[0] == undefined) {
            this.notify.error(this.l('Plz Select File'));
            return;
        }
        this.saving = true;

        this.LeadDocument.fileName = this.uploadedFiles[0].name;
        this.LeadDocument.fileType = '1';

    }
   
    // upload completed event
    onUpload(event): void {
        for (const file of event.files) {
            this.uploadedFiles.push(file);
            this.modal.hide();
            this.uploadedFiles = [];
            this.notify.info(this.l('SavedSuccessfully'));
            this.close();
            this.modalSave.emit(null);
            this.getLeadDocument();
        }
    }

    onBeforeSend(event): void {
        event.xhr.setRequestHeader('Authorization', 'Bearer ' + abp.auth.getToken());
    }
    fileChangeEvent(event) {
        const collection = document.getElementsByClassName("p-fileupload-filename");
        //var fileName = e[0];
    }
    fileToUpload: any;
    //FileFormate:any
    handleFileInput(files: FileList) {
        debugger;
        const file: File = files.item(0); //files.target.files[0];

        var filesize = this.formatBytes(file.size, this.StorageUnit == 'KB' ? 2 : 3);
        var fileExte = file.name.split('.').pop();
        if (this.FormateList.length > 0) {
            var serviceExist = this.FormateList.filter(function (item) {
                return item == "." + fileExte.toUpperCase();
            });
            if (serviceExist.length == 0) {
                this.notify.info(this.l('plz select proper file formate ' + this.FileFormate));
            }
        }
        if (filesize <= parseFloat(this.FileSizeReq)) {
            this.fileToUpload = files.item(0);
        }
        else {
            this.notify.info(this.l('file size require ' + this.FileSizeReq + this.StorageUnit));
            this.myFileRef.nativeElement.value = "";
            this.fileToUpload = '';
        }
    }
   
    changeurl(event) {
        this.FileSizeReq = '';
        this.FileFormate = '';
        this.myFileRef.nativeElement.accept = '';
        // var lablesize = this.documentListname.filter(x => x.id == event.target.value);
        var serviceExist = this.documentListname.filter(function (item) {
            return item.id == event.target.value;
        });
        if (serviceExist.length == 1) {
            var filedata = serviceExist[0].labeldata.split("#");
            if (filedata != null || filedata != '') {
                this.FileSizeReq = filedata[0];
                this.FileFormate = filedata[1];
                this.StorageUnit = filedata[2];
                if (this.FileFormate == null || this.FileFormate == '') {
                    this.notify.info(this.l('Plz any One File Extention Required'));
                }
                else {

                    this.FormateList = [];
                    let extdata = filedata[1].split(",");
                    extdata.forEach(item => {
                        this.FormateList.push("." + item);
                    });
                    this.myFileRef.nativeElement.accept = [this.FormateList]; //['.png', '.pdf'];
                    // if (this.LeadDocument.id == 0 && this.documentLibrary.id == null) {
                    this.uploadUrl = AppConsts.remoteServiceBaseUrl + '/LeadDocumentUpload/UploadLeadDocument?id=' + 0 + '&leadid=' + this.LeadDocument.leadId + '&documentid=' + this.LeadDocument.documentId + '&documentNumber=' + this.LeadDocument.documentNumber;
                }
            }
        }
    }
    formatBytes(bytes, decimals = 2) {
        // if (!+bytes) return '0 Bytes'

        // const k = 1024
        // const dm = decimals < 0 ? 0 : decimals
        // const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB']

        // const i = Math.floor(Math.log(bytes) / Math.log(k))

        // return `${parseFloat((bytes / Math.pow(k, i)).toFixed(dm))} ${sizes[i]}`
        var val = 0;
        if (decimals != 3) {
            val = parseFloat(bytes) / 1000;
        }
        else {
            val = bytes / 1000000;
        }
        return val;
    }

    saveLeadDocument(): void {
        debugger
        this.saving = true;
        this.spinnerService.show();
        const formData: FormData = new FormData();
        const file = this.fileToUpload;
        if (this.fileToUpload === null && this.leadDocumentList.leadDocumentPath == '') {
            this.notify.info(this.l('Plz Select any One File'));
            return
        }
        if (this.fileToUpload != null) {
            formData.append('file', this.fileToUpload, this.fileToUpload.name);
        }
        for (var key in this.LeadDocument) {
            formData.append(key, this.LeadDocument[key]);
        }
        // formData.append('SectionId', this.sectionId.toString());
        formData.append('applicationFlag', "0");
        formData.append('OrganizationUnitId', parseInt(this._dataTransfer.getOrgData()).toString());
        this._httpClient
            .post<any>(this.uploadUrl, formData)
            .subscribe((response) => {
                if (response.success) {
                    this.spinnerService.hide();
                    this.modal.hide();
                    this.getLeadDocument();
                    this.close();
                    this.modalSave.emit(null);
                    this.saving = false;
                    this.notify.info(this.l('SavedSuccessfully'));
                } else if (response.error != null) {
                    this.notify.error(this.l('Failed'));
                    this.spinnerService.hide();
                }
            }, error => { this.spinnerService.hide() });
    }

    downfile(filename): void {
        let FileName = AppConsts.docUrl + filename;//this.LeadDocument.filePath;
        window.open(FileName, "_blank");
    };
    close() {
        this.active = false;
        this.modal.hide();
    }
    getDocumentRequestModal() {
        this.documentRequestModal.show(this.leadId, 22)
    }
}
