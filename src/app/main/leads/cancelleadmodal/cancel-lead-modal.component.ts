import { Component, ElementRef, EventEmitter, Injector,Optional, Output, ViewChild } from '@angular/core';
import { AppConsts } from '@shared/AppConsts';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { CommonLookupServiceProxy,LeadsServiceProxy, GetLeadsForViewDto, LeadsDto,ActivityLogInput, GetLeadForChangeStatusOutput, CommonLookupDto } from '@shared/service-proxies/service-proxies';
import * as _ from 'lodash';
import * as moment from 'moment';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
@Component({
    selector: 'cancelLeadModel',
    templateUrl: './cancel-lead-modal.component.html',
    animations: [appModuleAnimation()]
})
export class CancelLeadModelComponent extends AppComponentBase{

    @ViewChild('addModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    CancelReason:string;
    CancelReasonId : number =0;
    LeadId:number;	
    active = false;
    saving = false;
    allCancelReasons: CommonLookupDto[];
    sectionId: number = AppConsts.SectionPage.CancelLeads;  
    constructor(
        injector: Injector,      
        private _leadsServiceProxy:LeadsServiceProxy,
        private _commonLookupServiceProxy: CommonLookupServiceProxy,
      
    ) {
        super(injector);
    }

     show(LeadId?: number): void {
        this.LeadId = LeadId;
        this.modal.show();
        this._commonLookupServiceProxy.getAllCancelReasonDropdown().subscribe(result => {						
             this.allCancelReasons = result;
         }, error => { this.spinnerService.hide() });
     }

     close(): void {
        this.active = false;
        this.CancelReason = '';
        this.modal.hide();
     }

     save(): void{
        if(this.CancelReasonId == 0) {
            this.notify.warn(this.l('PleaseSelectCancelReason'));
            return
         }
        let status :GetLeadForChangeStatusOutput = new GetLeadForChangeStatusOutput();
        status.id = this.LeadId;
        status.leadStatusID = 14;
        status.rejectReason = this.CancelReason;
        status.cancelReasonId = this.CancelReasonId;
        
        this._leadsServiceProxy.changeStatus(status)
            .subscribe(() => {
                
                this.notify.success(this.l('LeadCanceledSuccessfully'));
                this.CancelReason = '';
                this.modal.hide();                
                this.modalSave.emit(false);
                //document.getElementById("setcancelRejectClick").click();
                
            }, error => { this.spinnerService.hide() });
    }
}
