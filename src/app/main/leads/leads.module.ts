import { CommonModule,DatePipe } from '@angular/common';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule ,NO_ERRORS_SCHEMA} from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppCommonModule } from '@app/shared/common/app-common.module';
import { UtilsModule } from '@shared/utils/utils.module';
import { CountoModule } from 'angular2-counto';
import { ModalModule } from 'ngx-bootstrap/modal';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { PopoverModule } from 'ngx-bootstrap/popover';
import { LeadsRoutingModule } from './leads-routing.module';
import { BsDatepickerConfig, BsDaterangepickerConfig, BsLocaleService } from 'ngx-bootstrap/datepicker';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { NgxBootstrapDatePickerConfigService } from 'assets/ngx-bootstrap/ngx-bootstrap-datepicker-config.service';
import { SubheaderModule } from '@app/shared/common/sub-header/subheader.module';
import { ActivityLogHistoryComponent } from './leadactivity/activity-log-history.component';
import { CustomizableDashboardModule } from '@app/shared/common/customizable-dashboard/customizable-dashboard.module';
//import { ManageLeadsRoutingModule } from './manageleads/manageleads-routing.module';
import { ManageLeadsComponent } from './manageleads/manageleads.component';
import { createOrEditManageLeadComponent } from './customerleads/create-or-edit-manageleads.component';
import { ViewMyLeadComponent } from './customerleads/view-mylead.component';
import { ActivityLogLeadsComponent } from './leadactivity/activity-log-leads.component';
import { LeadDocumentsUploadComponent } from './leadDocuments/leadDocumetsUpload.componet';
import { NgSelectModule } from '@ng-select/ng-select';
import { AddActivityModalComponent } from './leadactivity/add-activity-model.component';
import { AgmCoreModule } from '@agm/core';
import { AdminSharedModule } from '@app/admin/shared/admin-shared.module';
import { AppSharedModule } from '@app/shared/app-shared.module';
import { EmailEditorModule } from 'angular-email-editor';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { PaginatorModule } from 'primeng/paginator';
import { EditorModule } from 'primeng/editor';
import { InputMaskModule } from 'primeng/inputmask'; import { FileUploadModule } from 'primeng/fileupload';
import { TableModule } from 'primeng/table';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { CancelledLeadsComponent } from './cancelled-leads/cancelledLeads.componet';
import { JobDetailsComponent } from '@app/main/jobs/jobDetails.componet';
import {ViewDuplicateModalComponent} from './customerleads/view-duplicate-lead.component';
import { MISReportComponent } from './misReport/misReport.component';
import { PdfTemplateShowModalComponent } from '../jobs/pdf-template-show-model.component';
import { CreateOrEditPaymentModalComponent } from '../jobs/create-or-edit-payment.component';
import { JobInstallerComponent } from '../jobs/job-installer/job-installer.component';
import { DataTransferService } from '@app/shared/common/projectservice/data-transfer.service';
import { BrowserModule } from '@angular/platform-browser';
import { CustomerleadsComponent } from './customerleads/customerleads.component';
import { JobQuotationComponent } from '../jobs/job-quotation/job-quotation.component';
// import { addPaymentModal } from '../jobs/add-payment.component';
import {JobRefundComponent}  from '../jobs/job-refund/job-refund.component';
import { PickListGenerateComponent } from '../tracker/picklist/pick-list-generate/pick-list-generate.component';
import { PickListEditComponent } from '../tracker/picklist/pick-list-edit/pick-list-edit.component'; 
import { DocumentRequestLinkModalComponent } from './leadDocuments/document-request-link.component';
NgxBootstrapDatePickerConfigService.registerNgxBootstrapDatePickerLocales();

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ModalModule,
        TabsModule,
        TooltipModule,
        AppCommonModule,
        UtilsModule,
        LeadsRoutingModule,
        CountoModule,
        BsDatepickerModule.forRoot(),
        BsDropdownModule.forRoot(),
        PopoverModule.forRoot(),
        SubheaderModule,
        AdminSharedModule,
        AppSharedModule,  
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyALoDvKqgmTfpfPOEjqS2fI1kcabuxImMA',            
            libraries: ['places']
        }),
        NgSelectModule,
        EmailEditorModule,
        AutoCompleteModule,
		PaginatorModule,
		EditorModule,
		InputMaskModule,
		TableModule,
        OwlDateTimeModule,
        OwlNativeDateTimeModule,                
       // BrowserModule,
    ],
    declarations: [  ManageLeadsComponent, createOrEditManageLeadComponent,ViewMyLeadComponent,ActivityLogLeadsComponent,
        LeadDocumentsUploadComponent,ActivityLogHistoryComponent, CancelledLeadsComponent,MISReportComponent,
        AddActivityModalComponent, JobDetailsComponent, CustomerleadsComponent,ViewDuplicateModalComponent,PdfTemplateShowModalComponent,        
        CreateOrEditPaymentModalComponent,PickListGenerateComponent,PickListEditComponent,
        CustomerleadsComponent,JobInstallerComponent,JobRefundComponent,JobQuotationComponent, DocumentRequestLinkModalComponent],
        exports: [ViewMyLeadComponent,AddActivityModalComponent,PickListGenerateComponent,PickListEditComponent,CreateOrEditPaymentModalComponent,JobRefundComponent],
        schemas: [ NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA ],
    providers: [DataTransferService,
         { provide: BsDatepickerConfig, useFactory: NgxBootstrapDatePickerConfigService.getDatepickerConfig },
         { provide: BsDaterangepickerConfig, useFactory: NgxBootstrapDatePickerConfigService.getDaterangepickerConfig },
         { provide: BsLocaleService, useFactory: NgxBootstrapDatePickerConfigService.getDatepickerLocale },
         { provide: DatePipe }
    ],
})
export class LeadsModule {}
