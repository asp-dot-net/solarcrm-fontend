import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ManageLeadsComponent } from './manageleads/manageleads.component';
import { createOrEditManageLeadComponent } from './customerleads/create-or-edit-manageleads.component';
import { CancelledLeadsComponent } from './cancelled-leads/cancelledLeads.componet';
import { JobDetailsComponent } from '../jobs/jobDetails.componet';
import { MISReportComponent } from './misReport/misReport.component';
import { CustomerleadsComponent } from './customerleads/customerleads.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '',
             
            },
            { path: 'createOrEdit', component: createOrEditManageLeadComponent },
            { path: 'manageleads', component: ManageLeadsComponent },
            { path: 'cancelled-leads', component: CancelledLeadsComponent },
            { path: 'misReport', component: MISReportComponent },
            { path: 'customerleads', component: CustomerleadsComponent},
            // { path: 'job-type', component: jobTypeComponent },
        ]),
    ],
    exports: [RouterModule],
})
export class LeadsRoutingModule { }