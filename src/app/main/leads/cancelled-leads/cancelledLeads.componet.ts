import { OnInit, Component, Injector, ViewEncapsulation, ViewChild, Input, Output, EventEmitter, ElementRef } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ActivatedRoute, Router } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { LazyLoadEvent } from 'primeng/api';
import { LeadsServiceProxy, LeadsDto, CommonLookupServiceProxy, CommonLookupDto, UserServiceProxy } from '@shared/service-proxies/service-proxies';
import { Paginator } from 'primeng/paginator';
import { Title } from '@angular/platform-browser';
import * as _ from 'lodash';
import * as moment from 'moment';
import { AppConsts } from '@shared/AppConsts';

@Component({
    selector: 'cancelledLead',
    templateUrl: './cancelledLeads.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class CancelledLeadsComponent extends AppComponentBase implements OnInit  {
    // @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @Output() reloadLead = new EventEmitter();
    //documentLibrary: CreateOrEditDocumentLibraryDto = new CreateOrEditDocumentLibraryDto();
    advancedFiltersAreShown = false;
    FiltersData = false;
    active = false;
    saving = false;
    recordCount:any;
    leadId:any;
    filterText : string = ''; 
    StartDate: moment.Moment;
    EndDate: moment.Moment;
    sectionId: number = AppConsts.SectionPage.CancelLeads;  
    sampleDateRange: moment.Moment[] = [moment().add(0, 'days').endOf('day'), moment().add(0, 'days').endOf('day')];  
            
    constructor(
        injector: Injector,
        private _leadsServiceProxy: LeadsServiceProxy,
        private _commonLookupService: CommonLookupServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private titleService: Title,
        private _router: Router,
    ) {
        super(injector);

    }
    show(leadId?: number, param?: number, sectionId?: number): void {
        this.leadId = leadId;
        // this._leadsServiceProxy.leadDocumentList(this.bindDocumentFilter).subscribe(result => {
        //     console.log(result);
        //     this.primengTableHelper.totalRecordsCount = result.totalCount;
        //     this.primengTableHelper.records = result.items;
        //     this.primengTableHelper.hideLoadingIndicator();
        //     this.recordCount = result.totalCount;
        //     //this.leadDocumentList = result;            
        // });
        this.getCancelledLead();
    }
    getCancelledLead(event?: LazyLoadEvent) {
        if(this.primengTableHelper.shouldResetPaging(event)){
            //this.paginator.changePage(0);
            //return;
        }
        this.primengTableHelper.showLoadingIndicator();
     
        //this.bindDocumentFilter.filter = this.leadId
        // this._leadsServiceProxy.leadDocumentList(this.bindDocumentFilter).subscribe((result) => {
        //     this.primengTableHelper.totalRecordsCount = result.totalCount;
        //     this.primengTableHelper.records = result.items;
        //     this.primengTableHelper.hideLoadingIndicator();
        //     this.recordCount = result.totalCount;
        // }
        //);
    }

    exportToExcel(): void {
        // this._leadsServiceProxy.getLeadsToExcel(
        //     this.filterText,
        // )
        //     .subscribe(result => {
        //         this._fileDownloadService.downloadTempFile(result);
        //     });
    }

    ngOnInit(): void {        
     
    }
    
    close() { 
        //this.modal.hide();
    }
}
