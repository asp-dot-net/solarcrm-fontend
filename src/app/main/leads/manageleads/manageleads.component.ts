import { OnInit, Component, Injector, ViewEncapsulation, ViewChild, Input, Output, EventEmitter, ElementRef, HostListener } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { ActivatedRoute, Router } from '@angular/router';
import { NotifyService ,TokenService} from 'abp-ng2-module';
import { TokenAuthServiceProxy } from '@shared/service-proxies/service-proxies';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import { finalize } from 'rxjs/operators';
import { FileUpload } from 'primeng/fileupload';
import { Title } from '@angular/platform-browser';
import { LazyLoadEvent } from 'primeng/api';

import { LeadsServiceProxy, LeadsDto, CommonLookupServiceProxy, GetLeadForChangeDuplicateStatusOutput,CommonLookupDto, OrganizationUnitDto, GetLeadForAssignOrTransferOutput, UserServiceProxy } from '@shared/service-proxies/service-proxies';
import { createOrEditManageLeadComponent } from './../customerleads/create-or-edit-manageleads.component';
import { ViewMyLeadComponent } from './../customerleads/view-mylead.component';
import { ModalDirective } from 'ngx-bootstrap/modal';
import * as moment from 'moment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CityServiceProxy, CityDto } from '@shared/service-proxies/service-proxies';
import { DateTime } from 'luxon';
import { AppConsts } from '@shared/AppConsts';
import { DataTransferService } from '@app/shared/common/projectservice/data-transfer.service';
import { truncate } from 'fs';

@Component({
    templateUrl: './manageleads.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()],
})
export class ManageLeadsComponent extends AppComponentBase implements OnInit {
    @ViewChild('ExcelFileUpload', { static: false }) excelFileUpload: FileUpload;
    @ViewChild('createOrEditManageLead', { static: true }) createOrEditManageLead: createOrEditManageLeadComponent;
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    @ViewChild('viewLeadDetail', { static: true }) viewLeadDetail: ViewMyLeadComponent;
    @Input() SelectedLeadId: number = 0;
    @Output() reloadLead = new EventEmitter();
    advancedFiltersAreShown = false;
    FiltersData = false;
    filterText = '';
    customerName:'';
    firstNameFilter = '';
    emailFilter = '';
    phoneFilter = '';
    mobileFilter = '';
    addressFilter = '';
    stateNameFilter = '';
    postCodeFilter = '';
    leadSourceNameFilter = '';
    leadSourceIdFilter = [];
    leadApplicationStatusFilter = [];
    uploadUrl: string;
    channelPartnerFilter = '';
    leadStatusName = '';
    leadTypeNameFilter = '';
    postCodePostalCode2Filter = '';
    leadStatusId = 0;
    organizationUnit = 0;
    teamId = 0;
    salesManagerId = 0;
    salesRepId = 0;
    leadStatusIDS = [];
    leadsourcesIDS = [];
    solarTypeIdFilter = [];
    TenderIdFilter = [];
    DiscomIdFilter = [];
    DivisionIdFilter = [];
    SubDivisionIdFilter = [];
    shouldShow: boolean = false;
    recordCount: any;
    count = 0;
    // StartDate: moment.Moment;
    // EndDate: moment.Moment;
    StartDate: DateTime;
    EndDate: DateTime;
    PageNo: number;
    tableRecords: any;
    ExpandedView: boolean = true;
    // flafValue:boolean = true;
    //SelectedLeadId: number = 0;
    date = '01/01/2022'
    //public sampleDateRange: moment.Moment[] = [moment(this.date), moment().endOf('day')];
    //sampleDateRange: moment.Moment[] = [moment().add(0, 'days').endOf('day'), moment().add(0, 'days').endOf('day')];
    //allOrganizationUnits: OrganizationUnitDto[];
    isAssign = false;
    isChange = false;
    organizationChangeUnit = 0;
    allUsers: CommonLookupDto[];
    allUsersss: CommonLookupDto[];
    filteredManagers: CommonLookupDto[];
    filteredReps: CommonLookupDto[];
    allSalesrepUsers: CommonLookupDto[];
    leadstatuss: CommonLookupDto[];
    allStates: CommonLookupDto[];
    allapplicationStatus: CommonLookupDto[];
    onlyoncheck = false;
    isChangetrue = false;
    isChangefalse = false;
    showOrganizationUnits = false;
    assignLead: number = 0;
    assignLead1: number = 0;

    suburbSuggestions: string[];
    dateFilterType = 'Creation';
    changeView: boolean = true;
    role: string = '';
    projectNumberFilter = '';
    leadName: string;
    sectionId: number = AppConsts.SectionPage.ManageLead;  
    
    jobStatusID = [];
    upgrateshow = false;
    allOrganizationUnits: OrganizationUnitDto[];
    allOrganizationUnitsList: OrganizationUnitDto[];
    leadAction: number = 0;
    duplicateFilter = '';
    flag = true;
    allLeadSources: CommonLookupDto[];
    allSolarType: CommonLookupDto[];
    allDiscom: CommonLookupDto[];
    allDivision: CommonLookupDto[];
    allSubDivision: CommonLookupDto[];
    allTenderData: CommonLookupDto[];
    firstrowcount = 0;
    last = 0;
    leadId: number = null;
    LeadFlag: string = '';
    organizationUnitlength: number = 0;
    currentUserId: number = 0;
    public screenWidth: any;  
    public screenHeight: any;  
    testHeight = 282;
    leadsources: CommonLookupDto[];
    IsAssignLead: boolean = true;
    PaymentStatusFilter:number = 0;   
    show: boolean = true;
    toggle: boolean = true;
    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }  
    testHeightSize () {
        if (this.FiltersData == true) {
            this.testHeight = this.testHeight + -83 ;
        }
        else {
            this.testHeight = this.testHeight - -83 ;
        }
    }
    toggleBlock() {
        this.show = !this.show;
    };
    change() {
        this.toggle = !this.toggle;
    }

    constructor(
        injector: Injector,
        private _leadsServiceProxy: LeadsServiceProxy,
        private _commonLookupService: CommonLookupServiceProxy,       
        private titleService: Title,
        private _httpClient: HttpClient,
        private _tokenService: TokenService,
        private _dataTransfer:DataTransferService,
        private _fileDownloadService: FileDownloadService
    ) {
        super(injector);
        this.titleService.setTitle("SalesDrive | Leads");
        this.uploadUrl = AppConsts.remoteServiceBaseUrl + '/Users/ImportLeadFromExcel';
    }
    
    ngOnInit(): void {
        this.screenHeight = window.innerHeight;
        this.isChangefalse = true;
        this._commonLookupService.getCurrentUserRole().subscribe(result => {

            this.role = result;
            //CP Are Hide the Assign Row 
            if (this.role.toLocaleLowerCase() == 'channel partners') {
                this.IsAssignLead = false;
            }
            if (this.role == 'Sales Rep') {
                this.leadStatusId = 10;
            }
        }, error => { this.spinnerService.hide() });
      
        this._commonLookupService.getAllLeadSourceDropdown().subscribe(result => {
            this.allLeadSources = result;
        });
        this._commonLookupService.getAllApplicationStatusForDropdown().subscribe(result => {
            this.allapplicationStatus = result;
        }, error => { this.spinnerService.hide() });
        this._commonLookupService.getOrganizationUnitUserWise().subscribe(output => {
            
            this.allOrganizationUnits = output;
            //this.allOrganizationUnitsList = this.allOrganizationUnits.filter(item => item.id != this.organizationUnit)
            this.organizationUnit = this.allOrganizationUnits[0].id;
            this.organizationUnitlength = this.allOrganizationUnits.length;
           
            this._commonLookupService.getSalesManagerUser(this.organizationUnit).subscribe(result => {
                this.allUsers = result;
            });
            this.onsalesmanger();
            this.onsalesrep();
            this._commonLookupService.getCurrentUserId().subscribe(result => {
                this.currentUserId = result;
                if (this.role == 'Sales Manager') {

                    this.salesManagerId = this.currentUserId;
                }
                this.getManageLead();
            }, error => { this.spinnerService.hide() });
           
        });

        this._commonLookupService.getAllLeadStatusDropdown().subscribe(result => {
            this.leadstatuss = result;
        }, error => { this.spinnerService.hide() });
        this._commonLookupService.getAllDiscomFilterDropdown(0).subscribe(result => {
            this.allDiscom = result;
        }, error => { this.spinnerService.hide() });
        this._commonLookupService.getAllDivisionFilterDropdown(0).subscribe(result => {
            this.allDivision = result;
        }, error => { this.spinnerService.hide() });
        this._commonLookupService.getAllSubdivisionWiseData('').subscribe(result => {
            this.allSubDivision = result;
        }, error => { this.spinnerService.hide() });
        this._commonLookupService.getAllSolarTypeForDropdown().subscribe(result => {
            this.allSolarType = result;
        }, error => { this.spinnerService.hide() });
        this._commonLookupService.getAllStateForDropdown(0).subscribe(result => {
            this.allStates = result;
        }, error => { this.spinnerService.hide() });
        this._commonLookupService.getAllLeadSourceDropdown().subscribe(result => {
            this.leadsources = result;
          }, error => { this.spinnerService.hide() });
    }
    chaneSolarType() {
        this._commonLookupService.getSolarTypeWiseTenderForDropdown(this.solarTypeIdFilter).subscribe(result => {
            this.allTenderData = result;
        }, error => { this.spinnerService.hide() });
    }
    clear() {
        this.filterText = '';
        this.addressFilter = '';
        //this.postCodeSuburbFilter = '';
        this.leadStatusIDS = [];
        this.leadsourcesIDS = [];
        this.leadApplicationStatusFilter = [];

        this.stateNameFilter = '';
        this.postCodePostalCode2Filter = '';
        this.leadSourceNameFilter = '';
        this.solarTypeIdFilter = [];
        this.TenderIdFilter = [];
        this.DiscomIdFilter = [];
        this.DivisionIdFilter = [];
        this.SubDivisionIdFilter = [];
        this.leadSourceIdFilter = [];        
       // this.sampleDateRange = [moment(this.date), moment().endOf('day')]; //[moment().add(-7, 'days').endOf('day'), moment().add(0, 'days').startOf('day')];
        this.jobStatusID = [];
        this.getManageLead();
    }
    
    getOrganizationuser() {
        if (this.isChange == false) {
            this._commonLookupService.getSalesManagerUser(this.organizationUnit).subscribe(result => {
                this.allUsers = result;
            }, error => { this.spinnerService.hide() });
        }
    }
    // getOrganizationWiseUser() {
    //     if (this.isChange == true) {
    //         this._commonLookupService.getSalesManagerUser(this.organizationChangeUnit).subscribe(result => {
    //             this.allUsersss = result;
    //             console.log(result)
    //         });
    //     }
    // }
    getOrganizationWiseUser() {
        
        if (this.leadAction == 2) {
            this.allOrganizationUnitsList = this.allOrganizationUnits.filter(item => item.id != this.organizationUnit);
            if (this.allOrganizationUnitsList.length > 0) {
                this.showOrganizationUnits = true;
                this._commonLookupService.getSalesManagerUser(this.organizationChangeUnit).subscribe(result => {
                    this.allUsers = result;
                }, error => { this.spinnerService.hide() });
            } else {
                this.showOrganizationUnits = false;
                this.notify.warn(this.l('NoOrganizationAvailable'));
                this._commonLookupService.getSalesManagerUser(this.organizationUnit).subscribe(result => {
                    this.allUsers = result;
                }, error => { this.spinnerService.hide() });
            }
        }
        if (this.leadAction == 3) {
            this._commonLookupService.getSalesManagerUser(this.organizationUnit).subscribe(result => {
                this.allUsers = result;
            }, error => { this.spinnerService.hide() });
        }
        if (this.leadAction == 4) {

            this.allOrganizationUnitsList = this.allOrganizationUnits.filter(item => item.id != this.organizationUnit);
            if (this.allOrganizationUnitsList.length > 0) {
                //this.organizationChangeUnit = this.allOrganizationUnitsList[0].id;
                this.showOrganizationUnits = true;
            } else {
                this.showOrganizationUnits = false;
                this.notify.warn(this.l('NoOrganizationAvailable'));
            }
        }
    }
    onsalesmanger() {
        this._commonLookupService.getSalesManagerForFilter(this.organizationUnit, undefined).subscribe(manager => {
            this.filteredManagers = manager;
        }, error => { this.spinnerService.hide() });
    }
    onsalesrep() {
        this._commonLookupService.getSalesRepForFilter(this.organizationUnit, undefined).subscribe(rep => {
            this.filteredReps = rep;
        }, error => { this.spinnerService.hide() });
    }
    getsalerep(event) {
        const id = event.target.value;
        if (id != 0) {
            
            if (this.organizationChangeUnit != 0) {
                this._commonLookupService.getSalesRepBySalesManagerid(id, this.organizationChangeUnit).subscribe(result => {
                    this.allSalesrepUsers = result;
                }, error => { this.spinnerService.hide() });
            } else {
                this._commonLookupService.getSalesRepBySalesManagerid(id, this.organizationUnit).subscribe(result => {
                    this.allSalesrepUsers = result;
                }, error => { this.spinnerService.hide() });
            }
        }
        this.assignLead1 = 0;
    }
    saving = false;

    getManageLead(event?: LazyLoadEvent) {

        this._dataTransfer.setOrgData(this.organizationUnit.toString());
        this.PageNo = this.primengTableHelper.getSkipCount(this.paginator, event);
        
        // if (this.StartDate != null && this.EndDate != null) {
        //     // this.StartDate = this.sampleDateRange[0];
        //     // this.EndDate = this.sampleDateRange[1];
        // } else {
        //     this.StartDate = null;
        //     this.EndDate = null;
        // }

        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            //return;

            //change the current primeng-paginator file getPageCount() method
        }

        this.primengTableHelper.showLoadingIndicator();
        
        this._leadsServiceProxy.getManageAllLeads(
            this.filterText,
            this.customerName,
            this.firstNameFilter,
            this.emailFilter,
            this.phoneFilter,
            this.mobileFilter,
            this.addressFilter,
            this.stateNameFilter,
            this.postCodeFilter,
            this.leadSourceNameFilter,
            this.leadsourcesIDS,
            this.leadStatusName,
            (this.leadStatusId == 0) ? undefined : this.leadStatusId,
            this.leadTypeNameFilter,
            this.solarTypeIdFilter,
            this.TenderIdFilter,
            this.DiscomIdFilter,
            this.DivisionIdFilter,
            this.SubDivisionIdFilter,
            0,
            this.StartDate,
            this.EndDate,
            this.organizationUnit,
            this.teamId == 0 ? undefined : this.teamId,
            this.salesManagerId == 0 ? undefined : this.salesManagerId,
            this.salesRepId == 0 ? undefined : this.salesRepId,
            this.dateFilterType,
            this.duplicateFilter,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            this.leadStatusIDS,           
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)

        ).subscribe((result) => {          
            
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
            this.firstrowcount = totalrows + 1;
            this.last = totalrows + result.items.length;
            this.tableRecords = result.items;
            if (result.totalCount > 0) {
                //this.getLeadSummaryCount();
            } else {
                
            }
            this.primengTableHelper.hideLoadingIndicator();
            this.recordCount = result.totalCount;
            this.shouldShow = false;
            // if (this.flag == false && result.totalCount != 0 && this.ExpandedView == false) {
            //     this.navigateToLeadDetail(this.SelectedLeadId)
            // }
            // else {
            //     if (this.ExpandedView == false && result.totalCount != 0) {
            //         this.navigateToLeadDetail(result.items[0].leads)
            //     }
            //     else {
            //         this.ExpandedView = true;
            //     }
            // }
        }, error => { this.spinnerService.hide() });
    }
   
    onUploadExcelError(): void {
        this.notify.error(this.l('ImportLeadUploadFailed'));
    }
  
    
    reloadPage(flafValue: boolean): void {
        
        this.ExpandedView = true;
        this.flag = flafValue;
        this.paginator.changePage(this.paginator.getPage());
        if (!flafValue) {
           // this.navigateToLeadDetail(this.SelectedLeadId);
        }
        this.paginator.changePage(this.paginator.getPage());
    }
    
    DeleteselectedLead(){
        let selectedids = [];
       
        this.primengTableHelper.records.forEach(function (leads) {
            if (leads.leads.isSelected) {
                selectedids.push(leads.leads.id);
            }
        });
        if (selectedids.length != 0) {
            this._leadsServiceProxy.deleteManageLead(selectedids)
                .subscribe(() => {
                    this.reloadPage(true);
                    this.notify.info(this.l('DeleteSuccessfully'));
                    this.saving = false;
                }, error => { this.spinnerService.hide() });
        }
        else {
            this.notify.warn(this.l('No Leads Selected'));
            this.saving = false;
        }
    }
   
   
    duplicate(lead: LeadsDto): void {
        let status: GetLeadForChangeDuplicateStatusOutput = new GetLeadForChangeDuplicateStatusOutput();
        status.id = lead.id;
        status.isDuplicate = true;
        this.message.confirm('',
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._leadsServiceProxy.changeDuplicateStatus(status)
                        .subscribe(() => {
                            this.reloadPage(true);
                            document.body.classList.add('removeAlerticon');
                            this.notify.success(this.l('Status Change To Duplicate'));
                        });
                }
            }
        );
    }

    notduplicate(lead: LeadsDto): void {
        let status: GetLeadForChangeDuplicateStatusOutput = new GetLeadForChangeDuplicateStatusOutput();
        status.id = lead.id;
        status.isDuplicate = false;
        this.message.confirm('',
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._leadsServiceProxy.changeDuplicateStatus(status)
                        .subscribe(() => {
                            this.reloadPage(true);
                            document.body.classList.add('removeAlerticon');
                            this.notify.success(this.l('Status Change To Not Duplicate'));
                        });
                }
            }
        );
    }
    deleteLead(lead: LeadsDto){
        this.message.confirm('',
            this.l('AreYouSureYouWantToDeleteExistingLead'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._leadsServiceProxy.deleteDuplicateLeads(lead.id)
                        .subscribe(() => {
                            this.reloadPage(true);
                            this.notify.success(this.l('SuccessfullyDeleted'));
                        }, error => { this.spinnerService.hide() });
                }
            }
        );
    }

   webDuplicate(lead: LeadsDto): void {
        let status: GetLeadForChangeDuplicateStatusOutput = new GetLeadForChangeDuplicateStatusOutput();
        status.id = lead.id;
        status.isWebDuplicate = true;
        this.message.confirm('',
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._leadsServiceProxy.changeDuplicateStatus(status)
                        .subscribe(() => {
                            this.reloadPage(true);
                            document.body.classList.add('removeAlerticon');
                            this.notify.success(this.l('Status Change To Duplicate'));
                        });
                }
            }
        );
    }

    webNotduplicate(lead: LeadsDto): void {
        let status: GetLeadForChangeDuplicateStatusOutput = new GetLeadForChangeDuplicateStatusOutput();
        status.id = lead.id;
        status.isWebDuplicate = false;
        this.message.confirm('',
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._leadsServiceProxy.changeDuplicateStatus(status)
                        .subscribe(() => {
                            this.reloadPage(true);
                            document.body.classList.add('removeAlerticon');
                            this.notify.success(this.l('Status Change To Not Duplicate'));
                        });
                }
            }
        );
    }
    exportToExcel(excelorcsv): void {
        this._leadsServiceProxy.getLeadsToExcel(
            this.filterText,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            excelorcsv,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            this.organizationUnit,
            undefined
        )
        .subscribe(result => {
           this._fileDownloadService.downloadTempFile(result);
         }, error => { this.spinnerService.hide() });
   }

    uploadExcel(data: { files: File }): void {
        
        var headers_object = new HttpHeaders().set("Authorization", "Bearer " + this._tokenService.getToken());

        const httpOptions = {
            headers: headers_object
        };

        const formData: FormData = new FormData();
        const file = data.files[0];
        formData.append('file' + ',' + this.organizationUnit, file, file.name);

        this._httpClient
            .post<any>(this.uploadUrl, formData, httpOptions)
            .pipe(finalize(() => this.excelFileUpload.clear()))
            .subscribe(response => {
                if (response.success) {
                    this.notify.success(this.l('ImportLeadsProcessStart'));
                } else if (response.error != null) {
                    this.notify.error(this.l('ImportLeadUploadFailed'));
                }
            }, error => { this.spinnerService.hide() });
    }

    
    createManageLead(id): void {
        this.createOrEditManageLead.show(this.organizationUnit, id, this.IsAssignLead, this.currentUserId,this.LeadFlag = 'M');
    }
    isAllChecked() {
        if (this.tableRecords)
            return this.tableRecords.every(_ => _.leads.isSelected);
    }
    checkAll(ev) {

        this.tableRecords.forEach(x => x.leads.isSelected = ev.target.checked);
        this.oncheckboxCheck();
        /// this.totalcount = this.tableRecords.forEach(x => x.lead.isSelected = ev.target.checked);
    }
    oncheck(event) {
        if (event.target.checked == true) {
            if (this.allOrganizationUnitsList.length > 0) {
                this.isChange == true;
                this.showOrganizationUnits = true;
                this.isChangetrue = true;
                this.isChangefalse = false
                this.getOrganizationWiseUser();
            } else {
                this.isChange == false;
                this.showOrganizationUnits = false;
                this.notify.warn(this.l('NoOrganizationAvailable'));

            }
        } else {
            this.isChange == false;
            this.showOrganizationUnits = false;
            this.isChangetrue = false;
            this.isChangefalse = true;
            this.getOrganizationuser();
        }
    }

    oncheckboxCheck() {
        this.count = 0;
        this.tableRecords.forEach(item => {
            if (item.leads.isSelected == true) {
                this.count = this.count + 1;
            }
        })
    }

    submit(): void {        
        this.saving = true;
        let selectedids = [];
        this.primengTableHelper.records.forEach(function (leads) {
            if (leads.leads.isSelected) {
                selectedids.push(leads.leads.id);
            }
        });
        if (this.leadAction == 1) {           
            if (selectedids.length != 0) {
                this._leadsServiceProxy.deleteManageLead(selectedids)
                    .subscribe(() => {
                        this.reloadPage(true);
                        this.notify.info(this.l('DeleteSuccessfully'));
                        this.saving = false;
                    }, error => { this.spinnerService.hide() });
            }
            else {
                this.notify.warn(this.l('NoLeadsSelected'));
                this.saving = false;
            }
        }else if (this.leadAction == 2) {          
            if (this.assignLead == 0) {
                this.notify.warn(this.l('NoUserSelected'));
                this.saving = false;
            }
            let assignleads: GetLeadForAssignOrTransferOutput = new GetLeadForAssignOrTransferOutput();
            assignleads.assignToUserID = this.assignLead;
            assignleads.leadIds = selectedids;
            assignleads.organizationID = this.organizationChangeUnit;
            if (selectedids.length != 0) {
                this._leadsServiceProxy.assignOrTransferLead(assignleads)
                    .pipe(finalize(() => { this.saving = false; }))
                    .subscribe(() => {
                        this.reloadPage(true);
                        this.notify.info(this.l('AssignedSuccessfully'));
                        this.saving = false;
                    }, error => { this.spinnerService.hide() });
            }
            else {
                this.notify.warn(this.l('NoLeadsSelected'));
                this.saving = false;
            }
        }
        else if (this.leadAction == 3) {           
            if (this.assignLead == 0) {
                this.notify.warn(this.l('NoUserSelected'));
                this.saving = false;
            }
            let assignleads: GetLeadForAssignOrTransferOutput = new GetLeadForAssignOrTransferOutput();
            assignleads.assignToUserID = this.assignLead;
            assignleads.leadIds = selectedids;
            if(this.organizationChangeUnit == 0){
                assignleads.organizationID = this.organizationUnit;
            }
            else{
                assignleads.organizationID = this.organizationChangeUnit;
            }
            if (selectedids.length != 0) {
                this._leadsServiceProxy.assignOrTransferLead(assignleads)
                    .pipe(finalize(() => { this.saving = false; }))
                    .subscribe(() => {
                        this.reloadPage(true);
                        this.notify.info(this.l('AssignedSuccessfully'));
                        this.saving = false;
                    }, error => { this.spinnerService.hide() });
            }
            else {
                this.notify.warn(this.l('NoLeadsSelected'));
                this.saving = false;
            }
        }
        else if (this.leadAction == 4) {           
                    
            if (selectedids.length == 1) {
                if (this.organizationChangeUnit) {
                    this._leadsServiceProxy.checkCopyExist(this.organizationChangeUnit, selectedids[0]).subscribe(result => {
                        if (!result) {
                            this._leadsServiceProxy.copyLead(this.organizationChangeUnit, selectedids[0])
                                .pipe(finalize(() => { this.saving = false; }))
                                .subscribe(() => {
                                    this.reloadPage(true);
                                    this.notify.info(this.l('CoppiedSuccessfully'));
                                    this.saving = false;
                                }, error => { this.spinnerService.hide() });
                        }
                        else {
                            this.notify.warn(this.l('LeadAlreadyCopied'));
                            this.saving = false;
                        }
                    });
                }
                else {
                    this.notify.warn(this.l('PleaseSelectOrganization'));
                    this.saving = false;
                }
            }
            else {
                this.notify.warn(this.l('OnlyOneLead'));
                this.saving = false;
            }
        }
        else if (this.leadAction == 5) {
           debugger;
            let selectedids = [];
            this.primengTableHelper.records.forEach(function (leads) {
                if (leads.leads.isSelected) {
                    if(leads.duplicate == true || leads.webDuplicate == true)
                    {
                        selectedids.push(leads.leads.id);
                    }
                }
            });
            if (selectedids.length != 0) {
                let status: GetLeadForChangeDuplicateStatusOutput = new GetLeadForChangeDuplicateStatusOutput();
                status.leadId = selectedids;
                status.hideDublicate = true;
                this.message.confirm('',
                    this.l('AreYouSure'),
                    (isConfirmed) => {
                        if (isConfirmed) {
                            this._leadsServiceProxy.changeDuplicateStatusForMultipleLeadsHide(status)
                                .subscribe(() => {
                                    // this.reloadPage();
                                    this._leadsServiceProxy.updateWebDupLeads(status)
                                        .subscribe(() => {
                                        this.saving = false;
                                        this.reloadPage(true);
                                        this.notify.success(this.l('StatusChangeToDuplicate'));
                                    });
                                    
                                    // this.notify.success(this.l('StatusChangeToDuplicate'));
                                });
                        } else {
                            this.saving = false;
                        }
                    }
                );

            }
            else {
                this.notify.warn(this.l('NoLeadsSelected'));
                this.saving = false;
            }
        }
        else if (this.leadAction == 6) {
            this._leadsServiceProxy.updateFacebookLead()
            .subscribe(() => {
                this.saving = false;
                this.reloadPage(true);
                this.notify.success(this.l('DataUpdatedSuccessfully'));
            }, err => { this.saving = false; });
        }
        else {
            this.saving = false;
        }
        // if (this.isChange) {
           
        //     let assignleads: GetLeadForAssignOrTransferOutput = new GetLeadForAssignOrTransferOutput();
        //     assignleads.assignToUserID = this.assignLead;
        //     assignleads.leadIds = selectedids;
        //     assignleads.organizationID = this.organizationChangeUnit;
        //     if (this.organizationChangeUnit == 0) {
        //         this.notify.warn(this.l('PleaseSelectOrganization'));
        //         this.saving = false;
        //         return;
        //     }
        //     if (selectedids.length == 0) {
        //         this.notify.warn(this.l('NoLeadsSelected'));
        //         this.saving = false;
        //         return;
        //     }
        //     if (this.assignLead == 0) {
        //         this.notify.warn(this.l('PleaseSelectUser'));
        //         this.saving = false;
        //         return;
        //     }
        //     if (this.isAssign) {
        //         if (this.assignLead1 == 0) {
        //             this.notify.warn(this.l('PleaseSelctSalesRep'));
        //             this.saving = false;
        //             return;
        //         }
        //         else {
        //             assignleads.assignToUserID = this.assignLead1;
        //         }
        //     }

        //     //Lead Transfer to other Organization
        //     this._leadsServiceProxy.transferLeadToOtherOrganization(assignleads)
        //         .pipe(finalize(() => { this.saving = false; }))
        //         .subscribe(() => {
        //             this.reloadPage(true);
        //             this.isChange = false;
        //             this.saving = false;
        //             this.notify.info(this.l('AssignedSuccessfully'));
        //         });
        // }
        // else {
        //     this.primengTableHelper.records.forEach(function (leads) {
        //         if (leads.leads.isSelected) {
        //             selectedids.push(leads.leads.id);
        //         }
        //     });
        //     let assignleads: GetLeadForAssignOrTransferOutput = new GetLeadForAssignOrTransferOutput();
        //     assignleads.assignToUserID = this.assignLead;
        //     assignleads.leadIds = selectedids;
        //     assignleads.organizationID = this.organizationUnit;
        //     if (selectedids.length == 0) {
        //         this.notify.warn(this.l('NoLeadsSelected'));
        //         this.saving = false;
        //         return;
        //     }
        //     if (this.assignLead == 0) {
        //         this.notify.warn(this.l('PleaseSelectUser'));
        //         this.saving = false;
        //         return;
        //     }
        //     if (this.isAssign) {
        //         if (this.assignLead1 == 0) {
        //             this.notify.warn(this.l('PleaseSelctSalesRep'));
        //             this.saving = false;
        //             return;
        //         }
        //         else {
        //             assignleads.assignToUserID = this.assignLead1;
        //         }
        //     }

        //     this._leadsServiceProxy.assignOrTransferLead(assignleads)
        //         .pipe(finalize(() => { this.saving = false; }))
        //         .subscribe(() => {
        //             this.reloadPage(true);
        //             this.saving = false;
        //             this.notify.info(this.l('AssignedSuccessfully'));
        //         });
        // }

    }

   
}
