import { HttpClient } from '@angular/common/http';
import { Component, EventEmitter, Injector, Input, OnInit, Output, ViewChild, HostListener } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { MISReportServiceProxy, MISReportDto,OrganizationUnitDto, CommonLookupServiceProxy } from '@shared/service-proxies/service-proxies';
import { FileUpload } from 'primeng/fileupload';
import { finalize } from 'rxjs/operators';
import { AppConsts } from '@shared/AppConsts';
import { LazyLoadEvent } from 'primeng/api';

import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { appModuleAnimation } from '@shared/animations/routerTransition';
@Component({
    selector: 'misReport',
    templateUrl: './misReport.component.html',
    animations: [appModuleAnimation()]
})
export class MISReportComponent extends AppComponentBase {
    @ViewChild('ExcelFileUpload', { static: false }) excelFileUpload: FileUpload;
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    sectionId: number = AppConsts.SectionPage.MISReport; 
    recordCount : any;
    uploadUrl: string;
    filterText : string = '';
    organizationUnit = 0;
    organizationUnitlength: number = 0;
    allOrganizationUnits: OrganizationUnitDto[];
    allOrganizationUnitsList: OrganizationUnitDto[];
    public screenWidth: any;  
    public screenHeight: any;  
    testHeight = 200;
    show: boolean = true;
    toggle: boolean = true;
    GovermentAgency:number = 0;
    toggleBlock() {
        this.show = !this.show;
    };
    change() {
        this.toggle = !this.toggle;
    }
    constructor(
        injector: Injector,
        private _httpClient: HttpClient,        
         private _misReportServiceProxy: MISReportServiceProxy,
        // private _leadsServiceProxy: LeadsServiceProxy,
         private _commonLookupService: CommonLookupServiceProxy,
        // private _activatedRoute: ActivatedRoute,
        // private titleService: Title,
        // private _router: Router,
    ) {
        super(injector);
        this.uploadUrl = AppConsts.remoteServiceBaseUrl + '/MISReport/ImportFromExcelMIS';

    }
    ngOnInit(): void {
        this.screenHeight = window.innerHeight;
        this._commonLookupService.getOrganizationUnitUserWise().subscribe(output => {

            this.allOrganizationUnits = output;
            this.organizationUnit = this.allOrganizationUnits[0].id;
            this.organizationUnitlength = this.allOrganizationUnits.length;

            //change Organization
            // this.allOrganizationUnitsList = this.allOrganizationUnits.filter(item => item.id != this.organizationUnit) 
            // if(this.allOrganizationUnitsList == undefined){
            //     this.allOrganizationUnitsList = [];
            // }
        }, error => { this.spinnerService.hide() })
    }
    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }  
    
    getMISReport(event?: LazyLoadEvent) {
        if(this.primengTableHelper.shouldResetPaging(event)){
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();
        
        this._misReportServiceProxy.getAllMisFileImportList(
            
            this.filterText,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe((result) => {            
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            this.primengTableHelper.hideLoadingIndicator();
            this.recordCount = result.totalCount;

        }, error => { this.spinnerService.hide() });
    }

    downfile(filename): void {        
        let FileName = AppConsts.remoteServiceBaseUrl + filename;//this.LeadDocument.filePath;
        window.open(FileName, "_blank");
    };
    changeAgency(event){
        if (event.target.value != 0) {

        }
    }
    uploadExcel(data: { files: File }): void {        
        const formData: FormData = new FormData();
        const file = data.files[0];
        formData.append('file', file, file.name);
        formData.append('GovermentAgency',  this.GovermentAgency.toString());
        this._httpClient
            .post<any>(this.uploadUrl, formData)
            .pipe(finalize(() => this.excelFileUpload.clear()))
            .subscribe((response) => {
                if (response.success) {
                    this.notify.success(this.l('ImportMISReportProcessStart'));
                } else if (response.error != null) {
                    this.notify.error(this.l('ImportMISReportUploadFailed'));
                }
            }, error => { this.spinnerService.hide() });
    }
  
    
    onUploadExcelError(): void {
        this.notify.error(this.l('ImportStockItemUploadFailed'));
    }
}