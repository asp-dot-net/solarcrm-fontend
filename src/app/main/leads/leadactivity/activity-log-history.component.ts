import { Component, ViewChild, Injector, Output, EventEmitter, OnInit, ElementRef, Directive, Optional } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { LeadsActivityLogServiceProxy, LeadtrackerHistoryDto} from '@shared/service-proxies/service-proxies';

import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';
import { ActivatedRoute, Router } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AgmCoreModule } from '@agm/core';

@Component({
    selector: 'activityloghistory',
    templateUrl: './activity-log-history.component.html',
    animations: [appModuleAnimation()]
})
export class ActivityLogHistoryComponent extends AppComponentBase {
    @ViewChild("myNameElem") myNameElem: ElementRef;
    @ViewChild('addModal', { static: true }) modal: ModalDirective;
    @ViewChild('SampleDatePicker', { static: true }) sampleDatePicker: ElementRef;

    leadtrackerHistory: LeadtrackerHistoryDto[];
    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
        private _leadsServiceProxy: LeadsActivityLogServiceProxy,
        private _router: Router,
        private _el: ElementRef

    ) {
        super(injector);
        this._el.nativeElement.setAttribute('autocomplete', 'off');
        this._el.nativeElement.setAttribute('autocorrect', 'off');
        this._el.nativeElement.setAttribute('autocapitalize', 'none');
        this._el.nativeElement.setAttribute('spellcheck', 'false');
    }

    show(leadId?: number, leadorjob?: number): void {
        
        //if (leadorjob == 1) {
            this._leadsServiceProxy.getLeadActivityLogHistory(leadId, 2,0, 0, false, false, 0).subscribe(result => {
                this.leadtrackerHistory = result;
                this.modal.show();
            }, error => { this.spinnerService.hide() });
        //}
        // if (leadorjob == 2) {
        //     this._leadsServiceProxy.getJobActivityLogHistory(leadId, 2, 0, false, false, 0).subscribe(result => {
        //         this.leadtrackerHistory = result;
        //         this.modal.show();
        //     });
        // }
    }
    close(): void {

        this.modal.hide();
    }
}


