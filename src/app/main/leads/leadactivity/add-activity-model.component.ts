import { Component, Inject, Renderer2, ElementRef, EventEmitter, Injectable, Injector, OnInit, Optional, Output, ViewChild, Input } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
//import { LeadsServiceProxy, GetLeadForViewDto, LeadDto, ActivityLogInput, CommonLookupDto, EmailTemplateServiceProxy, SmsTemplatesServiceProxy, JobsServiceProxy, JobPromotionsServiceProxy, JobPromotionPromotionMasterLookupTableDto } from '@shared/service-proxies/service-proxies';
import { GetLeadsForViewDto, CommonLookupDto, UserListDto, LeadsDto, LeadsServiceProxy, CommonLookupServiceProxy, ActivityLogInput, LeadsActivityLogServiceProxy } from '@shared/service-proxies/service-proxies';
import * as _ from 'lodash';
import { DOCUMENT } from '@angular/common';
import * as moment from 'moment';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { EmailEditorComponent } from 'angular-email-editor';
import { AppConsts } from '@shared/AppConsts';
//import { ViewMyLeadComponent } from './view-mylead.component';
import { ViewMyLeadComponent } from '../customerleads/view-mylead.component';
import { NgxSpinnerService } from 'ngx-spinner';
import { DataTransferService } from '@app/shared/common/projectservice/data-transfer.service';
import { appModuleAnimation } from '@shared/animations/routerTransition';

@Component({
    selector: 'addActivityModal',
    templateUrl: './add-activity-model.component.html',
    animations: [appModuleAnimation()]
})

export class AddActivityModalComponent extends AppComponentBase implements OnInit {

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @Output() reloadLead = new EventEmitter<boolean>();
    @Input() selectedactionId :number = 3;
    @Input() selectedsectionId:number = 0;
    //@Input() mainorganizationID :number = 0;
    @ViewChild("myNameElem") myNameElem: ElementRef;
    @ViewChild("area_select") area_select: ElementRef;
    @ViewChild("modal_body") modal_body: ElementRef;
    @ViewChild('addModal') public modal: ModalDirective;
    @ViewChild('btnEmail', { static: true }) btnEmail: ElementRef;
    @ViewChild('btnsmsnotify', { static: true }) btnsmsnotify: ElementRef;
    @ViewChild('SampleDatePicker', { static: true }) sampleDatePicker: ElementRef;
    @ViewChild('organizationunitid', { static: true }) organizationunitid :ElementRef;
   
    saving = false;    
    item: GetLeadsForViewDto;
    activityLog: ActivityLogInput = new ActivityLogInput();
    activityType: number = 6;
    priorityType: number = 1;
    activityName = "";
    actionNote = "";
    id = 0;
    allEmailTemplates: CommonLookupDto[];
    allSMSTemplates: CommonLookupDto[];
    drpleadactivity: CommonLookupDto[];
    emailData = '';
    smsData = '';
    emailTemplate: number;
    uploadUrl: string;
    uploadedFiles: any[] = [];
    actionId: number = 0;
    sectionId: number = AppConsts.SectionPage.ManageActivity; 
    //date = moment(this.myDate);
    total = 0;
    credit = 0;
    customeTagsId = 0;
    role: string = '';
    subject = '';
    userName: string;
    leadCompanyName: any;
    pageid = 0;
    allUsers: UserListDto[];
    referanceId = 0;
    CurrentDate = new Date();
    //freebieTransport: JobPromotionPromotionMasterLookupTableDto[];
    emailfromlist: any[];
    emailto: string = '';
    fromemails: CommonLookupDto[];
    leadid: number;
    criteriaavaibaleornot = false;
    emailbtnHidden: boolean;
    smsnotifybtnhidden: boolean;
    constructor(
        injector: Injector,
        private _leadsServiceProxy: LeadsServiceProxy,
        private _commonLookupServiceProxy: CommonLookupServiceProxy,
        private _leadactivityLogServiceProxy: LeadsActivityLogServiceProxy,      
        private _dataTransfer:DataTransferService,
        @Inject(DOCUMENT) private document: Document,
        // private _jobsServiceProxy: JobsServiceProxy,
        // private _jobPromotionsServiceProxy: JobPromotionsServiceProxy,
        private spinner: NgxSpinnerService,
        @Optional() private viewLeadDetail?: ViewMyLeadComponent       
    ) {
        super(injector);
        this.item = new GetLeadsForViewDto();
        this.item.leads = new LeadsDto();
        this.uploadUrl = AppConsts.remoteServiceBaseUrl + '/DemoUiComponents/UploadAttachments';
    }
    
    @ViewChild(EmailEditorComponent)
    private emailEditor: EmailEditorComponent;
    ngOnInit(): void {
        this.emailfromlist = [];
        this.activityLog.emailTemplateId = 0;
        this.activityLog.smsTemplateId = 0;
        try {
            this.spinnerService.show();
            this._commonLookupServiceProxy.getAllLeadActivityDropdown().subscribe(result => {
                this.drpleadactivity = result;
            }, error => { this.spinnerService.hide(); });     
            this._commonLookupServiceProxy.getCurrentUserRole().subscribe(result => {                            
                this.role = result;
            }, error => { this.spinnerService.hide(); });
    
            this._commonLookupServiceProxy.getCurrentUserIdName().subscribe(result => {                
                this.userName = result;               
            }, error => { this.spinnerService.hide(); });
            
            this._commonLookupServiceProxy.getAllUserList().subscribe(result => {
                this.allUsers = result;
               let emailfrom = this.allUsers.find(x => x.userName == this.userName);
                this.emailfromlist.push(emailfrom);        
            }, error => { this.spinnerService.hide(); }); 
            this.spinnerService.hide();
        } catch (error) {
            this.spinnerService.hide();
        }        
    }
 
    show(leadId: number, activityid?: number, pageid?: number, sectionId?: number, refundfreebiesid?: number,actionId?: number): void {
        this.spinner.show();
        this._commonLookupServiceProxy.getAllEmailTemplateDropdown(parseInt(this._dataTransfer.getOrgData())).subscribe(result => {
            this.allEmailTemplates = result;
        }, error => { this.spinnerService.hide(); });

        this._commonLookupServiceProxy.getAllSMSTemplateDropdown(parseInt(this._dataTransfer.getOrgData())).subscribe(result => {
            this.allSMSTemplates = result;
        }, error => { this.spinnerService.hide(); });
        
        this.activityLog.activityNote = "";
        this.activityLog.emailTemplateId = 0;
        this.activityLog.smsTemplateId = 0;
        this.activityLog.referanceId = 0;
        this.actionNote = actionId == 12 ? 'quote' : '';
        this.activityLog.body = "";
        this.pageid = pageid;
        this.activityLog.sectionId = sectionId;
        this.activityLog.referanceId = refundfreebiesid;
        this.activityLog.organizationUnit = parseInt(this._dataTransfer.getOrgData());
        //this.emailfromlist = AppConsts.EmailConfigure.emailFrom;
        //this.activityLog.emailFrom = this.emailfrom; //this.fromemails[0].displayName; //
        this.leadid = leadId;
        
        try {
            if (activityid == AppConsts.leadActivity.SMS) {
                
                this.id = activityid;
                this.activityType = AppConsts.leadActivity.SMS;
                this.activityName = 'SMS';
                this.activityLog.activityId = AppConsts.leadActivity.SMS;
                this.activityLog.actionId = this.selectedactionId;
            }
            else if (activityid == AppConsts.leadActivity.Email) {
                this.id = activityid;
                this.activityType = AppConsts.leadActivity.Email;
                this.activityName = 'Email';
                this.activityLog.activityId = AppConsts.leadActivity.Email;
                this.activityLog.actionId = this.selectedactionId;
            }
            else if (activityid == AppConsts.leadActivity.Notify) {
                this.id = activityid;
                this.activityType = AppConsts.leadActivity.Notify;
                this.activityName = 'Notify';
                this.activityLog.activityId = AppConsts.leadActivity.Notify;
                this.activityLog.actionId = this.selectedactionId;
            }
            else if (activityid == AppConsts.leadActivity.Comment) {
                this.id = activityid;
                this.activityType = AppConsts.leadActivity.Comment;
                this.activityName = 'Comment';
                this.activityLog.activityId = AppConsts.leadActivity.Comment;
                this.activityLog.actionId = this.selectedactionId;
            }
            else if (activityid == AppConsts.leadActivity.ToDo) {
                this.id = activityid;
                this.activityType = AppConsts.leadActivity.ToDo;
                this.activityName = 'ToDo';
                this.activityLog.activityId = AppConsts.leadActivity.ToDo;
                this.activityLog.actionId = this.selectedactionId;
            }
            else {
                this.id = activityid;
                this.activityType = AppConsts.leadActivity.Reminder;
                this.activityName = 'Reminder';
                this.activityLog.activityId = AppConsts.leadActivity.Reminder;
                this.activityLog.actionId = this.selectedactionId;
            }
            this._leadsServiceProxy.getLeadsForView(leadId).subscribe(result => {
                this.item = result;
                this.spinner.hide();
                this.leadCompanyName = result.leads.firstName;
                if (result.leads.emailId != null) {
                    this.emailto = result.leads.emailId;
                }                
                this.modal.show();
            }, error => { this.spinnerService.hide() });
            
            //this._commonLookupServiceProxy.getOrganizationUnit().subscribe(result => {
    
            //this.activityLog.organizationUnit = parseInt(this._dataTransfer.getOrgData()); //this.mainorganizationID; //result[]
            
            
        } catch (error) {
            this.spinner.hide();
        }
    }

    close(): void {
        this.modal.hide();
    }
    setHTML(emailHTML) {

        let htmlTemplate = this.getEmailTemplate(emailHTML);
        this.activityLog.body = htmlTemplate;
        this.save();
    }
    countCharcters(): void {
        if (this.role != 'Admin') {
            this.total = this.activityLog.body.length;
            this.credit = Math.ceil(this.total / 160);
            if (this.total > 320) {
                this.notify.warn(this.l('You Can Not Add more than 320 characters'));
            }
        }
        else {
            this.total = this.activityLog.body.length;
            this.credit = Math.ceil(this.total / 160);
        }


    }l̥
  
    selection(event): void {
        this.emailbtnHidden = false;
        this.smsnotifybtnhidden = false;
        let selectElement = event.target;
        var optionIndex = selectElement.selectedIndex;
        var optionText = selectElement.options[optionIndex].text;
        this.activityLog.body = '';
        this.activityLog.emailTemplateId = 0;
        this.activityLog.customeTagsId = 0;
        this.activityLog.subject = '';
        this.activityName = optionText;
        this.actionNote = optionText;
        this.activityLog.activityId = event.target.value;        
        this.total = 0;
        this.credit = 0;
        if (this.emailfromlist.length == 0 || this.emailto == '') {
            this.emailbtnHidden = true;
        }

        if (optionText.toLocaleLowerCase() == 'sms' && this.item.leads.mobileNumber == "") {
            this.smsnotifybtnhidden = true;
        }
        
    }
    ////// Sms ////
    smseditorLoaded() {
        this.spinner.show();
        this.total = 0;
        this.credit = 0;
        this.activityLog.body = "";
        if ((this.activityLog.smsTemplateId != 0 && this.activityLog.smsTemplateId !== null && this.activityLog.smsTemplateId !== undefined)) {
            this._commonLookupServiceProxy.getSmsTemplateForEditForSms(this.activityLog.smsTemplateId).subscribe(result => {
                this.smsData = result.smsTemplate.text;
                //$('#SMSBody').attr('disabled', 'disabled');
                //document.getElementById('SMSBody').attr('disabled', 'disabled');
                this.spinner.hide();
                this.activityLog.smsConfig_TempId = result.smsTemplate.providerTemp_Id;
                if (this.smsData != "") {
                    this.setsmsHTML(this.smsData)
                }
            }, error => { this.spinnerService.hide() });
        }
        else {
            this.spinner.hide();
        }
    }
    setsmsHTML(smsHTML) {
        let htmlTemplate = this.getsmsTemplate(smsHTML);
        this.activityLog.body = htmlTemplate;
        this.countCharcters();
    }
    editorLoaded() {
        this.spinner.show();
        this.activityLog.body = "";
        if ((this.activityLog.emailTemplateId != 0 && this.activityLog.emailTemplateId !== null && this.activityLog.emailTemplateId !== undefined)) {
            this._commonLookupServiceProxy.getEmailTemplateForEditForEmail(this.activityLog.emailTemplateId).subscribe(result => {
                this.activityLog.subject = result.emailTemplates.subject;
                this.emailData = result.emailTemplates.body;
                this.spinner.hide();
                if (this.emailData != "") {
                    this.emailData = this.getEmailTemplate(this.emailData);
                    this.emailEditor.tools = null
                    this.emailEditor.editor.loadDesign(JSON.parse(this.emailData));
                    // this.emailEditor.editor.exportHtml((data) =>
                    //     this.setHTML(data.html)
                    // );
                }
            }, error => { this.spinnerService.hide() });
        }
        else {
            this.spinner.hide();
        }
    }
    getsmsTemplate(smsHTML) {
        let myTemplateStr = smsHTML;
        let addressValue = this.item.leads.address //+ " " + this.item.lead.suburb + ", " + this.item.lead.state + "-" + this.item.lead.postCode;
        let myVariables = {
            Customer: {
                Name: this.item.leads.firstName,
                Address: addressValue,
                Mobile: this.item.leads.mobileNumber,
                Email: this.item.leads.emailId,
                Phone: this.item.leads.alt_Phone,
                SalesRep: this.item.currentAssignUserName
            },
            Sales: {
                Name: this.item.currentAssignUserName,
                Mobile: this.item.currentAssignUserMobile,
                Email: this.item.currentAssignUserEmail
            },
            Quote: {
                ProjectNo: this.item.jobNumber,
                SystemCapacity: this.item.systemCapacity != null ? this.item.systemCapacity + " " + 'KW' : this.item.systemCapacity,
                AllproductItem: this.item.qunityAndModelList,
                TotalQuoteprice: this.item.totalQuotaion,
                InstallationDate: this.item.installationDate,
                InstallerName: this.item.installerName,
            },
            Freebies: {
                DispatchedDate: this.item.dispatchedDate,
                TrackingNo: this.item.trackingNo,
                TransportCompany: this.item.transportCompanyName,
                TransportLink: this.item.transportLink,
                PromoType: this.item.freebiesPromoType,
            },
            Invoice: {
                UserName: this.item.userName,
                UserMobile: this.item.userPhone,
                UserEmail: this.item.userEmail,
                Owning: this.item.owning,
            },
            Organization: {
                orgName: this.item.orgName,
                orgEmail: this.item.orgEmail,
                orgMobile: this.item.orgMobile,
                orglogo: AppConsts.docUrl + "/" + this.item.orgLogo,
            },
            Review: {
                link: this.item.reviewlink,
            },
        }

        // use custom delimiter {{ }}
        _.templateSettings.interpolate = /{{([\s\S]+?)}}/g;

        // interpolate
        let compiled = _.template(myTemplateStr);
        let myTemplateCompiled = compiled(myVariables);
        return myTemplateCompiled;
    }
    getEmailTemplate(emailHTML) {
        //let myTemplateStr = "Hello {{user.name}}, you are {{user.age.years}} years old";
        let myTemplateStr = emailHTML;
        let addressValue = this.item.leads.address //+ " " + this.item.leads.suburb + ", " + this.item.lead.state + "-" + this.item.lead.postCode;
        let myVariables = {
            Customer: {
                Name: this.item.leads.firstName,
                Address: addressValue,
                Mobile: this.item.leads.mobileNumber,
                Email: this.item.leads.emailId,
                Phone: this.item.leads.alt_Phone,
                SalesRep: this.item.currentAssignUserName
            },
            Sales: {
                Name: this.item.currentAssignUserName,
                Mobile: this.item.currentAssignUserMobile,
                Email: this.item.currentAssignUserEmail
            },
            Quote: {
                ProjectNo: this.item.jobNumber,
                SystemCapacity: this.item.systemCapacity != null ? this.item.systemCapacity + " " + 'KW' : this.item.systemCapacity,
                AllproductItem: this.item.qunityAndModelList,
                TotalQuoteprice: this.item.totalQuotaion,
                InstallationDate: this.item.installationDate,
                InstallerName: this.item.installerName,
            },
            Freebies: {
                DispatchedDate: this.item.dispatchedDate,
                TrackingNo: this.item.trackingNo,
                TransportCompany: this.item.transportCompanyName,
                TransportLink: this.item.transportLink,
                PromoType: this.item.freebiesPromoType,
            },
            Invoice: {
                UserName: this.item.userName,
                UserMobile: this.item.userPhone,
                UserEmail: this.item.userEmail,
                Owning: this.item.owning,
            },
            Organization: {
                orgName: this.item.orgName,
                orgEmail: this.item.orgEmail,
                orgMobile: this.item.orgMobile,
                orglogo: AppConsts.docUrl + "/" + this.item.orgLogo,
            },
            Review: {
                link: this.item.reviewlink,
            },
        }

        // use custom delimiter {{ }}
        _.templateSettings.interpolate = /{{([\s\S]+?)}}/g;

        // interpolate
        let compiled = _.template(myTemplateStr);
        let myTemplateCompiled = compiled(myVariables);
        return myTemplateCompiled;
    }
    
    onTagChange(event): void {

        const id = event.target.value;
        if (id == 1) {
            if (this.activityLog.body != null) {
                this.activityLog.body = this.activityLog.body + " " + "{{Customer.Name}}";
            } else {
                this.activityLog.body = "{{Customer.Name}}";
            }

        } else if (id == 2) {
            if (this.activityLog.body != null) {
                this.activityLog.body = this.activityLog.body + " " + "{{Customer.Mobile}}";
            } else {
                this.activityLog.body = "{{Customer.Mobile}}";
            }
        } else if (id == 3) {
            if (this.activityLog.body != null) {
                this.activityLog.body = this.activityLog.body + " " + "{{Customer.Phone}}";
            } else {
                this.activityLog.body = "{{Customer.Phone}}";
            }
        } else if (id == 4) {
            if (this.activityLog.body != null) {
                this.activityLog.body = this.activityLog.body + " " + "{{Customer.Email}}";
            } else {
                this.activityLog.body = "{{Customer.Email}}";
            }
        } else if (id == 5) {

            if (this.activityLog.body != null) {
                this.activityLog.body = this.activityLog.body + " " + "{{Customer.Address}}";
            } else {
                this.activityLog.body = "{{Customer.Address}}";
            }
        } else if (id == 6) {
            if (this.activityLog.body != null) {
                this.activityLog.body = this.activityLog.body + " " + "{{Sales.Name}}";
            } else {
                this.activityLog.body = "{{Sales.Name}}";
            }
        } else if (id == 7) {
            if (this.activityLog.body != null) {
                this.activityLog.body = this.activityLog.body + " " + "{{Sales.Mobile}}";
            } else {
                this.activityLog.body = "{{Sales.Mobile}}";
            }
        } else if (id == 8) {
            if (this.activityLog.body != null) {
                this.activityLog.body = this.activityLog.body + " " + "{{Sales.Email}}";
            } else {
                this.activityLog.body = "{{Sales.Email}}";
            }
        }
        else if (id == 9) {
            if (this.activityLog.body != null) {
                this.activityLog.body = this.activityLog.body + " " + "{{Quote.ProjectNo}}";
            } else {
                this.activityLog.body = "{{Quote.ProjectNo}}";
            }
        }
        else if (id == 10) {
            if (this.activityLog.body != null) {
                this.activityLog.body = this.activityLog.body + " " + "{{Quote.SystemCapacity}}";
            } else {
                this.activityLog.body = "{{Quote.SystemCapacity}}";
            }
        }
        else if (id == 11) {
            if (this.activityLog.body != null) {
                this.activityLog.body = this.activityLog.body + " " + "{{Quote.AllproductItem}}";
            } else {
                this.activityLog.body = "{{Quote.AllproductItem}}";
            }
        }
        else if (id == 12) {
            if (this.activityLog.body != null) {
                this.activityLog.body = this.activityLog.body + " " + "{{Quote.TotalQuoteprice}}";
            } else {
                this.activityLog.body = "{{Quote.TotalQuoteprice}}";
            }
        }
        else if (id == 13) {
            if (this.activityLog.body != null) {
                this.activityLog.body = this.activityLog.body + " " + "{{Quote.InstallationDate}}";
            } else {
                this.activityLog.body = "{{Quote.InstallationDate}}";
            }
        }
        else if (id == 14) {
            if (this.activityLog.body != null) {
                this.activityLog.body = this.activityLog.body + " " + "{{Quote.InstallerName}}";
            } else {
                this.activityLog.body = "{{Quote.InstallerName}}";
            }
        }
        else if (id == 15) {
            if (this.activityLog.body != null) {
                this.activityLog.body = this.activityLog.body + " " + "{{Freebies.TransportCompany}}";
            } else {
                this.activityLog.body = "{{Freebies.TransportCompany}}";
            }
        }
        else if (id == 16) {
            if (this.activityLog.body != null) {
                this.activityLog.body = this.activityLog.body + " " + "{{Freebies.TransportLink}}";
            } else {
                this.activityLog.body = "{{Freebies.TransportLink}}";
            }
        }
        else if (id == 17) {
            if (this.activityLog.body != null) {
                this.activityLog.body = this.activityLog.body + " " + "{{Freebies.DispatchedDate}}";
            } else {
                this.activityLog.body = "{{Freebies.DispatchedDate}}";
            }
        }
        else if (id == 18) {
            if (this.activityLog.body != null) {
                this.activityLog.body = this.activityLog.body + " " + "{{Freebies.TrackingNo}}";
            } else {
                this.activityLog.body = "{{Freebies.TrackingNo}}";
            }
        }
        else if (id == 19) {
            if (this.activityLog.body != null) {
                this.activityLog.body = this.activityLog.body + " " + "{{Freebies.PromoType}}";
            } else {
                this.activityLog.body = "{{Freebies.PromoType}}";
            }
        }
        else if (id == 20) {
            if (this.activityLog.body != null) {
                this.activityLog.body = this.activityLog.body + " " + "{{Invoice.UserName}}";
            } else {
                this.activityLog.body = "{{Invoice.UserName}}";
            }
        }
        else if (id == 21) {
            if (this.activityLog.body != null) {
                this.activityLog.body = this.activityLog.body + " " + "{{Invoice.UserMobile}}";
            } else {
                this.activityLog.body = "{{Invoice.UserMobile}}";
            }
        }
        else if (id == 22) {
            if (this.activityLog.body != null) {
                this.activityLog.body = this.activityLog.body + " " + "{{Invoice.UserEmail}}";
            } else {
                this.activityLog.body = "{{Invoice.UserEmail}}";
            }
        }
        else if (id == 23) {
            if (this.activityLog.body != null) {
                this.activityLog.body = this.activityLog.body + " " + "{{Invoice.Owning}}";
            } else {
                this.activityLog.body = "{{Invoice.Owning}}";
            }
        }
        else if (id == 24) {
            if (this.activityLog.body != null) {
                this.activityLog.body = this.activityLog.body + " " + "{{Organization.orgName}}";
            } else {
                this.activityLog.body = "{{Organization.orgName}}";
            }
        }
        else if (id == 25) {
            if (this.activityLog.body != null) {
                this.activityLog.body = this.activityLog.body + " " + "{{Organization.orgEmail}}";
            } else {
                this.activityLog.body = "{{Organization.orgEmail}}";
            }
        }
        else if (id == 26) {
            if (this.activityLog.body != null) {
                this.activityLog.body = this.activityLog.body + " " + "{{Organization.orgMobile}}";
            } else {
                this.activityLog.body = "{{Organization.orgMobile}}";
            }
        }
        else if (id == 27) {
            if (this.activityLog.body != null) {
                this.activityLog.body = this.activityLog.body + " " + "{{Organization.orglogo}}";
            } else {
                this.activityLog.body = "{{Organization.orglogo}}";
            }
        }
        else if (id == 28) {
            if (this.activityLog.body != null) {
                this.activityLog.body = this.activityLog.body + " " + "{{Review.link}}";
            } else {
                this.activityLog.body = "{{Review.link}}";
            }
        }
    }
    saveDesign() {
        if (this.activityLog.emailTemplateId == 0) {
            this.saving = true;
            const emailHTML = this.activityLog.body;
            // if (emailHTML != "") {
            //     this.emailEditor.editor.loadDesign(JSON.parse(emailHTML));
            //     this.emailEditor.editor.exportHtml((data) =>
            this.setHTML(emailHTML)
            // );
        }
        // let htmlTemplate = this.getEmailTemplate(emailHTML);
        // this.activityLog.body = htmlTemplate;
        // this.save();
        // }
        else {
            this.saving = true;
            this.emailEditor.editor.exportHtml((data) =>
                this.setHTML(data.html)
            );
        }
    }

    save(): void {        
        try {
            if (this.activityName.toLocaleLowerCase() == 'reminder') {
                if (this.myNameElem.nativeElement.value != '') {
                    let reminderdate = moment(this.myNameElem.nativeElement.value);
                    let date = moment(this.CurrentDate)
                    if (reminderdate > date) {
                        this.activityLog.activityDate = this.myNameElem.nativeElement.value; //reminderdate;
                        this.activityLog.body = this.myNameElem.nativeElement.value.toString();
                    } else {
                        this.notify.warn(this.l('Please Select Greater DateTime Than Current Date'));
                        return;
                    }
                }
                else {
                    this.notify.warn(this.l('Please Select Date'));
                    return;
                }
            }
            if (this.activityName.toLocaleLowerCase() == 'todo') {
                this.activityLog.todopriorityId = this.priorityType;
                if (this.priorityType == 1) { this.activityLog.todopriority = "Low"; }
                if (this.priorityType == 2) { this.activityLog.todopriority = "Heigh"; }
                if (this.priorityType == 3) { this.activityLog.todopriority = "Medium"; }
    
                if (this.activityLog.referanceId != 0) {
                    this.activityLog.userId = this.activityLog.referanceId;
                }
                else {
                    this.notify.warn(this.l('Please Select User'));
                    return;
                }
            }
            this.activityLog.actionNote = this.actionNote;
            this.activityLog.mobile = this.item.leads.mobileNumber;
            this.activityLog.emailId = this.item.leads.emailId;
            this.activityLog.leadId = this.item.leads.id;
            this.saving = true;
            if (this.pageid == 0) {
                if (this.activityName.toLocaleLowerCase() == 'sms') {
                    if (this.activityLog.smsTemplateId == 0) {
                        this.notify.warn(this.l('Plz Select at Least One Template'));
                        this.saving = false;
                        return;
                    }
                    if (this.total > 320) {
                        this.notify.warn(this.l('You Can Not Add more than 320 characters'));
                        this.saving = false;
                    } else {
                        this.activityLog.actionId = AppConsts.leadAction.SMSSent;
                        this.savedata();
                    }
                }
                else if (this.activityName.toLocaleLowerCase() == 'email') {
                    this.activityLog.actionId = AppConsts.leadAction.EmailSent;
                    this.savedata();
                }
                else if (this.activityName.toLocaleLowerCase() == 'reminder') {
                    this.activityLog.actionId = AppConsts.leadAction.ReminderSent;
                    this.savedata();
                }
                else if (this.activityName.toLocaleLowerCase() == 'comment') {
                    this.activityLog.actionId = AppConsts.leadAction.CommentAdded;
                    this.savedata();
                }
                else if (this.activityName.toLocaleLowerCase() == 'notify') {
                    this.activityLog.actionId = AppConsts.leadAction.Notify;
                    this.savedata();
                }
                else if (this.activityName.toLocaleLowerCase() == 'todo') {
                    this.activityLog.actionId = AppConsts.leadAction.ToDo;
                    this.savedata();
                }
    
            } 
        } catch (error) {
            this.spinnerService.hide();
        }  
    }
    savedata() {        
        this._leadactivityLogServiceProxy.addActivityLog(this.activityLog)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                this.modal.hide();                
                this.viewLeadDetail.reloadLead.emit(false);               
                this.modalSave.emit(false);
                this.notify.info(this.l('SavedSuccessfully'));
                this.activityType = 6;
                this.activityName = "";
                this.actionNote = "";
                this.activityLog.body = "";
                this.activityLog.activityNote = "";
            }, error => { this.spinnerService.hide() });
    }

}