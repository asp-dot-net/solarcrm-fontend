import { Component, EventEmitter, Injector, Input, OnInit, Output, ViewChild } from '@angular/core';
import {
    LeadsServiceProxy, LeadsActivityLogServiceProxy, CommonLookupDto, GetLeadsActivityLogForViewDto
} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ActivatedRoute, Router } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import PlaceResult = google.maps.places.PlaceResult;
import { AddActivityModalComponent } from './add-activity-model.component';
//import { JobsComponent } from '@app/main/jobs/jobs/jobs.component';
import { ActivityLogHistoryComponent } from './activity-log-history.component';
//import { PromotionStopResponceComponent } from '@app/main/promotions/promotionUsers/stop-responce.component';
import { NgxSpinnerService } from 'ngx-spinner';
import * as moment from 'moment';
import { AppConsts } from '@shared/AppConsts';

@Component({
    selector: 'activityLog',
    templateUrl: './activity-log-leads.component.html',
    animations: [appModuleAnimation()]
})
export class ActivityLogLeadsComponent extends AppComponentBase implements OnInit {

    @ViewChild('addActivityModal', { static: true }) addActivityModal: AddActivityModalComponent;
    @ViewChild('activityloghistory', { static: true }) activityloghistory: ActivityLogHistoryComponent;

    active = false;
    saving = false;
    //item: GetLeadForViewDto;
    activeTabIndex: number = 0;


    activity: boolean = false;
    leadActivityList: GetLeadsActivityLogForViewDto[];
    leadActionList: CommonLookupDto[];
    leadId: number = 0;
    id: number = 0;
    role: string = '';
    actionId: number = 0;
    sectionId: number = 0;
    showsectionwise = false;
    currentactivity: boolean = false;
    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
        private _leadsServiceProxy: LeadsServiceProxy,
        private _leadsActivityLogServiceProxy: LeadsActivityLogServiceProxy,
        private _router: Router,
        private spinner: NgxSpinnerService
    ) {
        super(injector);
        //this.item = new GetLeadForViewDto();
        //this.item.lead = new LeadDto();
    }
    ngOnInit(): void {
        //
    }

    showLeadActivity(leadId?: number, param?: number, sectionId?: number): void {
        this.sectionId = sectionId;
        this.activity = false;
        //if (this.sectionId == 0 || this.sectionId < 37) {
            this.showsectionwise = true;
        //}
        if (param != null) {
            this.id = param;
        }
        this.leadId = leadId;
        if (this.sectionId == 23 || this.sectionId == 24 || this.sectionId == 25) {
            this.currentactivity = true;
        }
        this.getLeadActivityLog(leadId, this.actionId);
    }
    ChangeLeadActivityLog(event) {
        this.getLeadActivityLog(this.leadId, event.target.value);
    }
    myActivity(leadId: number) {

        this.getLeadActivityLog(this.leadId, this.actionId);
    }
    getLeadActivityLog(leadId, actionId) {
        this._leadsActivityLogServiceProxy.getAllActivityLog(leadId, actionId, this.sectionId, 0, this.currentactivity, this.activity, 0).subscribe(result => {
            this.leadActivityList = result;
        }, error => { this.spinnerService.hide() });
    }

    addActivitySuccess() {
        this.showDetail(this.leadId);
    }

    showDetail(leadId: number): void {
        let that = this;

        // this._leadsServiceProxy.getLeadActivityLog(leadId,0,0,false,false, 0).subscribe(result => {
        //     let lastdatetime !: moment.Moment;
        //     this.activityLog = result;
        //     let obj = {
        //     activityDate:"",
        //     DatewiseActivity:[]
        //     }
        //     this.activityDatabyDate = [];
        //     this.activityLog.forEach(function(log){
        //         //this.getParsedDate(log.creationTime)
        //         if(that.getParsedDate(log.creationTime) == that.getParsedDate(lastdatetime)){
        //             log.logDate = "";
        //             let item1 = this.activityDatabyDate.find(activityDate => activityDate.activityDate === that.getParsedDate(lastdatetime));
        //             item1.DatewiseActivity.push({actionNote:log.actionNote});
        //         }
        //         else{
        //             log.logDate = that.getParsedDate(log.creationTime);
        //             obj.activityDate=log.logDate;
        //             obj.DatewiseActivity.push(log.actionNote);
        //             this.activityDatabyDate.push({activityDate:log.logDate,
        //             DatewiseActivity:[{actionNote:log.actionNote}]});
        //         }
        //         lastdatetime = log.creationTime;
        //     }.bind(that));
    }
    downfile(filename): void {
        let FileName = AppConsts.docUrl + filename;//this.LeadDocument.filePath;
        window.open(FileName, "_blank");
    };
}