import { Component, ElementRef, EventEmitter, Injector, Output, ViewChild } from '@angular/core';
import { AppConsts } from '@shared/AppConsts';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { LeadsServiceProxy, CommonLookupServiceProxy, GetLeadsForViewDto, LeadsDto, ActivityLogInput, GetLeadForChangeStatusOutput, CommonLookupDto } from '@shared/service-proxies/service-proxies';
import * as _ from 'lodash';
import * as moment from 'moment';
import { ModalDirective } from 'ngx-bootstrap/modal';

@Component({
    selector: 'rejectLeadModel',
    templateUrl: './reject-lead-modal.component.html',
    animations: [appModuleAnimation()]
})
export class RejectLeadModelComponent extends AppComponentBase {

    @ViewChild('addModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    RejectReason: string;
    RejectReasonId: number = 0;
    LeadId: number;
    active = false;
    saving = false;
    allRejectReasons: CommonLookupDto[];
    sectionId: number = AppConsts.SectionPage.RejectLeads;  
    constructor(
        injector: Injector,
        private _leadsServiceProxy: LeadsServiceProxy,
        private _commonLookupServiceProxy: CommonLookupServiceProxy
    ) {
        super(injector);
    }

    show(LeadId?: number): void {
        this.LeadId = LeadId;
        this.modal.show();
        this._commonLookupServiceProxy.getAllRejectReasonDropdown().subscribe(result => {
            this.allRejectReasons = result;
        }, error => { this.spinnerService.hide() });
    }

    close(): void {
        this.active = false;
        this.RejectReason = '';
        this.modal.hide();
    }

    save(): void {
        if (this.RejectReasonId == 0) {
            this.notify.warn(this.l('PleaseSelectRejectReason'));
            return;
        }
        let status: GetLeadForChangeStatusOutput = new GetLeadForChangeStatusOutput();
        status.id = this.LeadId;
        status.leadStatusID = 15;
        status.rejectReason = this.RejectReason;
        status.rejectReasonId = this.RejectReasonId;

        this._leadsServiceProxy.changeStatus(status)
            .subscribe(() => {
                this.notify.success(this.l('LeadRejectedSuccessfully'));
                this.RejectReason = '';
                this.modal.hide();
                this.modalSave.emit(false);
                //document.getElementById("setcancelRejectClick").click();
            }, error => { this.spinnerService.hide() });
    }
}