import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { LeadsServiceProxy, GetDuplicateLeadPopupDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';

@Component({
    selector: 'viewDuplicateModal',
    templateUrl: './view-duplicate-lead.component.html',
    animations: [appModuleAnimation()]
})
export class ViewDuplicateModalComponent extends AppComponentBase {
    @ViewChild('duplicateModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    saving = false;
    item: GetDuplicateLeadPopupDto[];
    DupCount:number = 0;

    constructor(
        injector: Injector,
        private _leadsServiceProxy:LeadsServiceProxy
    ) {
        super(injector);
    }
    show(leadId: number , organizationId: number): void {
        this._leadsServiceProxy.getDuplicateLeadForView(leadId,organizationId).subscribe(result => {    
            this.item = result;
            this.DupCount = this.item.length;
            this.modal.show();
        }, error => { this.spinnerService.hide() });
    }

    close(): void {
        this.modal.hide();
    }
}