import { Component, ViewChild, Injector, Optional, Input, Output, EventEmitter, OnInit, ElementRef, HostListener } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { LeadsServiceProxy, GetLeadsForViewDto, LeadsDto, GetLeadForChangeStatusOutput, CommonLookupDto, CommonLookupServiceProxy } from '@shared/service-proxies/service-proxies'; //GetLeadForViewDto, LeadDto, GetActivityLogViewDto, GetLeadForChangeStatusOutput 
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AddActivityModalComponent } from '../leadactivity/add-activity-model.component';
import PlaceResult = google.maps.places.PlaceResult;
//import { Appearance, GermanAddress, Location } from '@angular-material-extensions/google-maps-autocomplete';
//import { LazyLoadEvent } from 'primeng/public_api';
//import { ActivityLogMyLeadComponent } from './activity-log-mylead.component';
//import { CreateEditLeadComponent } from '@app/main/leads/leads/create-edit-lead.component';
//import { PromotionStopResponceComponent } from '@app/main/promotions/promotionUsers/stop-responce.component';
import { NgxSpinnerService } from 'ngx-spinner';
import { ActivityLogLeadsComponent } from '../leadactivity/activity-log-leads.component';
import { createOrEditManageLeadComponent } from './create-or-edit-manageleads.component';
import { LeadDocumentsUploadComponent } from '../leadDocuments/leadDocumetsUpload.componet';
import { emit } from 'process';
import { JobDetailsComponent } from '@app/main/jobs/jobDetails.componet';
import { DataTransferService } from '@app/shared/common/projectservice/data-transfer.service';
import { CustomerleadsComponent } from './customerleads.component';
import { Paginator } from 'primeng/paginator';
import { AppConsts } from '@shared/AppConsts';

@Component({
    selector: 'viewLeadDetail',
    templateUrl: './view-mylead.component.html',
    styleUrls: ['./view-mylead.component.css'],
    animations: [appModuleAnimation()]
})
export class ViewMyLeadComponent extends AppComponentBase implements OnInit {
    @Input() SelectedLeadId: number = 0;
    @Input() SectionName: string = "";
    @Input() mainorganizationID: number = 0;
    @Output() reloadLead = new EventEmitter<boolean>();
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    @ViewChild('activityLog', { static: true }) activityLogLead: ActivityLogLeadsComponent;
    @ViewChild('leadDocumetsUpload', { static: true }) leadDocumetsUpload: LeadDocumentsUploadComponent;
    @ViewChild('addActivityModal', { static: true }) addActivityModal: AddActivityModalComponent;
    @ViewChild('createOrEditManageLead', { static: true }) createOrEditManageLead: createOrEditManageLeadComponent;
    @ViewChild('jobDetails', { static: true }) jobDetails: JobDetailsComponent;
    //@ViewChild('VerifiedTabShowHide',{static:true}) VerifiedTabShowHide:ElementRef;
    allapplicationStatus: CommonLookupDto[];
    item: GetLeadsForViewDto;
    active = false;
    saving = false;
    lat: number;
    lng: number;
    zoom: number = 40;
    activityLog: []; //GetActivityLogViewDto[];
    activeTabIndex: number = 0;
    activityDatabyDate: any[] = [];
    leadData: LeadsDto;
    showsectionwise = false;
    sectionId: number = AppConsts.SectionPage.ManageActivity;  
    //applicationStatusData:any;
    applicationstatusName: String = '';
    applicationstatusColor: String = '';
    IsVerified: boolean = false;
    addEditLeadModelpopup: boolean = false;
    public screenWidth: any;
    public screenHeight: any;
    testHeight = 230;
    contentHeight = 340;
    @HostListener('window:resize', ['$event'])
    onResize(event) {
        this.screenHeight = window.innerHeight;
    }

    constructor(
        injector: Injector,
        private _leadsServiceProxy: LeadsServiceProxy,
        private _commonLookupService: CommonLookupServiceProxy,
        private spinner: NgxSpinnerService,
        private _dataTransfer: DataTransferService,
        @Optional() private _customerLeadList?: CustomerleadsComponent,
    ) {
        super(injector);
        this.item = new GetLeadsForViewDto();
        this.item.leads = new LeadsDto();
    }

    ngOnInit(): void {
        this.screenHeight = window.innerHeight;
        //this.VerifiedTabShowHide.nativeElement.style.display = 'none';
        if (this.SelectedLeadId > 0)
            this.showDetail(this.SelectedLeadId);

    }
    LeadFlag: string = '';
    //leadId:number = 0;
    createManageLead(id = 0): void {    
            
        this.createOrEditManageLead.show(this.mainorganizationID, this.SelectedLeadId,true,1,this.LeadFlag = 'C');
    }

    showDetail(leaddata: any, applicationStatusData?: string, LeadName?: string, sectionId?: number): void {
        // this.applicationStatusData = applicationStatusData;
        //let that = this;    
        this.mainorganizationID = parseInt(this._dataTransfer.getOrgData());
        //this.leadData = this._dataTransfer.getLeadData();

        if (sectionId == 15) {
            // this.activeTabIndexForPromotion = 0;
            this.activeTabIndex = 1;
        } else {
            this.activeTabIndex = 0;
            // this.activeTabIndexForPromotion = 1;
        }
        if (sectionId == 30) {
            //this.mainsearch = true;
        }

        this.sectionId = sectionId;
        // this.sectionId < 12
        if (this.sectionId == 0 || this.sectionId < 37) {
            this.showsectionwise = true;
        }
        this.spinner.show();
        this.applicationstatusName = "";
        this.applicationstatusColor = "";
        this.IsVerified = leaddata.isVerified;

        if (applicationStatusData != null) {
            this.applicationstatusName = applicationStatusData['applicationStatusName'];
            this.applicationstatusColor = applicationStatusData['applicationStatusLabelColor']
        }

        this._commonLookupService.getAllApplicationStatusForDropdown().subscribe(result => {
            if (result.length > 0) {
                result.forEach(obj => {
                    if (applicationStatusData != null) {
                        if (obj.id <= applicationStatusData['id']) {
                            obj.value = 1;
                        }
                    }
                });
                this.allapplicationStatus = result;
            }
        }, error => { this.spinnerService.hide() });

        this._leadsServiceProxy.getLeadsForView(leaddata.id).subscribe(result => {
            this.item = result;
            this.spinner.hide();
            this.lng = result.longitude;
            this.lat = result.latitude;

            //setTimeout(() => {                           // <<<---using ()=> syntax
            //Activity Log List
            this.activityLogLead.showLeadActivity(this.item.leads.id, 0, this.sectionId);
            //Lead Document List
            this.leadDocumetsUpload.showLeadDocument(this.item.leads.id, 0, this.sectionId);
            this.jobDetails.jobNumber = '';
            if (this.IsVerified == true) {
                //Lead Job List           
                setTimeout(() => {                           // <<<---using ()=> syntax
                    this.jobDetails.showJobDetails(this.item.leads.id, 0, this.sectionId, this.IsVerified);
                }, 1500);
            }
        }, error => { this.spinnerService.hide() });
    }
    LeadChangeStatus(lead: LeadsDto, leadstatusid: number): void {
        let status: GetLeadForChangeStatusOutput = new GetLeadForChangeStatusOutput();
        status.id = lead.id;
        status.leadStatusID = leadstatusid; // 4;
        this.message.confirm('',
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {

                    this._leadsServiceProxy.changeStatus(status)
                        .subscribe(() => {
                            this.notify.success(this.l('LeadStatusChangedToWarm'));
                            this.showDetail(lead.id);
                        }, error => { this.spinnerService.hide() });
                }
            }
        );
    }

    cancel(lead: LeadsDto, leadstatusid: number): void {
        abp.event.trigger('app.show.cancelLeadModal', lead.id);
        //this.showDetail(this.leadData);
    }
    reject(lead: LeadsDto, leadstatusid: number): void {
        abp.event.trigger('app.show.rejectLeadModal', lead.id);
        // this.showDetail(this.leadData);
    }
   
    deleteLead(lead: LeadsDto): void {
        this.message.confirm(
            '',
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this.spinnerService.show();
                    this._leadsServiceProxy.deleteCustomer(lead.id)
                        .subscribe(() => {
                            this.notify.success(this.l('SuccessfullyDeleted'));
                            //this._customerLeadList.reloadPage(true);
                            this.spinnerService.hide();
                            this.reloadLead.emit(true);
                        }, error => { this.spinnerService.hide(); });
                }
            }
        );
    }
}