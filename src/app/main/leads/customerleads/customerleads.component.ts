
import { OnInit, Component, Injector, ViewEncapsulation, ViewChild, Input, Output, EventEmitter, ElementRef, HostListener } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { ActivatedRoute, Router } from '@angular/router';
import { NotifyService } from 'abp-ng2-module';
import { TokenAuthServiceProxy } from '@shared/service-proxies/service-proxies';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import { finalize } from 'rxjs/operators';
import { FileUpload } from 'primeng/fileupload';
import { Title } from '@angular/platform-browser';
import { LazyLoadEvent } from 'primeng/api';
import { LeadsServiceProxy, LeadsDto, CommonLookupServiceProxy, CommonLookupDto, OrganizationUnitDto, GetLeadForAssignOrTransferOutput, UserServiceProxy } from '@shared/service-proxies/service-proxies';
import { DatePipe } from '@angular/common';
import { createOrEditManageLeadComponent } from './create-or-edit-manageleads.component';
import { DataTransferService } from '@app/shared/common/projectservice/data-transfer.service';
import { ViewMyLeadComponent } from './view-mylead.component';
import { DateTime } from 'luxon';
import { DateTimeService } from '@app/shared/common/timing/date-time.service';
import { AppConsts } from '@shared/AppConsts';
@Component({
  selector: 'app-customerleads',
  templateUrl: './customerleads.component.html',
  styleUrls: ['./customerleads.component.css'],
  encapsulation: ViewEncapsulation.None,
  animations: [appModuleAnimation()],
  providers: [DatePipe]
})
export class CustomerleadsComponent extends AppComponentBase implements OnInit {

  @ViewChild('createOrEditManageLead', { static: true }) createOrEditManageLead: createOrEditManageLeadComponent;
  @ViewChild('dataTable', { static: true }) dataTable: Table;
  @ViewChild('paginator', { static: true }) paginator: Paginator;
  @ViewChild('viewLeadDetail', { static: true }) viewLeadDetail: ViewMyLeadComponent;
  @ViewChild('SampleDatePicker', { static: true }) sampleDatePicker: ElementRef;
  @Input() SelectedLeadId: number = 0;
  @Output() reloadLead = new EventEmitter();
  advancedFiltersAreShown = false;
  FiltersData = false;
  filterText = '';
  firstNameFilter = '';
  emailFilter = '';
  phoneFilter = '';
  mobileFilter = '';
  addressFilter = '';
  stateNameFilter = '';
  postCodeFilter = '';
  channelPartnerFilter = '';
  leadTypeNameFilter = '';
  postCodePostalCode2Filter = '';
  ManagerFilter: number = 0;
  SalesRepFilter: number = 0;
  leadStatusName = '';
  leadStatusId = 0;
  organizationUnit = 0;
  teamId = 0;
  salesManagerId = 0;
  salesRepId = 0;
  leadSourceNameFilter = '';
  leadSourceIdFilter = [];
  leadApplicationStatusFilter = [];
  leadStatusIDS = [];
  solarTypeIdFilter = [];
  TenderIdFilter = [];
  DiscomIdFilter = [];
  DivisionIdFilter = [];
  SubDivisionIdFilter = [];
  shouldShow: boolean = false;
  recordCount: any;
  count = 0;
  date: Date = new Date('01/01/2022');
  StartDate: DateTime = this._dateTimeService.getStartOfDayForDate(this.date);
  EndDate: DateTime = this._dateTimeService.getDate();
  // sampleDate:Date = new Date();
  PageNo: number;
  tableRecords: any;
  ExpandedView: boolean = true;
  // flafValue:boolean = true;
  //SelectedLeadId: number = 0;

  isAssign: boolean = false;
  organizationChangeUnit = 0;
  allUsers: CommonLookupDto[];
  allUsersss: CommonLookupDto[];
  filteredManagers: CommonLookupDto[];
  filteredReps: CommonLookupDto[];
  allSalesrepUsers: CommonLookupDto[];

  onlyoncheck: boolean = false;
  isChange: boolean = false;
  isChangetrue: boolean = false;
  isChangefalse: boolean = false;
  showOrganizationUnits: boolean = false;
  assignLead: number = 0;
  assignLead1: number = 0;
  suburbSuggestions: string[];
  dateFilterType: string = 'Creation';
  changeView: boolean = true;
  role: string = '';
  projectNumberFilter = '';
  leadName: string;
  sectionId: number = AppConsts.SectionPage.ManageLead;
  jobStatusID = [];
  upgrateshow: boolean = false;
  allOrganizationUnits: OrganizationUnitDto[];
  allOrganizationUnitsList: OrganizationUnitDto[];
  IsAssignLead: boolean = true;
  saving: boolean = false;
  flag: boolean = true;
  leadId: number = null
  LeadFlag: string = '';

  leadstatuss: CommonLookupDto[];
  allStates: CommonLookupDto[];
  allapplicationStatus: CommonLookupDto[];
  allLeadSources: CommonLookupDto[];
  allSolarType: CommonLookupDto[];
  allDiscom: CommonLookupDto[];
  allDivision: CommonLookupDto[];
  allSubDivision: CommonLookupDto[];
  allTenderData: CommonLookupDto[];
  firstrowcount = 0;
  last = 0;
  organizationUnitlength: number = 0;
  currentUserId: number = 0;

  public screenWidth: any;
  public screenHeight: any;
  testHeight = 200;

  show: boolean = true;
  toggle: boolean = true;

  toggleBlock() {
    this.show = !this.show;
  };

  constructor(
    injector: Injector,
    private _leadsServiceProxy: LeadsServiceProxy,
    private _commonLookupService: CommonLookupServiceProxy,
    private _fileDownloadService: FileDownloadService,
    private titleService: Title,
    private _router: Router,
    private _dataTransfer: DataTransferService,
    private _dateTimeService: DateTimeService
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this.screenHeight = window.innerHeight;
    this.isChangefalse = true;
    this.GetFilterMasterData();
  }

  change() {
    this.toggle = !this.toggle;
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.screenHeight = window.innerHeight;
  }
  testHeightSize() {
    if (this.FiltersData == true) {
      this.testHeight = this.testHeight + 80;
    }
    else {
      this.testHeight = this.testHeight - 80;
    }
  }

  GetFilterMasterData() {
    //get current role
    this._commonLookupService.getCurrentUserRole().subscribe(result => {
      this.role = result;
      //CP Are Hide the Assign Row 
      if (this.role.toLocaleLowerCase() == 'channel partners') {
        this.IsAssignLead = false;
      }
      if (this.role == 'Sales Rep') {
        this.leadStatusId = 10;
      }
    }, error => { this.spinnerService.hide() });

    this._commonLookupService.getAllApplicationStatusForDropdown().subscribe(result => {
      this.allapplicationStatus = result;
    }, error => { this.spinnerService.hide() });
    this._commonLookupService.getAllLeadStatusDropdown().subscribe(result => {
      this.leadstatuss = result;
    }, error => { this.spinnerService.hide() });
    this._commonLookupService.getAllDiscomFilterDropdown(0).subscribe(result => {
      this.allDiscom = result;
    }, error => { this.spinnerService.hide() });
    this._commonLookupService.getAllDivisionFilterDropdown(0).subscribe(result => {
      this.allDivision = result;
    }, error => { this.spinnerService.hide() });
    this._commonLookupService.getAllSubdivisionWiseData('').subscribe(result => {
      this.allSubDivision = result;
    }, error => { this.spinnerService.hide() });
    this._commonLookupService.getAllSolarTypeForDropdown().subscribe(result => {
      this.allSolarType = result;
    }, error => { this.spinnerService.hide() });
    this._commonLookupService.getAllStateForDropdown(0).subscribe(result => {
      this.allStates = result;
    }, error => { this.spinnerService.hide() });
    this._commonLookupService.getOrganizationUnitUserWise().subscribe(output => {
      if (output != null) {
        this.allOrganizationUnits = output;
        this.organizationUnit = this.allOrganizationUnits[0].id;
        this.organizationUnitlength = this.allOrganizationUnits.length;

        //change Organization
        this.allOrganizationUnitsList = this.allOrganizationUnits.filter(item => item.id != this.organizationUnit)
        if (this.allOrganizationUnitsList == undefined) {
          this.allOrganizationUnitsList = [];
        }
        this._commonLookupService.getSalesManagerUser(this.organizationUnit).subscribe(result => {
          this.allUsers = result;
          if (this.role == 'Manager' && result != null) {
            this.salesManagerId = result[0]['value'];
            this.dateFilterType = 'Assign';
            this._commonLookupService.getSalesRepBySalesManagerid(this.salesManagerId, this.organizationUnit).subscribe(result => {
              this.allSalesrepUsers = result;
            });
          }

          //this.getCustomer();
        }, error => { this.spinnerService.hide() });
        this.changeLeadSource();
        this.onsalesmanger();
        this.onsalesrep();
        this._commonLookupService.getCurrentUserId().subscribe(result => {
          this.currentUserId = result;
          if (this.role == 'Sales Manager' ) {
            this.salesManagerId = this.currentUserId;
          }
        }, error => { this.spinnerService.hide() });
        setTimeout(() => {                           // <<<---using ()=> syntax
          this.getCustomer();
        }, 500);
      }


    }, error => { this.spinnerService.hide() });
  }
  leadstatuschange() {
    if (this.leadStatusIDS.includes(6)) {
      //this.showJobStatus = true;
    } else {
      //this.showJobStatus = false;
      //this.jobStatusID = [];
    }
  }


  getOrganizationuser() {
    if (this.isChange == false) {
      this._commonLookupService.getSalesManagerUser(this.organizationUnit).subscribe(result => {
        this.allUsers = result;
      }, error => { this.spinnerService.hide() });
    }
  }
  getOrganizationWiseUser() {
    if (this.isChange == true) {
      this._commonLookupService.getSalesManagerUser(this.organizationChangeUnit).subscribe(result => {
        this.allUsersss = result;
      }, error => { this.spinnerService.hide() });
    }
  }
  onsalesmanger() {
    this._commonLookupService.getSalesManagerForFilter(this.organizationUnit, undefined).subscribe(manager => {
      this.filteredManagers = manager;
    }, error => { this.spinnerService.hide() });
  }
  onsalesrep() {
    this._commonLookupService.getSalesRepForFilter(this.organizationUnit, undefined).subscribe(rep => {
      this.filteredReps = rep;
    }, error => { this.spinnerService.hide() });
  }
  getsalerep(event) {
    const id = event.target.value;
    if (id != 0) {
      if (this.organizationChangeUnit != 0) {
        this._commonLookupService.getSalesRepBySalesManagerid(id, this.organizationChangeUnit).subscribe(result => {
          this.allSalesrepUsers = result;
        }, error => { this.spinnerService.hide() });
      } else {
        this._commonLookupService.getSalesRepBySalesManagerid(id, this.organizationUnit).subscribe(result => {
          this.allSalesrepUsers = result;
        }, error => { this.spinnerService.hide() });
      }
    }
    this.assignLead1 = 0;
  }

  changeLeadSource() {
    this._commonLookupService.getAllLeadSourceOrganizationWise(this.organizationUnit).subscribe(result => {
      this.allLeadSources = result;
    }, error => { this.spinnerService.hide() });
  }
  chaneSolarType() {
    this.allTenderData = undefined;
    this._commonLookupService.getSolarTypeWiseTenderForDropdown(this.solarTypeIdFilter).subscribe(result => {
      this.allTenderData = result;
      if (this.allTenderData.length == 0) {
        this.allTenderData = undefined;
      }
    }, error => { this.spinnerService.hide() });
  }
  getCustomer(event?: LazyLoadEvent) {
    this._dataTransfer.setOrgData(this.organizationUnit.toString());
    this.PageNo = this.primengTableHelper.getSkipCount(this.paginator, event);
    if (this.StartDate != null && this.EndDate != null) {
      //this.StartDate = this.datepipe.transform('','');//this.datePipe.transform(this.myDate, 'yyyy-MM-dd');//this.sampleDateRange[0];
      //this.EndDate = //this.sampleDateRange[1];
    } else {
      this.StartDate = undefined;
      this.EndDate = undefined;
    }
    if (this.primengTableHelper.shouldResetPaging(event)) {
      this.paginator.changePage(0);
      //return;
      //change the current primeng-paginator file getPageCount() method
    }
    //
    this.primengTableHelper.showLoadingIndicator();

    this._leadsServiceProxy.getAllForMyLeadCustomer(
      this.filterText,
      undefined,
      this.firstNameFilter,
      this.emailFilter,
      this.phoneFilter,
      this.mobileFilter,
      this.addressFilter,
      this.stateNameFilter,
      this.postCodeFilter,
      this.leadSourceNameFilter,
      this.leadSourceIdFilter,
      this.leadStatusName,
      (this.leadStatusId == 0) ? undefined : this.leadStatusId,
      this.leadTypeNameFilter,
      this.solarTypeIdFilter,
      this.TenderIdFilter,
      this.DiscomIdFilter,
      this.DivisionIdFilter,
      this.SubDivisionIdFilter,
      0,
      this.StartDate,
      this.EndDate,
      this.organizationUnit,
      this.teamId == 0 ? undefined : this.teamId,
      this.salesManagerId == 0 ? undefined : this.salesManagerId,
      this.salesRepId == 0 ? undefined : this.salesRepId,
      this.dateFilterType,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      this.leadStatusIDS,
      this.primengTableHelper.getSorting(this.dataTable),
      this.primengTableHelper.getSkipCount(this.paginator, event),
      this.primengTableHelper.getMaxResultCount(this.paginator, event)

    ).subscribe((result) => {
      // 

      this.primengTableHelper.totalRecordsCount = result.totalCount;
      this.primengTableHelper.records = result.items;
      const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
      this.firstrowcount = totalrows + 1;
      this.last = totalrows + result.items.length;
      this.tableRecords = result.items;
      if (result.totalCount > 0) {
        // this.getLeadSummaryCount();
      } else {

      }
      this.primengTableHelper.hideLoadingIndicator();
      this.recordCount = result.totalCount;
      this.shouldShow = false;

      if (this.flag == false && result.totalCount != 0 && this.ExpandedView == false) {
        this.navigateToLeadDetail(this.Selectedleaddata)
      }
      else {
        if (this.ExpandedView == false && result.totalCount != 0 && result.items != null) {
          this.navigateToLeadDetail(result.items[0])
        }
        else {
          this.ExpandedView = true;
        }
      }
    }, error => { this.spinnerService.hide() });
  }

  clear() {
    this.filterText = '';
    this.addressFilter = '';
    this.leadStatusIDS = [];
    this.leadApplicationStatusFilter = [];
    this.solarTypeIdFilter = [];
    this.stateNameFilter = '';
    this.postCodePostalCode2Filter = '';
    this.leadSourceNameFilter = '';
    this.TenderIdFilter = [];
    this.DiscomIdFilter = [];
    this.DivisionIdFilter = [];
    this.SubDivisionIdFilter = [];
    this.leadSourceIdFilter = [];
    this.salesManagerId = 0;
    this.salesRepId = 0;
    this.dateFilterType = 'Creation';
    this.jobStatusID = [];
    this.getCustomer();
  }

  getLeadSummaryCount() {

    // this.allOrganizationUnitsList = this.allOrganizationUnits.filter(item => item.id != this.organizationUnit)
    if (this.StartDate != null && this.EndDate != null) {
      //this.StartDate = this.datepipe.transform('','');//this.datePipe.transform(this.myDate, 'yyyy-MM-dd');//this.sampleDateRange[0];
      //this.EndDate = //this.sampleDateRange[1];
    } else {
      this.StartDate = undefined;
      this.EndDate = undefined;
    }

    // this._leadsServiceProxy.getmyLeadSummaryCount(
    //   this.filterText,
    //   undefined,
    //   this.firstNameFilter,
    //   this.emailFilter,
    //   this.phoneFilter,
    //   this.mobileFilter,
    //   this.addressFilter,
    //   this.stateNameFilter,
    //   this.postCodeFilter,
    //   this.leadSourceNameFilter,
    //   this.leadSourceIdFilter,
    //   this.leadStatusName,
    //   (this.leadStatusId == 0) ? undefined : this.leadStatusId,
    //   this.leadTypeNameFilter,
    //   this.solarTypeIdFilter,
    //   this.TenderIdFilter,
    //   this.DiscomIdFilter,
    //   this.DivisionIdFilter,
    //   this.SubDivisionIdFilter,
    //   0,
    //   this.StartDate,
    //   this.EndDate,
    //   this.organizationUnit,
    //   this.teamId == 0 ? undefined : this.teamId,
    //   this.salesManagerId == 0 ? undefined : this.salesManagerId,
    //   this.salesRepId == 0 ? undefined : this.salesRepId,
    //   this.dateFilterType,
    //   undefined,
    //   undefined,
    //   undefined,
    //   undefined,
    //   undefined,
    //   undefined,
    //   undefined,
    //   undefined,
    //   this.leadStatusIDS, //this.leadStatusIDS ==  0 ? undefined : this.leadStatusIDS,
    //   undefined,
    //   undefined,
    //   undefined

    // ).subscribe(result => {
    //   ;
    //   if (result) {

    //   }
    // }, error => { this.spinnerService.hide() });
  }

  expandGrid() {
    this.ExpandedView = true;
    this.getCustomer();
  }
  Selectedleaddata: any;
  navigateToLeadDetail(leaddata): void {
    this.Selectedleaddata = '';
    this.Selectedleaddata = leaddata;
    this.ExpandedView = !this.ExpandedView;
    this.ExpandedView = false;
    this.SelectedLeadId = this.Selectedleaddata.leads.id;
    this.leadName = "myleads"
    this.viewLeadDetail.showDetail(this.Selectedleaddata.leads, this.Selectedleaddata.applicationStatusdata, this.leadName, this.sectionId);
  }

  reloadPage(flafValue: boolean): void {
    //
    this.ExpandedView = true;
    this.flag = flafValue;
    this.paginator.changePage(this.paginator.getPage());
    if (!flafValue) {
      this.navigateToLeadDetail(this.Selectedleaddata);
    }
    this.paginator.changePage(this.paginator.getPage());
  }

  deleteCustomer(customer: LeadsDto): void {
    this.message.confirm(
      this.l('CustomerDeleteWarningMessage', customer.firstName),
      this.l('AreYouSure'),
      (isConfirmed) => {
        if (isConfirmed) {
          this.spinnerService.show();
          this._leadsServiceProxy.deleteCustomer(customer.id)
            .subscribe(() => {

              this.paginator.changePage(this.paginator.getPage());
              this.notify.success(this.l('SuccessfullyDeleted'));
            }, error => { this.spinnerService.hide(); });
        }
      }
    );
  }

  exportToExcel(excelorcsv: number): void {
    this._leadsServiceProxy.getMyLeadToExcel(
      this.filterText,
      undefined,
      this.firstNameFilter,
      this.emailFilter,
      this.phoneFilter,
      this.mobileFilter,
      this.addressFilter,
      this.stateNameFilter,
      this.postCodeFilter,
      this.leadSourceNameFilter,
      this.leadSourceIdFilter,
      this.leadStatusName,

      (this.leadStatusId == 0) ? undefined : this.leadStatusId,
      this.leadTypeNameFilter,
      this.solarTypeIdFilter,
      this.TenderIdFilter,
      this.DiscomIdFilter,
      this.DivisionIdFilter,
      this.SubDivisionIdFilter,
      0,
      this.StartDate,
      this.EndDate,
      this.organizationUnit,
      this.teamId == 0 ? undefined : this.teamId,
      this.salesManagerId == 0 ? undefined : this.salesManagerId,
      this.salesRepId == 0 ? undefined : this.salesRepId,
      this.dateFilterType,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      this.leadStatusIDS, //this.leadStatusIDS ==  0 ? undefined : this.leadStatusIDS,
      excelorcsv,
    )
      .subscribe(result => {
        this._fileDownloadService.downloadTempFile(result);
      }, error => { this.spinnerService.hide() });
  }


  createManageLead(id = 0): void {

    this.createOrEditManageLead.show(this.organizationUnit, id, this.IsAssignLead, this.currentUserId, this.LeadFlag = 'C');
  }
  isAllChecked() {
    if (this.tableRecords)
      return this.tableRecords.every(_ => _.leads.isSelected);
  }
  checkAll(ev) {

    this.tableRecords.forEach(x => x.leads.isSelected = ev.target.checked);
    this.oncheckboxCheck();
    /// this.totalcount = this.tableRecords.forEach(x => x.lead.isSelected = ev.target.checked);
  }
  oncheck(event) {
    if (event.target.checked == true) {
      if (this.allOrganizationUnitsList.length > 0 || this.allOrganizationUnitsList != undefined) {
        this.isChange == true;
        this.showOrganizationUnits = true;
        this.isChangetrue = true;
        this.isChangefalse = false
        this.getOrganizationWiseUser();
      } else {
        this.isChange == false;
        this.showOrganizationUnits = false;
        this.notify.warn(this.l('NoOrganizationAvailable'));

      }
    } else {
      this.isChange == false;
      this.showOrganizationUnits = false;
      this.isChangetrue = false;
      this.isChangefalse = true;
      this.getOrganizationuser();
    }
  }

  oncheckboxCheck() {
    this.count = 0;
    this.tableRecords.forEach(item => {
      if (item.leads.isSelected == true) {
        this.count = this.count + 1;
      }
    })
  }

  submit(): void {
    let selectedids = [];
    this.saving = true;
    if (this.isChange) {
      this.primengTableHelper.records.forEach(function (leads) {
        if (leads.leads.isSelected) {
          selectedids.push(leads.leads.id);
        }
      });
      let assignleads: GetLeadForAssignOrTransferOutput = new GetLeadForAssignOrTransferOutput();
      assignleads.assignToUserID = this.assignLead;
      assignleads.leadIds = selectedids;
      assignleads.organizationID = this.organizationChangeUnit;
      if (this.organizationChangeUnit == 0) {
        this.notify.warn(this.l('Please Select Organization'));
        this.saving = false;
        return;
      }
      if (selectedids.length == 0) {
        this.notify.warn(this.l('No Leads Selected'));
        this.saving = false;
        return;
      }
      if (this.assignLead == 0) {
        this.notify.warn(this.l('Please Select User'));
        this.saving = false;
        return;
      }
      if (this.isAssign) {
        if (this.assignLead1 == 0) {
          this.notify.warn(this.l('Please Select SalesRep'));
          this.saving = false;
          return;
        }
        else {
          assignleads.assignToUserID = this.assignLead1;
        }
      }
      this._leadsServiceProxy.transferLeadToOtherOrganization(assignleads)
        .pipe(finalize(() => { this.saving = false; }))
        .subscribe(() => {
          this.reloadPage(true);
          this.isChange = false;
          this.saving = false;
          this.notify.info(this.l('AssignedSuccessfully'));
        }, error => { this.spinnerService.hide() });
    }
    else {
      this.primengTableHelper.records.forEach(function (leads) {
        if (leads.leads.isSelected) {
          selectedids.push(leads.leads.id);
        }
      });
      let assignleads: GetLeadForAssignOrTransferOutput = new GetLeadForAssignOrTransferOutput();
      assignleads.assignToUserID = this.assignLead;
      assignleads.leadIds = selectedids;
      assignleads.organizationID = this.organizationUnit;
      if (selectedids.length == 0) {
        this.notify.warn(this.l('NoLeadsSelected'));
        this.saving = false;
        return;
      }
      if (this.assignLead == 0) {
        this.notify.warn(this.l('PleaseSelectUser'));
        this.saving = false;
        return;
      }
      if (this.isAssign) {
        if (this.assignLead1 == 0) {
          this.notify.warn(this.l('PleaseSelctSalesRep'));
          this.saving = false;
          return;
        }
        else {
          assignleads.assignToUserID = this.assignLead1;
        }
      }

      this._leadsServiceProxy.assignOrTransferLead(assignleads)
        .pipe(finalize(() => { this.saving = false; }))
        .subscribe(() => {
          this.reloadPage(true);
          this.saving = false;
          this.notify.info(this.l('AssignedSuccessfully'));
        }, error => { this.spinnerService.hide() });
    }
  }
}
