// /// <reference  types="@types/googlemaps"  />
import { Component, ViewChild, Injector, Output, Optional, EventEmitter, OnInit, ElementRef, Directive, Input } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { FilterService } from "primeng/api";
import { SelectItemGroup } from "primeng/api";
import { MessageService } from 'abp-ng2-module';
import {
    UserListDto,
    LeadSourceServiceProxy, LeadTypeServiceProxy,
    CreateOrEditLeadsDto, LeadsServiceProxy, CommonLookupServiceProxy, LeadSourceLookupTableDto, SolarTypeLookupTable, StateLookupTableDto, DistrictLookupTableDto, TalukaLookupTableDto, HeightStructureLookupTable, CommonLookupDto, DiscomServiceProxy, DivisionServiceProxy, CheckExistLeadDto, GetDuplicateLeadPopupDto
} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
//import * as moment from 'moment';
import { ActivatedRoute, Router } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { ManageLeadsComponent } from './../manageleads/manageleads.component';
import { ViewMyLeadComponent } from './view-mylead.component';
//import { AgmCoreModule } from '@agm/core';
//import { MatGoogleMapsAutocompleteModule } from '@angular-material-extensions/google-maps-autocomplete';
//import PlaceResult = google.maps.places.PlaceResult;
//import { Appearance, GermanAddress, Location } from '@angular-material-extensions/google-maps-autocomplete';
//import { ViewDuplicatePopUpModalComponent } from './duplicate-lead-popup.component';
import { NgxSpinnerService } from 'ngx-spinner';
//const place = null as google.maps.places.PlaceResult;
//type Components = typeof place.address_components;

@Component({
    selector: 'createOrEditManageLead',
    templateUrl: './create-or-edit-manageleads.component.html',
    styleUrls: ['./create-or-edit-manageleads.component.less'],
    providers: [FilterService],
    animations: [appModuleAnimation()]

})
export class createOrEditManageLeadComponent extends AppComponentBase {

    //@ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @Input() addEditLeadModelpopup: boolean = false;
    @ViewChild('addresstext') addresstext: ElementRef;
    @ViewChild('addModal', { static: true }) modal: ModalDirective;
    @Input() organizationUnit: number = 0;
    @Input() leadId: number = 0;
    // @Input() mainorganizationID:number = 0;
    active = false;
    saving = false;
    from: string;
    manageLeads: CreateOrEditLeadsDto = new CreateOrEditLeadsDto();
    // allStates: LeadStateLookupTableDto[];
    drpleadsources: LeadSourceLookupTableDto[];
    drpleadstatus: LeadSourceLookupTableDto[];
    drpleadtype: CommonLookupDto[];
    drpsolartype: CommonLookupDto[];
    drpheightstructure: CommonLookupDto[];
    drpdiscom: any[];
    drpcircle: any[];
    drpdivision: any[];
    drpsubdivision: any[];
    drpchanelpartner: CommonLookupDto[];
    drpstate: StateLookupTableDto[];
    drpdistrict: DistrictLookupTableDto[];
    drptaluka: TalukaLookupTableDto[];
    drpcity: CommonLookupDto[];
    CityListData: any[];
    suburbSuggestions: any;
    filteredGroups: any[];
    cityfilerData: SelectItemGroup[];
    subDivisionmodel: any;
    filteredSubDivision: any[];
    filteredSubDivisionList: any[];
    subdivisionfilerData: SelectItemGroup[];
    ListSubdivisionData: any[];
    citymodel: any;
    // drpheightstructure:CommonLookupDto[];
    role: string = '';
    filteredCountriesSingle: any[];
    countries: any[];
    chanelPartnerId: number = 2;
    CurrentUserRole: string = '';
    IsAssignLead: boolean = true;
    UserId: number = 0;
    item: GetDuplicateLeadPopupDto[];
    constructor(
        injector: Injector,
        private _manageLeadsServiceProxy: LeadsServiceProxy,
        private _leadsourceServiceProxy: LeadSourceServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _leadTypeServiceProxy: LeadTypeServiceProxy,
        private _commonLookupService: CommonLookupServiceProxy,
        private _discomService: DiscomServiceProxy,
        private _router: Router,
        private _el: ElementRef,
        private spinner: NgxSpinnerService,
        private filterService: FilterService,
        private _messageService: MessageService,
        @Optional() private ManageLeadDetail?: ManageLeadsComponent,
        @Optional() private viewLeadDetail?: ViewMyLeadComponent

    ) {
        super(injector);
        this._el.nativeElement.setAttribute('autocomplete', 'off');
        this._el.nativeElement.setAttribute('autocorrect', 'off');
        this._el.nativeElement.setAttribute('autocapitalize', 'none');
        this._el.nativeElement.setAttribute('spellcheck', 'false');

        //this.manageLeads.customerName = this.manageLeads.firstName + " " + this.manageLeads.middleName + " " + this.manageLeads.lastName;
    }
    ngOnInit(): void {
        //alert(this.mainorganizationID)
        // alert(this.organizationUnit)
        //this.modal.hide();
        if (this.addEditLeadModelpopup) {
            debugger
            this.show(this.organizationUnit, this.leadId);
        }


    }
    ChangeText(event) {
        if (event.target.value.length > 0) {
            if (this.manageLeads.firstName === undefined) {
                this.manageLeads.firstName = '';
            }
            if (this.manageLeads.middleName === undefined) {
                this.manageLeads.middleName = '';
            }
            if (this.manageLeads.lastName === undefined) {
                this.manageLeads.lastName = '';
            }
            if (this.manageLeads.customerName === undefined) {
                this.manageLeads.customerName = '';
            }

            this.manageLeads.customerName = this.manageLeads.firstName + " " + this.manageLeads.middleName + " " + this.manageLeads.lastName;
        }
    }

    filtercity(event): void {
        this.getCityList(event.query);
    }

    keyPressNumbers(event) {
        var charCode = (event.which) ? event.which : event.keyCode;
        // Only Numbers 0-9
        if ((charCode < 48 || charCode > 57)) {
            event.preventDefault();
            return false;
        } else {
            return true;
        }
    }
    CheckDuplicateConsumerNo(event) {
        if (this.manageLeads.solarTypeId === undefined) {
            this._messageService.info('Please Select Solar Type!');
            return;
        }
        if (event.target.value != "" && this.manageLeads.solarTypeId != 0) {
            this.spinner.show();

            this._commonLookupService.getCheckDuplicateConsumerNo(event.target.value, this.manageLeads.solarTypeId).subscribe(output => {
                if (output.length > 0 && this.manageLeads.id == 0) {
                    this._messageService.info('This Consumer Already Exist :-' + output[0]["displayName"]);
                    //this.notify.error("This Consumer No Duplicate");                  

                    this.manageLeads.consumerNumber = null;
                    //document.getElementById('ConsumerNoInput').focus();
                }
                this.spinner.hide();
            }, error => { this.spinnerService.hide() });
        }
        else {

            this.manageLeads.consumerNumber = null;
            //document.getElementById('ConsumerNoInput').focus();
            this.spinner.hide();
        }

    }
    keyPressNumbersWithDecimal(event) {
        var charCode = (event.which) ? event.which : event.keyCode;
        if (charCode != 46 && charCode > 31
            && (charCode < 48 || charCode > 57)) {
            event.preventDefault();
            return false;
        }
        return true;
    }
    getCityList(id) {
        //this.citymodel = undefined;
        this.manageLeads.cityId = undefined;
        this.manageLeads.talukaId = undefined;
        this.manageLeads.districtId = undefined;
        this.manageLeads.stateId = undefined;

        if (isNaN(+id)) {
            if (id.length < 3) {
                return false;
            }
        }
        this._commonLookupService.getAllCityWiseData(id).subscribe(output => {
            this.suburbSuggestions = [];
            let query = id;
            let filteredGroups = [];
            let filteredSubOptions
            this.CityListData = output;
            this.cityfilerData = this.CityListData;
            for (let optgroup of this.cityfilerData) {
                filteredSubOptions = this.filterService.filter(
                    this.CityListData,
                    ["displayName"],
                    query,
                    "contains"
                );
                if (filteredSubOptions && filteredSubOptions.length) {
                    filteredGroups.push({
                        label: optgroup["displayName"],
                        id: optgroup["id"],
                        value: optgroup["value"],
                        items: filteredSubOptions
                    });
                }
            }
            if (this.manageLeads.id != null && this.cityfilerData.length > 0) {
                if (this.manageLeads.id != 0 && filteredSubOptions.length == 0) {
                    filteredGroups.push({
                        label: this.cityfilerData[0]["displayName"],
                        id: this.cityfilerData[0]["id"],
                        value: this.cityfilerData[0]["value"],
                        items: this.cityfilerData[0]
                    });
                    this.citymodel = filteredGroups[0]; // this.cityfilerData[0]["displayName"];
                    this.manageLeads.cityId = this.cityfilerData[0]["id"];
                    this.gettaluka(this.cityfilerData[0]["value"]);
                }
                else {
                    this.filteredGroups = filteredGroups;
                    //this.citymodel = filteredGroups;
                }
            }
            else {
                this.filteredGroups = filteredGroups;
                //this.citymodel = filteredGroups;
            }


        }, error => { this.spinnerService.hide() });
    }
    selectedCity(event) {
        if (event.value != "") {
            this.manageLeads.cityId = event.id;
            this.spinner.show();
            this.gettaluka(event.value);
        }
    }

    filtersubdivision(event): void {
        this.getsubdivision(event.query);
    }


    getsubdivision(id) {
        this.manageLeads.subDivisionId = undefined;
        this.manageLeads.divisionId = undefined;
        this.manageLeads.circleId = undefined;
        this.manageLeads.discomId = undefined;

        this._commonLookupService.getAllSubdivisionWiseData(id).subscribe(output => {
            this.filteredSubDivisionList = [];
            this.ListSubdivisionData = [];
            let query = id;
            let filteredGroups = [];
            let filteredSubOptions
            this.ListSubdivisionData = output;
            this.subdivisionfilerData = this.ListSubdivisionData;
            for (let optgroup of this.subdivisionfilerData) {
                filteredSubOptions = this.filterService.filter(
                    this.ListSubdivisionData,
                    ["displayName"],
                    query,
                    "contains"
                );
                if (filteredSubOptions && filteredSubOptions.length) {
                    filteredGroups.push({
                        label: optgroup["displayName"],
                        id: optgroup["id"],
                        value: optgroup["value"],
                        items: filteredSubOptions
                    });
                }
            }
            if (this.manageLeads.id != null && this.subdivisionfilerData.length > 0) {
                if (this.manageLeads.id != 0 && filteredSubOptions.length == 0) {
                    filteredGroups.push({
                        label: this.subdivisionfilerData[0]["displayName"],
                        id: this.subdivisionfilerData[0]["id"],
                        value: this.subdivisionfilerData[0]["value"],
                        items: this.subdivisionfilerData[0]
                    });
                    this.subDivisionmodel = filteredGroups[0]; // this.cityfilerData[0]["displayName"];
                    this.manageLeads.subDivisionId = this.subdivisionfilerData[0]["id"];
                    this.getdivision(this.subdivisionfilerData[0]["value"]);
                }
                else {
                    this.filteredSubDivisionList = filteredGroups;
                    this.subDivisionmodel = filteredGroups;
                }
            }
            else {
                this.filteredSubDivisionList = filteredGroups;
                this.subDivisionmodel = filteredGroups;
            }

        }, error => { this.spinnerService.hide() });
    }
    selectedSubdivision(event): void {
        this.drpdivision = [];

        if (event.value != "") {

            // this._commonLookupService.getAllDivisionFilterDropdown(event.value).subscribe(result => {
            //     //this.drpdivision = result;
            //     this.manageLeads.subDivisionId = event.id;
            //     this.manageLeads.divisionId = result[0]['id'];
            //     
            //     this.getdivision(result[0]['value']);
            // });
            this.manageLeads.subDivisionId = event.id;
            this.manageLeads.divisionId = event.value; //result[0]['id'];

            this.getdivision(this.manageLeads.divisionId);
        }
    }
    //division List
    getdivision(subDivisionid) {
        this.spinner.show();
        this.drpdivision = [];
        this._commonLookupService.getAllDivisionFilterDropdown(subDivisionid).subscribe(result => {
            if (result != null) {
                if (result != null) {
                    this.drpdivision = result;
                    this.manageLeads.divisionId = result[0]['id'];
                    this.selectedCircle(result[0]['value']);
                    //this.selectedDiscom(result[0]['value']);          
                }
            }
        }, error => { this.spinnerService.hide() });
    }
    //Circle List
    selectedCircle(id): void {
        this.drpdiscom = [];
        if (id != "") {
            this._commonLookupService.getCircleForDivisionWiseDropdown(id).subscribe(result => {
                if (result != null) {
                    if (result != null) {
                        this.drpcircle = result;
                        this.manageLeads.circleId = result[0]['id'];
                        this.selectedDiscom(result[0]['value']);
                    }
                }
            }, error => { this.spinnerService.hide() });
        }
    }
    selectedDiscom(id): void {
        this.drpdiscom = [];
        if (id != "") {
            this._commonLookupService.getAllDiscomFilterDropdown(id).subscribe(result => {
                this.spinner.hide();
                if (result != null) {
                    if (result != null) {
                        this.drpdiscom = result;
                        this.manageLeads.discomId = result[0]['id'];
                    }
                }
            }, error => { this.spinnerService.hide() });
        }
    }

    LeadFlag: string = '';
    show(OrganizationId?: number, leadId?: number, IsAssignLead?: boolean, currentUserId?: number, LeadFlag?: string): void {

        this.spinner.show();
        this.IsAssignLead = IsAssignLead;
        this.UserId = currentUserId;
        this.LeadFlag = LeadFlag;
        this.active = true;
        this.manageLeads.emailId = undefined;
        this.manageLeads = new CreateOrEditLeadsDto();
        this.manageLeads.id = leadId;
        this.manageLeads.organizationUnitId = OrganizationId;

        //manageLead Condition Flag hide/Show field

        //this.LeadFlag = 'C'    
        // if (leadId != null ) {
        //     this.LeadFlag = 'C'
        // }
        // this._divisionService.getAllDivisionForDropdown().subscribe(result => {
        //     this.drpdivision = result;
        // });

        // this._commonLookupService.getAllStateForDropdown().subscribe(result => {
        //     this.drpstate = result;
        // });
        //get user role
        this._commonLookupService.getCurrentUserRole().subscribe(result => {
            this.CurrentUserRole = result;
            this.IsAssignLead = true
            if (this.role.toLocaleLowerCase() == 'channel partners') {
                this.IsAssignLead = false;
            }
        }, error => { this.spinnerService.hide() });

        //LeadStatus dropdown
        this._commonLookupService.getAllLeadStatusDropdown().subscribe(result => {
            this.drpleadstatus = result;
            //this.manageLeads.leadStatusId = 2;
        }, error => { this.spinnerService.hide() });
        this._commonLookupService.getAllHeightStructureForDropdown().subscribe(result => {
            this.drpheightstructure = result;
        }, error => { this.spinnerService.hide() });

        //LeadType dropdonw
        this._commonLookupService.getAllLeadTypeForDropdown().subscribe(result => {
            this.drpleadtype = result;
            this.manageLeads.leadTypeId = 1;
        }, error => { this.spinnerService.hide() });

        //solartype dropdonw
        this._commonLookupService.getAllSolarTypeForDropdown().subscribe(result => {
            this.drpsolartype = result;
            this.manageLeads.solarTypeId = 2;
        }, error => { this.spinnerService.hide() });


        this._commonLookupService.getDiscomForDropdown().subscribe(result => {
            this.drpdiscom = result;
        }, error => { this.spinnerService.hide() });

        this._commonLookupService.getEmployees(this.chanelPartnerId).subscribe(result => {
            this.drpchanelpartner = result;
        }, error => { this.spinnerService.hide() });

        // OrganizationId = parseInt(document.getElementById('mainOrganization').textContent);
        //this.manageLeads.organizationUnitId = this.mainorganizationID;
        this.manageLeads.isActive = true;
        this.changeLeadSource(OrganizationId);
        if (!leadId) {
            this.manageLeads.leadStatusId = 1;
            if (this.LeadFlag == 'M') {
                this.manageLeads.isDuplicate = false;
            }
        } else {
            // this.manageLeads = new CreateOrEditLeadsDto();
            // this.manageLeads.id = leadId;
            // this.manageLeads.organizationUnitId = OrganizationId;
            this._manageLeadsServiceProxy.getLeadsForEdit(leadId).subscribe(result => {
                this.manageLeads = result.leads;
                this.active = true;
                this.spinner.hide();
                if (result.leads.cityId != null) {
                    this.getCityList(result.leads.cityId);
                }
                if (result.leads.subDivisionId != null) {
                    this.getsubdivision(result.leads.subDivisionId);
                }
            }, error => { this.spinnerService.hide(); });
        }

        this.modal.show();
        this.spinnerService.hide();
    }

    changeLeadSource(OrganizationId) {
        this._commonLookupService.getAllLeadSourceOrganizationWise(OrganizationId).subscribe(result => {
            this.drpleadsources = result;
        }, error => { this.spinnerService.hide() });
    }
    changeState(event) {
        if (event.target.selectedIndex > 0) {
            this.getDistrict(event.target.value)
        }
        else {
            this.drpdistrict = [];
            this.drptaluka = [];
            this.drpcity = [];
        }
    }
    getDistrict(stateid) {
        this.drpdistrict = [];
        this.manageLeads.districtId = 0;
        this.manageLeads.stateId = 0
        this._commonLookupService.getAllDistrictForDropdown(stateid).subscribe(result => {
            if (result.length > 0) {
                this.drpdistrict = result;
                this.manageLeads.districtId = result[0]['id'];
                this._commonLookupService.getAllStateForDropdown(result[0]['value']).subscribe(result => {
                    this.drpstate = result;
                    this.manageLeads.stateId = result[0]['id'];
                });
            }
            this.spinner.hide();
        }, error => { this.spinnerService.hide() });
    }
    changeDistrict(event) {
        if (event.target.selectedIndex > 0) {
            this.gettaluka(event.target.value)
        }
        else {
            //alert("Please First Add the State Name");
            this.drptaluka = [];
            this.drpcity = [];
        }
    }
    districtId = [];
    gettaluka(districtid) {
        this.drptaluka = [];

        this.districtId = [];
        this.districtId.push(districtid);
        this._commonLookupService.getAllTalukaForDropdown(this.districtId).subscribe(result => {
            if (result != null) {
                this.drptaluka = result;
                this.manageLeads.talukaId = result[0]['id'];
                this.getDistrict(result[0]['value'])
                //this.drptaluka.values;
            }
        }, error => { this.spinnerService.hide() });
    }
    changeTaluka(event) {
        if (event.target.selectedIndex > 0) {
            // this._commonLookupService.getAllCityForDropdown(event.target.value).subscribe(result => {
            //     this.drpcity = result;
            // });
        }
        else {
            this.drpcity = [];
        }
    }
    changeDivision(event) {
        if (event.target.selectedIndex > 0) {
            // this._commonLookupService.getAllSubDivisionForDropdown(event.target.value).subscribe(result => {
            //     this.drpsubdivision = result;
            // });
        }
        else {
            this.drpsubdivision = [];
        }

    }

    // selectAddressType(type) {
    //     alert(type);
    // }
    onShown(): void {
        document.getElementById('FirstNameInput').focus();
    }

    save(): void {
        this.saving = true;
        // if(this.LeadFlag == 'M'){
        //     this.manageLeads.divisionId = undefined;
        //     this.manageLeads.subDivisionId = undefined;
        //     this.manageLeads.discomId = undefined;
        // }
        if (!this.IsAssignLead) {
            this.manageLeads.chanelPartnerId = this.UserId
        }
        // this._manageLeadsServiceProxy
        //     .createOrEdit(this.manageLeads)
        //     .pipe(
        //         finalize(() => {
        //             this.saving = false;
        //         })
        //     )
        //     .subscribe(() => {
        //         this.modalSave.emit(null);
        //         this.saving = false;
        //         this.modal.hide();
        //         this.notify.info(this.l('SavedSuccessfully'));
        //         if (this.manageLeads.id == null) {
        //             this.ManageLeadDetail.reloadPage(true);
        //             this.ManageLeadDetail.reloadLead.emit(false);
        //             document.getElementById("RefreshButtonId").click();
        //         }
        //         else {
        //             document.getElementById("RefreshButtonId").click();
        //             this.viewLeadDetail.reloadLead.emit(false);
        //         }
        //     }, error => { this.spinnerService.hide() });
        let input = {} as CheckExistLeadDto;
        input.email = this.manageLeads.emailId;
        input.mobile = this.manageLeads.mobileNumber;
        input.addressLine1 = this.manageLeads.addressLine1;
        input.addressLine2 = this.manageLeads.addressLine2;
        input.state = this.manageLeads.stateId;
        input.districtId = this.manageLeads.districtId;
        input.cityId = this.manageLeads.cityId;
        input.talukaId = this.manageLeads.talukaId;
        input.pincode = this.manageLeads.pincode;
        input.organizationId = this.manageLeads.organizationUnitId;
        input.id = this.manageLeads.id;
        this._manageLeadsServiceProxy.checkValidation(this.manageLeads)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(result => {
                if (result) {
                    this._manageLeadsServiceProxy.checkExistLeadList(input)
                        .pipe(finalize(() => { this.saving = false; }))
                        .subscribe(result => {
                            this.item = result;
                            if (result.length > 0) {
                                //if (this.role == 'Admin') {
                                this.message.confirm('Do You Still Want to Proceed',
                                    "Looks Like Duplicate Record",
                                    (isConfirmed) => {
                                        if (isConfirmed) {

                                            this._manageLeadsServiceProxy.createOrEdit(this.manageLeads)
                                                .pipe(finalize(() => { this.saving = false; }))
                                                .subscribe(() => {
                                                    this.modalSave.emit(null);
                                                    this.saving = false;
                                                    this.modal.hide();
                                                    this.notify.info(this.l('SavedSuccessfully'));
                                                    if (this.manageLeads.id == null) {
                                                        this.ManageLeadDetail.reloadPage(true);
                                                        this.ManageLeadDetail.reloadLead.emit(false);
                                                        document.getElementById("RefreshButtonId").click();
                                                    }
                                                    else {
                                                        document.getElementById("RefreshButtonId").click();
                                                        this.viewLeadDetail.reloadLead.emit(false);
                                                    }
                                                });

                                        } else {
                                            this.saving = false;
                                        }
                                    }
                                );
                                // }
                                // else {
                                //     // let element: HTMLElement = document.getElementById('auto_trigger') as HTMLElement;
                                //     // element.click();
                                //     this.saving = false;
                                // }
                            }
                            else {

                                this._manageLeadsServiceProxy.createOrEdit(this.manageLeads)
                                    .pipe(finalize(() => { this.saving = false; }))
                                    .subscribe(() => {
                                        this.modalSave.emit(null);
                                        this.saving = false;
                                        this.modal.hide();
                                        this.notify.info(this.l('SavedSuccessfully'));
                                        if (this.manageLeads.id == null) {
                                            this.ManageLeadDetail.reloadPage(true);
                                            this.ManageLeadDetail.reloadLead.emit(false);
                                            document.getElementById("RefreshButtonId").click();
                                        }
                                        else {
                                            document.getElementById("RefreshButtonId").click();
                                            this.viewLeadDetail.reloadLead.emit(false);
                                        }
                                    });


                            }
                            //this.modal.show();
                        });
                }
                else {
                    this.saving = false;
                }
            });
    }

    cancel(): void {
        this.active = false;
        this.addEditLeadModelpopup = false;
    }
    close() {
        this.active = false;
        this.modal.hide();
        this.addEditLeadModelpopup = false;
    }
}
