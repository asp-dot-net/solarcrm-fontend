
import { Component, Injector, ViewEncapsulation, ViewChild, OnInit, HostListener } from '@angular/core';
import { Paginator } from 'primeng/paginator';
import { FileDownloadService } from '@shared/utils/file-download.service';
import { EmailServiceProxy, EmailMessage, EmailAddress } from '@shared/service-proxies/service-proxies';
import { NgxSpinnerService } from 'ngx-spinner';
import { json } from 'stream/consumers';
import { ModalDirective } from 'ngx-bootstrap/modal';
@Component({
  selector: 'app-inbox',
  templateUrl: './inbox.component.html',
  styleUrls: ['./inbox.component.css']
})
export class InboxComponent implements OnInit {
  @ViewChild('EmailBodyTemplate') public EmailBodymodal: ModalDirective;
  @ViewChild('EmailSendTemplate') public SendEmailmodal: ModalDirective;

  ComposeBox = false;
    emailDetailView = false;  
  FiltersData = false;
    
    public screenWidth: any;  
    public screenHeight: any;  
    testHeight = 100;

  constructor(
    injector: Injector,
    private _emailService: EmailServiceProxy,
    private _spinerService: NgxSpinnerService,
  ) {
    //super(injector);
  }
  ComposeEmailData: EmailMessage; 
  ToEmailIdSelectedList:EmailAddress;
  
  InboxList: any;
  EmailBody: any;
  toEmailaddress:any=[];
  ToEmailList:[''];
  ngOnInit(): void {
    this.screenWidth = window.innerWidth;  
    this.screenHeight = window.innerHeight; 
    
    this.emaillist();
    this.ComposeEmailData = new EmailMessage;
    this.ToEmailIdSelectedList = new EmailAddress({name:'df',address:'pravin1.arisesolar@gmail.com'});
    //this.ToEmailIdSelectedList[0]['address'] = 'pravin1.arisesolar@gmail.com';
    this.ComposeEmailData.toAddresses = [this.ToEmailIdSelectedList];
    this.ToEmailIdSelectedList = new EmailAddress({name:'df',address:'pravin1.arisesolar@outlook.com'});
    this.ComposeEmailData.fromAddresses = [this.ToEmailIdSelectedList];    
  }

  @HostListener('window:resize', ['$event'])  
    onResize(event) {  
        this.screenWidth = window.innerWidth;  
        this.screenHeight = window.innerHeight;
    }  

    testHeightSize () {
        if (this.FiltersData == true) {
            this.testHeight = this.testHeight + 90 ;
        }

        else {
            this.testHeight = this.testHeight - 90 ;
        }
    }

  emaillist() {
    this._spinerService.show();
    this._emailService.receiveEmail(10)
      .subscribe((result) => {        
        
        this.InboxList = result; //.sort((a,b) => b- a);
        this._spinerService.hide();
      });
  }
  htmlContentbidy: any;
  GetMessageBody(messageid: number) {
    this._spinerService.show();
    this._emailService.receiveEmailBody(messageid).subscribe((result) => {
      
      this.htmlContentbidy = result;
      this.EmailBodymodal.show();
      this._spinerService.hide();
    });
  }
  ComposeEmail(){
    this.SendEmailmodal.show();
  }
 
  
  sendEmail(){
    
    //this.ToEmailIdSelectedList.address = '';
    //this.ComposeEmailData.toAddresses
    this.ComposeEmailData.toAddresses['address'] = 'pravin1.arisesolar@gmail.com'; //this.ToEmailIdSelectedList;
    this._emailService.sendComposeEmail(this.ComposeEmailData).subscribe((result) => {
      alert(result)
    })
  }
  close(): void {
    this.EmailBodymodal.hide();
    this.SendEmailmodal.hide();
  }
}
