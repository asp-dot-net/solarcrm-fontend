import { CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { InboxComponent } from './inbox/inbox.component';
import { BsDatepickerConfig, BsDaterangepickerConfig, BsLocaleService } from 'ngx-bootstrap/datepicker';
import { NgxBootstrapDatePickerConfigService } from 'assets/ngx-bootstrap/ngx-bootstrap-datepicker-config.service';
import { FormsModule } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap/modal';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { AppCommonModule } from '@app/shared/common/app-common.module';
import { UtilsModule } from '@shared/utils/utils.module';
import CountoModule from 'angular2-counto';
import { PaginatorModule } from 'primeng/paginator';
import { EditorModule } from 'primeng/editor';
import { InputMaskModule } from 'primeng/inputmask';
import { TableModule } from 'primeng/table';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { EmailBoxRoutingModule } from './emailbox-routing.module';



@NgModule({
 
  imports: [
    CommonModule,    
    FormsModule,
    ModalModule,
    TabsModule,
    TooltipModule,
    AppCommonModule,
    UtilsModule,
    CountoModule,
    // BsDatepickerModule.forRoot(),
    // BsDropdownModule.forRoot(),
    // PopoverModule.forRoot(),
    // SubheaderModule,
    // AdminSharedModule,
    // AppSharedModule,  
    // AgmCoreModule.forRoot({
    //     apiKey: 'AIzaSyALoDvKqgmTfpfPOEjqS2fI1kcabuxImMA',            
    //     libraries: ['places']
    // }),        
    // EmailEditorModule,
    // AutoCompleteModule,
    PaginatorModule,
    EditorModule,
    InputMaskModule,
    TableModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    EmailBoxRoutingModule,
    //LeadsModule,
    //NgSelectModule
  ],
  declarations: [InboxComponent],
  exports: [],
  schemas: [NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA],
  providers: [
    { provide: BsDatepickerConfig, useFactory: NgxBootstrapDatePickerConfigService.getDatepickerConfig },
    { provide: BsDaterangepickerConfig, useFactory: NgxBootstrapDatePickerConfigService.getDaterangepickerConfig },
    { provide: BsLocaleService, useFactory: NgxBootstrapDatePickerConfigService.getDatepickerLocale },
    { provide: DatePipe }
  ],
})
export class EmailboxModule { }
