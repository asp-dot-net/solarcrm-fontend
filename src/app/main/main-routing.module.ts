import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '',
                children: [
                    {
                        path: 'dashboard',
                        loadChildren: () => import('./dashboard/dashboard.module').then((m) => m.DashboardModule),
                        data: { permission: 'Pages.Tenant.Dashboard' },
                    },
                    {
                        path: 'datavaults',
                        loadChildren: () => import('./datavaults/datavaults.module').then((m) => m.DataVaultsModule),
                        data: { permission: 'Pages.Tenant.DataVaults', features: 'App.DataVaults' },
                    },
                    {
                        path: 'leads',
                        loadChildren: () => import('./leads/leads.module').then((m) => m.LeadsModule),
                        data: { permission: 'Pages.Tenant.Leads' },
                    },
                    {
                        path: 'jobs',
                        loadChildren: () => import('./jobs/job.module').then((m) => m.JobModule),
                        data: { permission: 'Pages.Tenant.Jobs' },
                    },
                    {
                        path: 'tracker',
                        loadChildren: () => import('./tracker/tracker.module').then((m) => m.TrackerModule),
                        data: { permission: 'Pages.Tenant.Tracker' },
                    },
                    {
                        path: 'subsidy',
                        loadChildren: () => import('./subsidy/subsidy.module').then((m) => m.SubsidyModule),
                        data: { permission: 'Pages.Tenant.Tracker' },
                    },
                    {
                        path: 'installation',
                        loadChildren: () => import('./installation/installation.module').then((m) => m.InstallationModule),
                        //data: { permission: 'Pages.Tenant.DataVaults' },
                    },
                    {
                        path: 'accounts',
                        loadChildren: () => import('./accounts/accounts.module').then((m) => m.AccountsModule),
                        data: { permission: 'Pages.Tenant.Accounts' },
                    },
                    {
                        path: 'emailbox',
                        loadChildren: () => import('./emailbox/emailbox.module').then((m) => m.EmailboxModule),
                        //data: { permission: 'Pages.Tenant.Accounts' },
                    },
                    // {
                    //     path: 'signature',
                    //     loadChildren: () => import('./signature/signature.module').then((m) => m.SignatureModule),
                    //     //data: { permission: 'Pages.Tenant.DataVaults' },
                    // },
                    { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
                    { path: '**', redirectTo: 'dashboard' },
                ],
            },
        ]),
    ],
    exports: [RouterModule],
})
export class MainRoutingModule {}
