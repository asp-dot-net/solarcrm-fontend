import { NgModule } from '@angular/core';
import { AdminSharedModule } from '@app/admin/shared/admin-shared.module';
import { AppSharedModule } from '@app/shared/app-shared.module';
import { CustomizableDashboardModule } from '@app/shared/common/customizable-dashboard/customizable-dashboard.module';
import { CreateOrEditPaymentModeModalComponent } from './create-or-edit-paymentMode.component';
import { PaymentModeRoutingModule } from './paymentMode-routing.module';
import { PaymentModeComponent } from './paymentMode.component';

@NgModule({
    declarations: [PaymentModeComponent, CreateOrEditPaymentModeModalComponent],
    imports: [AppSharedModule, AdminSharedModule, PaymentModeRoutingModule, CustomizableDashboardModule],
})
export class PaymentModeModule {}