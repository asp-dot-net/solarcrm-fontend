import { Component, EventEmitter, Injector, Output, ViewChild } from '@angular/core';
import { AppConsts } from '@shared/AppConsts';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { map as _map, filter as _filter } from 'lodash-es';
import { LeadTypeServiceProxy, CreateOrEditLeadTypeDto, CreateOrEditPaymentModeDto, PaymentModeServiceProxy } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'createOrEditPaymentModeModal',
    templateUrl: './create-or-edit-paymentMode.component.html',
    //styleUrls: ['./create-or-edit-paymentMode.component.less'],

})
export class CreateOrEditPaymentModeModalComponent extends AppComponentBase {
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    paymentMode: CreateOrEditPaymentModeDto = new CreateOrEditPaymentModeDto();

    constructor(
        injector: Injector,
        private _paymentModeServiceProxy: PaymentModeServiceProxy
    ) {
        super(injector);
    }

    show(paymentTypeId?: number): void {
        if (!paymentTypeId) {
            this.paymentMode = new CreateOrEditPaymentModeDto();
            this.paymentMode.id = paymentTypeId;
            this.active = true;
            this.modal.show();
        } else {
            this.paymentMode = new CreateOrEditPaymentModeDto();
            this.paymentMode.id = paymentTypeId
            this._paymentModeServiceProxy.getPaymentModeForEdit(paymentTypeId).subscribe(result => {
                this.paymentMode = result.payBy;
                this.active = true;
                this.modal.show();
            }, error => { this.spinnerService.hide() });
        }
    }



    onShown(): void {
        document.getElementById('Name').focus();
    }

    save(): void {
        this.saving = true;
        this._paymentModeServiceProxy
            .createOrEdit(this.paymentMode)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
            }, error => { this.spinnerService.hide() });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}