import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PaymentModeComponent } from './paymentMode.component';

const routes: Routes = [
    {
        path: '',
        component: PaymentModeComponent,
        pathMatch: 'full',
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class PaymentModeRoutingModule {}
