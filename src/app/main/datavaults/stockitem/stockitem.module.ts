import { NgModule } from '@angular/core';
import { AdminSharedModule } from '@app/admin/shared/admin-shared.module';
import { AppSharedModule } from '@app/shared/app-shared.module';
import { StockItemRoutingModule } from './stockitem-routing.module';
import { CustomizableDashboardModule } from '@app/shared/common/customizable-dashboard/customizable-dashboard.module';
import { CreateOrEditStockItemModalComponent } from './create-or-edit-stockitem.component';
import { StockItemComponent } from './stockitem.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { AddAttachmentStockItemComponent } from './add-attachment.component';
@NgModule({
    declarations: [StockItemComponent, CreateOrEditStockItemModalComponent, AddAttachmentStockItemComponent],
    imports: [AppSharedModule, AdminSharedModule, StockItemRoutingModule,NgSelectModule, CustomizableDashboardModule],
})
export class StockItemModule {}
