import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StockItemComponent } from './stockitem.component';

const routes: Routes = [
    {
        path: '',
        component: StockItemComponent,
        pathMatch: 'full',
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class StockItemRoutingModule {}
