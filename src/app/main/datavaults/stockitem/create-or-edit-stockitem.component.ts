import { Component, EventEmitter, Injector, OnInit, Output, ViewChild } from '@angular/core';
import { AppConsts } from '@shared/AppConsts';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { map as _map, filter as _filter } from 'lodash-es';
import { StockItemServiceProxy, CreateOrEditStockItemDto, CommonLookupServiceProxy, LocationsServiceProxy, OrganizationUnitDto } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'createOrEditStockItemModal',
    templateUrl: './create-or-edit-stockitem.component.html',
    styleUrls: ['./create-or-edit-stockitem.component.less'],

})
export class CreateOrEditStockItemModalComponent extends AppComponentBase implements OnInit {
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    stockItem: CreateOrEditStockItemDto = new CreateOrEditStockItemDto();
    organizationList: OrganizationUnitDto[];
    stockCategory: any = [];

    constructor(
        injector: Injector,
        private _stockItemServiceProxy: StockItemServiceProxy,
        private _commonLookupServiceProxy: CommonLookupServiceProxy,
        
    ) {
        super(injector);
    }
    ngOnInit(): void {
        this._commonLookupServiceProxy.getAllStockCategorForDropdown().subscribe(result => {

            this.stockCategory = result;
        });
        this.getAllLocation();
        this.getOrganizationList();
    }

    locationShow: boolean = true;
    show(stockItemId?: number): void {
       
        if (!stockItemId) {
            this.locationShow = false;
            this.stockItem = new CreateOrEditStockItemDto();
            this.stockItem.id = stockItemId;
            this.active = true;
            this.modal.show();
        } else {
            this.stockItem = new CreateOrEditStockItemDto();
            this.stockItem.id = stockItemId;
            this.locationShow = true;
            this._stockItemServiceProxy.getStockItemForEdit(stockItemId).subscribe(result => {
                this.stockItem = result.stockItem;
                this.active = true;
                this.modal.show();
                //console.log(result.stockItemLocation);
            }, error => { this.spinnerService.hide() });

        }
    }

    getOrganizationList() {
        this._commonLookupServiceProxy.getOrganizationUnitUserWise().subscribe(result => {
            this.organizationList = result;
        }, error => { this.spinnerService.hide() });
    }
    
    getLocationName(id: any): string {
        ;
        return this.Locations.find(obj => {
            return obj.id === id;
        }).name;
    }

    onShown(): void {
        document.getElementById('Name').focus();
    }

    Locations: any = [];
    getAllLocation(): void {
        this._commonLookupServiceProxy.getAllLocationForDropdown().subscribe(result => {
            this.Locations = result;
        }, error => { this.spinnerService.hide() });
    }

    save(): void {
        this.saving = true;
        this._stockItemServiceProxy
            .createOrEdit(this.stockItem)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
            }, error => { this.spinnerService.hide() });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
