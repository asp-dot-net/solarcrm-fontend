import { Component, ViewChild, Injector, Output, EventEmitter, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import {StockItemServiceProxy, CreateOrEditStockItemDto, DemoUiComponentsServiceProxy, CommonLookupServiceProxy} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';
import { ActivatedRoute, Router } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Observable } from "@node_modules/rxjs";
import { FileItem, FileUploader, FileUploaderOptions } from 'ng2-file-upload';
import { AppConsts } from '@shared/AppConsts';
import { IAjaxResponse, TokenService } from 'abp-ng2-module';

@Component({
    selector: 'addAttachmentModal',
    templateUrl: './add-attachment.component.html',
    animations: [appModuleAnimation()]
})
export class AddAttachmentStockItemComponent extends AppComponentBase {

    @ViewChild('addAttachment', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    active = false;
    saving = false;
    stockItem: CreateOrEditStockItemDto = new CreateOrEditStockItemDto();
    stockTypeName = '';
    public uploader: FileUploader;
    public maxfileBytesUserFriendlyValue = 5;
    private _uploaderOptions: FileUploaderOptions = {};
    filenName = [];
    uploadUrl: string;
    uploadedFiles: any[] = [];
    public fileupload: FileUploader;
    private _fileuploadoption: FileUploaderOptions = {};
    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
        private _tokenService: TokenService,
        private _stockItemServiceProxy: StockItemServiceProxy,
        private demoUiComponentsService: DemoUiComponentsServiceProxy,
        private _router: Router,
        private _CommonLookupService: CommonLookupServiceProxy,
    ) {
        super(injector);
    }

    show(stockItemId: number): void {
        this._stockItemServiceProxy.getStockItemForEdit(stockItemId).subscribe(result => {
            this.stockItem = result.stockItem;
            //this.stockTypeName = result.stockItem.name;
            this.active = true;
            this.active = true;
            this.initializeModal();
            //this.uploadUrl = AppConsts.remoteServiceBaseUrl + '/DemoUiComponents/UploadProductItem?id=' + this.productItem.id;
            this.modal.show();
        });
    }

    // save(): void {
    //     if (this.uploader.queue.length == 0) {
    //         this.notify.warn(this.l('SelectFileToUpload'));
    //         return;
    //     }
    //     this.uploader.uploadAll();
    // }

    // upload completed event
    // onUpload(event): void {
        
    //     for (const file of event.files) {
    //         this.uploadedFiles.push(file);
    //         this.modal.hide();
    //         this.uploadedFiles = [];
    //         this.modalSave.emit(null);
    //     }
    // }

    // onBeforeSend(event): void {
    //     event.xhr.setRequestHeader('Authorization', 'Bearer ' + abp.auth.getToken());
    // }
    initializeModal(): void {
        this.active = true;
      //  this.temporaryPictureUrl = '';
        this.initFileUploader();
      }
    // save(): void {
    //     if (this.uploader.queue.length == 0) {
    //         this.notify.warn(this.l('SelectFileToUpload'));
    //         return;
    //     }
    //     this.uploader.uploadAll();
    // }

    // upload completed event
    fileChangeEvent(event: any): void {
        if (event.target.files[0].size > 5242880) { //5MB
            this.message.warn(this.l('ProfilePicture_Warn_SizeLimit', this.maxfileBytesUserFriendlyValue));
            return;
        }
        this.uploader.clearQueue();
        this.uploader.addToQueue([<File>event.target.files[0]]);
    }
    guid(): string {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
    }

    guid1(): string {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
    }
    


    updateFile(fileToken: string, fileName: string, id: number): void {
        debugger;
        this.notify.info("File Uploading Start");
        this.saving = true;
        this._stockItemServiceProxy.saveStockIteamDocument(fileToken,fileName,id)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.modal.hide();
                this.uploader = null;
                this.fileupload = null;
                this.modalSave.emit(null);
                this.saving = false;
            });
    }
    initFileUploader(): void {
        debugger;
        this.uploader = new FileUploader({ url: AppConsts.remoteServiceBaseUrl + '/Profile/UploadFile' });
        this._uploaderOptions.autoUpload = false;
        this._uploaderOptions.authToken = 'Bearer ' + this._tokenService.getToken();
        this._uploaderOptions.removeAfterUpload = true;
        this.uploader.onAfterAddingFile = (file) => {
            file.withCredentials = false;
        };
        this.fileupload = new FileUploader({ url: AppConsts.remoteServiceBaseUrl + '/Profile/UploadFile' });
        this._fileuploadoption.autoUpload = false;
        this._fileuploadoption.authToken = 'Bearer ' + this._tokenService.getToken();
        this._fileuploadoption.removeAfterUpload = true;
        this.fileupload.onAfterAddingFile = (file) => {
            file.withCredentials = false;
        };

        this.uploader.onBuildItemForm = (fileItem: FileItem, form: any) => {
            form.append('FileType', fileItem.file.type);
            form.append('FileName', fileItem.file.name);
            form.append('FileToken', this.guid());
            this.filenName.push(fileItem.file.name);

        };
        this.fileupload.onBuildItemForm = (fileItem: FileItem, form: any) => {
            form.append('FileType', fileItem.file.type);
            form.append('FileName', fileItem.file.name);
            form.append('FileToken', this.guid1());
            this.filenName.push(fileItem.file.name);

        };
        this.uploader.onSuccessItem = (item, response, status) => {
            debugger;
            const resp = <IAjaxResponse>JSON.parse(response);
            if (resp.success) {
                this.updateFile(resp.result.fileToken, resp.result.fileName, this.stockItem.id);
            } else {
                this.message.error(resp.error.message);
            }
        };
        this.fileupload.onSuccessItem = (item, response, status) => {
            debugger;
            const resp = <IAjaxResponse>JSON.parse(response);
            // if (resp.success) {
            //     this.updateInstallerInvoiceFile(resp.result.fileToken, resp.result.fileName, resp.result.fileType, resp.result.filePath);
            // } else {
            //     this.message.error(resp.error.message);
            // }
        };
        this.uploader.setOptions(this._uploaderOptions);
        this.fileupload.setOptions(this._fileuploadoption);
    }
    cancel(): void {
        this.modal.hide();

        
    }
    savedocument(): void {
        this.uploader.uploadAll();
    }
}
