import { Component, Injector, ViewEncapsulation, ViewChild, HostListener } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { ActivatedRoute, Router } from '@angular/router';
import { NotifyService } from 'abp-ng2-module';
import { TokenAuthServiceProxy } from '@shared/service-proxies/service-proxies';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import { Title } from '@angular/platform-browser';
import { LazyLoadEvent } from 'primeng/api';
import { StockItemServiceProxy, StockItemDto } from '@shared/service-proxies/service-proxies';
import { CreateOrEditStockItemModalComponent } from './create-or-edit-stockitem.component';
import { HttpClient } from '@angular/common/http';
import { FileUpload } from 'primeng/fileupload';
import { finalize } from 'rxjs/operators';
import { AppConsts } from '@shared/AppConsts';
import { AddAttachmentStockItemComponent } from './add-attachment.component';

@Component({
    templateUrl: './stockitem.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()],
})
export class StockItemComponent extends AppComponentBase {
    @ViewChild('createOrEditStockItemModal', { static: true }) createOrEditStockItemModal: CreateOrEditStockItemModalComponent;
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    @ViewChild('ExcelFileUpload', { static: false }) excelFileUpload: FileUpload;
    @ViewChild('addAttachmentModal', { static: true }) addAttachmentModal: AddAttachmentStockItemComponent;

    advancedFiltersAreShown = false;
    filterText: string = '';
    uploadUrl: string;
    public screenHeight: any;  
    testHeight = 230;
    show: boolean = true;
    toggle: boolean = true;
    recordCount: any;

    toggleBlock() {
        this.show = !this.show;
    };
    change() {
        this.toggle = !this.toggle;
    }

    constructor(
        injector: Injector,
        private _stockItemService: StockItemServiceProxy,
        private _notifyService: NotifyService,
        private _tokenAuth: TokenAuthServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _fileDownloadService: FileDownloadService,
        private titleService: Title,
        private _httpClient: HttpClient
    ) {
        super(injector);
        this.uploadUrl = AppConsts.remoteServiceBaseUrl + '/StockItem/ImportFromExcel';
    }
    ngOnInit(): void {
        this.screenHeight = window.innerHeight;
      }

    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }
    
    getStockItem(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._stockItemService.getAll(
            this.filterText,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe((result) => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            this.primengTableHelper.hideLoadingIndicator();
            this.recordCount = result.totalCount;
        }, error => { this.spinnerService.hide() });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    deleteStockItem(subDivision: StockItemDto): void {
        this.message.confirm(
            this.l('StockItemDeleteWarningMessage', subDivision.name),
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._stockItemService.delete(subDivision.id)
                        .subscribe(() => {
                            this.reloadPage();
                            this.notify.success(this.l('SuccessfullyDeleted'));
                        }, error => { this.spinnerService.hide() });
                }
            }
        );
    }

    exportToExcel(): void {
        this._stockItemService.getStockItemToExcel(
            this.filterText,
        )
            .subscribe(result => {
                this._fileDownloadService.downloadTempFile(result);
            }, error => { this.spinnerService.hide() });
    }

    createStockItem(): void {
        this.createOrEditStockItemModal.show();
    }

    uploadExcel(data: { files: File }): void {
        const formData: FormData = new FormData();
        const file = data.files[0];
        formData.append('file', file, file.name);

        this._httpClient
            .post<any>(this.uploadUrl, formData)
            .pipe(finalize(() => this.excelFileUpload.clear()))
            .subscribe((response) => {
                if (response.success) {
                    this.notify.success(this.l('ImportStockItemProcessStart'));
                } else if (response.error != null) {
                    this.notify.error(this.l('ImportStockItemUploadFailed'));
                }
            }, error => { this.spinnerService.hide() });
    }

    onUploadExcelError(): void {
        this.notify.error(this.l('ImportStockItemUploadFailed'));
    }

    downloadfile(docpath: string, filename: string): void {

        let FileName = AppConsts.docUrl + "/" + docpath + filename;
        window.open(FileName, "_blank");
    }

}
