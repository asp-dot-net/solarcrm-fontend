import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProjectStatusComponent } from './projectstatus.component';

const routes: Routes = [
    {
        path: '',
        component: ProjectStatusComponent,
        pathMatch: 'full',
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class ProjectTypeRoutingModule {}
