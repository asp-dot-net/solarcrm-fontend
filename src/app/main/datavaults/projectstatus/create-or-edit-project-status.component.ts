import { Component, EventEmitter, Injector, Output, ViewChild } from '@angular/core';
import { AppConsts } from '@shared/AppConsts';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { map as _map, filter as _filter } from 'lodash-es';
import { ProjectStatusServiceProxy, CreateOrEditProjectStatusDto } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'createOrEditProjectStatusModal',
    templateUrl: './create-or-edit-project-status.component.html',
    styleUrls: ['./create-or-edit-project-status.component.less'],

})
export class CreateOrEditProjectStatusModalComponent extends AppComponentBase {
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    projectStatus: CreateOrEditProjectStatusDto = new CreateOrEditProjectStatusDto();

    constructor(
        injector: Injector,
        private _projectStatusServiceProxy: ProjectStatusServiceProxy
    ) {
        super(injector);
    }

    show(projectStatusId?: number): void {
        if (!projectStatusId) {
            this.projectStatus = new CreateOrEditProjectStatusDto();
            this.projectStatus.id = projectStatusId;
            this.active = true;
            this.modal.show();
        } else {
            this.projectStatus = new CreateOrEditProjectStatusDto();
            this.projectStatus.id = projectStatusId;

            this._projectStatusServiceProxy.getProjectStatusForEdit(projectStatusId).subscribe(result => {
                this.projectStatus = result.projectStatus;
                this.active = true;
                this.modal.show();
            }, error => { this.spinnerService.hide() });
        }
    }

    onShown(): void {
        document.getElementById('Name').focus();
    }

    save(): void {
        this.saving = true;
        this._projectStatusServiceProxy
            .createOrEdit(this.projectStatus)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
            }, error => { this.spinnerService.hide() });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
