import { NgModule } from '@angular/core';
import { AdminSharedModule } from '@app/admin/shared/admin-shared.module';
import { AppSharedModule } from '@app/shared/app-shared.module';
import { ProjectTypeRoutingModule } from './projectstatus-routing.module';
import { ProjectStatusComponent } from './projectstatus.component';
import { CustomizableDashboardModule } from '@app/shared/common/customizable-dashboard/customizable-dashboard.module';
import { CreateOrEditProjectStatusModalComponent } from './create-or-edit-project-status.component';

@NgModule({
    declarations: [ProjectStatusComponent, CreateOrEditProjectStatusModalComponent],
    imports: [AppSharedModule, AdminSharedModule, ProjectTypeRoutingModule, CustomizableDashboardModule],
})
export class ProjectStatusModule {}
