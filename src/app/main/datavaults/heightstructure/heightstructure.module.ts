import { NgModule } from '@angular/core';
import { AdminSharedModule } from '@app/admin/shared/admin-shared.module';
import { AppSharedModule } from '@app/shared/app-shared.module';
import { HeightStructureRoutingModule } from './heightstructure-routing.module';
import { HeightStructureComponent } from './heightstructure.component';
import { CustomizableDashboardModule } from '@app/shared/common/customizable-dashboard/customizable-dashboard.module';
import { CreateOrEditHeightStructureModalComponent } from './create-or-edit-heightstructure.component';

@NgModule({
    declarations: [HeightStructureComponent, CreateOrEditHeightStructureModalComponent],
    imports: [AppSharedModule, AdminSharedModule, HeightStructureRoutingModule, CustomizableDashboardModule],
})
export class HeightStructureModule {}
