import { Component, EventEmitter, Injector, Output, ViewChild } from '@angular/core';
import { AppConsts } from '@shared/AppConsts';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { map as _map, filter as _filter } from 'lodash-es';
import { HeightStructureServiceProxy, CreateOrEditHeightStructureDto } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'createOrEditHeightStructureModal',
    templateUrl: './create-or-edit-heightstructure.component.html',
    styleUrls: ['./create-or-edit-heightstructure.component.less'],

})
export class CreateOrEditHeightStructureModalComponent extends AppComponentBase {
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    heightStructures: CreateOrEditHeightStructureDto = new CreateOrEditHeightStructureDto();

    constructor(
        injector: Injector,
        private _heightStructureServiceProxy: HeightStructureServiceProxy
    ) {
        super(injector);
    }

    show(heightStructureId?: number): void {
        if (!heightStructureId) {
            this.heightStructures = new CreateOrEditHeightStructureDto();
            this.heightStructures.id = heightStructureId;
            this.active = true;
            this.modal.show();
        } else {
            this.heightStructures = new CreateOrEditHeightStructureDto();
            this.heightStructures.id = heightStructureId;

            this._heightStructureServiceProxy.getHeightStructureForEdit(heightStructureId).subscribe(result => {
                this.heightStructures = result.heightStructures;
                this.active = true;
                this.modal.show();
            }, error => { this.spinnerService.hide() });
        }
    }

    onShown(): void {
        document.getElementById('Name').focus();
    }

    save(): void {
        this.saving = true;
        this._heightStructureServiceProxy
            .createOrEdit(this.heightStructures)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
            }, error => { this.spinnerService.hide() });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
