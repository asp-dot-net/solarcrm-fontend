import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SubDivisionComponent } from './subdivision.component';

const routes: Routes = [
    {
        path: '',
        component: SubDivisionComponent,
        pathMatch: 'full',
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class SubDivisionRoutingModule {}
