import { NgModule } from '@angular/core';
import { AdminSharedModule } from '@app/admin/shared/admin-shared.module';
import { AppSharedModule } from '@app/shared/app-shared.module';
import { SubDivisionRoutingModule } from './subdivision-routing.module';
import { CustomizableDashboardModule } from '@app/shared/common/customizable-dashboard/customizable-dashboard.module';
import { CreateOrEditSubDivisionModalComponent } from './create-or-edit-subdivision.component';
import { SubDivisionComponent } from './subdivision.component';

@NgModule({
    declarations: [SubDivisionComponent, CreateOrEditSubDivisionModalComponent],
    imports: [AppSharedModule, AdminSharedModule, SubDivisionRoutingModule, CustomizableDashboardModule],
})
export class SubDivisionModule {}
