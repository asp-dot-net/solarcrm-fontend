import { Component, EventEmitter, Injector, OnInit, Output, ViewChild } from '@angular/core';
import { AppConsts } from '@shared/AppConsts';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { map as _map, filter as _filter } from 'lodash-es';
import { SubSubDivisionServiceProxy, CreateOrEditSubDivisionDto, DivisionServiceProxy } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'createOrEditSubDivisionModal',
    templateUrl: './create-or-edit-subdivision.component.html',
    styleUrls: ['./create-or-edit-subdivision.component.less'],

})
export class CreateOrEditSubDivisionModalComponent extends AppComponentBase implements OnInit{
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    subDivision: CreateOrEditSubDivisionDto = new CreateOrEditSubDivisionDto();

    division: any = [];

    constructor(
        injector: Injector,
        private _subDivisionServiceProxy: SubSubDivisionServiceProxy,
        private _divisionServiceProxy: DivisionServiceProxy
    ) {
        super(injector);
    }
    ngOnInit(): void {
        this._divisionServiceProxy.getAllDivisionForDropdown().subscribe(result => {
            this.division = result;
        }, error => { this.spinnerService.hide() });

    }

    show(subDivisionId?: number): void {
        
        if (!subDivisionId) {
            this.subDivision = new CreateOrEditSubDivisionDto();
            this.subDivision.id = subDivisionId;
            this.active = true;
            this.modal.show();
        } else {
            this.subDivision = new CreateOrEditSubDivisionDto();
            this.subDivision.id = subDivisionId;

            this._subDivisionServiceProxy.getSubDivisionForEdit(subDivisionId).subscribe(result => {
                this.subDivision = result.subDivision;
                this.active = true;
                this.modal.show();
            }, error => { this.spinnerService.hide() });
        }
    }

    onShown(): void {
        document.getElementById('Name').focus();
    }

    save(): void {
        this.saving = true;
        this._subDivisionServiceProxy
            .createOrEdit(this.subDivision)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
            }, error => { this.spinnerService.hide() });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
