import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CustomeTagComponent } from './custometag.component';
const routes: Routes = [
    {
        path: '',
        component: CustomeTagComponent,
        pathMatch: 'full',
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class CustomeTagRoutingModule {}
