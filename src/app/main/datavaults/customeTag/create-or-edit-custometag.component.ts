import { Component, EventEmitter, Injector, Output, ViewChild } from '@angular/core';
import { AppConsts } from '@shared/AppConsts';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { map as _map, filter as _filter } from 'lodash-es';
import { CustomeTagServiceProxy, CreateOrEditCustomeTagDto, CommonLookupServiceProxy, CommonLookupDto } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'createOrEditCustomeTagModal',
    templateUrl: './create-or-edit-custometag.component.html',
    //styleUrls: ['./create-or-edit-custometag.component.less'],

})
export class CreateOrEditCustomeTagModalComponent extends AppComponentBase {
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    customeTag: CreateOrEditCustomeTagDto = new CreateOrEditCustomeTagDto();
    TableNameList: CommonLookupDto[];
    TableFieldNameList: CommonLookupDto[];
    constructor(
        injector: Injector,
        private _customeTagServiceProxy: CustomeTagServiceProxy,
        private _commonTagServiceProxy: CommonLookupServiceProxy
    ) {
        super(injector);
    }

    show(customeTagId?: number): void {
        if (!customeTagId) {
            this.customeTag = new CreateOrEditCustomeTagDto();
            this.customeTag.id = customeTagId;
            this.active = true;
            this.modal.show();
        } else {
            this.customeTag = new CreateOrEditCustomeTagDto();
            this.customeTag.id = customeTagId;

            this._customeTagServiceProxy.getCustomeTagForEdit(customeTagId).subscribe(result => {
                this.customeTag = result.customeTag;
                var tblName = this.TableNameList.filter(function (item) {
                    return item.displayName.toLowerCase() == result.customeTag.tagTableName.toLowerCase();
                });

                this.TableName = tblName[0]['displayName'];
                this.customeTag.tagTableName = tblName[0]['labeldata'];
                this.TableFieldName(tblName[0]['labeldata'])

                this.active = true;
                this.modal.show();

            }, error => { this.spinnerService.hide() });
        }
        this.GetTableNameDropdonw();
    }

    GetTableNameDropdonw() {
        this._commonTagServiceProxy.getAllTableForDropdown().subscribe(result => {
            this.TableNameList = result;
        }, error => { this.spinnerService.hide() });
    }
    TableName: string = '';
    GetTableFieldNameDropdonw(event) {
        this.TableName = event.target['options'][event.target['options'].selectedIndex].text;
        this.TableFieldName(this.customeTag.tagTableName)
    }
    TableFieldName(item) {
        this._commonTagServiceProxy.getAllTableFieldForDropdown(item).subscribe(result => {
            this.TableFieldNameList = result;
        }, error => { this.spinnerService.hide() });
    }
    onShown(): void {
        //document.getElementById('Name').focus();
    }

    save(): void {
        
        this.saving = true;
        this.customeTag.tagTableName = this.TableName;
        this._customeTagServiceProxy
            .createOrEdit(this.customeTag)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
            }, error => { this.spinnerService.hide() });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
