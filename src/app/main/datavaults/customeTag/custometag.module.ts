import { NgModule } from '@angular/core';
import { AdminSharedModule } from '@app/admin/shared/admin-shared.module';
import { AppSharedModule } from '@app/shared/app-shared.module';
import { CustomeTagRoutingModule } from './custometag-routing.module'; 
import { CustomeTagComponent } from './custometag.component'; 
import { CustomizableDashboardModule } from '@app/shared/common/customizable-dashboard/customizable-dashboard.module';
import { CreateOrEditCustomeTagModalComponent } from './create-or-edit-custometag.component'; 

@NgModule({
    declarations: [CustomeTagComponent, CreateOrEditCustomeTagModalComponent],
    imports: [AppSharedModule, AdminSharedModule, CustomeTagRoutingModule, CustomizableDashboardModule],
})
export class CustomeTagModule {}
