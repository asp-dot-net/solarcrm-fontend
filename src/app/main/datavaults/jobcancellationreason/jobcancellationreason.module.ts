import { NgModule } from '@angular/core';
import { AdminSharedModule } from '@app/admin/shared/admin-shared.module';
import { AppSharedModule } from '@app/shared/app-shared.module';
import { JobCancellationReasonRoutingModule } from './jobcancellationreason-routing.module';
import { JobCancellationReasonComponent } from './jobcancellationreason.component';
import { CustomizableDashboardModule } from '@app/shared/common/customizable-dashboard/customizable-dashboard.module';
import { CreateOrEditJobCancellationReasonModalComponent } from './create-or-edit-jobcancellationreason.component';

@NgModule({
    declarations: [JobCancellationReasonComponent, CreateOrEditJobCancellationReasonModalComponent],
    imports: [AppSharedModule, AdminSharedModule, JobCancellationReasonRoutingModule, CustomizableDashboardModule],
})
export class JobCancellationReasonModule {}
