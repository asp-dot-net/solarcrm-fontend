import { Component, EventEmitter, Injector, Output, ViewChild } from '@angular/core';
import { AppConsts } from '@shared/AppConsts';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { map as _map, filter as _filter } from 'lodash-es';
import { JobCancellationReasonServiceProxy, CreateOrEditJobCancellationReasonDto } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'createOrEditJobCancellationReasonModal',
    templateUrl: './create-or-edit-jobcancellationreason.component.html',
    styleUrls: ['./create-or-edit-jobcancellationreason.component.less'],

})
export class CreateOrEditJobCancellationReasonModalComponent extends AppComponentBase {
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    jobCancellationReason: CreateOrEditJobCancellationReasonDto = new CreateOrEditJobCancellationReasonDto();

    constructor(
        injector: Injector,
        private _jobCancellationReasonServiceProxy: JobCancellationReasonServiceProxy
    ) {
        super(injector);
    }

    show(jobCancellationReasonId?: number): void {
        if (!jobCancellationReasonId) {
            this.jobCancellationReason = new CreateOrEditJobCancellationReasonDto();
            this.jobCancellationReason.id = jobCancellationReasonId;
            this.active = true;
            this.modal.show();
        } else {
            this.jobCancellationReason = new CreateOrEditJobCancellationReasonDto();
            this.jobCancellationReason.id = jobCancellationReasonId;

            this._jobCancellationReasonServiceProxy.getJobCancellationReasonForEdit(jobCancellationReasonId).subscribe(result => {
                this.jobCancellationReason = result.jobcancellationreason
                this.active = true;
                this.modal.show();
            }, error => { this.spinnerService.hide() });
        }
    }

    onShown(): void {
        document.getElementById('Name').focus();
    }

    save(): void {
        this.saving = true;
        this._jobCancellationReasonServiceProxy
            .createOrEdit(this.jobCancellationReason)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
            }, error => { this.spinnerService.hide() });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
