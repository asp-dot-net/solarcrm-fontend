import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { JobCancellationReasonComponent } from './jobcancellationreason.component';

const routes: Routes = [
    {
        path: '',
        component: JobCancellationReasonComponent,
        pathMatch: 'full',
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class JobCancellationReasonRoutingModule {}
