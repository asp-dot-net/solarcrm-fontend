import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateOrEditPdftemplateModalComponent } from './create-or-edit-pdftemplate.component';
import { PdftemplateComponent } from './pdftemplate.component';

const routes: Routes = [
    {
        path: '',
        component: PdftemplateComponent,
        pathMatch: 'full',
    },
    { path: 'createOrEdit', component: CreateOrEditPdftemplateModalComponent },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class PdftemplateRoutingModule {}
