import { Component, Injector, ViewEncapsulation, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { ActivatedRoute , Router} from '@angular/router';
import { NotifyService } from 'abp-ng2-module';
import { PdfTemplateDto, PdfTemplateServiceProxy, TokenAuthServiceProxy } from '@shared/service-proxies/service-proxies';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import { Title } from '@angular/platform-browser';
import { LazyLoadEvent } from 'primeng/api';
import { PriceServiceProxy, PriceDto } from '@shared/service-proxies/service-proxies';
import { CreateOrEditPdftemplateModalComponent } from './create-or-edit-pdftemplate.component';


@Component({
    templateUrl: './pdftemplate.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()],
})
export class PdftemplateComponent extends AppComponentBase {
    @ViewChild('createOrEditPriceModal', { static: true}) createOrEditPriceModal: CreateOrEditPdftemplateModalComponent;
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    recordCount:any;
    advancedFiltersAreShown = false;
    filterText : string = '';
    public screenWidth: any;  
    public screenHeight: any;  
    testHeight = 200;  
    show: boolean = true;
    toggle: boolean = true;

    toggleBlock() {
        //alert();
        this.show = !this.show;
    };
    change() {
        this.toggle = !this.toggle;
    }

    constructor(
        injector: Injector,
        private _priceService: PriceServiceProxy,
        private _notifyService: NotifyService,
        private _tokenAuth: TokenAuthServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _fileDownloadService: FileDownloadService,
        private titleService: Title,
        private _router: Router,
        private _pdfTemplateService: PdfTemplateServiceProxy,
    ) {
        super(injector);       
    }

    ngOnInit(): void {
        this.screenHeight = window.innerHeight;
    }
    getPdfTemplate(event?: LazyLoadEvent) {
        // if(this.primengTableHelper.shouldResetPaging(event)){
        //     this.paginator.changePage(0);
        //     return;
        // }

        this.primengTableHelper.showLoadingIndicator();
        
        this._pdfTemplateService.getAll(
            this.filterText,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
             this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe((result) => {            
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            this.primengTableHelper.hideLoadingIndicator();
            this.recordCount = result.totalCount;
        }, error => { this.spinnerService.hide() });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    deletePdfTemplate(emailTemplate: PdfTemplateDto): void {
        this.message.confirm(
            this.l('UserDeleteWarningMessage', emailTemplate.pdfTemplateName),
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._pdfTemplateService.delete(emailTemplate.id)
                        .subscribe(() => {
                            this.reloadPage();
                            this.notify.success(this.l('SuccessfullyDeleted'));
                        }, error => { this.spinnerService.hide() });
                }
            }
        );
    }
    createpdfTemplate(id = 0): void {
        
        if (id == 0) {
            this._router.navigate(['/app/main/datavaults/pdftemplate/createOrEdit']);
        }
        else {
            this._router.navigate(['/app/main/datavaults/pdftemplate/createOrEdit'], { queryParams: { eid: id } });
        }
    }
}