import { NgModule } from '@angular/core';
import { AdminSharedModule } from '@app/admin/shared/admin-shared.module';
import { AppSharedModule } from '@app/shared/app-shared.module';
import { CustomizableDashboardModule } from '@app/shared/common/customizable-dashboard/customizable-dashboard.module';
import { PdftemplateComponent } from './pdftemplate.component';
import { CreateOrEditPdftemplateModalComponent } from './create-or-edit-pdftemplate.component';
import { PdftemplateRoutingModule } from './pdftemplate-routing.module';
import { AngularEditorModule } from '@kolkov/angular-editor';
@NgModule({
    declarations: [PdftemplateComponent, CreateOrEditPdftemplateModalComponent],
    imports: [AppSharedModule, AdminSharedModule, PdftemplateRoutingModule, CustomizableDashboardModule,AngularEditorModule],
})
export class PdftemplateModule {}
