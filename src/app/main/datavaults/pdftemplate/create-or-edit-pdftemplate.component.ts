import { Component, EventEmitter, Injector, Output, ViewChild } from '@angular/core';
import { AppConsts } from '@shared/AppConsts';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { map as _map, filter as _filter } from 'lodash-es';
import { PriceServiceProxy, CreateOrEditPriceDto, CommonLookupServiceProxy, CommonLookupDto, OrganizationUnitDto, PdfTemplateServiceProxy, CreateOrEditPdfTemplateDto } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';
import { AngularEditorConfig } from "@kolkov/angular-editor";
import { ActivatedRoute, Router } from '@angular/router';
@Component({
    selector: 'createOrEditPriceModal',
    templateUrl: './create-or-edit-pdftemplate.component.html',
    styleUrls: ['./create-or-edit-pdftemplate.component.less'],

})
export class CreateOrEditPdftemplateModalComponent extends AppComponentBase {
    organizationList: OrganizationUnitDto[];
    saving = false;
    emailData = '';
    active = false;
    pdfTemplate: CreateOrEditPdfTemplateDto = new CreateOrEditPdfTemplateDto();
    organizationUnit = 0;
    constructor(
        injector: Injector,
        private _commonLookupServiceProxy: CommonLookupServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _pdfTemplateServiceProxy: PdfTemplateServiceProxy,
        private _router: Router
    ) {
        super(injector);
    }

    ngOnInit(): void {
        this._activatedRoute.queryParams.subscribe(params => {
            this.pdfTemplate.id = params['eid'];
        }, error => { this.spinnerService.hide() });
        if (this.pdfTemplate.id != 0) {
            this.editorLoaded()
        }
        this.getOrganizationList();
    }
    getOrganizationList() {
        this._commonLookupServiceProxy.getOrganizationUnitUserWise().subscribe(result => {
            this.organizationList = result;
        }, error => { this.spinnerService.hide() });
    }
    cancel(): void {
        this._router.navigate(['app/main/datavaults/pdftemplate']);
    }
    editorLoaded() {        
        if ((this.pdfTemplate.id !== null && this.pdfTemplate.id !== undefined)) {
            this._pdfTemplateServiceProxy.getPdfTemplateForEdit(this.pdfTemplate.id).subscribe(result => {
                this.htmlContentbidy = result.pdfTemplates.viewHtml;
                //  this.emailTemplate.subject = result.emailTemplates.subject;
                this.pdfTemplate.pdfTemplateName = result.pdfTemplates.pdfTemplateName;
                this.pdfTemplate.viewHtml = result.pdfTemplates.viewHtml;
                this.pdfTemplate.apiHtml = result.pdfTemplates.apiHtml;
                this.pdfTemplate.organizationUnitId = result.pdfTemplates.organizationUnitId;
                if (this.htmlContentbidy != "") {
                    if (this.pdfTemplate.id == 0) {
                        this.htmlContentbidy.mergeTags = ({
                            customer: {
                                name: "Customer",
                                mergeTags: {
                                    customer_name: {
                                        name: "Name",
                                        value: "{{Customer.Name}}"
                                    },
                                    address: {
                                        name: "Address",
                                        value: "{{Customer.Address}}"
                                    },
                                    mobile: {
                                        name: "Mobile",
                                        value: "{{Customer.Mobile}}"
                                    },
                                    email: {
                                        name: "Email",
                                        value: "{{Customer.Email}}"
                                    },
                                    phone: {
                                        name: "Phone",
                                        value: "{{Customer.Phone}}"
                                    }
                                }
                            },
                            salesrep: {
                                name: "Sales Rep",
                                mergeTags: {
                                    customer_name: {
                                        name: "Name",
                                        value: "{{Sales.Name}}"
                                    },
                                    mobile: {
                                        name: "Mobile",
                                        value: "{{Sales.Mobile}}"
                                    },
                                    email: {
                                        name: "Email",
                                        value: "{{Sales.Email}}"
                                    }
                                }
                            },
                            quote: {
                                name: "Quote",
                                mergeTags: {
                                    project_no: {
                                        name: "Project No",
                                        value: "{{Quote.ProjectNo}}"
                                    },
                                    quote_systemCapacity: {
                                        name: "System Capacity",
                                        value: "{{Quote.SystemCapacity}}"
                                    },
                                    quote_allproductItem: {
                                        name: "All product Item",
                                        value: "{{Quote.AllproductItem}}"
                                    },
                                    quote_totalQuoteprice: {
                                        name: "Total Quote price",
                                        value: "{{Quote.TotalQuoteprice}}"
                                    },
                                    quote_installationDate: {
                                        name: "Installation Date",
                                        value: "{{Quote.InstallationDate}}"
                                    },
                                    quote_InstallerName: {
                                        name: "Installer Name",
                                        value: "{{Quote.InstallerName}}"
                                    },
                                }
                            },
                            freebies: {
                                name: "Freebies",
                                mergeTags: {
                                    freebies_transportcompany: {
                                        name: "Transport Company",
                                        value: "{{Freebies.TransportCompany}}"
                                    },
                                    freebies_transportlink: {
                                        name: "Transport Link",
                                        value: "{{Freebies.TransportLink}}"
                                    },
                                    freebies_dispatchedDate: {
                                        name: "Dispatched Date",
                                        value: "{{Freebies.DispatchedDate}}"
                                    },
                                    freebies_trackingNo: {
                                        name: "TrackingNo",
                                        value: "{{Freebies.TrackingNo}}"
                                    },
                                    freebies_promotype: {
                                        name: "TrackingNo",
                                        value: "{{Freebies.PromoType}}"
                                    },
                                }
                            },
                            invoice: {
                                name: "User",
                                mergeTags: {
                                    freebies_transportcompany: {
                                        name: "User Name",
                                        value: "{{Invoice.UserName}}"
                                    },
                                    freebies_transportlink: {
                                        name: "User Mobile",
                                        value: "{{Invoice.UserMobile}}"
                                    },
                                    freebies_dispatchedDate: {
                                        name: "User Email",
                                        value: "{{Invoice.UserEmail}}"
                                    },
                                }
                            },
                            Organization: {
                                name: "Organization",
                                mergeTags: {
                                    org_name: {
                                        name: "Organization Name",
                                        value: "{{Organization.orgName}}"
                                    },
                                    org_email: {
                                        name: "Organization Email",
                                        value: "{{Organization.orgEmail}}"
                                    },
                                    org_transportlink: {
                                        name: "Organization Mobile",
                                        value: "{{Organization.orgMobile}}"
                                    },
                                    org_dispatchedDate: {
                                        name: "Organization logo",
                                        value: "{{Organization.orglogo}}"
                                    },
                                }
                            },
                            Review: {
                                name: "Review",
                                mergeTags: {
                                    review_googlelink: {
                                        name: "Review link",
                                        value: "{{Review.link}}"
                                    },
                                }
                            },
                        });
                    }
                    //     else {
                    //         this.emailEditor.editor.setMergeTags({
                    //             customer: {
                    //                 name: "Customer",
                    //                 mergeTags: {
                    //                     customer_name: {
                    //                         name: "Name",
                    //                         value: "{{Customer.Name}}"
                    //                     },
                    //                     address: {
                    //                         name: "Address",
                    //                         value: "{{Customer.Address}}"
                    //                     },
                    //                     mobile: {
                    //                         name: "Mobile",
                    //                         value: "{{Customer.Mobile}}"
                    //                     },
                    //                     email: {
                    //                         name: "Email",
                    //                         value: "{{Customer.Email}}"
                    //                     },
                    //                     phone: {
                    //                         name: "Phone",
                    //                         value: "{{Customer.Phone}}"
                    //                     },

                    //                 }
                    //             },
                    //             salesrep: {
                    //                 name: "Sales Rep",
                    //                 mergeTags: {
                    //                     customer_name: {
                    //                         name: "Name",
                    //                         value: "{{Sales.Name}}"
                    //                     },
                    //                     mobile: {
                    //                         name: "Mobile",
                    //                         value: "{{Sales.Mobile}}"
                    //                     },
                    //                     email: {
                    //                         name: "Email",
                    //                         value: "{{Sales.Email}}"
                    //                     }
                    //                 }
                    //             },
                    //             quote: {
                    //                 name: "Quote",
                    //                 mergeTags: {
                    //                     project_no: {
                    //                         name: "Project No",
                    //                         value: "{{Quote.ProjectNo}}"
                    //                     },
                    //                     quote_systemCapacity: {
                    //                         name: "System Capacity",
                    //                         value: "{{Quote.SystemCapacity}}"
                    //                     },
                    //                     quote_allproductItem: {
                    //                         name: "All product Item",
                    //                         value: "{{Quote.AllproductItem}}"
                    //                     },
                    //                     quote_totalQuoteprice: {
                    //                         name: "Total Quote price",
                    //                         value: "{{Quote.TotalQuoteprice}}"
                    //                     },
                    //                     quote_installationDate: {
                    //                         name: "Installation Date",
                    //                         value: "{{Quote.InstallationDate}}"
                    //                     },
                    //                     quote_InstallerName: {
                    //                         name: "Installer Name",
                    //                         value: "{{Quote.InstallerName}}"
                    //                     },
                    //                 }
                    //             },
                    //             freebies: {
                    //                 name: "Freebies",
                    //                 mergeTags: {
                    //                     freebies_transportcompany: {
                    //                         name: "Transport Company",
                    //                         value: "{{Freebies.TransportCompany}}"
                    //                     },
                    //                     freebies_transportlink: {
                    //                         name: "Transport Link",
                    //                         value: "{{Freebies.TransportLink}}"
                    //                     },
                    //                     freebies_dispatchedDate: {
                    //                         name: "Dispatched Date",
                    //                         value: "{{Freebies.DispatchedDate}}"
                    //                     },
                    //                     freebies_trackingNo: {
                    //                         name: "TrackingNo",
                    //                         value: "{{Freebies.TrackingNo}}"
                    //                     },
                    //                     freebies_promotype: {
                    //                         name: "TrackingNo",
                    //                         value: "{{Freebies.PromoType}}"
                    //                     },
                    //                 }
                    //             },
                    //             invoice: {
                    //                 name: "User",
                    //                 mergeTags: {
                    //                     invoice_userName: {
                    //                         name: "User Name",
                    //                         value: "{{Invoice.UserName}}"
                    //                     },
                    //                     invoice_userMobile: {
                    //                         name: "User Mobile",
                    //                         value: "{{Invoice.UserMobile}}"
                    //                     },
                    //                     invoice_userEmail: {
                    //                         name: "User Email",
                    //                         value: "{{Invoice.UserEmail}}"
                    //                     },
                    //                     invoice_owning: {
                    //                         name: "Owning",
                    //                         value: "{{Invoice.Owning}}"
                    //                     },
                    //                 }
                    //             },
                    //             Organization: {
                    //                 name: "Organization",
                    //                 mergeTags: {
                    //                     org_name: {
                    //                         name: "Organization Name",
                    //                         value: "{{Organization.orgName}}"
                    //                     },
                    //                     org_email: {
                    //                         name: "Organization Email",
                    //                         value: "{{Organization.orgEmail}}"
                    //                     },
                    //                     org_transportlink: {
                    //                         name: "Organization Mobile",
                    //                         value: "{{Organization.orgMobile}}"
                    //                     },
                    //                     org_dispatchedDate: {
                    //                         name: "Organization logo",
                    //                         value: "{{Organization.orglogo}}"
                    //                     },
                    //                 }
                    //             },
                    //             Review: {
                    //                 name: "Review",
                    //                 mergeTags: {
                    //                     review_googlelink: {
                    //                         name: "Review link",
                    //                         value: "{{Review.link}}"
                    //                     },
                    //                 }
                    //             },
                    //         });
                    //     }

                    //     this.emailEditor.editor.loadDesign(JSON.parse(this.emailData));
                }
            }, error => { this.spinnerService.hide() });
        }
    }
    htmlContentbidy: any = '';

    config: AngularEditorConfig = {
        editable: true,
        spellcheck: true,
        height: "72vh",
        minHeight: "15rem",
        placeholder: "Enter text here...",
        translate: "no",
        defaultParagraphSeparator: "p",
        defaultFontName: "Arial",
        toolbarHiddenButtons: [["bold"]],
        sanitize: false,
        customClasses: [
            {
                name: "quote",
                class: "quote"
            },
            {
                name: "redText",
                class: "redText"
            },
            {
                name: "titleText",
                class: "titleText",
                tag: "h1"
            }
        ],
        uploadUrl: 'v1/image',
        //upload: (file: File) => { ... }
        uploadWithCredentials: false,
        //sanitize: true,
        toolbarPosition: 'top',
        // toolbarHiddenButtons: [
        //     ['bold', 'italic'],
        //     ['fontSize']
        // ]
    };

    onTagChange(event): void {

        const id = event.target.value;
        if (id == 1) {
            if (this.htmlContentbidy != null) {
                this.htmlContentbidy = this.htmlContentbidy + " " + "{{Customer.Name}}";
            } else {
                this.htmlContentbidy = "{{Customer.Name}}";
            }

        } else if (id == 2) {
            if (this.htmlContentbidy != null) {
                this.htmlContentbidy = this.htmlContentbidy + " " + "{{Customer.Mobile}}";
            } else {
                this.htmlContentbidy = "{{Customer.Mobile}}";
            }
        } else if (id == 3) {
            if (this.htmlContentbidy != null) {
                this.htmlContentbidy = this.htmlContentbidy + " " + "{{Customer.Phone}}";
            } else {
                this.htmlContentbidy = "{{Customer.Phone}}";
            }
        } else if (id == 4) {
            if (this.htmlContentbidy != null) {
                this.htmlContentbidy = this.htmlContentbidy + " " + "{{Customer.Email}}";
            } else {
                this.htmlContentbidy = "{{Customer.Email}}";
            }
        } else if (id == 5) {

            if (this.htmlContentbidy != null) {
                this.htmlContentbidy = this.htmlContentbidy + " " + "{{Customer.Address}}";
            } else {
                this.htmlContentbidy = "{{Customer.Address}}";
            }
        }
    }

    // saveDesign() {
    //     this.emailEditor.editor.saveDesign((data) =>
    //         this.saveHTML(data)
    //     );
    // }

    saveHTML() {
        
        //this.pdfTemplate.viewHtml = this.htmlContentbidy; //JSON.stringify(data);
        this._pdfTemplateServiceProxy.createOrEdit(this.pdfTemplate)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                //this.notify.info(this.l('SavedSuccessfully'));
                this._router.navigate(['/app/main/datavaults/pdftemplate']);
            }, error => { this.spinnerService.hide() });
    }
}