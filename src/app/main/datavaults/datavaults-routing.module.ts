import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '',
                children: [
                    {
                        path: 'locations',
                        loadChildren: () => import('./locations/locations.module').then(m => m.LocationsModule),
                        data: { permission: 'Pages.Tenant.DataVaults.Locations', features: 'App.DataVaults.Locations' },
                    },
                    {
                        path: 'division',
                        loadChildren: () => import('./division/division.module').then(m => m.DivisionModule),
                        data: { permission: 'Pages.Tenant.DataVaults.Division', features: 'App.DataVaults.Division' },
                    },
                    {
                        path: 'subdivision',
                        loadChildren: () => import('./subdivision/subdivision.module').then(m => m.SubDivisionModule),
                        data: { permission: 'Pages.Tenant.DataVaults.SubDivision', features: 'App.DataVaults.SubDivision' },
                    },
                    {
                        path: 'leadsource',
                        loadChildren: () => import('./leadsource/leadsource.module').then(m => m.LeadSourceModule),
                        data: { permission: 'Pages.Tenant.DataVaults.LeadSource', features: 'App.DataVaults.LeadSource'  },
                    },
                    {
                        path: 'leadtype',
                        loadChildren: () => import('./leadtype/leadtype.module').then(m => m.LeadTypeModule),
                        data: { permission: 'Pages.Tenant.DataVaults.LeadType', features: 'App.DataVaults.LeadType' },
                    },
                    {
                        path: 'documentlist',
                        loadChildren: () => import('./documentlist/documentlist.module').then(m => m.DocumentListModule),
                        data: { permission: 'Pages.Tenant.DataVaults.DocumentList', features: 'App.DataVaults.DocumentList' },
                    },
                    {
                        path: 'discom',
                        loadChildren: () => import('./discom/discom.module').then(m => m.DiscomModule),
                        data: { permission: 'Pages.Tenant.DataVaults.Discom', features: 'App.DataVaults.Discom' },
                    },
                    {
                        path: 'teams',
                        loadChildren: () => import('./teams/teams.module').then(m => m.TeamsModule),
                        data: { permission: 'Pages.Tenant.DataVaults.Teams', features: 'App.DataVaults.Teams' },
                    },
                    {
                        path: 'projecttype',
                        loadChildren: () => import('./projecttype/projecttype.module').then(m => m.ProjectTypeModule),
                        data: { permission: 'Pages.Tenant.DataVaults.ProjectType', features: 'App.DataVaults.ProjectType' },
                    },
                    {
                        path: 'dispatchtype',
                        loadChildren: () => import('./dispatchtype/dispatchtype.module').then(m => m.DispatchTypeTypeModule),
                        data: { permission: 'Pages.Tenant.DataVaults.DispatchType', features: 'App.DataVaults.DispatchType' },
                    },
                    {
                        path: 'projectstatus',
                        loadChildren: () => import('./projectstatus/projectstatus.module').then(m => m.ProjectStatusModule),
                        data: { permission: 'Pages.Tenant.DataVaults.ProjectStatus', features: 'App.DataVaults.ProjectStatus' },
                    },
                    {
                        path: 'stockcategory',
                        loadChildren: () => import('./stockcategory/stockcategory.module').then(m => m.StockCategoryModule),
                        data: { permission: 'Pages.Tenant.DataVaults.StockCategory', features: 'App.DataVaults.StockCategory' },
                    },
                    {
                        path: 'invoicetype',
                        loadChildren: () => import('./invoicetype/invoicetype.module').then(m => m.InvoiceTypeModule),
                        data: { permission: 'Pages.Tenant.DataVaults.InvoiceType', features: 'App.DataVaults.InvoiceType' },
                    },
                    
                    {
                        path: 'state',
                        loadChildren: () => import('./state/state.module').then(m => m.StateModule),
                        data: { permission: 'Pages.Tenant.DataVaults.State', features: 'App.DataVaults.State' },
                    },
                    {
                        path: 'district',
                        loadChildren: () => import('./district/district.module').then(m => m.DistrictModule),
                        data: { permission: 'Pages.Tenant.DataVaults.District', features: 'App.DataVaults.District' },
                    },
                    {
                        path: 'taluka',
                        loadChildren: () => import('./taluka/taluka.module').then(m => m.TalukaModule),
                        data: { permission: 'Pages.Tenant.DataVaults.Taluka', features: 'App.DataVaults.Taluka' },
                    },
                    {
                        path: 'city',
                        loadChildren: () => import('./city/city.module').then(m => m.CityModule),
                        data: { permission: 'Pages.Tenant.DataVaults.City', features: 'App.DataVaults.City' },
                    },
                    {
                        path: 'cancelreasons',
                        loadChildren: () => import('./cancelreasons/cancelreasons.module').then(m => m.CancelReasonsModule),
                        data: { permission: 'Pages.Tenant.DataVaults.CancelReasons', features: 'App.DataVaults.CancelReasons' },
                    },
                    {
                        path: 'holdreasons',
                        loadChildren: () => import('./holdreasons/holdreasons.module').then(m => m.HoldReasonsModule),
                        data: { permission: 'Pages.Tenant.DataVaults.CancelReasons', features: 'App.DataVaults.CancelReasons' },
                    },
                    {
                        path: 'rejectreasons',
                        loadChildren: () => import('./rejectreasons/rejectreasons.module').then(m => m.RejectReasonsModule),
                        data: { permission: 'Pages.Tenant.DataVaults.RejectReasons', features: 'App.DataVaults.RejectReasons' },
                    },  
                    {
                        path: 'refundreason',
                        loadChildren: () => import('./refundreasons/refundreasons.module').then(m => m.RefundReasonsModule),
                        data: { permission: 'Pages.Tenant.DataVaults.RefundReasons', features: 'App.DataVaults.RefundReasons' },
                    },                    
                    {
                        path: 'jobcancellationreason',
                        loadChildren: () => import('./jobcancellationreason/jobcancellationreason.module').then(m => m.JobCancellationReasonModule),
                        data: { permission: 'Pages.Tenant.DataVaults.JobCancellationReason', features: 'App.DataVaults.JobCancellationReason' },
                    },
                    {
                        path: 'stockitem',
                        loadChildren: () => import('./stockitem/stockitem.module').then(m => m.StockItemModule),
                        data: { permission: 'Pages.Tenant.DataVaults.StockItem', features: 'App.DataVaults.StockItem' },
                    },
                    {
                        path: 'documentlibrary',
                        loadChildren: () => import('./documentlibrary/documentlibrary.module').then(m => m.DocumentLibraryModule),
                        data: { permission: 'Pages.Tenant.DataVaults.DocumentLibrary', features: 'App.DataVaults.DocumentLibrary' },
                    },
                    {
                        path: 'emailtemplate',
                        loadChildren: () => import('./emailtemplate/emailtemplate.module').then(m => m.EmailTemplateModule),
                        data: { permission: 'Pages.Tenant.DataVaults.EmailTemplates', features: 'App.DataVaults.EmailTemplates' },
                    },                   
                     {
                         path: 'smstemplate',
                         loadChildren: () => import('./smstemplate/smstemplate.module').then(m => m.SmsTemplateModule),
                         data: { permission: 'Pages.Tenant.DataVaults.SmsTemplates', features: 'App.DataVaults.SmsTemplates' },
                     },
                     {
                        path: 'price',
                        loadChildren: () => import('./price/price.module').then(m => m.PriceModule),
                        data: { permission: 'Pages.Tenant.DataVaults.Prices', features: 'App.DataVaults.Prices' },
                    },
                    {
                        path: 'tender',
                        loadChildren: () => import('./tender/tender.module').then(m => m.TenderModule),
                        data: { permission: 'Pages.Tenant.DataVaults.Tender', features: 'App.DataVaults.Tender' },
                    },
                    {
                        path: 'circle',
                        loadChildren: () => import('./circle/circle.module').then(m => m.CircleModule),
                        data: { permission: 'Pages.Tenant.DataVaults.Circle', features: 'App.DataVaults.Circle' },
                    },
                     {
                        path: 'solartype',
                        loadChildren: () => import('./solartype/solartype.module').then(m => m.SolarTypeModule),
                        data: { permission: 'Pages.Tenant.DataVaults.SolarType', features: 'App.DataVaults.SolarType' },
                    },
                    {
                        path: 'heightstructure',
                        loadChildren: () => import('./heightstructure/heightstructure.module').then(m => m.HeightStructureModule),
                        data: { permission: 'Pages.Tenant.DataVaults.HeightStructure', features: 'App.DataVaults.HeightStructure' },
                    }, 
                    {
                        path: 'department',
                        loadChildren: () => import('./department/department.module').then(m => m.DepartmentModule),
                        data: { permission: 'Pages.Tenant.DataVaults.Department', features: 'App.DataVaults.Department' },
                    },
                    {
                        path: 'activitylog',
                        loadChildren: () => import('./activitylog/activitylog.module').then(m => m.ActivitylogModule),
                        data: { permission: 'Pages.Tenant.DataVaults.ActivityLogMasterVault', features: 'App.DataVaults.ActivityLogMasterVault' },
                    },
                    {
                        path: 'variation',
                        loadChildren: () => import('./variation/variation.module').then(m => m.VariationModule),
                        data: { permission: 'Pages.Tenant.DataVaults.Variation', features: 'App.DataVaults.Variation' },
                    },   
                    {
                        path: 'custometag',
                        loadChildren: () => import('./customeTag/custometag.module').then(m => m.CustomeTagModule),
                        data: { permission: 'Pages.Tenant.DataVaults.CustomeTag', features: 'App.DataVaults.CustomeTag' },
                    },
                    {
                        path: 'pdftemplate',
                        loadChildren: () => import('./pdftemplate/pdftemplate.module').then(m => m.PdftemplateModule),
                        data: { permission: 'Pages.Tenant.DataVaults.CustomeTag', features: 'App.DataVaults.CustomeTag' },
                    }, 
                    {
                        path: 'paymenttype',
                        loadChildren: () => import('./paymentType/paymenttype.module').then(m => m.PaymentTypeModule),
                        data: { permission: 'Pages.Tenant.DataVaults.CustomeTag', features: 'App.DataVaults.CustomeTag' }
                    }  ,
                    {
                        path: 'paymentMode',
                        loadChildren: () => import('./paymentMode/paymentMode.module').then(m => m.PaymentModeModule),
                        data: { permission: 'Pages.Tenant.DataVaults.PaymentMode', features: 'App.DataVaults.PaymentMode' }
                    },    
                    {
                        path: 'billofmaterial',
                        loadChildren: () => import('./billofmaterial/billofmaterial.module').then(m => m.BillofMaterialModule),
                        data: { permission: 'Pages.Tenant.DataVaults.BillOfMaterial', features: 'App.DataVaults.BillOfMaterial' }
                    }                      
                ]
            },
        ]),
    ],
    exports: [RouterModule],
})
export class DataVaultsRoutingModule { }
