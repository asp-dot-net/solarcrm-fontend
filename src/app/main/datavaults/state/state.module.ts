import { NgModule } from '@angular/core';
import { AdminSharedModule } from '@app/admin/shared/admin-shared.module';
import { AppSharedModule } from '@app/shared/app-shared.module';
import { StateRoutingModule } from './state-routing.module';
import { StateComponent } from './state.component';
import { CustomizableDashboardModule } from '@app/shared/common/customizable-dashboard/customizable-dashboard.module';
import { CreateOrEditStateModalComponent } from './create-or-edit-state.component';

@NgModule({
    declarations: [StateComponent, CreateOrEditStateModalComponent],
    imports: [AppSharedModule, AdminSharedModule, StateRoutingModule, CustomizableDashboardModule],
})
export class StateModule {}
