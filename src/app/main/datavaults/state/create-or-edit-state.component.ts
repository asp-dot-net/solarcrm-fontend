import { Component, EventEmitter, Injector, Output, ViewChild } from '@angular/core';
import { AppConsts } from '@shared/AppConsts';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { map as _map, filter as _filter } from 'lodash-es';
import { StateServiceProxy, CreateOrEditStateDto } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'createOrEditStateModal',
    templateUrl: './create-or-edit-state.component.html',
    styleUrls: ['./create-or-edit-state.component.less'],

})
export class CreateOrEditStateModalComponent extends AppComponentBase {
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    states: CreateOrEditStateDto = new CreateOrEditStateDto();

    constructor(
        injector: Injector,
        private _stateServiceProxy: StateServiceProxy
    ) {
        super(injector);
    }

    show(stateId?: number): void {
        if (!stateId) {
            this.states = new CreateOrEditStateDto();
            this.states.id = stateId;
            this.active = true;
            this.modal.show();
        } else {
            this.states = new CreateOrEditStateDto();
            this.states.id = stateId;

            this._stateServiceProxy.getStateForEdit(stateId).subscribe(result => {
                this.states = result.states;
                this.active = true;
                this.modal.show();
            }, error => { this.spinnerService.hide() });
        }
    }

    onShown(): void {
        document.getElementById('Name').focus();
    }

    save(): void {
        this.saving = true;
        this._stateServiceProxy
            .createOrEdit(this.states)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
            }, error => { this.spinnerService.hide() });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
