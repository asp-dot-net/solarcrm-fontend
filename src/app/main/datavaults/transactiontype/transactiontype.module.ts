import { NgModule } from '@angular/core';
import { AdminSharedModule } from '@app/admin/shared/admin-shared.module';
import { AppSharedModule } from '@app/shared/app-shared.module';
import { TransactionTypeRoutingModule } from './transactiontype-routing.module';
import { TransactionTypeComponent } from './transactiontype.component';
import { CustomizableDashboardModule } from '@app/shared/common/customizable-dashboard/customizable-dashboard.module';
import { CreateOrEditTransactionTypeModalComponent } from './create-or-edit-transactiontype.component';

@NgModule({
    declarations: [TransactionTypeComponent, CreateOrEditTransactionTypeModalComponent],
    imports: [AppSharedModule, AdminSharedModule, TransactionTypeRoutingModule, CustomizableDashboardModule],
})
export class TransactionTypeModule {}
