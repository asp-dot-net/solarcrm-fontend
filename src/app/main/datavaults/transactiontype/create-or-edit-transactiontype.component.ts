import { Component, EventEmitter, Injector, Output, ViewChild } from '@angular/core';
import { AppConsts } from '@shared/AppConsts';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { map as _map, filter as _filter } from 'lodash-es';
import { TransactionTypeServiceProxy, CreateOrEditTransactionTypeDto  } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'createOrEditTransactionTypeModal',
    templateUrl: './create-or-edit-transactiontype.component.html',
    styleUrls: ['./create-or-edit-transactiontype.component.less'],
    
})
export class CreateOrEditTransactionTypeModalComponent extends AppComponentBase{
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    
    active = false;
    saving = false;

    transactionType: CreateOrEditTransactionTypeDto = new CreateOrEditTransactionTypeDto();

    constructor(
        injector: Injector,
        private _transactionTypeServiceProxy: TransactionTypeServiceProxy
    ) {
        super(injector);
    }

    show(transactionTypeId?: number): void {
        if (!transactionTypeId) {
            this.transactionType = new CreateOrEditTransactionTypeDto();
            this.transactionType.id = transactionTypeId;
            this.active = true;
            this.modal.show();
        } else {
            this.transactionType = new CreateOrEditTransactionTypeDto();
            this.transactionType.id = transactionTypeId;

            this._transactionTypeServiceProxy.getTransactionTypeForEdit(transactionTypeId).subscribe(result => {
                this.transactionType = result.transactionType;
                this.active = true;
                this.modal.show();
            }, error => { this.spinnerService.hide() });
        }
    }

    onShown(): void {
        document.getElementById('Name').focus();
    }

    save(): void {
        this.saving = true;
        this._transactionTypeServiceProxy
            .createOrEdit(this.transactionType)
            .pipe(
                finalize(() => { this.saving = false;
                })
            )
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
            }, error => { this.spinnerService.hide() });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
