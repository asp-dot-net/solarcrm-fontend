import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TransactionTypeComponent } from './transactiontype.component';

const routes: Routes = [
    {
        path: '',
        component: TransactionTypeComponent,
        pathMatch: 'full',
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class TransactionTypeRoutingModule {}
