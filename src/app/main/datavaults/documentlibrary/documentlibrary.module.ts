import { NgModule } from '@angular/core';
import { AdminSharedModule } from '@app/admin/shared/admin-shared.module';
import { AppSharedModule } from '@app/shared/app-shared.module';
import { DocumentLibraryRoutingModule } from './documentlibrary-routing.module';
import { DocumentLibraryComponent } from './documentlibrary.component';
import { CustomizableDashboardModule } from '@app/shared/common/customizable-dashboard/customizable-dashboard.module';
import { CreateOrEditDocumentLibraryModalComponent } from './create-or-edit-documentlibrary.component';
import { FileUploadModule } from 'ng2-file-upload';
import { FileUploadModule as PrimeNgFileUploadModule } from 'primeng/fileupload';
@NgModule({
    declarations: [DocumentLibraryComponent, CreateOrEditDocumentLibraryModalComponent],
    imports: [AppSharedModule, AdminSharedModule, DocumentLibraryRoutingModule, CustomizableDashboardModule,
        FileUploadModule,PrimeNgFileUploadModule],
})
export class DocumentLibraryModule {}
