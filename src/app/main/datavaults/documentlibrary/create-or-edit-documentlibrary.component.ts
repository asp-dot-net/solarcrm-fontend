import { Component, ViewChild, Injector, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { AppComponentBase } from '@shared/common/app-component-base';
//import * as moment from 'moment';
import { FileUploader } from 'ng2-file-upload';
import { AppConsts } from '@shared/AppConsts';
import { DocumentLibraryServiceProxy, CreateOrEditDocumentLibraryDto } from '@shared/service-proxies/service-proxies';


@Component({
    selector: 'createOrEditDocumentLibraryModal',
    templateUrl: './create-or-edit-documentlibrary.component.html',
    styleUrls: ['./create-or-edit-documentlibrary.component.less'],

})
export class CreateOrEditDocumentLibraryModalComponent extends AppComponentBase {
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;
    uploadUrl: string;
    documentLibrary: CreateOrEditDocumentLibraryDto = new CreateOrEditDocumentLibraryDto();

    constructor(
        injector: Injector,
        private _documentLibraryServiceProxy: DocumentLibraryServiceProxy
    ) {
        super(injector);
    }

    show(documentId?: number): void {

        if (!documentId) {
            this.documentLibrary = new CreateOrEditDocumentLibraryDto();
            this.documentLibrary.id = 0;
            this.active = true;
            this.modal.show();
        } else {
            this.documentLibrary = new CreateOrEditDocumentLibraryDto();
            this.documentLibrary.id = documentId;

            this._documentLibraryServiceProxy.getDocumentLibraryForEdit(documentId).subscribe(result => {
                this.documentLibrary = result.documentLibrary;
                this.changeurl();
                this.active = true;
                this.modal.show();
            }, error => { this.spinnerService.hide() });
        }
    }
    uploadedFiles: any[] = [];

    onShown(): void {

        document.getElementById('Department_Name').focus();
    }

    // upload completed event
    onUpload(event): void {

        for (const file of event.files) {
            this.uploadedFiles.push(file);
            this.modal.hide();
            this.uploadedFiles = [];
            this.notify.info(this.l('SavedSuccessfully'));
            this.close();
            this.modalSave.emit(null);
        }
    }

    onBeforeSend(event): void {
        event.xhr.setRequestHeader('Authorization', 'Bearer ' + abp.auth.getToken());
    }

    downfile(): void {

        let FileName = AppConsts.docUrl + "/" + this.documentLibrary.filePath;
        window.open(FileName, "_blank");
    };

    changeurl() {

        if (this.documentLibrary.id == 0 && this.documentLibrary.id == null) {
            this.uploadUrl = AppConsts.remoteServiceBaseUrl + '/DemoUiComponents/UploadDocumentLibrary?id=' + 0 + '&title=' + this.documentLibrary.title;
        } else {
            this.uploadUrl = AppConsts.remoteServiceBaseUrl + '/DemoUiComponents/UploadDocumentLibrary?id=' + this.documentLibrary.id + '&title=' + this.documentLibrary.title;
        }

    }
    save(): void {

        this.saving = true;
        this.documentLibrary.fileName = this.uploadedFiles[0].filname;
        this.documentLibrary.fileType = '1';
        this._documentLibraryServiceProxy
            .createOrEdit(this.documentLibrary)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
            }, error => { this.spinnerService.hide() });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
