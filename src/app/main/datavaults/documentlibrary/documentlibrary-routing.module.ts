import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DocumentLibraryComponent } from './documentlibrary.component';

const routes: Routes = [
    {
        path: '',
        component: DocumentLibraryComponent,
        pathMatch: 'full',
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class DocumentLibraryRoutingModule {}
