import { Component, EventEmitter, Injector, Output, ViewChild } from '@angular/core';
import { AppConsts } from '@shared/AppConsts';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { map as _map, filter as _filter } from 'lodash-es';
import { VariationServiceProxy, CreateOrEditVariationDto  } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'createOrEditVariationModal',
    templateUrl: './create-or-edit-variation.component.html',
    styleUrls: ['./create-or-edit-variation.component.less'],
    
})
export class CreateOrEditVariationModalComponent extends AppComponentBase{
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    
    active = false;
    saving = false;

    variation: CreateOrEditVariationDto = new CreateOrEditVariationDto();

    constructor(
        injector: Injector,
        private _variationServiceProxy: VariationServiceProxy
    ) {
        super(injector);
    }

    show(variationId?: number): void {
       
        if (!variationId) {
            this.variation = new CreateOrEditVariationDto();
            this.variation.id = variationId;
            this.active = true;
            this.modal.show();
        } else {
            this.variation = new CreateOrEditVariationDto();
            this.variation.id = variationId;

            this._variationServiceProxy.getVariationForEdit(variationId).subscribe(result => {
                this.variation = result.variation;
                this.active = true;
                this.modal.show();
            }, error => { this.spinnerService.hide() });
        }
    }

    onShown(): void {
        document.getElementById('Name').focus();
    }

    save(): void {
        this.saving = true;
        this._variationServiceProxy
            .createOrEdit(this.variation)
            .pipe(
                finalize(() => { this.saving = false;
                })
            )
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
            }, error => { this.spinnerService.hide() });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
