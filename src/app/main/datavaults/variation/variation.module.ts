import { NgModule } from '@angular/core';
import { AdminSharedModule } from '@app/admin/shared/admin-shared.module';
import { AppSharedModule } from '@app/shared/app-shared.module';
import { VariationRoutingModule } from './variation-routing.module';
import { VariationComponent } from './variation.component'; 
import { CustomizableDashboardModule } from '@app/shared/common/customizable-dashboard/customizable-dashboard.module';
import { CreateOrEditVariationModalComponent } from './create-or-edit-variation.component';

@NgModule({
    declarations: [VariationComponent, CreateOrEditVariationModalComponent],
    imports: [AppSharedModule, AdminSharedModule, VariationRoutingModule, CustomizableDashboardModule],
})
export class VariationModule {}
