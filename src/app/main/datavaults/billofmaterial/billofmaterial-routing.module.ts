import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BillOfMaterialComponent } from './billofmaterial.component';
const routes: Routes = [
    {
        path: '',
        component: BillOfMaterialComponent,
        pathMatch: 'full',
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class BillofMaterialRoutingModule {}
