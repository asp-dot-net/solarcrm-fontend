import { Component, EventEmitter, Injector, Output, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { map as _map, filter as _filter } from 'lodash-es';
import { BillOfMaterialServiceProxy, CommonLookupDto, CommonLookupServiceProxy, CreateOrEditBillOfMaterialDto } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'createOrEditBillOfMaterialModal',
    templateUrl: './create-or-edit-billofmaterial.component.html',
    //styleUrls: ['./create-or-edit-billofmaterial.component.less'],

})
export class CreateOrEditBillOfMaterialModalComponent extends AppComponentBase {
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;
    bill: CreateOrEditBillOfMaterialDto = new CreateOrEditBillOfMaterialDto();
    drpStockItem: CommonLookupDto[];
    drpHeightStructureItem: CommonLookupDto[];
    constructor(
        injector: Injector,
        private _billOfMaterialServiceProxy: BillOfMaterialServiceProxy,
        private _commonLookupServiceProxy: CommonLookupServiceProxy,

    ) {
        super(injector);
    }
    ngOnInit(): void {
        this._commonLookupServiceProxy.getAllStockItemForDropdown().subscribe(result => {
            this.drpStockItem = result;
        }, error => { this.spinnerService.hide() });

        this._commonLookupServiceProxy.getAllHeightStructureForDropdown().subscribe(result => {
            this.drpHeightStructureItem = result;
        }, error => { this.spinnerService.hide() });
    }
    
    show(billId?: number): void {
        
        if (!billId) {
            this.bill = new CreateOrEditBillOfMaterialDto();
            this.bill.id = billId;
            this.active = true;
            this.modal.show();
        } else {
            this.bill = new CreateOrEditBillOfMaterialDto();
            this.bill.id = billId;

            this._billOfMaterialServiceProxy.getBillOfMaterialForEdit(billId).subscribe(result => {
                this.bill = result.billOfMaterialItem;
                this.active = true;
                this.modal.show();
            }, error => { this.spinnerService.hide() });
        }
    }

    onShown(): void {
        document.getElementById('StockItemId').focus();
    }

    save(): void {
        this.saving = true;
        this._billOfMaterialServiceProxy
            .createOrEdit(this.bill)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
            }, error => { this.spinnerService.hide() });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
