import { Component, Injector, ViewEncapsulation, ViewChild, HostListener } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import * as _ from 'lodash';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { BillOfMaterialDto, BillOfMaterialServiceProxy, TokenAuthServiceProxy } from '@shared/service-proxies/service-proxies';
import { NotifyService } from 'abp-ng2-module';
import { ActivatedRoute } from '@angular/router';
import { FileDownloadService } from '@shared/utils/file-download.service';
import { Title } from '@angular/platform-browser';
import { LazyLoadEvent } from 'primeng/api';
import { CreateOrEditBillOfMaterialModalComponent } from './create-or-edit-billofmaterial.component';
import { HttpClient } from '@angular/common/http';
import { FileUpload } from 'primeng/fileupload';
import { finalize } from 'rxjs/operators';
import { AppConsts } from '@shared/AppConsts';

@Component({
    templateUrl: './billofmaterial.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()],
})
export class BillOfMaterialComponent extends AppComponentBase {
    @ViewChild('createOrEditBillOfMaterialModal', { static: true}) createOrEditBillOfMaterialModal: CreateOrEditBillOfMaterialModalComponent;
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    @ViewChild('ExcelFileUpload', { static: false }) excelFileUpload: FileUpload;
    recordCount:any;
    advancedFiltersAreShown = false;
    filterText : string = '';
    uploadUrl:string = '';
    public screenHeight: any;  
    testHeight = 200;
    show: boolean = true;
    toggle: boolean = true;

    toggleBlock() {
        this.show = !this.show;
    };
    change() {
        this.toggle = !this.toggle;
    }
    constructor(
        injector: Injector,
        private _billofMaterialService: BillOfMaterialServiceProxy,
        private _notifyService: NotifyService,
        private _tokenAuth: TokenAuthServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _fileDownloadService: FileDownloadService,
        private titleService: Title,
        private _httpClient: HttpClient
    ) {
        super(injector);
        this.uploadUrl = AppConsts.remoteServiceBaseUrl + '/BillOfMaterial/ImportFromExcel';
        
    }
    ngOnInit(): void {
        this.screenHeight = window.innerHeight;
      }

    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }
    getBillOfMaterial(event?: LazyLoadEvent) {       
        // if(this.primengTableHelper.shouldResetPaging(event)){
        //     this.paginator.changePage(0);
        //     return;
        // }

        this.primengTableHelper.showLoadingIndicator();
        
        this._billofMaterialService.getAll(
            this.filterText,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
             this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe((result) => {
            
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            this.primengTableHelper.hideLoadingIndicator();
            this.recordCount = result.totalCount;
        }, error => { this.spinnerService.hide() });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    deleteBillOfMaterial(bill: BillOfMaterialDto): void {
        this.message.confirm(
            this.l('BillOfMaterialDeleteWarningMessage', bill.id),
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._billofMaterialService.delete(bill.id)
                        .subscribe(() => {
                            this.reloadPage();
                            this.notify.success(this.l('SuccessfullyDeleted'));
                        }, error => { this.spinnerService.hide() });
                }
            });
    }

    exportToExcel(): void {
        this._billofMaterialService.getBillOfMaterialToExcel(
        this.filterText,
        )
        .subscribe(result => {            
            this._fileDownloadService.downloadTempFile(result);
         }, error => { this.spinnerService.hide() });
    }
    exportSampleToExcel(): void {
        this._billofMaterialService.getBillOfMaterialToExcelSample(
        this.filterText,
        )
        .subscribe(result => {
            this._fileDownloadService.downloadTempFile(result);
         }, error => { this.spinnerService.hide() });
    }
    createBillOfMaterial() : void {
        this.createOrEditBillOfMaterialModal.show();
    }
    uploadExcel(data: { files: File }): void {
        const formData: FormData = new FormData();
        const file = data.files[0];
        formData.append('file', file, file.name);

        this._httpClient
            .post<any>(this.uploadUrl, formData)
            .pipe(finalize(() => this.excelFileUpload.clear()))
            .subscribe((response) => {
                if (response.success) {
                    this.notify.success(this.l('ImportBillOfMaterialProcessStart'));
                } else if (response.error != null) {
                    this.notify.error(this.l('ImportBillOfMaterialUploadFailed'));
                }
            }, error => { this.spinnerService.hide() });
    }

    onUploadExcelError(): void {
        this.notify.error(this.l('ImportBillOfMaterialUploadFailed'));
    }
}