import { NgModule } from '@angular/core';
import { AdminSharedModule } from '@app/admin/shared/admin-shared.module';
import { AppSharedModule } from '@app/shared/app-shared.module';
import { CustomizableDashboardModule } from '@app/shared/common/customizable-dashboard/customizable-dashboard.module';
import { BillofMaterialRoutingModule } from './billofmaterial-routing.module';
import { BillOfMaterialComponent } from './billofmaterial.component';
import { CreateOrEditBillOfMaterialModalComponent } from './create-or-edit-billofmaterial.component';

@NgModule({
    declarations: [BillOfMaterialComponent,CreateOrEditBillOfMaterialModalComponent],
    imports: [AppSharedModule, AdminSharedModule, BillofMaterialRoutingModule, CustomizableDashboardModule],
})
export class BillofMaterialModule {}
