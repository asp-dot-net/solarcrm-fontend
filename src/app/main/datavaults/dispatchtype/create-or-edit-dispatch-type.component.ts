import { Component, EventEmitter, Injector, Output, ViewChild } from '@angular/core';
import { AppConsts } from '@shared/AppConsts';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { map as _map, filter as _filter } from 'lodash-es';
import { DispatchTypeServiceProxy, CreateOrEditDispatchTypeDto } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'createOrEditDispatchTypeModal',
    templateUrl: './create-or-edit-dispatch-type.component.html',
    styleUrls: ['./create-or-edit-dispatch-type.component.less'],

})
export class CreateOrEditDispatchTypeModalComponent extends AppComponentBase {
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    dispatchType: CreateOrEditDispatchTypeDto = new CreateOrEditDispatchTypeDto();

    constructor(
        injector: Injector,
        private _dispatchTypeServiceProxy: DispatchTypeServiceProxy
    ) {
        super(injector);
    }

    show(projectTypeId?: number): void {
        if (!projectTypeId) {
            this.dispatchType = new CreateOrEditDispatchTypeDto();
            this.dispatchType.id = projectTypeId;
            this.active = true;
            this.modal.show();
        } else {
            this.dispatchType = new CreateOrEditDispatchTypeDto();
            this.dispatchType.id = projectTypeId;

            this._dispatchTypeServiceProxy.getDispatchTypeForEdit(projectTypeId).subscribe(result => {
                this.dispatchType = result.dispatchType;
                this.active = true;
                this.modal.show();
            }, error => { this.spinnerService.hide() });
        }
    }

    onShown(): void {
        document.getElementById('Name').focus();
    }

    save(): void {
        this.saving = true;
        this._dispatchTypeServiceProxy
            .createOrEdit(this.dispatchType)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
            }, error => { this.spinnerService.hide() });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
