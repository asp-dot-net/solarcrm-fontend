import { NgModule } from '@angular/core';
import { AdminSharedModule } from '@app/admin/shared/admin-shared.module';
import { AppSharedModule } from '@app/shared/app-shared.module';
import { DispatchTypeRoutingModule } from './dispatchtype-routing.module';
import { DispatchTypeComponent } from './dispatchtype.component';
import { CustomizableDashboardModule } from '@app/shared/common/customizable-dashboard/customizable-dashboard.module';
import { CreateOrEditDispatchTypeModalComponent } from './create-or-edit-dispatch-type.component';

@NgModule({
    declarations: [DispatchTypeComponent, 
        CreateOrEditDispatchTypeModalComponent
    ],
    imports: [AppSharedModule, AdminSharedModule, DispatchTypeRoutingModule, CustomizableDashboardModule],
})
export class DispatchTypeTypeModule {}
