import { Component, Injector, ViewEncapsulation, ViewChild, HostListener } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { ActivatedRoute, Router } from '@angular/router';
import { NotifyService } from 'abp-ng2-module';
import { DispatchTypeDto, DispatchTypeServiceProxy, TokenAuthServiceProxy } from '@shared/service-proxies/service-proxies';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import { Title } from '@angular/platform-browser';
import { LazyLoadEvent } from 'primeng/api';
import { CreateOrEditDispatchTypeModalComponent } from './create-or-edit-dispatch-type.component';

@Component({
    templateUrl: './dispatchtype.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()],
})
export class DispatchTypeComponent extends AppComponentBase {
    @ViewChild('createOrEditDispatchTypeModal', { static: true }) createOrEditDispatchTypeModal: CreateOrEditDispatchTypeModalComponent;
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    recordCount: any;
    advancedFiltersAreShown = false;
    filterText: string = '';
    public screenHeight: any;  
    testHeight = 230;
    constructor(
        injector: Injector,
        private _dispatchTypeService: DispatchTypeServiceProxy,
        private _fileDownloadService: FileDownloadService,
        private titleService: Title
    ) {
        super(injector);
    }
    ngOnInit(): void {
        this.screenHeight = window.innerHeight;
      }

    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }
    getDispatchType(event?: LazyLoadEvent) {
        
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            //return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._dispatchTypeService.getAll(
            this.filterText,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe((result) => {
            
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            this.primengTableHelper.hideLoadingIndicator();
            this.recordCount = result.totalCount;
        }, error => { this.spinnerService.hide() });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    deleteDispatchType(dispatchType: DispatchTypeDto): void {
        this.message.confirm(
            this.l('DispatchTypeDeleteWarningMessage', dispatchType.name),
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._dispatchTypeService.delete(dispatchType.id)
                        .subscribe(() => {
                            this.reloadPage();
                            this.notify.success(this.l('SuccessfullyDeleted'));
                        }, error => { this.spinnerService.hide() });
                }
            }
        );
    }
    exportToExcel(): void {
         this._dispatchTypeService.getDispatchTypeToExcel(
             this.filterText,
         )
             .subscribe(result => {
                 this._fileDownloadService.downloadTempFile(result);
             }, error => { this.spinnerService.hide() });
    }
    createDispatchType(): void {
        this.createOrEditDispatchTypeModal.show();
    }
}
