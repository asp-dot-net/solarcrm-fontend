import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DispatchTypeComponent } from './dispatchtype.component';

const routes: Routes = [
    {
        path: '',
        component: DispatchTypeComponent,
        pathMatch: 'full',
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class DispatchTypeRoutingModule {}
