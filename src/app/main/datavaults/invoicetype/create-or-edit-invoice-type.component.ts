import { Component, EventEmitter, Injector, Output, ViewChild } from '@angular/core';
import { AppConsts } from '@shared/AppConsts';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { map as _map, filter as _filter } from 'lodash-es';
import { InvoiceTypeServiceProxy, CreateOrEditInvoiceTypeDto } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'createOrEditInvoiceTypeModal',
    templateUrl: './create-or-edit-invoice-type.component.html',
    styleUrls: ['./create-or-edit-invoice-type.component.less'],

})
export class CreateOrEditInvoiceTypeModalComponent extends AppComponentBase {
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    invoiceType: CreateOrEditInvoiceTypeDto = new CreateOrEditInvoiceTypeDto();

    constructor(
        injector: Injector,
        private _invoiceTypeServiceProxy: InvoiceTypeServiceProxy
    ) {
        super(injector);
    }

    show(invoiceTypeId?: number): void {
        if (!invoiceTypeId) {
            this.invoiceType = new CreateOrEditInvoiceTypeDto();
            this.invoiceType.id = invoiceTypeId;
            this.active = true;
            this.modal.show();
        } else {
            this.invoiceType = new CreateOrEditInvoiceTypeDto();
            this.invoiceType.id = invoiceTypeId;

            this._invoiceTypeServiceProxy.getInvoiceTypeForEdit(invoiceTypeId).subscribe(result => {
                this.invoiceType = result.invoiceType;
                this.active = true;
                this.modal.show();
            }, error => { this.spinnerService.hide() });
        }
    }

    onShown(): void {
        document.getElementById('Name').focus();
    }

    save(): void {
        this.saving = true;
        this._invoiceTypeServiceProxy
            .createOrEdit(this.invoiceType)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
            }, error => { this.spinnerService.hide() });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
