import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InvoiceTypeComponent } from './invoicetype.component';

const routes: Routes = [
    {
        path: '',
        component: InvoiceTypeComponent,
        pathMatch: 'full',
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class InvoiceTypeRoutingModule {}
