import { NgModule } from '@angular/core';
import { AdminSharedModule } from '@app/admin/shared/admin-shared.module';
import { AppSharedModule } from '@app/shared/app-shared.module';
import { InvoiceTypeRoutingModule } from './invoicetype-routing.module';
import { InvoiceTypeComponent } from './invoicetype.component';
import { CustomizableDashboardModule } from '@app/shared/common/customizable-dashboard/customizable-dashboard.module';
import { CreateOrEditInvoiceTypeModalComponent } from './create-or-edit-invoice-type.component';

@NgModule({
    declarations: [InvoiceTypeComponent, CreateOrEditInvoiceTypeModalComponent],
    imports: [AppSharedModule, AdminSharedModule, InvoiceTypeRoutingModule, CustomizableDashboardModule],
})
export class InvoiceTypeModule {}
