import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TalukaComponent } from './taluka.component';

const routes: Routes = [
    {
        path: '',
        component: TalukaComponent,
        pathMatch: 'full',
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class TalukaRoutingModule {}
