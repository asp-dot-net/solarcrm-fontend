import { Component, EventEmitter, Injector, OnInit, Output, ViewChild } from '@angular/core';
import { AppConsts } from '@shared/AppConsts';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { map as _map, filter as _filter } from 'lodash-es';
import { TalukaServiceProxy, CreateOrEditTalukaDto,DistrictServiceProxy,  } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'createOrEditTalukaModal',
    templateUrl: './create-or-edit-taluka.component.html',
    styleUrls: ['./create-or-edit-taluka.component.less'],
    
})
export class CreateOrEditTalukaModalComponent extends AppComponentBase implements OnInit{
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    
    active = false;
    saving = false;
    district :any = [];
    talukas: CreateOrEditTalukaDto = new CreateOrEditTalukaDto();

    constructor(
        injector: Injector,
        private _talukaServiceProxy: TalukaServiceProxy,
        private _districtServiceProxy: DistrictServiceProxy
    ) {
        super(injector);
    }

    ngOnInit(): void {
        this._districtServiceProxy.getAllDistrictForDropdown().subscribe(result => {
            this.district = result;
        }, error => { this.spinnerService.hide() });
    }

    show(districtId?: number): void {
        if (!districtId) {
            this.talukas = new CreateOrEditTalukaDto();
            this.talukas.id = districtId;
            this.active = true;
            this.modal.show();
        } else {
            this.talukas = new CreateOrEditTalukaDto();
            this.talukas.id = districtId;

            this._talukaServiceProxy.getTalukaForEdit(districtId).subscribe(result => {
                this.talukas = result.talukas;
                this.active = true;
                this.modal.show();
            }, error => { this.spinnerService.hide() });
        }
    }

    onShown(): void {
        document.getElementById('Name').focus();
    }

    save(): void {
        this.saving = true;
        this._talukaServiceProxy
            .createOrEdit(this.talukas)
            .pipe(
                finalize(() => { this.saving = false;
                })
            )
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
            }, error => { this.spinnerService.hide() });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
