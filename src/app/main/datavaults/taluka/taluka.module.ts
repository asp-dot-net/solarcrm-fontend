import { NgModule } from '@angular/core';
import { AdminSharedModule } from '@app/admin/shared/admin-shared.module';
import { AppSharedModule } from '@app/shared/app-shared.module';
import { TalukaRoutingModule } from './taluka-routing.module';
import { TalukaComponent } from './taluka.component';
import { CustomizableDashboardModule } from '@app/shared/common/customizable-dashboard/customizable-dashboard.module';
import { CreateOrEditTalukaModalComponent } from './create-or-edit-taluka.component';

@NgModule({
    declarations: [TalukaComponent, CreateOrEditTalukaModalComponent],
    imports: [AppSharedModule, AdminSharedModule, TalukaRoutingModule, CustomizableDashboardModule],
})
export class TalukaModule {}
