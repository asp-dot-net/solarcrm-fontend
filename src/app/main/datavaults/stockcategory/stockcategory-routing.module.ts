import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StockCategoryComponent } from './stockcategory.component';

const routes: Routes = [
    {
        path: '',
        component: StockCategoryComponent,
        pathMatch: 'full',
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class StockCategoryRoutingModule {}
