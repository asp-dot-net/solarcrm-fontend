import { NgModule } from '@angular/core';
import { AdminSharedModule } from '@app/admin/shared/admin-shared.module';
import { AppSharedModule } from '@app/shared/app-shared.module';
import { StockCategoryRoutingModule } from './stockcategory-routing.module';
import { StockCategoryComponent } from './stockcategory.component';
import { CustomizableDashboardModule } from '@app/shared/common/customizable-dashboard/customizable-dashboard.module';
import { CreateOrEditStockCategoryModalComponent } from './create-or-edit-stockcategory.component'; //from './create-or-edit-project-type.component';

@NgModule({
    declarations: [StockCategoryComponent, CreateOrEditStockCategoryModalComponent],
    imports: [AppSharedModule, AdminSharedModule, StockCategoryRoutingModule, CustomizableDashboardModule],
})
export class StockCategoryModule {}
