import { Component, EventEmitter, Injector, Output, ViewChild } from '@angular/core';
import { AppConsts } from '@shared/AppConsts';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { map as _map, filter as _filter } from 'lodash-es';
import { StockCategoryServiceProxy, CreateOrEditStockCategoryDto } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'createOrEditStockCategoryModal',
    templateUrl: './create-or-edit-stockcategory.component.html',
    styleUrls: ['./create-or-edit-stockcategory.component.less'],

})
export class CreateOrEditStockCategoryModalComponent extends AppComponentBase {
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    stockCategory: CreateOrEditStockCategoryDto = new CreateOrEditStockCategoryDto();

    constructor(
        injector: Injector,
        private _stockcategoryServiceProxy: StockCategoryServiceProxy
    ) {
        super(injector);
    }

    show(projectTypeId?: number): void {
        if (!projectTypeId) {
            this.stockCategory = new CreateOrEditStockCategoryDto();
            this.stockCategory.id = projectTypeId;
            this.active = true;
            this.modal.show();
        } else {
            this.stockCategory = new CreateOrEditStockCategoryDto();
            this.stockCategory.id = projectTypeId;

            this._stockcategoryServiceProxy.getStockCategoryForEdit(projectTypeId).subscribe(result => {
                this.stockCategory = result.stockCategory;
                this.active = true;
                this.modal.show();
            }, error => { this.spinnerService.hide() });
        }
    }

    onShown(): void {
        document.getElementById('Name').focus();
    }

    save(): void {
        this.saving = true;
        this._stockcategoryServiceProxy
            .createOrEdit(this.stockCategory)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
            }, error => { this.spinnerService.hide() });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
