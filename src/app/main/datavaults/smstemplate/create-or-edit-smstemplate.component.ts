import { Component, EventEmitter, Injector, Output, ViewChild } from '@angular/core';
import { AppConsts } from '@shared/AppConsts';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { map as _map, filter as _filter } from 'lodash-es';
import { SmsTemplatesServiceProxy, GetRolesInput, CreateOrEditSmsTemplateDto, OrganizationUnitDto, RoleServiceProxy, CommonLookupServiceProxy } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'createOrEditSmsTemplateModal',
    templateUrl: './create-or-edit-smstemplate.component.html',
    styleUrls: ['./create-or-edit-smstemplate.component.less'],

})
export class CreateOrEditSmsTemplateModalComponent extends AppComponentBase {
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    organizationList: OrganizationUnitDto[];
    active = false;
    saving = false;
    total = 0;
    credit = 0;

    smsTemplate: CreateOrEditSmsTemplateDto = new CreateOrEditSmsTemplateDto();
    allOrganizationUnits: OrganizationUnitDto[];
    role: any;
    //freebieTransport: JobPromotionPromotionMasterLookupTableDto[];
    constructor(
        injector: Injector,
        private _smsTemplateServiceProxy: SmsTemplatesServiceProxy,
        private _commonLookupServiceProxy: CommonLookupServiceProxy,
        //private _roleServiceProxy: RoleServiceProxy,
    ) {
        super(injector);
    }
    getOrganizationList() {
        this._commonLookupServiceProxy.getOrganizationUnitUserWise().subscribe(result => {
            this.organizationList = result;
        }, error => { this.spinnerService.hide() });
    }
    show(smsTemplateId?: number): void {

        // this._roleServiceProxy.getRoles(new GetRolesInput({ permissions: [] })).subscribe(result => {
        //     this.role = result;
        // });
        this.getOrganizationList();
        if (!smsTemplateId) {
            this.smsTemplate = new CreateOrEditSmsTemplateDto();
            this.smsTemplate.id = smsTemplateId;
            this.total = 0;
            this.credit = 0;
            this.active = true;
            this.modal.show();
        } else {
            this.smsTemplate = new CreateOrEditSmsTemplateDto();
            this.smsTemplate.id = smsTemplateId;

            this._smsTemplateServiceProxy.getSmsTemplateForEdit(smsTemplateId).subscribe(result => {
                this.smsTemplate = result.smsTemplate;
                this.countCharcters();
                this.active = true;
                this.modal.show();
            }, error => { this.spinnerService.hide() });
        }
    }

    onShown(): void {
        //document.getElementById('Name').focus();
    }

    save(): void {
        this.saving = true;
        //if (this.role != 'Admin') {
        if (this.total > 320) {
            this.notify.warn(this.l('You Can Not Add more than 320 characters'));
            this.saving = false;
        } else {
            this._smsTemplateServiceProxy
                .createOrEdit(this.smsTemplate)
                .pipe(
                    finalize(() => {
                        this.saving = false;
                    })
                )
                .subscribe(() => {
                    this.notify.info(this.l('SavedSuccessfully'));
                    this.close();
                    this.modalSave.emit(null);
                }, error => { this.spinnerService.hide() });
        }
        //}
    }
    onTagChange(event): void {

        const id = event.target.value;
        if (id == 1) {
            if (this.smsTemplate.text != null) {
                this.smsTemplate.text = this.smsTemplate.text + " " + "{{Customer.Name}}";
            } else {
                this.smsTemplate.text = "{{Customer.Name}}";
            }

        } else if (id == 2) {
            if (this.smsTemplate.text != null) {
                this.smsTemplate.text = this.smsTemplate.text + " " + "{{Customer.Mobile}}";
            } else {
                this.smsTemplate.text = "{{Customer.Mobile}}";
            }
        } else if (id == 3) {
            if (this.smsTemplate.text != null) {
                this.smsTemplate.text = this.smsTemplate.text + " " + "{{Customer.Phone}}";
            } else {
                this.smsTemplate.text = "{{Customer.Phone}}";
            }
        } else if (id == 4) {
            if (this.smsTemplate.text != null) {
                this.smsTemplate.text = this.smsTemplate.text + " " + "{{Customer.Email}}";
            } else {
                this.smsTemplate.text = "{{Customer.Email}}";
            }
        } else if (id == 5) {

            if (this.smsTemplate.text != null) {
                this.smsTemplate.text = this.smsTemplate.text + " " + "{{Customer.Address}}";
            } else {
                this.smsTemplate.text = "{{Customer.Address}}";
            }
        } else if (id == 6) {
            if (this.smsTemplate.text != null) {
                this.smsTemplate.text = this.smsTemplate.text + " " + "{{Sales.Name}}";
            } else {
                this.smsTemplate.text = "{{Sales.Name}}";
            }
        } else if (id == 7) {
            if (this.smsTemplate.text != null) {
                this.smsTemplate.text = this.smsTemplate.text + " " + "{{Sales.Mobile}}";
            } else {
                this.smsTemplate.text = "{{Sales.Mobile}}";
            }
        } else if (id == 8) {
            if (this.smsTemplate.text != null) {
                this.smsTemplate.text = this.smsTemplate.text + " " + "{{Sales.Email}}";
            } else {
                this.smsTemplate.text = "{{Sales.Email}}";
            }
        }
        else if (id == 9) {
            if (this.smsTemplate.text != null) {
                this.smsTemplate.text = this.smsTemplate.text + " " + "{{Quote.ProjectNo}}";
            } else {
                this.smsTemplate.text = "{{Quote.ProjectNo}}";
            }
        }
        else if (id == 10) {
            if (this.smsTemplate.text != null) {
                this.smsTemplate.text = this.smsTemplate.text + " " + "{{Quote.SystemCapacity}}";
            } else {
                this.smsTemplate.text = "{{Quote.SystemCapacity}}";
            }
        }
        else if (id == 11) {
            if (this.smsTemplate.text != null) {
                this.smsTemplate.text = this.smsTemplate.text + " " + "{{Quote.AllproductItem}}";
            } else {
                this.smsTemplate.text = "{{Quote.AllproductItem}}";
            }
        }
        else if (id == 12) {
            if (this.smsTemplate.text != null) {
                this.smsTemplate.text = this.smsTemplate.text + " " + "{{Quote.TotalQuoteprice}}";
            } else {
                this.smsTemplate.text = "{{Quote.TotalQuoteprice}}";
            }
        }
        else if (id == 13) {
            if (this.smsTemplate.text != null) {
                this.smsTemplate.text = this.smsTemplate.text + " " + "{{Quote.InstallationDate}}";
            } else {
                this.smsTemplate.text = "{{Quote.InstallationDate}}";
            }
        }
        else if (id == 14) {
            if (this.smsTemplate.text != null) {
                this.smsTemplate.text = this.smsTemplate.text + " " + "{{Quote.InstallerName}}";
            } else {
                this.smsTemplate.text = "{{Quote.InstallerName}}";
            }
        }
        else if (id == 15) {
            if (this.smsTemplate.text != null) {
                this.smsTemplate.text = this.smsTemplate.text + " " + "{{Freebies.TransportCompany}}";
            } else {
                this.smsTemplate.text = "{{Freebies.TransportCompany}}";
            }
        }
        else if (id == 16) {
            if (this.smsTemplate.text != null) {
                this.smsTemplate.text = this.smsTemplate.text + " " + "{{Freebies.TransportLink}}";
            } else {
                this.smsTemplate.text = "{{Freebies.TransportLink}}";
            }
        }
        else if (id == 17) {
            if (this.smsTemplate.text != null) {
                this.smsTemplate.text = this.smsTemplate.text + " " + "{{Freebies.DispatchedDate}}";
            } else {
                this.smsTemplate.text = "{{Freebies.DispatchedDate}}";
            }
        }
        else if (id == 18) {
            if (this.smsTemplate.text != null) {
                this.smsTemplate.text = this.smsTemplate.text + " " + "{{Freebies.TrackingNo}}";
            } else {
                this.smsTemplate.text = "{{Freebies.TrackingNo}}";
            }
        }
        else if (id == 19) {
            if (this.smsTemplate.text != null) {
                this.smsTemplate.text = this.smsTemplate.text + " " + "{{Freebies.PromoType}}";
            } else {
                this.smsTemplate.text = "{{Freebies.PromoType}}";
            }
        }
        else if (id == 20) {
            if (this.smsTemplate.text != null) {
                this.smsTemplate.text = this.smsTemplate.text + " " + "{{Invoice.UserName}}";
            } else {
                this.smsTemplate.text = "{{Invoice.UserName}}";
            }
        }
        else if (id == 21) {
            if (this.smsTemplate.text != null) {
                this.smsTemplate.text = this.smsTemplate.text + " " + "{{Invoice.UserMobile}}";
            } else {
                this.smsTemplate.text = "{{Invoice.UserMobile}}";
            }
        }
        else if (id == 22) {
            if (this.smsTemplate.text != null) {
                this.smsTemplate.text = this.smsTemplate.text + " " + "{{Invoice.UserEmail}}";
            } else {
                this.smsTemplate.text = "{{Invoice.UserEmail}}";
            }
        }
        else if (id == 23) {
            if (this.smsTemplate.text != null) {
                this.smsTemplate.text = this.smsTemplate.text + " " + "{{Invoice.Owning}}";
            } else {
                this.smsTemplate.text = "{{Invoice.Owning}}";
            }
        }
        else if (id == 24) {
            if (this.smsTemplate.text != null) {
                this.smsTemplate.text = this.smsTemplate.text + " " + "{{Organization.orgName}}";
            } else {
                this.smsTemplate.text = "{{Organization.orgName}}";
            }
        }
        else if (id == 25) {
            if (this.smsTemplate.text != null) {
                this.smsTemplate.text = this.smsTemplate.text + " " + "{{Organization.orgEmail}}";
            } else {
                this.smsTemplate.text = "{{Organization.orgEmail}}";
            }
        }
        else if (id == 26) {
            if (this.smsTemplate.text != null) {
                this.smsTemplate.text = this.smsTemplate.text + " " + "{{Organization.orgMobile}}";
            } else {
                this.smsTemplate.text = "{{Organization.orgMobile}}";
            }
        }
        else if (id == 27) {
            if (this.smsTemplate.text != null) {
                this.smsTemplate.text = this.smsTemplate.text + " " + "{{Organization.orglogo}}";
            } else {
                this.smsTemplate.text = "{{Organization.orglogo}}";
            }
        }
        else if (id == 28) {
            if (this.smsTemplate.text != null) {
                this.smsTemplate.text = this.smsTemplate.text + " " + "{{Review.link}}";
            } else {
                this.smsTemplate.text = "{{Review.link}}";
            }
        }

        this.countCharcters();
    }
    countCharcters(): void {

        if (this.role != 'Admin') {
            this.total = this.smsTemplate.text.length;
            this.credit = Math.ceil(this.total / 160);
            if (this.total > 320) {
                this.notify.warn(this.l('You Can Not Add more than 320 characters'));
            }
        }
        else {
            this.total = this.smsTemplate.text.length;
            this.credit = Math.ceil(this.total / 160);
        }


    }
    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
