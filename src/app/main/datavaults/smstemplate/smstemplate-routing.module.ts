import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SmsTemplateComponent } from './smstemplate.component';

const routes: Routes = [
    {
        path: '',
        component: SmsTemplateComponent,
        pathMatch: 'full',
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class SmsTemplateRoutingModule {}
