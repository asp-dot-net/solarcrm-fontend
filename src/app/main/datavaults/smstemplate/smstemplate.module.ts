import { NgModule } from '@angular/core';
import { AdminSharedModule } from '@app/admin/shared/admin-shared.module';
import { AppSharedModule } from '@app/shared/app-shared.module';
import { SmsTemplateRoutingModule } from './smstemplate-routing.module';
import { SmsTemplateComponent } from './smstemplate.component';
import { CustomizableDashboardModule } from '@app/shared/common/customizable-dashboard/customizable-dashboard.module';
import { CreateOrEditSmsTemplateModalComponent } from './create-or-edit-smstemplate.component';
import { ViewSmsTemplateModalComponent } from './view-smstemplate.component';
import { NgSelectModule } from '@ng-select/ng-select';
@NgModule({
    declarations: [SmsTemplateComponent, CreateOrEditSmsTemplateModalComponent,ViewSmsTemplateModalComponent],
    imports: [AppSharedModule, AdminSharedModule, SmsTemplateRoutingModule,NgSelectModule, CustomizableDashboardModule],
})
export class SmsTemplateModule {}
