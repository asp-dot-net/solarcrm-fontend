import { Component, Injector, ViewEncapsulation, ViewChild, HostListener, OnInit } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { ActivatedRoute, Router } from '@angular/router';
import { NotifyService } from 'abp-ng2-module';
import { TokenAuthServiceProxy } from '@shared/service-proxies/service-proxies';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import { Title } from '@angular/platform-browser';
import { LazyLoadEvent } from 'primeng/api';
import { SmsTemplatesServiceProxy, SmsTemplateDto, OrganizationUnitDto, CommonLookupServiceProxy } from '@shared/service-proxies/service-proxies';
import { CreateOrEditSmsTemplateModalComponent } from './create-or-edit-smstemplate.component';
import { ViewSmsTemplateModalComponent } from './view-smstemplate.component';
@Component({
    templateUrl: './smstemplate.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()],
})
export class SmsTemplateComponent extends AppComponentBase implements OnInit {
    @ViewChild('createOrEditSmsTemplateModal', { static: true }) createOrEditSmsTemplateModal: CreateOrEditSmsTemplateModalComponent;
    @ViewChild('viewSmsTemplateModal', { static: true }) ViewSmsTemplateModal: ViewSmsTemplateModalComponent;
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    recordCount: any;
    advancedFiltersAreShown = false;
    filterText = '';
    allOrganizationUnits: OrganizationUnitDto[];

    organizationUnit = 0;
    organizationUnitlength = 0;
    firstrowcount = 0;
    last = 0;
    public screenHeight: any;  
    testHeight = 200;
    show: boolean = true;
    toggle: boolean = true;

    toggleBlock() {
        this.show = !this.show;
    };
    change() {
        this.toggle = !this.toggle;
    }
    constructor(
        injector: Injector,
        private _smsTemplatesService: SmsTemplatesServiceProxy,
        private _commonLookupService: CommonLookupServiceProxy,
        private _notifyService: NotifyService,
        private _tokenAuth: TokenAuthServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _fileDownloadService: FileDownloadService,
        private titleService: Title
    ) {
        super(injector);
        this.titleService.setTitle("SalesDrive | SMS Templates");
    }

    ngOnInit(): void {
        this.screenHeight = window.innerHeight;
        // this._commonLookupService.getOrganizationUnit().subscribe(output => {
        //     this.allOrganizationUnits = output;
        //     this.organizationUnit = this.allOrganizationUnits[0].id;
        //     this.organizationUnitlength = this.allOrganizationUnits.length;
        //     this.getSmsTemplate();
        // });
        //this.getSmsTemplate();
    }

    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }

    exportToExcel() {
        this._smsTemplatesService.getSmsTemplatesToExcel(
            this.filterText,
        )
            .subscribe(result => {
                this._fileDownloadService.downloadTempFile(result);
            }, error => { this.spinnerService.hide() });
    }

    getSmsTemplate(event?: LazyLoadEvent) {
        // if (this.primengTableHelper.shouldResetPaging(event)) {
        //     this.paginator.changePage(0);
        //     return;
        // }
        this.primengTableHelper.showLoadingIndicator();

        this._smsTemplatesService.getAll(
            this.filterText,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe((result) => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            this.primengTableHelper.hideLoadingIndicator();
            //this.RecordCount = result.items;
            this.recordCount = result.totalCount;
        }, error => { this.spinnerService.hide() });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    deleteSmsTemplate(smsTemplate: SmsTemplateDto): void {
        this.message.confirm(
            this.l('UserDeleteWarningMessage', smsTemplate.name),
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._smsTemplatesService.delete(smsTemplate.id)
                        .subscribe(() => {
                            this.reloadPage();
                            this.notify.success(this.l('SuccessfullyDeleted'));
                        }, error => { this.spinnerService.hide() });
                }
            }
        );
    }

    exportToSmsTemplates(): void {
        this._smsTemplatesService.getSmsTemplatesToExcel(
            this.filterText,
        )
            .subscribe(result => {
                this._fileDownloadService.downloadTempFile(result);
            }, error => { this.spinnerService.hide() });
    }

    createSmsTemplate(): void {
        this.createOrEditSmsTemplateModal.show();
    }
}
