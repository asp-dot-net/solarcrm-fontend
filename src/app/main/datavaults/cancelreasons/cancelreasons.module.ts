import { NgModule } from '@angular/core';
import { AdminSharedModule } from '@app/admin/shared/admin-shared.module';
import { AppSharedModule } from '@app/shared/app-shared.module';
import { CancelReasonsRoutingModule } from './cancelreasons-routing.module';
import { CancelReasonsComponent } from './cancelreasons.component';
import { CustomizableDashboardModule } from '@app/shared/common/customizable-dashboard/customizable-dashboard.module';
import { CreateOrEditCancelReasonsModalComponent } from './create-or-edit-cancelreasons.component';

@NgModule({
    declarations: [CancelReasonsComponent, CreateOrEditCancelReasonsModalComponent],
    imports: [AppSharedModule, AdminSharedModule, CancelReasonsRoutingModule, CustomizableDashboardModule],
})
export class CancelReasonsModule {}
