import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CancelReasonsComponent } from './cancelreasons.component';

const routes: Routes = [
    {
        path: '',
        component: CancelReasonsComponent,
        pathMatch: 'full',
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class CancelReasonsRoutingModule {}
