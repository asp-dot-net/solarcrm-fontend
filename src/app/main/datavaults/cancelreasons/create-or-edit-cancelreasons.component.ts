import { Component, EventEmitter, Injector, Output, ViewChild } from '@angular/core';
import { AppConsts } from '@shared/AppConsts';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { map as _map, filter as _filter } from 'lodash-es';
import { CancelReasonsServiceProxy, CreateOrEditCancelReasonsDto } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'createOrEditCancelReasonsModal',
    templateUrl: './create-or-edit-CancelReasons.component.html',
    styleUrls: ['./create-or-edit-CancelReasons.component.less'],

})
export class CreateOrEditCancelReasonsModalComponent extends AppComponentBase {
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    cancelReasons: CreateOrEditCancelReasonsDto = new CreateOrEditCancelReasonsDto();

    constructor(
        injector: Injector,
        private _cancelReasonsServiceProxy: CancelReasonsServiceProxy
    ) {
        super(injector);
    }

    show(cancelReasonsId?: number): void {
        if (!cancelReasonsId) {
            this.cancelReasons = new CreateOrEditCancelReasonsDto();
            this.cancelReasons.id = cancelReasonsId;
            this.active = true;
            this.modal.show();
        } else {
            this.cancelReasons = new CreateOrEditCancelReasonsDto();
            this.cancelReasons.id = cancelReasonsId;

            this._cancelReasonsServiceProxy.getCancelReasonsForEdit(cancelReasonsId).subscribe(result => {
                this.cancelReasons = result.cancelReasons;
                this.active = true;
                this.modal.show();
            }, error => { this.spinnerService.hide() });
        }
    }

    onShown(): void {
        document.getElementById('Name').focus();
    }

    save(): void {
        this.saving = true;
        this._cancelReasonsServiceProxy
            .createOrEdit(this.cancelReasons)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
            }, error => { this.spinnerService.hide() });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
