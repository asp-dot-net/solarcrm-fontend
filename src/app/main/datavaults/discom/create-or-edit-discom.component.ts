import { Component, EventEmitter, Injector, OnInit, Output, ViewChild } from '@angular/core';
import { AppConsts } from '@shared/AppConsts';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { map as _map, filter as _filter } from 'lodash-es';
import { DiscomServiceProxy, CreateOrEditDiscomDto, LocationsServiceProxy, CommonLookupServiceProxy } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'createOrEditDiscomModal',
    templateUrl: './create-or-edit-discom.component.html',
    styleUrls: ['./create-or-edit-discom.component.less'],

})
export class CreateOrEditDiscomModalComponent extends AppComponentBase implements OnInit {
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    discom: CreateOrEditDiscomDto = new CreateOrEditDiscomDto();

    location: any = [];

    constructor(
        injector: Injector,
        private _discomServiceProxy: DiscomServiceProxy,
        private _commonLookupServiceProxy: CommonLookupServiceProxy
    ) {
        super(injector);
    }

    ngOnInit(): void {
        this._commonLookupServiceProxy.getAllLocationForDropdown().subscribe(result => {
            this.location = result;
        }, error => { this.spinnerService.hide() });
    }

    show(discomId?: number): void {
        
        if (!discomId) {
            this.discom = new CreateOrEditDiscomDto();
            this.discom.id = discomId;
            this.active = true;
            this.modal.show();
        } else {
            this.discom = new CreateOrEditDiscomDto();
            this.discom.id = discomId;

            this._discomServiceProxy.getDiscomForEdit(discomId).subscribe(result => {
                this.discom = result.discom;
                this.active = true;
                this.modal.show();
            }, error => { this.spinnerService.hide() });
        }
    }

    onShown(): void {
        document.getElementById('Name').focus();
    }

    save(): void {
        this.saving = true;
        this._discomServiceProxy
            .createOrEdit(this.discom)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
            }, error => { this.spinnerService.hide() });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
