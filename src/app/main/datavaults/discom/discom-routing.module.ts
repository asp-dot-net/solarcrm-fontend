import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DiscomComponent } from './discom.component';

const routes: Routes = [
    {
        path: '',
        component: DiscomComponent,
        pathMatch: 'full',
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class DiscomRoutingModule {}
