import { NgModule } from '@angular/core';
import { AdminSharedModule } from '@app/admin/shared/admin-shared.module';
import { AppSharedModule } from '@app/shared/app-shared.module';
import { DiscomRoutingModule } from './discom-routing.module';
import { DiscomComponent } from './discom.component';
import { CustomizableDashboardModule } from '@app/shared/common/customizable-dashboard/customizable-dashboard.module';
import { CreateOrEditDiscomModalComponent } from './create-or-edit-discom.component';

@NgModule({
    declarations: [DiscomComponent, CreateOrEditDiscomModalComponent],
    imports: [AppSharedModule, AdminSharedModule, DiscomRoutingModule, CustomizableDashboardModule],
})
export class DiscomModule {}
