import { NgModule } from '@angular/core';
import { AdminSharedModule } from '@app/admin/shared/admin-shared.module';
import { AppSharedModule } from '@app/shared/app-shared.module';
import { SolarTypeRoutingModule } from './solartype-routing.module';
import { SolarTypeComponent } from './solartype.component';
import { CustomizableDashboardModule } from '@app/shared/common/customizable-dashboard/customizable-dashboard.module';
import { CreateOrEditSolarTypeModalComponent } from './create-or-edit-solartype.component';

@NgModule({
    declarations: [SolarTypeComponent, CreateOrEditSolarTypeModalComponent],
    imports: [AppSharedModule, AdminSharedModule, SolarTypeRoutingModule, CustomizableDashboardModule],
})
export class SolarTypeModule {}
