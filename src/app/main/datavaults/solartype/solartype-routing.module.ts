import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SolarTypeComponent } from './solartype.component';

const routes: Routes = [
    {
        path: '',
        component: SolarTypeComponent,
        pathMatch: 'full',
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class SolarTypeRoutingModule {}
