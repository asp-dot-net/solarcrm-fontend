import { Component, EventEmitter, Injector, Output, ViewChild } from '@angular/core';
import { AppConsts } from '@shared/AppConsts';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { map as _map, filter as _filter } from 'lodash-es';
import { SolarTypeServiceProxy, CreateOrEditSolarTypeDto } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'createOrEditSolarTypeModal',
    templateUrl: './create-or-edit-solartype.component.html',
    styleUrls: ['./create-or-edit-solartype.component.less'],

})
export class CreateOrEditSolarTypeModalComponent extends AppComponentBase {
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    solarType: CreateOrEditSolarTypeDto = new CreateOrEditSolarTypeDto();

    constructor(
        injector: Injector,
        private _solarTypeServiceProxy: SolarTypeServiceProxy
    ) {
        super(injector);
    }

    show(solarTypeId?: number): void {
        if (!solarTypeId) {
            this.solarType = new CreateOrEditSolarTypeDto();
            this.solarType.id = solarTypeId;
            this.active = true;
            this.modal.show();
        } else {
            this.solarType = new CreateOrEditSolarTypeDto();
            this.solarType.id = solarTypeId;

            this._solarTypeServiceProxy.getSolarTypeForEdit(solarTypeId).subscribe(result => {
                this.solarType = result.solarTypes;
                this.active = true;
                this.modal.show();
            }, error => { this.spinnerService.hide() });
        }
    }

    onShown(): void {
        document.getElementById('Name').focus();
    }

    save(): void {
        this.saving = true;
        this._solarTypeServiceProxy
            .createOrEdit(this.solarType)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
            }, error => { this.spinnerService.hide() });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
