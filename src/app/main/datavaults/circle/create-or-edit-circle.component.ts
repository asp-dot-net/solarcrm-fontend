import { Component, EventEmitter, Injector, OnInit, Output, ViewChild } from '@angular/core';
import { AppConsts } from '@shared/AppConsts';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { map as _map, filter as _filter } from 'lodash-es';
import { CircleServiceProxy, CreateOrEditCircleDto, CommonLookupServiceProxy, CommonLookupDto } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'createOrEditCircleModal',
    templateUrl: './create-or-edit-circle.component.html',
    styleUrls: ['./create-or-edit-circle.component.less'],

})
export class CreateOrEditCircleModalComponent extends AppComponentBase implements OnInit {
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;
    drpdiscom: CommonLookupDto[];
    drpdivision: CommonLookupDto[];
    drpsubdivision: CommonLookupDto[];
    circle: CreateOrEditCircleDto = new CreateOrEditCircleDto();

    constructor(
        injector: Injector,
        private _circleServiceProxy: CircleServiceProxy,
        private _commonServiceProxy: CommonLookupServiceProxy
    ) {
        super(injector);
    }
    
    ngOnInit(): void {
        this._commonServiceProxy.getDiscomForDropdown().subscribe(result => {
            this.drpdiscom = result;
        }, error => { this.spinnerService.hide() });
    }

    show(circleId?: number): void {

        // this._circleServiceProxy.getAllTalukaForDropdown().subscribe(result => {
        //     console.log(result);

        //     this.taluka = result;
        // });
        
        if (!circleId) {
            this.circle = new CreateOrEditCircleDto();
            this.circle.id = circleId;
            this.active = true;
            this.modal.show();
        } else {
            this.circle = new CreateOrEditCircleDto();
            this.circle.id = circleId;

            this._circleServiceProxy.getCircleForEdit(circleId).subscribe(result => {
                this.circle = result.circle;
                this.active = true;
                this.modal.show();
            }, error => { this.spinnerService.hide() });
        }
    }

    getdivision(event) {

        this.drpdivision = [];
        this._commonServiceProxy.getAllDivisionFilterDropdown(event.target.value).subscribe(result => {
            this.drpdivision = result;
        }, error => { this.spinnerService.hide() });
    }

    getSubdivision(divisionid) {
        this.drpdivision = [];
        this._commonServiceProxy.getAllSubdivisionWiseData(divisionid).subscribe(result => {
            this.drpsubdivision = result;
        }, error => { this.spinnerService.hide() });
    }

    onShown(): void {
        document.getElementById('Name').focus();
    }

    save(): void {
        this.saving = true;
        this._circleServiceProxy
            .createOrEdit(this.circle)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
            }, error => { this.spinnerService.hide() });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
