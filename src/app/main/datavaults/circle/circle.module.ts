import { NgModule } from '@angular/core';
import { AdminSharedModule } from '@app/admin/shared/admin-shared.module';
import { AppSharedModule } from '@app/shared/app-shared.module';
import { CircleRoutingModule } from './circle-routing.module';
import { CircleComponent } from './circle.component';
import { CustomizableDashboardModule } from '@app/shared/common/customizable-dashboard/customizable-dashboard.module';
import { CreateOrEditCircleModalComponent } from './create-or-edit-circle.component';

@NgModule({
    declarations: [CircleComponent, CreateOrEditCircleModalComponent],
    imports: [AppSharedModule, AdminSharedModule, CircleRoutingModule, CustomizableDashboardModule],
})
export class CircleModule {}
