import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LeadTypeComponent } from './leadtype.component';

const routes: Routes = [
    {
        path: '',
        component: LeadTypeComponent,
        pathMatch: 'full',
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class LeadTypeRoutingModule {}
