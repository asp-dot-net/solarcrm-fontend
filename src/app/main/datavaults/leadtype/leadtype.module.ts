import { NgModule } from '@angular/core';
import { AdminSharedModule } from '@app/admin/shared/admin-shared.module';
import { AppSharedModule } from '@app/shared/app-shared.module';
import { LeadTypeRoutingModule } from './leadtype-routing.module';
import { LeadTypeComponent } from './leadtype.component';
import { CustomizableDashboardModule } from '@app/shared/common/customizable-dashboard/customizable-dashboard.module';
import { CreateOrEditLeadTypeModalComponent } from './create-or-edit-lead-type.component';

@NgModule({
    declarations: [LeadTypeComponent, CreateOrEditLeadTypeModalComponent],
    imports: [AppSharedModule, AdminSharedModule, LeadTypeRoutingModule, CustomizableDashboardModule],
})
export class LeadTypeModule {}
