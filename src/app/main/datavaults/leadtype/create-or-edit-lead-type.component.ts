import { Component, EventEmitter, Injector, Output, ViewChild } from '@angular/core';
import { AppConsts } from '@shared/AppConsts';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { map as _map, filter as _filter } from 'lodash-es';
import { LeadTypeServiceProxy, CreateOrEditLeadTypeDto } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'createOrEditLeadTypeModal',
    templateUrl: './create-or-edit-lead-type.component.html',
    styleUrls: ['./create-or-edit-lead-type.component.less'],

})
export class CreateOrEditLeadTypeModalComponent extends AppComponentBase {
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    leadType: CreateOrEditLeadTypeDto = new CreateOrEditLeadTypeDto();

    constructor(
        injector: Injector,
        private _leadTypeServiceProxy: LeadTypeServiceProxy
    ) {
        super(injector);
    }

    show(leadTypeId?: number): void {
        if (!leadTypeId) {
            this.leadType = new CreateOrEditLeadTypeDto();
            this.leadType.id = leadTypeId;
            this.active = true;
            this.modal.show();
        } else {
            this.leadType = new CreateOrEditLeadTypeDto();
            this.leadType.id = leadTypeId;

            this._leadTypeServiceProxy.getLeadTypeForEdit(leadTypeId).subscribe(result => {
                this.leadType = result.leadType;
                this.active = true;
                this.modal.show();
            }, error => { this.spinnerService.hide() });
        }
    }

    onShown(): void {
        document.getElementById('Name').focus();
    }

    save(): void {
        this.saving = true;
        this._leadTypeServiceProxy
            .createOrEdit(this.leadType)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
            }, error => { this.spinnerService.hide() });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
