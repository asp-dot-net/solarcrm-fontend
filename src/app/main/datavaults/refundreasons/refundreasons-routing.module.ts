import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RefundReasonsComponent } from './refundreasons.component';

const routes: Routes = [
    {
        path: '',
        component: RefundReasonsComponent,
        pathMatch: 'full',
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class RefundReasonsRoutingModule {}
