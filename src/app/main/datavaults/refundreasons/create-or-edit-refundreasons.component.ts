import { Component, EventEmitter, Injector, Output, ViewChild } from '@angular/core';
import { AppConsts } from '@shared/AppConsts';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { map as _map, filter as _filter } from 'lodash-es';
import { RefundReasonsServiceProxy, CreateOrEditRefundReasonsDto } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'createOrEditRefundReasonsModal',
    templateUrl: './create-or-edit-refundreasons.component.html',
    styleUrls: ['./create-or-edit-refundreasons.component.less'],

})
export class CreateOrEditRefundReasonsModalComponent extends AppComponentBase {
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    refundReasons: CreateOrEditRefundReasonsDto = new CreateOrEditRefundReasonsDto();

    constructor(
        injector: Injector,
        private _refundReasonsServiceProxy: RefundReasonsServiceProxy
    ) {
        super(injector);
    }

    show(refundReasonsId?: number): void {
        if (!refundReasonsId) {
            this.refundReasons = new CreateOrEditRefundReasonsDto();
            this.refundReasons.id = refundReasonsId;
            this.active = true;
            this.modal.show();
        } else {
            this.refundReasons = new CreateOrEditRefundReasonsDto();
            this.refundReasons.id = refundReasonsId;

            this._refundReasonsServiceProxy.getRefundReasonsForEdit(refundReasonsId).subscribe(result => {
                this.refundReasons = result.refundReason;
                this.active = true;
                this.modal.show();
            }, error => { this.spinnerService.hide() });
        }
    }

    onShown(): void {
        document.getElementById('Name').focus();
    }

    save(): void {
        this.saving = true;
        this._refundReasonsServiceProxy
            .createOrEdit(this.refundReasons)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
            }, error => { this.spinnerService.hide() });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
