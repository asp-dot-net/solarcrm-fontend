import { NgModule } from '@angular/core';
import { AdminSharedModule } from '@app/admin/shared/admin-shared.module';
import { AppSharedModule } from '@app/shared/app-shared.module';
import { RefundReasonsRoutingModule } from './refundreasons-routing.module';
import { RefundReasonsComponent } from './refundreasons.component';
import { CreateOrEditRefundReasonsModalComponent } from './create-or-edit-refundreasons.component';
import { CustomizableDashboardModule } from '@app/shared/common/customizable-dashboard/customizable-dashboard.module';

@NgModule({
    declarations: [
        RefundReasonsComponent,CreateOrEditRefundReasonsModalComponent
    ],
    imports: [AppSharedModule, AdminSharedModule, RefundReasonsRoutingModule,CustomizableDashboardModule
    ],
})
export class RefundReasonsModule {}
