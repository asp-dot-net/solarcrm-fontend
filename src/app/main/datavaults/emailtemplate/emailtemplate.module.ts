import { NgModule } from '@angular/core';
import { AdminSharedModule } from '@app/admin/shared/admin-shared.module';
import { AppSharedModule } from '@app/shared/app-shared.module';
import { EmailTemplateRoutingModule } from './emailtemplate-routing.module';
import { EmailTemplateComponent } from './emailtemplate.component';
import { CustomizableDashboardModule } from '@app/shared/common/customizable-dashboard/customizable-dashboard.module';
import { CreateOrEditEmailTemplateComponent } from './create-or-edit-emailtemplate.component';
import { EmailEditorModule } from 'angular-email-editor';
import { NgSelectModule } from '@ng-select/ng-select';
@NgModule({
    declarations: [EmailTemplateComponent, CreateOrEditEmailTemplateComponent],
    imports: [AppSharedModule, NgSelectModule,AdminSharedModule, EmailTemplateRoutingModule, CustomizableDashboardModule,EmailEditorModule ],
})
export class EmailTemplateModule {}
