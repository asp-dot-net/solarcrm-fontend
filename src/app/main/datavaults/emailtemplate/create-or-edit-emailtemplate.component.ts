import { Component, EventEmitter, Injector, OnInit, Output, ViewChild } from '@angular/core';
import { AppConsts } from '@shared/AppConsts';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { map as _map, filter as _filter } from 'lodash-es';
import { EmailTemplateServiceProxy, CreateOrEditEmailTemplateDto, CommonLookupServiceProxy, OrganizationUnitDto } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import { EmailEditorComponent } from 'angular-email-editor';
@Component({
    selector: 'createOrEditemailtemplateModal',
    templateUrl: './create-or-edit-emailtemplate.component.html',
    styleUrls: ['./create-or-edit-emailtemplate.component.less'],

})
export class CreateOrEditEmailTemplateComponent extends AppComponentBase implements OnInit{
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    organizationList:OrganizationUnitDto[];
    saving = false;
    emailData = '';
    active = false;
    emailTemplate: CreateOrEditEmailTemplateDto = new CreateOrEditEmailTemplateDto();
    organizationUnit = 0;
    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
        private _emailTemplateServiceProxy: EmailTemplateServiceProxy,
        private _commonLookupServiceProxy:CommonLookupServiceProxy,
        private _router: Router
    ) {
        super(injector);
    }
    @ViewChild(EmailEditorComponent)
    private emailEditor: EmailEditorComponent;

    ngOnInit(): void {
        this._activatedRoute.queryParams.subscribe(params => {
            this.emailTemplate.id = params['eid'];
            this.getOrganizationList();
        }, error => { this.spinnerService.hide() });
    }
    editorLoaded(event) {
        if ((this.emailTemplate.id !== null && this.emailTemplate.id !== undefined)) {
            this._emailTemplateServiceProxy.getEmailTemplateForEdit(this.emailTemplate.id).subscribe(result => {
                this.emailData = result.emailTemplates.body;
                this.emailTemplate.subject = result.emailTemplates.subject;
                this.emailTemplate.templateName = result.emailTemplates.templateName;
                this.emailTemplate.organizationUnitId = result.emailTemplates.organizationUnitId;
                if (this.emailData != "") {
                    if (this.emailTemplate.id == 3) {
                        this.emailEditor.editor.setMergeTags({
                            customer: {
                                name: "Customer",
                                mergeTags: {
                                    customer_name: {
                                        name: "Name",
                                        value: "{{Customer.Name}}"
                                    },
                                    address: {
                                        name: "Address",
                                        value: "{{Customer.Address}}"
                                    },
                                    mobile: {
                                        name: "Mobile",
                                        value: "{{Customer.Mobile}}"
                                    },
                                    email: {
                                        name: "Email",
                                        value: "{{Customer.Email}}"
                                    },
                                    phone: {
                                        name: "Phone",
                                        value: "{{Customer.Phone}}"
                                    }
                                }
                            },
                            salesrep: {
                                name: "Sales Rep",
                                mergeTags: {
                                    customer_name: {
                                        name: "Name",
                                        value: "{{Sales.Name}}"
                                    },
                                    mobile: {
                                        name: "Mobile",
                                        value: "{{Sales.Mobile}}"
                                    },
                                    email: {
                                        name: "Email",
                                        value: "{{Sales.Email}}"
                                    }
                                }
                            },
                            quote: {
                                name: "Quote",
                                mergeTags: {
                                    project_no: {
                                        name: "Project No",
                                        value: "{{Quote.ProjectNo}}"
                                    },
                                    quote_systemCapacity: {
                                        name: "System Capacity",
                                        value: "{{Quote.SystemCapacity}}"
                                    },
                                    quote_allproductItem: {
                                        name: "All product Item",
                                        value: "{{Quote.AllproductItem}}"
                                    },
                                    quote_totalQuoteprice: {
                                        name: "Total Quote price",
                                        value: "{{Quote.TotalQuoteprice}}"
                                    },
                                    quote_installationDate: {
                                        name: "Installation Date",
                                        value: "{{Quote.InstallationDate}}"
                                    },
                                    quote_InstallerName: {
                                        name: "Installer Name",
                                        value: "{{Quote.InstallerName}}"
                                    },
                                }
                            },
                            freebies: {
                                name: "Freebies",
                                mergeTags: {
                                    freebies_transportcompany: {
                                        name: "Transport Company",
                                        value: "{{Freebies.TransportCompany}}"
                                    },
                                    freebies_transportlink: {
                                        name: "Transport Link",
                                        value: "{{Freebies.TransportLink}}"
                                    },
                                    freebies_dispatchedDate: {
                                        name: "Dispatched Date",
                                        value: "{{Freebies.DispatchedDate}}"
                                    },
                                    freebies_trackingNo: {
                                        name: "TrackingNo",
                                        value: "{{Freebies.TrackingNo}}"
                                    },
                                    freebies_promotype: {
                                        name: "TrackingNo",
                                        value: "{{Freebies.PromoType}}"
                                    },
                                }
                            },
                            invoice: {
                                name: "User",
                                mergeTags: {
                                    freebies_transportcompany: {
                                        name: "User Name",
                                        value: "{{Invoice.UserName}}"
                                    },
                                    freebies_transportlink: {
                                        name: "User Mobile",
                                        value: "{{Invoice.UserMobile}}"
                                    },
                                    freebies_dispatchedDate: {
                                        name: "User Email",
                                        value: "{{Invoice.UserEmail}}"
                                    },
                                }
                            },
                            Organization: {
                                name: "Organization",
                                mergeTags: {
                                    org_name: {
                                        name: "Organization Name",
                                        value: "{{Organization.orgName}}"
                                    },
                                    org_email: {
                                        name: "Organization Email",
                                        value: "{{Organization.orgEmail}}"
                                    },
                                    org_transportlink: {
                                        name: "Organization Mobile",
                                        value: "{{Organization.orgMobile}}"
                                    },
                                    org_dispatchedDate: {
                                        name: "Organization logo",
                                        value: "{{Organization.orglogo}}"
                                    },
                                }
                            },
                            Review: {
                                name: "Review",
                                mergeTags: {
                                    review_googlelink: {
                                        name: "Review link",
                                        value: "{{Review.link}}"
                                    },
                                }
                            },
                        });
                    }
                    else {
                        this.emailEditor.editor.setMergeTags({
                            customer: {
                                name: "Customer",
                                mergeTags: {
                                    customer_name: {
                                        name: "Name",
                                        value: "{{Customer.Name}}"
                                    },
                                    address: {
                                        name: "Address",
                                        value: "{{Customer.Address}}"
                                    },
                                    mobile: {
                                        name: "Mobile",
                                        value: "{{Customer.Mobile}}"
                                    },
                                    email: {
                                        name: "Email",
                                        value: "{{Customer.Email}}"
                                    },
                                    phone: {
                                        name: "Phone",
                                        value: "{{Customer.Phone}}"
                                    },

                                }
                            },
                            salesrep: {
                                name: "Sales Rep",
                                mergeTags: {
                                    customer_name: {
                                        name: "Name",
                                        value: "{{Sales.Name}}"
                                    },
                                    mobile: {
                                        name: "Mobile",
                                        value: "{{Sales.Mobile}}"
                                    },
                                    email: {
                                        name: "Email",
                                        value: "{{Sales.Email}}"
                                    }
                                }
                            },
                            quote: {
                                name: "Quote",
                                mergeTags: {
                                    project_no: {
                                        name: "Project No",
                                        value: "{{Quote.ProjectNo}}"
                                    },
                                    quote_systemCapacity: {
                                        name: "System Capacity",
                                        value: "{{Quote.SystemCapacity}}"
                                    },
                                    quote_allproductItem: {
                                        name: "All product Item",
                                        value: "{{Quote.AllproductItem}}"
                                    },
                                    quote_totalQuoteprice: {
                                        name: "Total Quote price",
                                        value: "{{Quote.TotalQuoteprice}}"
                                    },
                                    quote_installationDate: {
                                        name: "Installation Date",
                                        value: "{{Quote.InstallationDate}}"
                                    },
                                    quote_InstallerName: {
                                        name: "Installer Name",
                                        value: "{{Quote.InstallerName}}"
                                    },
                                }
                            },
                            freebies: {
                                name: "Freebies",
                                mergeTags: {
                                    freebies_transportcompany: {
                                        name: "Transport Company",
                                        value: "{{Freebies.TransportCompany}}"
                                    },
                                    freebies_transportlink: {
                                        name: "Transport Link",
                                        value: "{{Freebies.TransportLink}}"
                                    },
                                    freebies_dispatchedDate: {
                                        name: "Dispatched Date",
                                        value: "{{Freebies.DispatchedDate}}"
                                    },
                                    freebies_trackingNo: {
                                        name: "TrackingNo",
                                        value: "{{Freebies.TrackingNo}}"
                                    },
                                    freebies_promotype: {
                                        name: "TrackingNo",
                                        value: "{{Freebies.PromoType}}"
                                    },
                                }
                            },
                            invoice: {
                                name: "User",
                                mergeTags: {
                                    invoice_userName: {
                                        name: "User Name",
                                        value: "{{Invoice.UserName}}"
                                    },
                                    invoice_userMobile: {
                                        name: "User Mobile",
                                        value: "{{Invoice.UserMobile}}"
                                    },
                                    invoice_userEmail: {
                                        name: "User Email",
                                        value: "{{Invoice.UserEmail}}"
                                    },
                                    invoice_owning: {
                                        name: "Owning",
                                        value: "{{Invoice.Owning}}"
                                    },
                                }
                            },
                            Organization: {
                                name: "Organization",
                                mergeTags: {
                                    org_name: {
                                        name: "Organization Name",
                                        value: "{{Organization.orgName}}"
                                    },
                                    org_email: {
                                        name: "Organization Email",
                                        value: "{{Organization.orgEmail}}"
                                    },
                                    org_transportlink: {
                                        name: "Organization Mobile",
                                        value: "{{Organization.orgMobile}}"
                                    },
                                    org_dispatchedDate: {
                                        name: "Organization logo",
                                        value: "{{Organization.orglogo}}"
                                    },
                                }
                            },
                            Review: {
                                name: "Review",
                                mergeTags: {
                                    review_googlelink: {
                                        name: "Review link",
                                        value: "{{Review.link}}"
                                    },
                                }
                            },
                        });
                    }

                    this.emailEditor.editor.loadDesign(JSON.parse(this.emailData));
                }
            }, error => { this.spinnerService.hide(); });
        }

    }
    saveDesign() {
        this.emailEditor.editor.saveDesign((data) =>
            this.saveHTML(data)
        );
    }

    saveHTML(data) {
        this.emailTemplate.body = JSON.stringify(data);
        this._emailTemplateServiceProxy.createOrEdit(this.emailTemplate)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                this._router.navigate(['/app/main/datavaults/emailtemplate']);
            }, error => { this.spinnerService.hide() });
    }
    exportHtml() {
        this.emailEditor.editor.exportHtml(
            (data) => console.log('exportHtml', data.html)
        );
    }

    cancel(): void {
        this._router.navigate(['app/main/datavaults/emailtemplate']);
    }


    show(emailTemplateId?: number): void {
      
        
        if (!emailTemplateId) {
            this.emailTemplate = new CreateOrEditEmailTemplateDto();
            this.emailTemplate.id = emailTemplateId;
            this.active = true;
            this.modal.show();
        } else {
            this.emailTemplate = new CreateOrEditEmailTemplateDto();
            this.emailTemplate.id = emailTemplateId;

            this._emailTemplateServiceProxy.getEmailTemplateForEdit(emailTemplateId).subscribe(result => {
                this.emailTemplate = result.emailTemplates;
                this.active = true;
                this.modal.show();
            }, error => { this.spinnerService.hide() });
        }
    }
    getOrganizationList() {
        this._commonLookupServiceProxy.getOrganizationUnitUserWise().subscribe(result => {
            this.organizationList = result;
        }, error => { this.spinnerService.hide() });
    }
    onShown(): void {
        document.getElementById('Name').focus();
    }

    save(): void {
        this.saving = true;
        this._emailTemplateServiceProxy
            .createOrEdit(this.emailTemplate)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
            });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
