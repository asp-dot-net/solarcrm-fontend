import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EmailTemplateComponent } from './emailtemplate.component';
import { CreateOrEditEmailTemplateComponent } from './create-or-edit-emailtemplate.component';  
const routes: Routes = [
    {
        path: '',
        component: EmailTemplateComponent,
        pathMatch: 'full',
    },
    { path: 'createOrEdit', component: CreateOrEditEmailTemplateComponent },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class EmailTemplateRoutingModule {}
