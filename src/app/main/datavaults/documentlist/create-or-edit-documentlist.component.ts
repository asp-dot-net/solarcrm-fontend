import { Component, EventEmitter, Injector, Output, ViewChild } from '@angular/core';
import { AppConsts } from '@shared/AppConsts';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { map as _map, filter as _filter, forEach } from 'lodash-es';
import { DocumentListServiceProxy, CreateOrEditDocumentListDto } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';
import { AppEditionExpireAction } from '@shared/AppEnums';

@Component({
    selector: 'createOrEditDocumentListModal',
    templateUrl: './create-or-edit-documentlist.component.html',
    styleUrls: ['./create-or-edit-documentlist.component.less'],

})
export class CreateOrEditDocumentListModalComponent extends AppComponentBase {
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;
    DocumentFormateList:any[];
    documentList: CreateOrEditDocumentListDto = new CreateOrEditDocumentListDto();

    constructor(
        injector: Injector,
        private _documentListServiceProxy: DocumentListServiceProxy
    ) {
        super(injector);
    }
    DocFormateArray = [];
    show(documentId?: number): void {
        
        this.DocumentFormateList = AppEditionExpireAction.DocumentFormateList
        if (!documentId) {
            this.documentList = new CreateOrEditDocumentListDto();
            this.documentList.id = documentId;
            this.active = true;
            this.modal.show();
        } else {
            this.documentList = new CreateOrEditDocumentListDto();
            this.documentList.id = documentId;
            this._documentListServiceProxy.getDocumentListForEdit(documentId).subscribe(result => {   
                this.documentList = result.documentList;              
                var formatedata = result.documentList.documenFormate.split(',');
                this.DocFormateArray= [];
                 this.DocumentFormateList.forEach(element => {                     
                     if (formatedata.includes(element.name)) {                      
                        element.Selected = true;
                        this.DocFormateArray.push(element); 
                     }
                 });
                 //this.documentList.documenFormate = result.documentList.documenFormate.split(',');
                this.active = true;
                this.modal.show();
            }, error => { this.spinnerService.hide() });
        }
    }

    onShown(): void {
        document.getElementById('Name').focus();
    }
    save(): void {        
        this.saving = true;  
        this.documentList.documenFormate = '';
        
        this.DocFormateArray.forEach(element => {
            this.documentList.documenFormate  +=  ',' +element.id ;
        });
        if(this.documentList.documenFormate != ''){
            this.documentList.documenFormate = this.documentList.documenFormate.slice(1);
        }
        this._documentListServiceProxy
            .createOrEdit(this.documentList)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
            }, error => { this.spinnerService.hide() });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
