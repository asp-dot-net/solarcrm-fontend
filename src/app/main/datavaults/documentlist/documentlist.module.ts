import { NgModule } from '@angular/core';
import { AdminSharedModule } from '@app/admin/shared/admin-shared.module';
import { AppSharedModule } from '@app/shared/app-shared.module';
import { DocumentListRoutingModule } from './documentlist-routing.module';
import { DocumentListComponent } from './documentlist.component';
import { CustomizableDashboardModule } from '@app/shared/common/customizable-dashboard/customizable-dashboard.module';
import { CreateOrEditDocumentListModalComponent } from './create-or-edit-documentlist.component';
import {NgSelectModule, NgOption} from '@ng-select/ng-select';
@NgModule({
    declarations: [DocumentListComponent, CreateOrEditDocumentListModalComponent],
    imports: [AppSharedModule, AdminSharedModule, DocumentListRoutingModule, CustomizableDashboardModule,NgSelectModule],
})
export class DocumentListModule {}
