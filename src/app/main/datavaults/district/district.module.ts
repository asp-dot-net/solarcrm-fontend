import { NgModule } from '@angular/core';
import { AdminSharedModule } from '@app/admin/shared/admin-shared.module';
import { AppSharedModule } from '@app/shared/app-shared.module';
import { DistrictRoutingModule } from './district-routing.module';
import { DistrictComponent } from './district.component';
import { CustomizableDashboardModule } from '@app/shared/common/customizable-dashboard/customizable-dashboard.module';
import { CreateOrEditDistrictModalComponent } from './create-or-edit-district.component';

@NgModule({
    declarations: [DistrictComponent, CreateOrEditDistrictModalComponent],
    imports: [AppSharedModule, AdminSharedModule, DistrictRoutingModule, CustomizableDashboardModule],
})
export class DistrictModule {}
