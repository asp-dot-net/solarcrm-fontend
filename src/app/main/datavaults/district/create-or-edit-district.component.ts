import { Component, EventEmitter, Injector, OnInit, Output, ViewChild } from '@angular/core';
import { AppConsts } from '@shared/AppConsts';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { map as _map, filter as _filter } from 'lodash-es';
import { DistrictServiceProxy, CreateOrEditDistrictDto, StateServiceProxy } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'createOrEditDistrictModal',
    templateUrl: './create-or-edit-district.component.html',
    styleUrls: ['./create-or-edit-district.component.less'],

})
export class CreateOrEditDistrictModalComponent extends AppComponentBase implements OnInit {
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    state: any = [];
    active = false;
    saving = false;

    districts: CreateOrEditDistrictDto = new CreateOrEditDistrictDto();

    constructor(
        injector: Injector,
        private _districtServiceProxy: DistrictServiceProxy,
        private _stateServiceProxy: StateServiceProxy
    ) {
        super(injector);
    }
    ngOnInit(): void {
        this._stateServiceProxy.getAllStateForDropdown().subscribe(result => {
            this.state = result;
        }, error => { this.spinnerService.hide() });

    }

    show(districtId?: number): void {
       
        if (!districtId) {
            this.districts = new CreateOrEditDistrictDto();
            this.districts.id = districtId;
            this.active = true;
            this.modal.show();
        } else {
            this.districts = new CreateOrEditDistrictDto();
            this.districts.id = districtId;

            this._districtServiceProxy.getDistrictForEdit(districtId).subscribe(result => {
                this.districts = result.districts;
                this.active = true;
                this.modal.show();
            }, error => { this.spinnerService.hide() });
        }
    }

    onShown(): void {
        document.getElementById('Name').focus();
    }

    save(): void {
        this.saving = true;
        this._districtServiceProxy
            .createOrEdit(this.districts)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
            }, error => { this.spinnerService.hide() });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
