import { NgModule } from '@angular/core';
import { AdminSharedModule } from '@app/admin/shared/admin-shared.module';
import { AppSharedModule } from '@app/shared/app-shared.module';
import { PaymentTypeRoutingModule } from './paymenttype-routing.module';
import { PaymentTypeComponent } from './paymenttype.component';
import { CustomizableDashboardModule } from '@app/shared/common/customizable-dashboard/customizable-dashboard.module';
import { CreateOrEditPaymentTypeModalComponent } from './create-or-edit-payment-type.component';

@NgModule({
    //declarations: [PaymentTypeComponent, CreateOrEditPaymentTypeModalComponent],
    declarations: [PaymentTypeComponent, CreateOrEditPaymentTypeModalComponent ],
    imports: [AppSharedModule, AdminSharedModule, PaymentTypeRoutingModule, CustomizableDashboardModule],
})
export class PaymentTypeModule {}
