import { Component, EventEmitter, Injector, OnInit, Output, ViewChild } from '@angular/core';
import { AppConsts } from '@shared/AppConsts';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { map as _map, filter as _filter } from 'lodash-es';
import { CityServiceProxy, CreateOrEditCityDto, TalukaServiceProxy } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'createOrEditCityModal',
    templateUrl: './create-or-edit-city.component.html',
    styleUrls: ['./create-or-edit-city.component.less'],

})
export class CreateOrEditCityModalComponent extends AppComponentBase implements OnInit {
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;
    taluka: any = [];
    citys: CreateOrEditCityDto = new CreateOrEditCityDto();

    constructor(
        injector: Injector,
        private _cityServiceProxy: CityServiceProxy,
        private _talukaServiceProxy: TalukaServiceProxy
    ) {
        super(injector);
    }
    ngOnInit(): void {
        this._talukaServiceProxy.getAllTalukaForDropdown().subscribe(result => {

            this.taluka = result;
        }, error => { this.spinnerService.hide() });
    }

    show(cityId?: number): void {

       
        if (!cityId) {
            this.citys = new CreateOrEditCityDto();
            this.citys.id = cityId;
            this.active = true;
            this.modal.show();
        } else {
            this.citys = new CreateOrEditCityDto();
            this.citys.id = cityId;

            this._cityServiceProxy.getCityForEdit(cityId).subscribe(result => {
                this.citys = result.citys;
                this.active = true;
                this.modal.show();
            }, error => { this.spinnerService.hide() });
        }
    }

    onShown(): void {
        document.getElementById('Name').focus();
    }

    save(): void {
        this.saving = true;
        this._cityServiceProxy
            .createOrEdit(this.citys)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
            }, error => { this.spinnerService.hide() });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
