import { NgModule } from '@angular/core';
import { AdminSharedModule } from '@app/admin/shared/admin-shared.module';
import { AppSharedModule } from '@app/shared/app-shared.module';
import { CityRoutingModule } from './city-routing.module';
import { CityComponent } from './city.component';
import { CustomizableDashboardModule } from '@app/shared/common/customizable-dashboard/customizable-dashboard.module';
import { CreateOrEditCityModalComponent } from './create-or-edit-City.component';

@NgModule({
    declarations: [CityComponent, CreateOrEditCityModalComponent],
    imports: [AppSharedModule, AdminSharedModule, CityRoutingModule, CustomizableDashboardModule],
})
export class CityModule {}
