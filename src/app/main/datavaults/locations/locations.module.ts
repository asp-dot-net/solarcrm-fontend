import { NgModule } from '@angular/core';
import { AdminSharedModule } from '@app/admin/shared/admin-shared.module';
import { AppSharedModule } from '@app/shared/app-shared.module';
import { LocationsRoutingModule } from './locations-routing.module';
import { LocationsComponent } from './locations.component'; 
import { CustomizableDashboardModule } from '@app/shared/common/customizable-dashboard/customizable-dashboard.module';
//import { CreateOrEditLocationsModalComponent } from './create-or-edit-locations.component';
import { CreateOrEditLocationsModalComponent } from './create-or-edit-locations.component';

@NgModule({
    declarations: [LocationsComponent, CreateOrEditLocationsModalComponent],
    imports: [AppSharedModule, AdminSharedModule, LocationsRoutingModule, CustomizableDashboardModule],
})
export class LocationsModule {}
