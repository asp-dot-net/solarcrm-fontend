import { Component, EventEmitter, Injector, Output, ViewChild } from '@angular/core';
import { AppConsts } from '@shared/AppConsts';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { map as _map, filter as _filter } from 'lodash-es';
import { LocationsServiceProxy, CreateOrEditLocationsDto } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'createOrEditLocationsModal',
    templateUrl: './create-or-edit-locations.component.html',
    styleUrls: ['./create-or-edit-locations.component.less'],

})
export class CreateOrEditLocationsModalComponent extends AppComponentBase {
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    location: CreateOrEditLocationsDto = new CreateOrEditLocationsDto();

    constructor(
        injector: Injector,
        private _locationServiceProxy: LocationsServiceProxy
    ) {
        super(injector);
    }

    show(locationId?: number): void {

        if (!locationId) {
            this.location = new CreateOrEditLocationsDto();
            this.location.id = locationId;
            this.active = true;
            this.modal.show();
        } else {
            this.location = new CreateOrEditLocationsDto();
            this.location.id = locationId;

            this._locationServiceProxy.getLocationForEdit(locationId).subscribe(result => {
                this.location = result.location;
                this.active = true;
                this.modal.show();
            }, error => { this.spinnerService.hide() });
        }
    }

    onShown(): void {
        document.getElementById('Name').focus();
    }

    save(): void {
        this.saving = true;
        this._locationServiceProxy
            .createOrEdit(this.location)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
            }, error => { this.spinnerService.hide() });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
