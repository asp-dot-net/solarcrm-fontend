import { NgModule } from '@angular/core';
import { AdminSharedModule } from '@app/admin/shared/admin-shared.module';
import { AppSharedModule } from '@app/shared/app-shared.module';
import { RejectReasonsRoutingModule } from './rejectreasons-routing.module';
import { RejectReasonsComponent } from './rejectreasons.component';
import { CustomizableDashboardModule } from '@app/shared/common/customizable-dashboard/customizable-dashboard.module';
import { CreateOrEditRejectReasonsModalComponent } from './create-or-edit-rejectreasons.component';

@NgModule({
    declarations: [RejectReasonsComponent, CreateOrEditRejectReasonsModalComponent],
    imports: [AppSharedModule, AdminSharedModule, RejectReasonsRoutingModule, CustomizableDashboardModule],
})
export class RejectReasonsModule {}
