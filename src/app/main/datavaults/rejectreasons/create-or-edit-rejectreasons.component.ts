import { Component, EventEmitter, Injector, Output, ViewChild } from '@angular/core';
import { AppConsts } from '@shared/AppConsts';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { map as _map, filter as _filter } from 'lodash-es';
import { RejectReasonsServiceProxy, CreateOrEditRejectReasonsDto } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'createOrEditRejectReasonsModal',
    templateUrl: './create-or-edit-rejectreasons.component.html',
    styleUrls: ['./create-or-edit-rejectreasons.component.less'],

})
export class CreateOrEditRejectReasonsModalComponent extends AppComponentBase {
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    rejectReasons: CreateOrEditRejectReasonsDto = new CreateOrEditRejectReasonsDto();

    constructor(
        injector: Injector,
        private _rejectReasonsServiceProxy: RejectReasonsServiceProxy
    ) {
        super(injector);
    }

    show(rejectReasonsId?: number): void {
        if (!rejectReasonsId) {
            this.rejectReasons = new CreateOrEditRejectReasonsDto();
            this.rejectReasons.id = rejectReasonsId;
            this.active = true;
            this.modal.show();
        } else {
            this.rejectReasons = new CreateOrEditRejectReasonsDto();
            this.rejectReasons.id = rejectReasonsId;

            this._rejectReasonsServiceProxy.getRejectReasonsForEdit(rejectReasonsId).subscribe(result => {
                this.rejectReasons = result.rejectreasons;
                this.active = true;
                this.modal.show();
            }, error => { this.spinnerService.hide() });
        }
    }

    onShown(): void {
        document.getElementById('Name').focus();
    }

    save(): void {
        this.saving = true;
        this._rejectReasonsServiceProxy
            .createOrEdit(this.rejectReasons)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
            }, error => { this.spinnerService.hide() });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
