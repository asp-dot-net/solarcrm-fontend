import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RejectReasonsComponent } from './rejectreasons.component';

const routes: Routes = [
    {
        path: '',
        component: RejectReasonsComponent,
        pathMatch: 'full',
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class RejectReasonsRoutingModule {}
