import { Component, EventEmitter, Injector, Output, ViewChild } from '@angular/core';
import { AppConsts } from '@shared/AppConsts';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { map as _map, filter as _filter } from 'lodash-es';
import { PriceServiceProxy, CreateOrEditPriceDto, CommonLookupServiceProxy, CommonLookupDto } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'createOrEditPriceModal',
    templateUrl: './create-or-edit-price.component.html',
    styleUrls: ['./create-or-edit-price.component.less'],

})
export class CreateOrEditPriceModalComponent extends AppComponentBase {
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;
    drptenderList: CommonLookupDto[];
    prices: CreateOrEditPriceDto = new CreateOrEditPriceDto();

    constructor(
        injector: Injector,
        private _priceServiceProxy: PriceServiceProxy,
        private _commonServiceProxy: CommonLookupServiceProxy

    ) {
        super(injector);
    }
    areaTypeList = [
        { id: 'urban', name: 'Urban' },
        { id: 'rural', name: 'Rural' },

    ];
    GetTenderdropDown() {
        this._commonServiceProxy.getAllTenderForDropdown().subscribe(result => {
            this.drptenderList = result;
        }, error => { this.spinnerService.hide() });
    }

    show(priceId?: number): void {
        this.prices.tenderId = null;
        this.GetTenderdropDown();
        if (!priceId) {
            this.prices = new CreateOrEditPriceDto();
            this.prices.id = priceId;
            this.active = true;
            this.modal.show();
        } else {
            this.prices = new CreateOrEditPriceDto();
            this.prices.id = priceId;
            this._priceServiceProxy.getPriceForEdit(priceId).subscribe(result => {
                this.prices = result.price;
                this.active = true;
                this.modal.show();
            }, error => { this.spinnerService.hide() });
        }
    }

    onShown(): void {
        document.getElementById('tenderId').focus();
    }

    save(): void {
        this.saving = true;
        // if (this.prices.meterType) {
        //     this.prices.meterType = 1;
        // }
        // else {
        //     this.prices.tenderId = 0;
        // }
        this._priceServiceProxy
            .createOrEdit(this.prices)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
            }, error => { this.spinnerService.hide() });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
