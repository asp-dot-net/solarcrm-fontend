import { NgModule } from '@angular/core';
import { AdminSharedModule } from '@app/admin/shared/admin-shared.module';
import { AppSharedModule } from '@app/shared/app-shared.module';
import { PriceRoutingModule } from './price-routing.module';
import { PriceComponent } from './price.component';
import { CustomizableDashboardModule } from '@app/shared/common/customizable-dashboard/customizable-dashboard.module';
import { CreateOrEditPriceModalComponent } from './create-or-edit-price.component';
import { NgSelectModule } from '@ng-select/ng-select';

@NgModule({
    declarations: [PriceComponent, CreateOrEditPriceModalComponent],
    imports: [AppSharedModule, AdminSharedModule, PriceRoutingModule, CustomizableDashboardModule,NgSelectModule],
})
export class PriceModule {}
