import { Component, EventEmitter, Injector, Output, ViewChild } from '@angular/core';
import { AppConsts } from '@shared/AppConsts';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { map as _map, filter as _filter } from 'lodash-es';
import { HoldReasonsServiceProxy, CreateOrEditHoldReasonsDto } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'createOrEditHoldReasonsModal',
    templateUrl: './create-or-edit-holdreasons.component.html',
    styleUrls: ['./create-or-edit-holdreasons.component.less'],

})
export class CreateOrEditHoldReasonsModalComponent extends AppComponentBase {
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    holdReasons: CreateOrEditHoldReasonsDto = new CreateOrEditHoldReasonsDto();

    constructor(
        injector: Injector,
        private _holdReasonsServiceProxy: HoldReasonsServiceProxy
    ) {
        super(injector);
    }

    show(holdReasonsId?: number): void {
        if (!holdReasonsId) {
            this.holdReasons = new CreateOrEditHoldReasonsDto();
            this.holdReasons.id = holdReasonsId;
            this.active = true;
            this.modal.show();
        } else {
            this.holdReasons = new CreateOrEditHoldReasonsDto();
            this.holdReasons.id = holdReasonsId;

            this._holdReasonsServiceProxy.getHoldReasonsForEdit(holdReasonsId).subscribe(result => {
                this.holdReasons = result.holdreasons;
                this.active = true;
                this.modal.show();
            }, error => { this.spinnerService.hide() });
        }
    }

    onShown(): void {
        document.getElementById('Name').focus();
    }

    save(): void {
        this.saving = true;
        this._holdReasonsServiceProxy
            .createOrEdit(this.holdReasons)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
            }, error => { this.spinnerService.hide() });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
