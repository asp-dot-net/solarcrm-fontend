import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HoldReasonsComponent } from './holdreasons.component';

const routes: Routes = [
    {
        path: '',
        component: HoldReasonsComponent,
        pathMatch: 'full',
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class HoldReasonsRoutingModule {}
