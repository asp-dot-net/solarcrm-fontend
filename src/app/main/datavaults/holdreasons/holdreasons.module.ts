import { NgModule } from '@angular/core';
import { AdminSharedModule } from '@app/admin/shared/admin-shared.module';
import { AppSharedModule } from '@app/shared/app-shared.module';
import { HoldReasonsRoutingModule } from './holdreasons-routing.module';
import { HoldReasonsComponent } from './holdreasons.component';
import { CustomizableDashboardModule } from '@app/shared/common/customizable-dashboard/customizable-dashboard.module';
import { CreateOrEditHoldReasonsModalComponent } from './create-or-edit-holdreasons.component';

@NgModule({
    declarations: [HoldReasonsComponent, CreateOrEditHoldReasonsModalComponent],
    imports: [AppSharedModule, AdminSharedModule, HoldReasonsRoutingModule, CustomizableDashboardModule],
})
export class HoldReasonsModule {}
