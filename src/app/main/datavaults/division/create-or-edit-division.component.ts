import { Component, EventEmitter, Injector, OnInit, Output, ViewChild } from '@angular/core';
import { AppConsts } from '@shared/AppConsts';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { map as _map, filter as _filter } from 'lodash-es';
import { DivisionServiceProxy, CreateOrEditDivisionDto, DiscomServiceProxy, CommonLookupServiceProxy } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'createOrEditDivisionModal',
    templateUrl: './create-or-edit-division.component.html',
    styleUrls: ['./create-or-edit-division.component.less'],

})
export class CreateOrEditDivisionModalComponent extends AppComponentBase  implements OnInit {
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    division: CreateOrEditDivisionDto = new CreateOrEditDivisionDto();

    circleList: any = [];

    constructor(
        injector: Injector,
        private _divisionServiceProxy: DivisionServiceProxy,
        private _discomServiceProxy: DiscomServiceProxy,
        private _commonLookupServiceProxy: CommonLookupServiceProxy
    ) {
        super(injector);
    }

    ngOnInit(): void {
        this._commonLookupServiceProxy.getCircleForDropdown().subscribe(result => {
            this.circleList = result;
        }, error => { this.spinnerService.hide() })

    }

    show(divisionId?: number): void {
        if (!divisionId) {
            this.division = new CreateOrEditDivisionDto();
            this.division.id = divisionId;
            this.active = true;
            this.modal.show();
        } else {
            this.division = new CreateOrEditDivisionDto();
            this.division.id = divisionId;

            this._divisionServiceProxy.getDivisionForEdit(divisionId).subscribe(result => {
                this.division = result.division;
                this.active = true;
                this.modal.show();
            }, error => { this.spinnerService.hide() });
        }
    }

    onShown(): void {
        document.getElementById('Name').focus();
    }

    save(): void {
        this.saving = true;
        this._divisionServiceProxy
            .createOrEdit(this.division)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
            }, error => { this.spinnerService.hide() });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
