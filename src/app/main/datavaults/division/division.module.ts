import { NgModule } from '@angular/core';
import { AdminSharedModule } from '@app/admin/shared/admin-shared.module';
import { AppSharedModule } from '@app/shared/app-shared.module';
import { DivisionRoutingModule } from './division-routing.module';
import { DivisionComponent } from './division.component';
import { CustomizableDashboardModule } from '@app/shared/common/customizable-dashboard/customizable-dashboard.module';
import { CreateOrEditDivisionModalComponent } from './create-or-edit-division.component';

@NgModule({
    declarations: [DivisionComponent, CreateOrEditDivisionModalComponent],
    imports: [AppSharedModule, AdminSharedModule, DivisionRoutingModule, CustomizableDashboardModule],
})
export class DivisionModule {}
