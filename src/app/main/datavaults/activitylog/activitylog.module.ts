import { NgModule } from '@angular/core';
import { AdminSharedModule } from '@app/admin/shared/admin-shared.module';
import { AppSharedModule } from '@app/shared/app-shared.module';
import { ActivityRoutingModule } from './activitylog-routing.module';
import { ActivityLogComponent } from './activitylog.component';
import { CustomizableDashboardModule } from '@app/shared/common/customizable-dashboard/customizable-dashboard.module';
import { DatavaultLogHistoryComponent } from './datavault-log-history.component';

@NgModule({
   declarations: [ActivityLogComponent,DatavaultLogHistoryComponent],
    imports: [AppSharedModule, AdminSharedModule, ActivityRoutingModule, CustomizableDashboardModule],
})
export class ActivitylogModule {}
