import { Component, Injector, ViewEncapsulation, ViewChild, HostListener } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { ActivatedRoute , Router} from '@angular/router';
import { NotifyService } from 'abp-ng2-module';
import { TokenAuthServiceProxy } from '@shared/service-proxies/service-proxies';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import { Title } from '@angular/platform-browser';
import { LazyLoadEvent } from 'primeng/api';
import { ActivityLogMasterVaultServiceProxy, DataVaultLogDto,DataVaultHistoriesLogDto } from '@shared/service-proxies/service-proxies';
//import { CreateOrEditStateModalComponent } from './create-or-edit-activitylog.component';
import { ModalDirective } from 'ngx-bootstrap/modal';

@Component({
    templateUrl: './activitylog.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()],
})
export class ActivityLogComponent extends AppComponentBase {
    //@ViewChild('createOrEditStateModal', { static: true}) createOrEditStateModal: CreateOrEditStateModalComponent;
    @ViewChild('detailActivityModal', { static: true }) modal: ModalDirective;
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    recordCount :any;
    advancedFiltersAreShown = false;
    filterText : string = '';
    ExpandedView: boolean = true;
    SectionWiseActionList:DataVaultLogDto[];
    DataVaultHistoryList:DataVaultHistoriesLogDto[];
    public screenHeight: any;  
    testHeight = 200;
    show: boolean = true;
    toggle: boolean = true;

    toggleBlock() {
        this.show = !this.show;
    };
    change() {
        this.toggle = !this.toggle;
    }
    constructor(
        injector: Injector,
        private _dataVaultLogService: ActivityLogMasterVaultServiceProxy,
        private _notifyService: NotifyService,
        private _tokenAuth: TokenAuthServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _fileDownloadService: FileDownloadService,
        private titleService: Title
    ) {
        super(injector);    
        //this.getActivity();    
    }
    ngOnInit(): void {
        this.screenHeight = window.innerHeight;
      }

    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }
    getActivity(event?: LazyLoadEvent) {
        // if(this.primengTableHelper.shouldResetPaging(event)){
        //     this.paginator.changePage(0);
        //     return;
        // }

        this.primengTableHelper.showLoadingIndicator();
        
         this._dataVaultLogService.getAllDataVaultLog(
             this.filterText,
             this.primengTableHelper.getSorting(this.dataTable),
             this.primengTableHelper.getSkipCount(this.paginator, event),
              this.primengTableHelper.getMaxResultCount(this.paginator, event)
         ).subscribe((result) => {            
             this.primengTableHelper.totalRecordsCount = result.totalCount;
             this.primengTableHelper.records = result.items;
             this.primengTableHelper.hideLoadingIndicator();
             this.recordCount = result.totalCount;
         }, error => { this.spinnerService.hide() });
    }

    navigateToLeadDetail(dataVaultSectionid): void {
        
        this.ExpandedView = !this.ExpandedView;
        this.ExpandedView = false;
        this.GetDataVaultSectionWiseLogList(dataVaultSectionid);
        
    }
    setctionName:string = '';
    GetDataVaultSectionWiseLogList(sectionId){
        this._dataVaultLogService.getDataVaultLogForView(sectionId).subscribe(result => {
            this.setctionName = result[0].sectionName;
            this.SectionWiseActionList = result;
        }, error => { this.spinnerService.hide() });
    }
    DataVaultloghistory(DataVaultHistoryId){
        this._dataVaultLogService.getDataVaultHistoriesLogForView(DataVaultHistoryId).subscribe(result => {
        
            this.DataVaultHistoryList = result;
        }, error => { this.spinnerService.hide() });
        this.modal.show();
    }
    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    exportToExcel(): void {
        // this._stateService.getStateToExcel(
        // this.filterText,
        // )
        // .subscribe(result => {
        //     this._fileDownloadService.downloadTempFile(result);
        //  });
    }    
    //@ViewChild('detailActivityModal', { static: true}) detailActivityModal: detailActivityModalComponent;
    viewActivityDetail() {
        this.modal.show();
        //this.detailActivityModal.show();
    }
    close(): void {
        this.modal.hide();
    }
    expandGrid() {
        this.ExpandedView = true;
    }
}
