import { Component, Injector, ViewEncapsulation, ViewChild, HostListener } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { ActivatedRoute, Router } from '@angular/router';
import { NotifyService } from 'abp-ng2-module';
import { TokenAuthServiceProxy } from '@shared/service-proxies/service-proxies';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import { Title } from '@angular/platform-browser';
import { LazyLoadEvent } from 'primeng/api';
import { DepartmentServiceProxy, DepartmentDto } from '@shared/service-proxies/service-proxies';
//import { LeadSourceServiceProxy, LeadSourceDto } from '@shared/service-proxies/service-proxies';
import { CreateOrEditDepartmentModalComponent } from './create-or-edit-department.component';

@Component({
    templateUrl: './department.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()],
})
export class DepartmentComponent extends AppComponentBase {
    @ViewChild('createOrEditDepartmentModal', { static: true }) createOrEditDepartmentModal: CreateOrEditDepartmentModalComponent;
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;

    advancedFiltersAreShown = false;
    filterText: string = '';
    recordCount: any;
    scrollableCols: any[];
    frozenCols: any[];
    rowGroupMetadata: any;
    selectedProduct1: any;

    public screenHeight: any;  
    testHeight = 200;
    show: boolean = true;
    toggle: boolean = true;

    toggleBlock() {
        this.show = !this.show;
    };
    change() {
        this.toggle = !this.toggle;
    }

    constructor(
        injector: Injector,
        private _departmentService: DepartmentServiceProxy,
        private _notifyService: NotifyService,
        private _tokenAuth: TokenAuthServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _fileDownloadService: FileDownloadService,
        private titleService: Title
    ) {
        super(injector);
    }

    ngOnInit(): void {
        this.screenHeight = window.innerHeight;
      }

    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }

    getDepartment(event?: LazyLoadEvent) {

        // if (this.primengTableHelper.shouldResetPaging(event)) {
        //     this.paginator.changePage(0);
        //     return;
        // }

        this.primengTableHelper.showLoadingIndicator();

        this._departmentService.getAll(
            this.filterText,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe((result) => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            this.primengTableHelper.hideLoadingIndicator();
            //this.RecordCount = result.items;
            this.recordCount = result.totalCount;
        }, error => { this.spinnerService.hide() });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    deleteDepartment(department: DepartmentDto): void {
        this.message.confirm(
            this.l('DepartmentDeleteWarningMessage', department.name),
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._departmentService.delete(department.id)
                        .subscribe(() => {
                            this.reloadPage();
                            this.notify.success(this.l('SuccessfullyDeleted'));
                        }, error => { this.spinnerService.hide() });
                }
            }
        );
    }

    exportToExcel(): void {
        this._departmentService.getDepartmentToExcel(
            this.filterText,
        )
            .subscribe(result => {
                this._fileDownloadService.downloadTempFile(result);
            }, error => { this.spinnerService.hide() });
    }

    createDepartment(): void {
        this.createOrEditDepartmentModal.show();
    }
}
