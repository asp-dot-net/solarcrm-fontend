import { Component, EventEmitter, Injector, Output, ViewChild } from '@angular/core';
import { AppConsts } from '@shared/AppConsts';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { map as _map, filter as _filter } from 'lodash-es';
import { DepartmentServiceProxy, CreateOrEditDepartmentDto } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'createOrEditDepartmentModal',
    templateUrl: './create-or-edit-department.component.html',
    styleUrls: ['./create-or-edit-department.component.less'],

})
export class CreateOrEditDepartmentModalComponent extends AppComponentBase {
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    department: CreateOrEditDepartmentDto = new CreateOrEditDepartmentDto();

    constructor(
        injector: Injector,
        private _departmentServiceProxy: DepartmentServiceProxy
    ) {
        super(injector);
    }

    show(departmentId?: number): void {

        if (!departmentId) {
            this.department = new CreateOrEditDepartmentDto();
            this.department.id = departmentId;
            this.active = true;
            this.modal.show();
        } else {
            this.department = new CreateOrEditDepartmentDto();
            this.department.id = departmentId;

            this._departmentServiceProxy.getDepartmentForEdit(departmentId).subscribe(result => {
                this.department = result.department;
                this.active = true;
                this.modal.show();
            }, error => { this.spinnerService.hide() });
        }
    }

    onShown(): void {
        document.getElementById('Name').focus();
    }

    save(): void {
        this.saving = true;
        this._departmentServiceProxy
            .createOrEdit(this.department)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
            }, error => { this.spinnerService.hide() });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
