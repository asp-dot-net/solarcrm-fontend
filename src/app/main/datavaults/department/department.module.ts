import { NgModule } from '@angular/core';
import { AdminSharedModule } from '@app/admin/shared/admin-shared.module';
import { AppSharedModule } from '@app/shared/app-shared.module';
import { DepartmentRoutingModule } from './department-routing.module';
import { DepartmentComponent } from './department.component'; 
import { CustomizableDashboardModule } from '@app/shared/common/customizable-dashboard/customizable-dashboard.module';
import { CreateOrEditDepartmentModalComponent } from './create-or-edit-department.component';

@NgModule({
    declarations: [DepartmentComponent, CreateOrEditDepartmentModalComponent],
    imports: [AppSharedModule, AdminSharedModule, DepartmentRoutingModule, CustomizableDashboardModule],
})
export class DepartmentModule {}
