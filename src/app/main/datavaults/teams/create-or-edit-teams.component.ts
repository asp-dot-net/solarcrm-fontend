import { Component, EventEmitter, Injector, Output, ViewChild } from '@angular/core';
import { AppConsts } from '@shared/AppConsts';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { map as _map, filter as _filter } from 'lodash-es';
import { TeamsServiceProxy, CreateOrEditTeamsDto  } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'createOrEditTeamsModal',
    templateUrl: './create-or-edit-teams.component.html',
    styleUrls: ['./create-or-edit-teams.component.less'],
    
})
export class CreateOrEditTeamsModalComponent extends AppComponentBase{
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    
    active = false;
    saving = false;

    teams: CreateOrEditTeamsDto = new CreateOrEditTeamsDto();

    constructor(
        injector: Injector,
        private _teamsServiceProxy: TeamsServiceProxy
    ) {
        super(injector);
    }

    show(teamId?: number): void {
        if (!teamId) {
            this.teams = new CreateOrEditTeamsDto();
            this.teams.id = teamId;
            this.active = true;
            this.modal.show();
        } else {
            this.teams = new CreateOrEditTeamsDto();
            this.teams.id = teamId;

            this._teamsServiceProxy.getTeamsForEdit(teamId).subscribe(result => {
                this.teams = result.teams;
                this.active = true;
                this.modal.show();
            }, error => { this.spinnerService.hide() });
        }
    }

    onShown(): void {
        document.getElementById('Name').focus();
    }

    save(): void {
        this.saving = true;
        this._teamsServiceProxy
            .createOrEdit(this.teams)
            .pipe(
                finalize(() => { this.saving = false;
                })
            )
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
            }, error => { this.spinnerService.hide() });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
