import { NgModule } from '@angular/core';
import { AdminSharedModule } from '@app/admin/shared/admin-shared.module';
import { AppSharedModule } from '@app/shared/app-shared.module';
import { TeamsRoutingModule } from './teams-routing.module';
import { TeamsComponent } from './teams.component';
import { CustomizableDashboardModule } from '@app/shared/common/customizable-dashboard/customizable-dashboard.module';
import { CreateOrEditTeamsModalComponent } from './create-or-edit-teams.component';

@NgModule({
    declarations: [TeamsComponent, CreateOrEditTeamsModalComponent],
    imports: [AppSharedModule, AdminSharedModule, TeamsRoutingModule, CustomizableDashboardModule],
})
export class TeamsModule {}
