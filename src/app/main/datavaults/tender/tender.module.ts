import { NgModule } from '@angular/core';
import { AdminSharedModule } from '@app/admin/shared/admin-shared.module';
import { AppSharedModule } from '@app/shared/app-shared.module';
import { TenderRoutingModule } from './tender-routing.module';
import { TenderComponent } from './tender.component';
import { CustomizableDashboardModule } from '@app/shared/common/customizable-dashboard/customizable-dashboard.module';
import { CreateOrEditTenderModalComponent } from './create-or-edit-tender.component';

@NgModule({
    declarations: [TenderComponent, CreateOrEditTenderModalComponent],
    imports: [AppSharedModule, AdminSharedModule, TenderRoutingModule, CustomizableDashboardModule],
})
export class TenderModule {}
