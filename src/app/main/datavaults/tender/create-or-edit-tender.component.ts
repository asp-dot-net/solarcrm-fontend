import { Component, ElementRef, EventEmitter, Injector, OnInit, Output, ViewChild } from '@angular/core';
import { AppConsts } from '@shared/AppConsts';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { map as _map, filter as _filter } from 'lodash-es';
import { TenderServiceProxy, CreateOrEditTenderDto ,CommonLookupServiceProxy, CommonLookupDto } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';
import { DateTime } from 'luxon';

@Component({
    selector: 'createOrEditTendereModal',
    templateUrl: './create-or-edit-tender.component.html',
    styleUrls: ['./create-or-edit-tender.component.less'],
    
})
export class CreateOrEditTenderModalComponent extends AppComponentBase implements OnInit{
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    //@ViewChild('SampleDatePicker', { static: true }) sampleDatePicker: ElementRef;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    
    active = false;
    saving = false;
    drpSolarType: CommonLookupDto[];
    tender: CreateOrEditTenderDto = new CreateOrEditTenderDto();
    startDate: DateTime;
    endDate: DateTime;

    constructor(
        injector: Injector,
        private _tenderServiceProxy: TenderServiceProxy,
        private _commonLookupServiceProxy: CommonLookupServiceProxy
    ) {
        super(injector);
    }

    ngOnInit(): void {
        this._commonLookupServiceProxy.getAllSolarTypeForDropdown().subscribe(result => {            
            this.drpSolarType = result;
        }, error => { this.spinnerService.hide() });
    }
    
    show(tenderId?: number): void {
         
         if (!tenderId) 
         {
            this.tender = new CreateOrEditTenderDto();
            this.tender.id = tenderId;
            this.active = true;
            this.modal.show();
        }
        else
        {
            this.tender = new CreateOrEditTenderDto();
            this.tender.id = tenderId;
            
            this._tenderServiceProxy.getTenderForEdit(tenderId).subscribe(result => {
                this.tender = result.tender;
                this.active = true;
                this.modal.show();
            }, error => { this.spinnerService.hide() });
        }
    }

    onShown(): void {
        document.getElementById('Name').focus();
    }

     save(): void {
         this.saving = true;
         this._tenderServiceProxy
             .createOrEdit(this.tender)
             .pipe(
                 finalize(() => { this.saving = false;
                 })
             )
             .subscribe(() => {
                 this.notify.info(this.l('SavedSuccessfully'));
                 this.close();
                 this.modalSave.emit(null);
             }, error => { this.spinnerService.hide() });
     }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
