import { Component, Injector, ViewEncapsulation, ViewChild, HostListener } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { ActivatedRoute , Router} from '@angular/router';
import { NotifyService } from 'abp-ng2-module';
import { TokenAuthServiceProxy } from '@shared/service-proxies/service-proxies';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import { Title } from '@angular/platform-browser';
import { LazyLoadEvent } from 'primeng/api';
import { TenderServiceProxy, TenderDto } from '@shared/service-proxies/service-proxies';
import { CreateOrEditTenderModalComponent } from './create-or-edit-tender.component';

@Component({
    templateUrl: './tender.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()],
})
export class TenderComponent extends AppComponentBase {
    @ViewChild('createOrEditTendereModal', { static: true}) createOrEditTendereModal: CreateOrEditTenderModalComponent;
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    recordCount:any;
    advancedFiltersAreShown = false;
    filterText : string = '';
    public screenHeight: any;  
    testHeight = 200;
    show: boolean = true;
    toggle: boolean = true;

    toggleBlock() {
        this.show = !this.show;
    };
    change() {
        this.toggle = !this.toggle;
    }
    constructor(
        injector: Injector,
        private _tenderService: TenderServiceProxy,
        private _fileDownloadService: FileDownloadService,       
        private titleService: Title
    ) {
        super(injector);
        
    }
    ngOnInit(): void {
        this.screenHeight = window.innerHeight;
      }

    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }
    getTender(event?: LazyLoadEvent) {
        // if(this.primengTableHelper.shouldResetPaging(event)){
        //     this.paginator.changePage(0);
        //     return;
        // }

        this.primengTableHelper.showLoadingIndicator();
        
        this._tenderService.getAll(
            this.filterText,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
             this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe((result) => {            
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            this.primengTableHelper.hideLoadingIndicator();
            this.recordCount = result.totalCount;
        }, error => { this.spinnerService.hide() });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    deleteTender(tender: TenderDto): void {
        this.message.confirm(
            this.l('Do you Want to remove this record?', tender.name),
            this.l('AreYouSure'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._tenderService.delete(tender.id)
                        .subscribe(() => {
                            this.reloadPage();
                            this.notify.success(this.l('SuccessfullyDeleted'));
                        }, error => { this.spinnerService.hide() });
                }
            }
        );
    }

    exportToExcel(): void {
        this._tenderService.getTenderToExcel(
        this.filterText,
        )
        .subscribe(result => {            
            this._fileDownloadService.downloadTempFile(result);
         }, error => { this.spinnerService.hide() });
    }

    createTender() : void {
        this.createOrEditTendereModal.show();
    }
}
