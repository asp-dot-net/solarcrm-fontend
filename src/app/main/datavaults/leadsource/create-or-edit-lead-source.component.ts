import { Component, EventEmitter, Injector, Output, ViewChild } from '@angular/core';
import { AppConsts } from '@shared/AppConsts';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { map as _map, filter as _filter } from 'lodash-es';
import { LeadSourceServiceProxy, CreateOrEditLeadSourceDto, CommonLookupServiceProxy, OrganizationUnitDto } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'createOrEditLeadSourceModal',
    templateUrl: './create-or-edit-lead-source.component.html',
    styleUrls: ['./create-or-edit-lead-source.component.less'],

})
export class CreateOrEditLeadSourceModalComponent extends AppComponentBase {
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;
    organizationList: OrganizationUnitDto[];
    leadSource: CreateOrEditLeadSourceDto = new CreateOrEditLeadSourceDto();
    OrgnazitionListData: [];
    constructor(
        injector: Injector,
        private _leadSourceServiceProxy: LeadSourceServiceProxy,
        private _commonLookupServiceProxy: CommonLookupServiceProxy
    ) {
        super(injector);
    }

    show(leadSourceId?: number): void {
        this.getOrganizationList();
        if (!leadSourceId) {
            this.leadSource = new CreateOrEditLeadSourceDto();
            this.leadSource.id = leadSourceId;
            this.active = true;
            this.modal.show();
        } else {
            this.leadSource = new CreateOrEditLeadSourceDto();
            this.leadSource.id = leadSourceId;

            this._leadSourceServiceProxy.getLeadSourceForEdit(leadSourceId).subscribe(result => {
                this.leadSource = result.leadSource;
                this.active = true;
                this.modal.show();
            }, error => { this.spinnerService.hide() });
        }
    }
    getOrganizationList() {
        this._commonLookupServiceProxy.getOrganizationUnitUserWise().subscribe(result => {
            this.organizationList = result;
        }, error => { this.spinnerService.hide() });
    }
    onShown(): void {
        document.getElementById('Name').focus();
    }

    save(): void {
        this.saving = true;
        if (this.leadSource.organizationUnits.length == 0) {
            this.notify.warn(this.l('Please Select any One Organization'));
        }
        this._leadSourceServiceProxy
            .createOrEdit(this.leadSource)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
            }, error => { this.spinnerService.hide() });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
