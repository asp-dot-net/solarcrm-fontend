import { NgModule } from '@angular/core';
import { AdminSharedModule } from '@app/admin/shared/admin-shared.module';
import { AppSharedModule } from '@app/shared/app-shared.module';
import { LeadSourceRoutingModule } from './leadsource-routing.module';
import { LeadSourceComponent } from './leadsource.component';
import { CustomizableDashboardModule } from '@app/shared/common/customizable-dashboard/customizable-dashboard.module';
import { CreateOrEditLeadSourceModalComponent } from './create-or-edit-lead-source.component';
import { NgSelectModule } from '@ng-select/ng-select';
@NgModule({
    declarations: [LeadSourceComponent, CreateOrEditLeadSourceModalComponent],
    imports: [AppSharedModule,NgSelectModule, AdminSharedModule, LeadSourceRoutingModule, CustomizableDashboardModule],
})
export class LeadSourceModule {}
