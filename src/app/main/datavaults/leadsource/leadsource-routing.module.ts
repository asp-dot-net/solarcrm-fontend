import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LeadSourceComponent } from './leadsource.component';

const routes: Routes = [
    {
        path: '',
        component: LeadSourceComponent,
        pathMatch: 'full',
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class LeadSourceRoutingModule {}
