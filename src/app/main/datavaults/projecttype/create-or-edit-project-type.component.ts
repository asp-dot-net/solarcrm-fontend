import { Component, EventEmitter, Injector, Output, ViewChild } from '@angular/core';
import { AppConsts } from '@shared/AppConsts';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { map as _map, filter as _filter } from 'lodash-es';
import { ProjectTypeServiceProxy, CreateOrEditProjectTypeDto } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'createOrEditProjectTypeModal',
    templateUrl: './create-or-edit-project-type.component.html',
    styleUrls: ['./create-or-edit-project-type.component.less'],

})
export class CreateOrEditProjectTypeModalComponent extends AppComponentBase {
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    projectType: CreateOrEditProjectTypeDto = new CreateOrEditProjectTypeDto();

    constructor(
        injector: Injector,
        private _projectTypeServiceProxy: ProjectTypeServiceProxy
    ) {
        super(injector);
    }

    show(projectTypeId?: number): void {
        if (!projectTypeId) {
            this.projectType = new CreateOrEditProjectTypeDto();
            this.projectType.id = projectTypeId;
            this.active = true;
            this.modal.show();
        } else {
            this.projectType = new CreateOrEditProjectTypeDto();
            this.projectType.id = projectTypeId;

            this._projectTypeServiceProxy.getProjectTypeForEdit(projectTypeId).subscribe(result => {
                this.projectType = result.projectType;
                this.active = true;
                this.modal.show();
            }, error => { this.spinnerService.hide() });
        }
    }

    onShown(): void {
        document.getElementById('Name').focus();
    }

    save(): void {
        this.saving = true;
        this._projectTypeServiceProxy
            .createOrEdit(this.projectType)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
            }, error => { this.spinnerService.hide() });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
