import { NgModule } from '@angular/core';
import { AdminSharedModule } from '@app/admin/shared/admin-shared.module';
import { AppSharedModule } from '@app/shared/app-shared.module';
import { ProjectTypeRoutingModule } from './projecttype-routing.module';
import { ProjectTypeComponent } from './projecttype.component';
import { CustomizableDashboardModule } from '@app/shared/common/customizable-dashboard/customizable-dashboard.module';
import { CreateOrEditProjectTypeModalComponent } from './create-or-edit-project-type.component';

@NgModule({
    declarations: [ProjectTypeComponent, CreateOrEditProjectTypeModalComponent],
    imports: [AppSharedModule, AdminSharedModule, ProjectTypeRoutingModule, CustomizableDashboardModule],
})
export class ProjectTypeModule {}
