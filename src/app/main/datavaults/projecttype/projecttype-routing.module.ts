import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProjectTypeComponent } from './projecttype.component';

const routes: Routes = [
    {
        path: '',
        component: ProjectTypeComponent,
        pathMatch: 'full',
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class ProjectTypeRoutingModule {}
