import { Component, EventEmitter, Injector, Output, ViewChild } from '@angular/core';
import { AppConsts } from '@shared/AppConsts';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { map as _map, filter as _filter } from 'lodash-es';
import { InvoiceTypeServiceProxy, CreateOrEditInvoiceTypeDto, CreateOrEditPaymentTypeDto, PaymentTypeServiceProxy } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'createOrEditPaymentTypeModal',
    templateUrl: './create-or-edit-payment-type.component.html',
    styleUrls: ['./create-or-edit-payment-type.component.less'],

})
export class CreateOrEditPaymentTypeModalComponent extends AppComponentBase {
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    paymentType: CreateOrEditPaymentTypeDto = new CreateOrEditPaymentTypeDto();

    constructor(
        injector: Injector,
        private _paymentTypeServiceProxy: PaymentTypeServiceProxy
    ) {
        super(injector);
    }

    show(paymentTypeId?: number): void {
        if (!paymentTypeId) {
            this.paymentType = new CreateOrEditPaymentTypeDto();
            this.paymentType.id = paymentTypeId;
            this.active = true;
            this.modal.show();
        } else {
            this.paymentType = new CreateOrEditPaymentTypeDto();
            this.paymentType.id = paymentTypeId
            this._paymentTypeServiceProxy.getPaymentTypeForEdit(paymentTypeId).subscribe(result => {
                this.paymentType = result.paymentType;
                this.active = true;
                this.modal.show();
            }, error => { this.spinnerService.hide() });
        }
    }



    onShown(): void {
        document.getElementById('Name').focus();
    }

    save(): void {
        this.saving = true;
        this._paymentTypeServiceProxy
            .createOrEdit(this.paymentType)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
            }, error => { this.spinnerService.hide() });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
