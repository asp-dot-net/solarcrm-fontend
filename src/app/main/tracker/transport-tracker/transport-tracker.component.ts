import { OnInit, Component, Injector, ViewEncapsulation, ViewChild, Input, Output, EventEmitter, AfterViewInit, HostListener } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { LazyLoadEvent } from 'primeng/api';
import { CommonLookupServiceProxy, CommonLookupDto, UserServiceProxy, OrganizationUnitDto, UserListDto, TransportTrackerServiceProxy, TransportTrackerSummaryCount } from '@shared/service-proxies/service-proxies';
import { Paginator } from 'primeng/paginator';
import { Title } from '@angular/platform-browser';
import * as _ from 'lodash';
import * as moment from 'moment';
import { DataTransferService } from '@app/shared/common/projectservice/data-transfer.service';
import { TransportTrackerDetailModal } from './transport-tracker-detail-modal.component';
import { ExtraMaterialModal } from './extra-material-modal.component';
import { ReturnMaterialModal } from './return-material-modal.component';
import { RevertDispatchModal } from './revert-dispatch-modal.component';
import { DateTime } from 'luxon';
import { DateTimeService } from '@app/shared/common/timing/date-time.service';
import { Table } from 'primeng/table';
import { ViewMyLeadComponent } from '@app/main/leads/customerleads/view-mylead.component';
import { AddActivityModalComponent } from '@app/main/leads/leadactivity/add-activity-model.component';
import { AppConsts } from '@shared/AppConsts';
import { FileDownloadService } from '@shared/utils/file-download.service';

@Component({
    selector: 'transporttracker',
    templateUrl: './transport-tracker.component.html',
    styleUrls: ['./transport-tracker.component.css'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class TransportTrackerComponent extends AppComponentBase implements OnInit {
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    @ViewChild('TransportTrackerDetailModal', { static: true }) TransportTrackerDetailModal: TransportTrackerDetailModal;
    @ViewChild('ExtraMaterialModal', { static: true }) ExtraMaterialModal: ExtraMaterialModal;
    @ViewChild('ReturnMaterialModal', { static: true }) ReturnMaterialModal: ReturnMaterialModal;
    @ViewChild('RevertDispatchModal', { static: true }) RevertDispatchModal: RevertDispatchModal;
    @ViewChild('viewLeadDetail', { static: true }) viewLeadDetail: ViewMyLeadComponent;
    @ViewChild('addActivityModal', { static: true }) addActivityModal: AddActivityModalComponent;
    @Input() SectionName: string = "TransportTracker";
    @Output() reloadLead = new EventEmitter();

    advancedFiltersAreShown = false;
    FiltersData = false;
    active = false;
    saving = false;
    recordCount: any;
    leadId: any;
    filterText: string = '';
    shouldShow: boolean = false;
    //sampleDateRange: moment.Moment[] = [moment().add(0, 'days').endOf('day'), moment().add(0, 'days').endOf('day')];
    ExpandedView: boolean = true;
    leadName: string;
    flag = true;
    PageNo: number;
    allOrganizationUnits: OrganizationUnitDto[];
    allOrganizationUnitsList: OrganizationUnitDto[];
    organizationUnitlength: number = 0;
    organizationUnit = 0;
    billtypefilter: string = '';
    applicationstatus: number = 0;
    leadstatuss: CommonLookupDto[];
    allVehicalType: CommonLookupDto[];
    allapplicationStatus: CommonLookupDto[];
    allSolarType: CommonLookupDto[];
    allEmployeeList: UserListDto[];
    allDiscom: CommonLookupDto[];
    allDivision: CommonLookupDto[];
    allSubDivision: CommonLookupDto[];
    allTenderData: CommonLookupDto[];
    allCircleData: CommonLookupDto[];
    //filter variable
    leadStatusIDS = [];
    leadSourceIdFilter = [];
    leadApplicationStatusFilter = [];
    applicationstatusTrackerFilter: number;
    VehicalTypeFilter = [];
    employeeIdFilter = [];
    DiscomIdFilter = [];
    CircleIdFilter = [];
    DivisionIdFilter = [];
    SubDivisionIdFilter = [];
    solarTypeIdFilter = [];
    TenderIdFilter = [];
    FilterbyDate = '';
    date: Date = new Date('01/01/2022');
    StartDate: DateTime = this._dateTimeService.getStartOfDayForDate(this.date);
    EndDate: DateTime = this._dateTimeService.getDate();
    sectionId: number = AppConsts.SectionPage.TransportTracker;
    public screenWidth: any;
    public screenHeight: any;
    testHeight = 280;
    contentHeight = 230;
    @Input() SelectedLeadId: number = 0;
    toggle: boolean = true;
    show1: boolean = true;
    @HostListener('window:resize', ['$event'])
    transportTrackerSummaryCount: TransportTrackerSummaryCount;

    onResize(event) {
        this.screenHeight = window.innerHeight;
    }
    testHeightSize() {
        if (this.FiltersData == true) {
            this.testHeight = this.testHeight + -82;
        }
        else {
            this.testHeight = this.testHeight - -80;
        }
    }
    constructor(
        injector: Injector,
        private _transportServiceProxy: TransportTrackerServiceProxy,
        private _dateTimeService: DateTimeService,
        private _commonLookupService: CommonLookupServiceProxy,
        private _dataTransfer: DataTransferService,
        private _fileDownloadService: FileDownloadService,
    ) {
        super(injector);

    }
    ngOnInit(): void {
        this.screenHeight = window.innerHeight;
        this._commonLookupService.getOrganizationUnitUserWise().subscribe(output => {
            this.allOrganizationUnits = output;
            this.organizationUnit = this.allOrganizationUnits[0].id;
            this.organizationUnitlength = this.allOrganizationUnits.length;
            //change Organization
            this.allOrganizationUnitsList = this.allOrganizationUnits.filter(item => item.id != this.organizationUnit);
            this.getTransportList();
        }, error => { this.spinnerService.hide() });
        this._commonLookupService.getAllSolarTypeForDropdown().subscribe(result => {
            this.allSolarType = result;
        })
        this._commonLookupService.getDiscomForDropdown().subscribe(result => {
            this.allDiscom = result;
        })
        this._commonLookupService.getAllVehicalForDropdown().subscribe(output => {
            this.allVehicalType = output;
        })
        this.transportTrackerSummaryCount = new TransportTrackerSummaryCount();
    }

    expandGrid() {
        this.ExpandedView = true;
    }

    close() {
        //this.modal.hide();
    }
    toggleBlock() {
        this.show1 = !this.show1;
    };
    change() {
        this.toggle = !this.toggle;
    }


    show(leadId?: number, param?: number, sectionId?: number): void {

        this.leadId = leadId;
        // this._leadsServiceProxy.leadDocumentList(this.bindDocumentFilter).subscribe(result => {        
        // });
        this.getTransportList();
    }
    public getTransportList(event?: LazyLoadEvent) {
        this._dataTransfer.setOrgData(this.organizationUnit.toString());
        this.PageNo = this.primengTableHelper.getSkipCount(this.paginator, event);
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            //return;
        }
        this.primengTableHelper.showLoadingIndicator();
        //this.bindDocumentFilter.filter = this.leadId
        this._transportServiceProxy.getAllTransportTracker(
            this.filterText,
            this.organizationUnit,
            this.billtypefilter,
            this.VehicalTypeFilter,
            this.DiscomIdFilter,
            undefined,
            this.solarTypeIdFilter,
            this.TenderIdFilter,
            this.FilterbyDate,
            this.StartDate,
            this.EndDate,
            undefined,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe((result) => {
            if (result != undefined) {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items;
                this.primengTableHelper.hideLoadingIndicator();
                this.recordCount = result.totalCount;
                this.transportTrackerSummaryCount = result.items[0].transportTrackerSummaryCount;
                this.shouldShow = false;
                if (this.flag == false && result.totalCount != 0 && this.ExpandedView == false) {
                    this.navigateToLeadDetail(this.SelectedLeadId)
                }
                else {
                    if (this.ExpandedView == false && result.totalCount != 0) {
                        this.navigateToLeadDetail(result.items[0].transportTracker.leaddata.id)
                    }
                    else {
                        this.ExpandedView = true;
                    }
                }
            }
        }, error => { this.spinnerService.hide() });
    }
    public changeDiscom() {
        if (this.DiscomIdFilter.length > 0) {
            this._commonLookupService.getDiscomWiseCircleFilterForDropdown(this.DiscomIdFilter).subscribe(result => {
                this.allCircleData = result;
                if (this.allCircleData.length == 0) {
                    this.allCircleData = undefined;
                }
            }, error => { this.spinnerService.hide() });
        }
        else {
            this.allCircleData = [];
            this.CircleIdFilter = [];
            this.allDivision = [];
            this.DivisionIdFilter = [];
            this.allSubDivision = [];
            this.SubDivisionIdFilter = [];
        }
    }
    public changeCircle() {
        if (this.CircleIdFilter.length > 0) {
            this._commonLookupService.getCircleWiseDivisionFilterForDropdown(this.CircleIdFilter).subscribe(result => {
                this.allDivision = result;
                if (this.allDivision.length == 0) {
                    this.allDivision = undefined
                }
            }, error => { this.spinnerService.hide() });
        }
        else {
            this.allDivision = [];
            this.DivisionIdFilter = [];
            this.allSubDivision = [];
            this.SubDivisionIdFilter = [];
        }
    }
    public changeDivision() {
        if (this.CircleIdFilter.length > 0) {
            this._commonLookupService.getDivisionWiseSubDivisionFilterForDropdown(this.DivisionIdFilter).subscribe(result => {
                this.allSubDivision = result;
                if (this.allSubDivision.length == 0) {
                    this.allSubDivision = undefined
                }
            }, error => { this.spinnerService.hide() });
        }
        else {
            this.allSubDivision = [];
            this.SubDivisionIdFilter = [];
        }
    }
    public leadstatuschange() {
        if (this.leadStatusIDS.includes(6)) {
            //this.showJobStatus = true;
        } else {
            //this.showJobStatus = false;
            //this.jobStatusID = [];
        }
    }
    public chaneSolarType() {
        this.allTenderData = undefined;
        this._commonLookupService.getSolarTypeWiseTenderForDropdown(this.solarTypeIdFilter).subscribe(result => {
            this.allTenderData = result;
            if (this.allTenderData.length == 0) {
                this.allTenderData = undefined;
            }
        }, error => { this.spinnerService.hide() });
    }

    public navigateToLeadDetail(leaddata): void {

        this.ExpandedView = !this.ExpandedView;
        this.ExpandedView = false;
        this.SelectedLeadId = leaddata.transportTracker.leaddata.id;
        this.leadName = "TransportTracker"
        this.viewLeadDetail.showDetail(leaddata.transportTracker.leaddata, leaddata.transportTracker.applicationStatusdata, this.leadName, this.sectionId);
    }
    public reloadPage(flafValue: boolean): void {
        this.ExpandedView = true;
        this.flag = flafValue;
        this.paginator.changePage(this.paginator.getPage());
        if (!flafValue) {
            this.navigateToLeadDetail(this.SelectedLeadId);
        }
        this.paginator.changePage(this.paginator.getPage());
    }

    public exportToExcel() {
        this._transportServiceProxy.getTransportTrackerToExcel(
            this.filterText,
            this.organizationUnit,
            this.billtypefilter,
            this.VehicalTypeFilter,
            this.DiscomIdFilter,
            undefined,
            this.solarTypeIdFilter,
            this.TenderIdFilter,
            this.FilterbyDate,
            this.StartDate,
            this.EndDate,
            undefined,
            this.primengTableHelper.getSorting(this.dataTable),
            undefined,
            undefined
        ).subscribe((result) => {
            this._fileDownloadService.downloadTempFile(result);
        });
    }
    public clear() {
        this.filterText = '';
        this.leadStatusIDS = [];
        this.leadApplicationStatusFilter = [];
        this.billtypefilter = '';
        this.VehicalTypeFilter = [];
        this.DiscomIdFilter = [];
        this.solarTypeIdFilter = [];
        this.TenderIdFilter = [];
        this.employeeIdFilter = [];
        this.FilterbyDate = '';
        this.StartDate = this._dateTimeService.getStartOfDayForDate(this.date);
        this.EndDate = this._dateTimeService.getDate();
        this.getTransportList();
    }
    showDetailModal(): void {
        this.TransportTrackerDetailModal.show();
    }

    showExtraMaterialModal(): void {
        this.ExtraMaterialModal.show();
    }
    showReturnMaterialModal(): void {
        this.ReturnMaterialModal.show();
    }
    showRevertModal(): void {
        this.RevertDispatchModal.show();
    }


}
