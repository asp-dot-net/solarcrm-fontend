import { Component, Injector, ViewChild } from '@angular/core';
import { DateTimeService } from '@app/shared/common/timing/date-time.service';
import { AppComponentBase } from '@shared/common/app-component-base';
import { CreateOrEditJobDto, EstimatedTrackerQuickViewDto } from '@shared/service-proxies/service-proxies';
import { DateTime } from 'luxon';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { EstimatedPaidTrackerServiceProxy } from '@shared/service-proxies/service-proxies';
import { parse } from 'path';
import { AppConsts } from '@shared/AppConsts';
@Component({
    selector: 'ExtraMaterialModal',
    templateUrl: './extra-material-modal.component.html',
})
export class ExtraMaterialModal extends AppComponentBase {
    @ViewChild('ExtraMaterialModal', { static: true }) modal: ModalDirective;
    QuickViewData:EstimatedTrackerQuickViewDto;
    ExpandedViewApp: boolean = true;
    active = false;
    changeEstimated:boolean = false;
    eastimateDueDate:DateTime;
    customertitle:string = '';
    sectionId: number = AppConsts.SectionPage.ApplicationTracker;  
    //@Output() reloadLead = new EventEmitter<boolean>();
    constructor(injector: Injector, private _dateTimeService: DateTimeService,
        private _estimatedTrackerServiceProxy: EstimatedPaidTrackerServiceProxy,       
    ) {
        super(injector);       
    }
    
    ngOnInit(): void {
       
        this.QuickViewData = new EstimatedTrackerQuickViewDto;  
   }
   
   show() {
    this.modal.show();        
    }
   
    GetEstimatedQuickView(jobdata) {
        this.changeEstimated = false;
        this.spinnerService.show();
        this.customertitle = jobdata.customerName +'('+ jobdata.projectNumber +')'
        this._estimatedTrackerServiceProxy.getEstimatedQuickView(jobdata.id).subscribe((result) => {
            this.modal.show();
            this.QuickViewData = result;         
           
            this.spinnerService.hide();
        }, error => { this.spinnerService.hide() });
    }
   saving = true;
    submit() {        
        this.saving = true;     
        if (this.saving) { 
            let estimatedEdit:  CreateOrEditJobDto = new CreateOrEditJobDto();
            estimatedEdit.id =  this.QuickViewData.id;
            estimatedEdit.eastimatePaymentRefNumber = this.QuickViewData.estimatePayementRefNumber;
            estimatedEdit.eastimatePaidStatus = this.QuickViewData.eastimatePaidStatus;
            estimatedEdit.eastimateQuoteNumber = this.QuickViewData.eastimateQuoteNo;
            estimatedEdit.eastimateQuoteAmount = this.QuickViewData.eastimateQuoteAmount;
            estimatedEdit.eastimateDueDate = this.QuickViewData.eastimateDueDate;
            estimatedEdit.eastimatePaymentResponse = this.QuickViewData.eastimatePayResponse;
            this._estimatedTrackerServiceProxy.estimatedDetailsEdit(estimatedEdit)
                .pipe(finalize(() => { this.saving = false; }))
                .subscribe(() => {
                    //this.reloadPage(true);
                   
                    this.saving = false;
                    this.modal.hide();
                    document.getElementById("btnRefrsh").click();
                    this.notify.info(this.l('Added Estimate Pay Response Successfully'));
                }, error => { this.spinnerService.hide() });
        }      
    }
    
    close(): void {
        this.active = false;
        this.modal.hide();
    }

    navigateToAppDetail(): void {
        this.ExpandedViewApp = !this.ExpandedViewApp;
        this.ExpandedViewApp = true;
    }
    expandApp() {
        this.ExpandedViewApp = false;
    }

}
