import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TransportTrackerComponent } from './transport-tracker.component';

describe('TransportTrackerComponent', () => {
  let component: TransportTrackerComponent;
  let fixture: ComponentFixture<TransportTrackerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TransportTrackerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TransportTrackerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
