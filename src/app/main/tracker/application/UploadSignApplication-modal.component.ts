import { Component, Injector, ViewChild, ElementRef } from '@angular/core';
import { DateTimeService } from '@app/shared/common/timing/date-time.service';
import { AppComponentBase } from '@shared/common/app-component-base';
import { DateTime } from 'luxon';
import { ModalDirective } from 'ngx-bootstrap/modal';
import {
    JobServiceProxy,
    CreateOrEditJobDto,
    ApplicationTrackerServiceProxy,
} from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { AppConsts } from '@shared/AppConsts';
@Component({
    selector: 'UploadSignApplication',
    templateUrl: './UploadSignApplication-modal.html',
})

export class UploadSignApplication extends AppComponentBase {
    @ViewChild('UploadSignApplication', { static: true }) modal: ModalDirective;
    @ViewChild('SampleDatePicker', { static: true }) sampleDatePicker: ElementRef;
    showMyContainer: boolean = false;
    ExpandedViewApp: boolean = true;
    active = false;
    saving = false;
    recordCount: any;
    leadId: number = 0;
    job: CreateOrEditJobDto = new CreateOrEditJobDto();
    jobid: number = 0;
    UploadSignDate: DateTime;
    sectionId: number = AppConsts.SectionPage.ApplicationTracker;
    uploadUrl: string = '';
    fileToUpload: any;
    applicationFlag: string = '';
    UploadDocPath: string = '';
    signApplicationDate: DateTime = this._dateTimeService.getDate();
    customertitle: string = '';

    constructor(injector: Injector, private _dateTimeService: DateTimeService,       
        private _jobServiceProxy: JobServiceProxy,
        private _httpClient: HttpClient,

    ) {
        super(injector);        
        this.uploadUrl = AppConsts.remoteServiceBaseUrl + '/ApplicationTracker/SignUploadapplicationDocument';
    }

    show() {
        this.modal.show();
    }
    
    EditUploadSignApplicationDetails(jobdata) {
        this.spinnerService.show();
        this.customertitle = jobdata.customerName + '(' + jobdata.projectNumber + ')'
        this._jobServiceProxy.getJobForEdit(jobdata.leaddata.id).subscribe(result => {
            this.spinnerService.hide();
            this.job = result.job;
            this.UploadDocPath = AppConsts.remoteServiceBaseUrl + result.job.signApplicationPath;            
            this.signApplicationDate = result.job.signApplicationDate == null ? this._dateTimeService.getDate() : result.job.signApplicationDate; //

            if (result.job.otpVerifed === null) {
                this.job.otpVerifed = 'Yes';
            }
            this.modal.show();
            this.jobid = this.job.id;
        }, error => { this.spinnerService.hide() });

    }
    close(): void {
        this.active = false;
        this.modal.hide();
    }

    navigateToAppDetail(): void {
        this.ExpandedViewApp = !this.ExpandedViewApp;
        this.ExpandedViewApp = true;
    }
    expandApp() {
        this.ExpandedViewApp = false;
    }

    
    handleFileInput(files: FileList) {
        // let fileToUpload = <File>files[0];
        // this.fileToUpload1.data = <File>files[0]
        this.fileToUpload = files.item(0);
        this.job.signApplicationPath = files.item(0).name;

    }
    
    saveUploadSignJob(): void {
        //  this.job.leadId = this.leadId;
        this.job.signApplicationDate = this.signApplicationDate
        this.spinnerService.show();
        this.saving = true;        
        const formData: FormData = new FormData();
        const file = this.fileToUpload;
        if (this.fileToUpload != null) {
            formData.append('file', this.fileToUpload, this.fileToUpload.name);
        }
        for (var key in this.job) {
            formData.append(key, this.job[key]);
        }
        formData.append('SectionId', this.sectionId.toString());
        formData.append('applicationFlag', "1");
        this._httpClient
            .post<any>(this.uploadUrl, formData)
            .pipe(finalize(() => 12))
            .subscribe((response) => {
                if (response.success) {
                    this.modal.hide();
                    this.spinnerService.hide();
                    this.notify.info(this.l('SavedSuccessfully'));
                } else if (response.error != null) {
                    this.notify.error(this.l('Failed'));
                }
            }, error => { this.spinnerService.hide() });


        // this._AppTrackerServiceProxy.applicationUploadSignEdit(this.sectionId, this.job.id, this.job.signApplicationPath, this.job.signApplicationDate, this.fileToUpload).pipe(
        //     finalize(() => { this.saving = false; })
        // )
        //     .subscribe(() => {
        //         this.active = true;
        //         document.getElementById('').click();

        //         //this.activityLog.getLeadActivityLog(this.leadId,0);
        //         this.notify.info(this.l('SavedSuccessfully'));
        //     });
    }
}

