import { Component, Injector, ViewChild } from '@angular/core';
import { DateTimeService } from '@app/shared/common/timing/date-time.service';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { HttpClient } from '@angular/common/http';
import { AppConsts } from '@shared/AppConsts';
import { NgxSpinnerService } from 'ngx-spinner';
import {
    JobServiceProxy,
    CreateOrEditJobDto,
} from '@shared/service-proxies/service-proxies';

@Component({
    selector: 'loadReduceDetailModal',
    templateUrl: './load-reduce-modal.html',
})
export class loadReduceDetailModal extends AppComponentBase {
    @ViewChild('loadReduceDetailModal', { static: true }) modal: ModalDirective;
    showMyContainer: boolean = false;
    ExpandedViewApp: boolean = true;
    active = false;
    saving = false;
    recordCount: any;
    leadId: number = 0;
    job: CreateOrEditJobDto = new CreateOrEditJobDto();
    jobid: number = 0;
    uploadUrl: string;
    fileToUpload: any;
    sectionId: number = AppConsts.SectionPage.ApplicationTracker;
    UploadDocPath:string = '';
    customertitle:string = '';

    constructor(injector: Injector, private _dateTimeService: DateTimeService,
        private _jobServiceProxy: JobServiceProxy,
        private _httpClient: HttpClient,
        private spinner: NgxSpinnerService
       ) {
        super(injector);
        this.uploadUrl = AppConsts.remoteServiceBaseUrl + '/ApplicationTracker/SignUploadapplicationDocument';
    }
    
    EditLoadReduceDetails(jobdata) {
        this.spinner.show();       
        this.modal.show();
        this.customertitle = jobdata.customerName +'('+ jobdata.projectNumber +')'
        this._jobServiceProxy.getJobForEdit(jobdata.leaddata.id).subscribe(result => {
            this.spinner.hide();
            this.job = result.job;
            this.UploadDocPath = AppConsts.remoteServiceBaseUrl +  this.job.loadReducePathApplication;
            if (result.job.otpVerifed === null) {
                this.job.otpVerifed = 'Yes';
            }
            this.jobid = this.job.id;
        }, error => { this.spinnerService.hide() });

    }
    handleFileInput(files: FileList) {
        this.fileToUpload = files.item(0);
        this.job.loadReducePathApplication = this.fileToUpload;
    }
    close(): void {
        this.active = false;
        this.modal.hide();
    }
    saveLoadReduceJob(): void {            
        this.saving = true;
        this.spinner.show();
        const formData: FormData = new FormData();
        const file = this.fileToUpload;
        
         if(this.fileToUpload === null && this.job.loadReducePathApplication == ''){
             this.notify.info(this.l('Plz Select any One File'));
             this.spinner.hide();
             return
         }
        if (this.fileToUpload != null) {
            formData.append('file', this.fileToUpload, this.fileToUpload.name);
        }
        for (var key in this.job) {
            formData.append(key, this.job[key]);
        }
        formData.append('SectionId', this.sectionId.toString());
        formData.append('applicationFlag', "0");
        this._httpClient
            .post<any>(this.uploadUrl, formData)          
            .subscribe((response) => {
                if (response.success) {
                    this.spinner.hide();
                    this.saving = false;
                    this.modal.hide();
                    this.notify.info(this.l('SavedSuccessfully'));
                } else if (response.error != null) {
                    this.notify.error(this.l('Failed'));
                }
            }, error => { this.spinnerService.hide() });
    }
   
}
