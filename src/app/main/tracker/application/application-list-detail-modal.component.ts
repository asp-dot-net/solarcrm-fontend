import { Component, Injector, ViewChild } from '@angular/core';
import { DateTimeService } from '@app/shared/common/timing/date-time.service';
import { AppComponentBase } from '@shared/common/app-component-base';
import { AuditLogListDto } from '@shared/service-proxies/service-proxies';
import { AppConsts } from '@shared/AppConsts';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { LeadsServiceProxy, ApplicationTrackerServiceProxy, CommonLookupServiceProxy, ApplicationTrackerQuickViewDto, DocumentListDto } from '@shared/service-proxies/service-proxies';
@Component({
    selector: 'applicationListDetailModal',
    templateUrl: './application-list-detail-modal.component.html',
})
export class applicationListDetailModal extends AppComponentBase {
    @ViewChild('applicationListDetailModal', { static: true }) modal: ModalDirective;
    ExpandedViewApp: boolean = true;
    active = false;    
    customertitle:string = '';
    QuickViewData:ApplicationTrackerQuickViewDto;

    constructor(injector: Injector,
        private _applicationTrackerServiceProxy: ApplicationTrackerServiceProxy,
        private _dateTimeService: DateTimeService) {
        super(injector);
    }
    ngOnInit(): void {        
        this.QuickViewData = new ApplicationTrackerQuickViewDto;  
    }
   
    downfile(filenamepath): void {        
        let FileName = AppConsts.remoteServiceBaseUrl + "/" +  filenamepath;//this.LeadDocument.filePath;
        window.open(FileName, "_blank");
    };
    close(): void {
        this.active = false;
        this.modal.hide();
    }

    navigateToAppDetail(): void {
        this.ExpandedViewApp = !this.ExpandedViewApp;
        this.ExpandedViewApp = true;                
    }
    expandApp() {
        this.ExpandedViewApp = false;
    }  
    
    GetApplicationQuickView(jobdata){               
        this.spinnerService.show();
        this.customertitle = jobdata.customerName +' ( '+ jobdata.projectNumber +' ) '
        this._applicationTrackerServiceProxy.getApplicationQuickView(jobdata.id).subscribe((result) => {
            this.spinnerService.hide();            
            this.QuickViewData = result;            
            this.modal.show();   
        }, error => { this.spinnerService.hide() });
    }
    copyText(val: string){
        let selBox = document.createElement('textarea');
          selBox.style.position = 'fixed';
          selBox.style.left = '0';
          selBox.style.top = '0';
          selBox.style.opacity = '0';
          selBox.value = val;
          document.body.appendChild(selBox);
          selBox.focus();
          selBox.select();
          document.execCommand('copy');
          document.body.removeChild(selBox);
        }

}
