import { Component, Injector, ViewChild,ElementRef } from '@angular/core';
import { DateTimeService } from '@app/shared/common/timing/date-time.service';
import { AppComponentBase } from '@shared/common/app-component-base';
import { DateTime } from 'luxon';
import { finalize } from 'rxjs/operators';
import { ModalDirective } from 'ngx-bootstrap/modal';
import {
    JobServiceProxy,
    GetJobForEditOutput,
    CreateOrEditJobDto,
    ApplicationTrackerServiceProxy
} from '@shared/service-proxies/service-proxies';
import { AppConsts } from '@shared/AppConsts';

@Component({
    selector: 'EditapplicationDetailModal',
    templateUrl: './edit-application-detail-modal.html',
})
export class EditapplicationDetailModal extends AppComponentBase {
    @ViewChild('SampleDatePicker', { static: true }) sampleDatePicker: ElementRef;
    @ViewChild('EditapplicationDetailModal', { static: true }) modal: ModalDirective;
    active = false;
    saving = false;
    recordCount:any;
    leadId:number = 0;
    sectionId: number = AppConsts.SectionPage.ApplicationTracker;  
    activeTabIndex: number = 0;
    ExpandedViewApp: boolean = true;   
    jobId: number;
    jobDetails: GetJobForEditOutput = new GetJobForEditOutput();
    job: CreateOrEditJobDto = new CreateOrEditJobDto();
    jobid:number = 0;
    applicationDate:DateTime = this._dateTimeService.getDate();
    customertitle:string = '';
    isOTPVerified: boolean = true

    constructor(injector: Injector, private _dateTimeService: DateTimeService,
        private _AppTrackerServiceProxy: ApplicationTrackerServiceProxy,
        private _jobServiceProxy: JobServiceProxy,) {
        super(injector);
    }
    EditApplicationDetails(jobdata) {
        this.spinnerService.show();
        this.customertitle = jobdata.customerName +'('+ jobdata.projectNumber +')'
        this.isOTPVerified = true;
        
        this._jobServiceProxy.getJobForEdit(jobdata.leaddata.id).subscribe(result => {
            this.spinnerService.hide();
            this.jobDetails = result;
            this.job = result.job;  
                      
            this.applicationDate = this.job.applicationDate == null ? this.applicationDate : this.job.applicationDate;
            if(result.job.otpVerifed === null)
            {
                this.job.otpVerifed = 'Yes';
            }
           if(this.job.otpVerifed.toLocaleLowerCase() != 'yes'){
            this.isOTPVerified = false;
           }
           this.modal.show();
            this.jobid = jobdata.id;
            
        }, error => { this.spinnerService.hide() });

    }
    saveJob(): void {       
       this.job.applicationDate = this.applicationDate;
        this.saving = true;
        this._AppTrackerServiceProxy.applicationDetailsEdit(this.sectionId, this.job).pipe(
                finalize(() => { this.saving = false; })
            )
            .subscribe(() => {
                this.active = true;
                this.modal.hide();
                document.getElementById('btnRefresh').click();              
                this.notify.info(this.l('SavedSuccessfully'));
            }, error => { this.spinnerService.hide() });
    }

    chnageOTPverifiedd(event) {
        if (event.target.value == 'Yes') {
            this.isOTPVerified = true
        }
        else {
            this.isOTPVerified = false
        }
    }
    close(): void {
        this.active = false;
        this.modal.hide();
    }

    navigateToAppDetail(): void {
        this.ExpandedViewApp = !this.ExpandedViewApp;
        this.ExpandedViewApp = true;
    }
    expandApp() {
        this.ExpandedViewApp = false;
    }

}
