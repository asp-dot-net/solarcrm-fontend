import { Component, Injector, ViewChild } from '@angular/core';
import { DateTimeService } from '@app/shared/common/timing/date-time.service';
import { AppComponentBase } from '@shared/common/app-component-base';
import { AuditLogListDto } from '@shared/service-proxies/service-proxies';
import { DateTime } from 'luxon';
import { ModalDirective } from 'ngx-bootstrap/modal';

@Component({
    selector: 'applicationReadyStatusModal',
    templateUrl: './application-ready-status-modal.html',
})
export class applicationReadyStatusModal extends AppComponentBase {
    @ViewChild('applicationReadyStatusModal', { static: true }) modal: ModalDirective;

    ExpandedViewApp: boolean = true;
    active = false;
    //auditLog: AuditLogListDto;

    constructor(injector: Injector, private _dateTimeService: DateTimeService) {
        super(injector);
    }

    show() {
        this.modal.show();        
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }

    navigateToAppDetail(): void {
        this.ExpandedViewApp = !this.ExpandedViewApp;
        this.ExpandedViewApp = true;                
    }
    expandApp() {
        this.ExpandedViewApp = false;
    }

}
