import { OnInit, Component, Injector, ViewEncapsulation, ViewChild, Input, Output, EventEmitter, AfterViewInit, HostListener } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ActivatedRoute, Router } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { LazyLoadEvent } from 'primeng/api';
import { LeadsServiceProxy, LeadsDto, CommonLookupServiceProxy, CommonLookupDto, EstimatedPaidTrackerServiceProxy, OrganizationUnitDto, GetProjectTransferForEstimatePayResponseOutput, EstimatePaidTrackerSummaryCount } from '@shared/service-proxies/service-proxies';
import { Paginator } from 'primeng/paginator';
import * as _ from 'lodash';
import { estimateListDetailModal } from './estimate-list-detail-modal.component';
import { paymentRecieptModal } from './payment-reciept-modal.component';
import { ViewMyLeadComponent } from '@app/main/leads/customerleads/view-mylead.component';
import { AddActivityModalComponent } from '@app/main/leads/leadactivity/add-activity-model.component';
import { DateTime } from 'luxon';
import { DateTimeService } from '@app/shared/common/timing/date-time.service';
import { Table } from 'primeng/table';
import { DataTransferService } from '@app/shared/common/projectservice/data-transfer.service';
import { FileDownloadService } from '@shared/utils/file-download.service';
import { AppConsts } from '@shared/AppConsts';
@Component({
    selector: 'estimate-list',
    templateUrl: './estimate-list.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class EstimateListComponent extends AppComponentBase implements OnInit {
    @ViewChild('estimateListDetailModal', { static: true }) estimateListDetailModal: estimateListDetailModal;
    @ViewChild('paymentRecieptModal', { static: true }) paymentRecieptModal: paymentRecieptModal;

    @ViewChild('viewLeadDetail', { static: true }) viewLeadDetail: ViewMyLeadComponent;
    @ViewChild('addActivityModal', { static: true }) addActivityModal: AddActivityModalComponent;
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    @Output() reloadLead = new EventEmitter();
    @Input() SelectedLeadId: number = 0;
    @Input() SectionName: string = "ApplicationTracker";
    advancedFiltersAreShown = false;
    FiltersData = false;
    active = false;
    saving = false;
    recordCount: any;
    leadId: any;
    filterText: string = '';
    shouldShow: boolean = false;
    leadStatusId = 0;
    //sampleDateRange: moment.Moment[] = [moment().add(0, 'days').endOf('day'), moment().add(0, 'days').endOf('day')];
    ExpandedView: boolean = true;
    leadName: string;
    flag = true;
    PageNo: number;
    allOrganizationUnits: OrganizationUnitDto[];
    allOrganizationUnitsList: OrganizationUnitDto[];
    organizationUnitlength: number = 0;
    organizationUnit = 0;
    applicationstatus: number = 0;
    leadstatuss: CommonLookupDto[];
    allStates: CommonLookupDto[];
    allapplicationStatus: CommonLookupDto[];
    allLeadSources: CommonLookupDto[];
    allSolarType: CommonLookupDto[];
    allDiscom: CommonLookupDto[];
    allDivision: CommonLookupDto[];
    allSubDivision: CommonLookupDto[];
    allTenderData: CommonLookupDto[];
    allCircleData: CommonLookupDto[];
    //filter variable
    priorityFilter = '';
    leadStatusIDS = [];
    leadSourceIdFilter = [];

    applicationstatusTrackerFilter: [];
    estimatedPaidFilter = '';
    estimatedPayResonseFilter = '';
    DiscomIdFilter = [];
    CircleIdFilter = [];
    DivisionIdFilter = [];
    SubDivisionIdFilter = [];
    solarTypeIdFilter = [];
    TenderIdFilter = [];
    employeeIdFilter = [];
    FilterbyDate = '';
    date: Date = new Date('01/01/2022');
    StartDate: DateTime = this._dateTimeService.getStartOfDayForDate(this.date);
    EndDate: DateTime = this._dateTimeService.getDate();
    tableRecords: any;
    sectionId: number = AppConsts.SectionPage.EstimatePaidTracker;  
    manualPymentFilter: string = '';
    assignEstimatePayResponse: string = '';
    isChange: boolean = false;
    isChangetrue: boolean = false;
    isChangefalse: boolean = false;
    IsAssignPayResponse: boolean = true;
    showOrganizationUnits: boolean = false;
    count = 0;
    public screenWidth: any;
    public screenHeight: any;
    testHeight = 280;
    contentHeight = 230;
    show1: boolean = true;
    toggle: boolean = true;
    estimatePaidSummaryCount :EstimatePaidTrackerSummaryCount;
    FiltersData1 = false;
    toggleBlock() {
        this.show1 = !this.show1;
    };
    change() {
        this.toggle = !this.toggle;
    }

    constructor(
        injector: Injector,
        private _dateTimeService: DateTimeService,
        private _commonLookupService: CommonLookupServiceProxy,
        private _estimatedTrackerService: EstimatedPaidTrackerServiceProxy,
        private _dataTransfer: DataTransferService,
        private _fileDownloadService: FileDownloadService,
    ) {
        super(injector);
       
    }
    ngOnInit(): void {
        this.screenHeight = window.innerHeight;
        //selected Project Application status
        //this.applicationstatusTrackerFilter = 4;
        this._commonLookupService.getAllApplicationStatusForDropdown().subscribe(result => {
            this.allapplicationStatus = result;
        }, error => { this.spinnerService.hide() });
        this._commonLookupService.getAllLeadStatusDropdown().subscribe(result => {
            this.leadstatuss = result;
        }, error => { this.spinnerService.hide() });
        this._commonLookupService.getAllDiscomFilterDropdown(0).subscribe(result => {
            this.allDiscom = result;
        }, error => { this.spinnerService.hide() });

        this._commonLookupService.getAllSubdivisionWiseData('').subscribe(result => {
            this.allSubDivision = result;
        }, error => { this.spinnerService.hide() });
        this._commonLookupService.getAllSolarTypeForDropdown().subscribe(result => {
            this.allSolarType = result;
        });
        this._commonLookupService.getOrganizationUnitUserWise().subscribe(output => {
            this.allOrganizationUnits = output;
            this.organizationUnit = this.allOrganizationUnits[0].id;
            this.organizationUnitlength = this.allOrganizationUnits.length;

            //change Organization
            this.allOrganizationUnitsList = this.allOrganizationUnits.filter(item => item.id != this.organizationUnit);
            this.getEstimateList();
        }, error => { this.spinnerService.hide() });
        this.estimatePaidSummaryCount = new EstimatePaidTrackerSummaryCount();
    }

    @HostListener('window:resize', ['$event'])
    onResize(event) {
        this.screenHeight = window.innerHeight;
    }
    testHeightSize() {
        if (this.FiltersData == true) {
            this.testHeight = this.testHeight + -80;
        }
        else {
            this.testHeight = this.testHeight - -80;
        }
    }

    testHeightSize1() {
        if (this.FiltersData1 == true) {
            this.testHeight = this.testHeight + 87;            
        }
        else {
            this.testHeight = this.testHeight - 87;            
        }
    }

    chaneSolarType() {
        this.allTenderData = undefined;
        this._commonLookupService.getSolarTypeWiseTenderForDropdown(this.solarTypeIdFilter).subscribe(result => {
            this.allTenderData = result;
            if (this.allTenderData.length == 0) {
                this.allTenderData = undefined;
            }
        }, error => { this.spinnerService.hide() });
    }
    commonfilterfun() {
        if (this.CircleIdFilter.length > 0) {
            this.CircleIdFilter.filter((obj) => this.allCircleData.indexOf(obj.id) === -1);
        }
    }
    changeDiscom() {
        if (this.DiscomIdFilter.length > 0) {
            this._commonLookupService.getDiscomWiseCircleFilterForDropdown(this.DiscomIdFilter).subscribe(result => {
                this.allCircleData = result;
                if (this.allCircleData.length == 0) {
                    this.allCircleData = undefined;
                }
                this.commonfilterfun();
            }, error => { this.spinnerService.hide() });
        }
        else {
            this.allCircleData = [];
            this.CircleIdFilter = [];
            this.allDivision = [];
            this.DivisionIdFilter = [];
            this.allSubDivision = [];
            this.SubDivisionIdFilter = [];
        }
    }
    changeCircle() {
        this.allDivision = [];
        this.DivisionIdFilter = [];
        this.allSubDivision = [];
        this.SubDivisionIdFilter = [];
        if (this.CircleIdFilter.length > 0) {
            this._commonLookupService.getCircleWiseDivisionFilterForDropdown(this.CircleIdFilter).subscribe(result => {
                this.allDivision = result;
                if (this.allDivision.length == 0) {
                    this.allDivision = undefined
                }
            }, error => { this.spinnerService.hide() });
        }
    }
    changeDivision() {
        this.allSubDivision = [];
        this.SubDivisionIdFilter = [];
        if (this.CircleIdFilter.length > 0) {
            this._commonLookupService.getDivisionWiseSubDivisionFilterForDropdown(this.DivisionIdFilter).subscribe(result => {
                this.allSubDivision = result;
                if (this.allSubDivision.length == 0) {
                    this.allSubDivision = undefined
                }
            }, error => { this.spinnerService.hide() });
        }
    }

    show(leadId?: number, param?: number, sectionId?: number): void {
        this.leadId = leadId;
        this.getEstimateList();
    }

    getEstimateList(event?: LazyLoadEvent) {

        this._dataTransfer.setOrgData(this.organizationUnit.toString())
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            //return;
        }
        this.primengTableHelper.showLoadingIndicator();

        //this.bindDocumentFilter.filter = this.leadId
        this._estimatedTrackerService.getAllEstimatedTracker(
            this.filterText,
            this.organizationUnit,
            this.leadStatusIDS,
            this.applicationstatusTrackerFilter,
            this.priorityFilter,
            this.estimatedPaidFilter,
            this.estimatedPayResonseFilter,
            this.DiscomIdFilter,
            this.CircleIdFilter,
            this.DivisionIdFilter,
            this.SubDivisionIdFilter,
            this.solarTypeIdFilter,
            this.TenderIdFilter,
            this.employeeIdFilter,
            this.FilterbyDate,
            this.StartDate,
            this.EndDate,
            undefined,
            this.manualPymentFilter,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe((result) => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            this.primengTableHelper.hideLoadingIndicator();
            this.recordCount = result.totalCount;
            this.estimatePaidSummaryCount = result.items[0].estimatePadiTrackerSummaryCount;
            this.shouldShow = false;
            //const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
            //this.firstrowcount = totalrows + 1;
            //this.last = totalrows + result.items.length;
            this.tableRecords = result.items;
            if (this.flag == false && result.totalCount != 0 && this.ExpandedView == false) {
                this.navigateToLeadDetail(this.SelectedLeadId)
            }
            else {
                if (this.ExpandedView == false && result.totalCount != 0) {
                    this.navigateToLeadDetail(result.items[0].estimateTracker.leaddata.id)
                }
                else {
                    this.ExpandedView = true;
                }
            }
        }, error => { this.spinnerService.hide() });
    }
    navigateToLeadDetail(estimatedata): void {
        this.ExpandedView = !this.ExpandedView;
        this.ExpandedView = false;
        this.SelectedLeadId = estimatedata.estimateTracker.leaddata.id;
        this.leadName = "estimatedTracker";        
        this.viewLeadDetail.showDetail(estimatedata.estimateTracker.leaddata, estimatedata.estimateTracker.applicationStatusdata, this.leadName, this.sectionId);
    }
    reloadPage(flafValue: boolean): void {
        this.ExpandedView = true;
        this.flag = flafValue;
        this.paginator.changePage(this.paginator.getPage());
        if (!flafValue) {
            this.navigateToLeadDetail(this.SelectedLeadId);
        }
        this.paginator.changePage(this.paginator.getPage());
    }
    
    clear() {
        this.filterText = '';
        this.leadStatusIDS = [];
        this.manualPymentFilter = '';
        this.estimatedPaidFilter = '';
        this.applicationstatusTrackerFilter = [];
        this.priorityFilter = '';
        this.estimatedPayResonseFilter = '';
        this.DiscomIdFilter = [];
        this.CircleIdFilter = [];
        this.DivisionIdFilter = [];
        this.SubDivisionIdFilter = [];
        this.solarTypeIdFilter = [];
        this.TenderIdFilter = [];
        this.StartDate = this._dateTimeService.getStartOfDayForDate(this.date);
        this.EndDate = this._dateTimeService.getDate();
        this.getEstimateList();
    }
    isAllChecked() {
        if (this.tableRecords)
            return this.tableRecords.every(_ => _.estimateTracker.isSelected);
    }
    checkAll(ev) {

        this.tableRecords.forEach(x => x.estimateTracker.isSelected = ev.target.checked);
        this.oncheckboxCheck();
        /// this.totalcount = this.tableRecords.forEach(x => x.lead.isSelected = ev.target.checked);
    }


    oncheck(event) {
        if (event.target.checked == true) {
            if (this.allOrganizationUnitsList.length > 0 || this.allOrganizationUnitsList != undefined) {
                this.isChange == true;
                this.showOrganizationUnits = true;
                this.isChangetrue = true;
                this.isChangefalse = false
                //this.getOrganizationWiseUser();
            } else {
                this.isChange == false;
                this.showOrganizationUnits = false;
                this.notify.warn(this.l('NoOrganizationAvailable'));

            }
        } else {
            this.isChange == false;
            // this.showOrganizationUnits = false;
            this.isChangetrue = false;
            this.isChangefalse = true;
            // this.getOrganizationuser();
        }
    }

    oncheckboxCheck() {
        this.count = 0;
        this.tableRecords.forEach(item => {
            if (item.estimateTracker.isSelected == true) {
                this.count = this.count + 1;
            }
        })
    }
    submit() {
        let selectedids = [];
        this.saving = true;

        if (this.isChange) {
            this.primengTableHelper.records.forEach(function (estimateTracker) {
                if (estimateTracker.estimateTracker.isSelected) {
                    selectedids.push(estimateTracker.estimateTracker.id);
                }
            });
            let addpayresponse: GetProjectTransferForEstimatePayResponseOutput = new GetProjectTransferForEstimatePayResponseOutput();
            addpayresponse.payResponse = this.assignEstimatePayResponse;
            addpayresponse.jobIds = selectedids;
            addpayresponse.organizationID = this.organizationUnit;
            if (this.organizationUnit == 0) {
                this.notify.warn(this.l('Please Select Organization'));
                this.saving = false;
                return;
            }
            if (this.assignEstimatePayResponse == '') {
                this.notify.warn(this.l('Please Select Pay Response'));
                this.saving = false;
                return;
            }
            if (selectedids.length == 0) {
                this.notify.warn(this.l('No Leads Selected'));
                this.saving = false;
                return;
            }
            this._estimatedTrackerService.addToEstimatePayResponse(addpayresponse)
                .pipe(finalize(() => { this.saving = false; }))
                .subscribe(() => {
                    //this.reloadPage(true);
                    this.isChange = false;
                    this.saving = false;
                    this.notify.info(this.l('Add Estimate Pay Response Successfully'));
                }, error => { this.spinnerService.hide() });
        }
        else {
            //if(selectedids.length == 0){
            this.notify.warn(this.l('Please Estimated Pay Response On'));
            this.saving = false;
            return;
            // }
        }
    }
    
    exportToExcel(): void {
        this._estimatedTrackerService.getEstimatedTrackerToExcel(
            this.filterText,
            this.organizationUnit,
            this.leadStatusIDS,
            this.applicationstatusTrackerFilter,
            this.priorityFilter,
            this.estimatedPaidFilter,
            this.estimatedPayResonseFilter,
            this.DiscomIdFilter,
            this.CircleIdFilter,
            this.DivisionIdFilter,
            this.SubDivisionIdFilter,
            this.solarTypeIdFilter,
            this.TenderIdFilter,
            this.employeeIdFilter,
            this.FilterbyDate,
            this.StartDate,
            this.EndDate,
            undefined,
            this.primengTableHelper.getSorting(this.dataTable),
            undefined,
            undefined,
            undefined,
        )
            .subscribe(result => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }



    expandGrid() {
        this.ExpandedView = true;
    }

    close() {
        //this.modal.hide();
    }
}
