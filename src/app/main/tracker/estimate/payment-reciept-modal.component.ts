import { Component, Injector, ViewChild } from '@angular/core';
import { DateTimeService } from '@app/shared/common/timing/date-time.service';
import { HttpClient } from '@angular/common/http';
import { AppComponentBase } from '@shared/common/app-component-base';
import { AuditLogListDto, EstimatedPaidTrackerServiceProxy, EstimatedTrackerQuickViewDto } from '@shared/service-proxies/service-proxies';
import { DateTime } from 'luxon';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { AppConsts } from '@shared/AppConsts';
@Component({
    selector: 'paymentRecieptModal',
    templateUrl: './payment-reciept-modal.component.html',
})
export class paymentRecieptModal extends AppComponentBase {
    @ViewChild('paymentRecieptModal', { static: true }) modal: ModalDirective;
    sectionId: number = AppConsts.SectionPage.paymentReceiptUpload;  
    uploadUrl: string;
    fileToUpload: any;
    ExpandedViewApp: boolean = true;
    active = false;
    eastimatePayDate: DateTime;
    paymentReceipt: EstimatedTrackerQuickViewDto;
    UploadDocPath: string = '';
    customertitle: string = '';
    saving: boolean = false;
    constructor(injector: Injector, private _dateTimeService: DateTimeService,
        private _httpClient: HttpClient,
        private _estimatedTrackerServiceProxy: EstimatedPaidTrackerServiceProxy) {
        super(injector);
        this.uploadUrl = AppConsts.remoteServiceBaseUrl + '/EstimatedPaidTracker/PaymentReceiptUploadDocument';
    }
    ngOnInit(): void {
        this.paymentReceipt = new EstimatedTrackerQuickViewDto;
    }

    GetPaymentReceipt(jobdata) {
        this.spinnerService.show();
        this.eastimatePayDate = this._dateTimeService.getDate();
       
        this.customertitle = jobdata.customerName + '(' + jobdata.projectNumber + ')'
        this._estimatedTrackerServiceProxy.getEstimatedQuickView(jobdata.id).subscribe((result) => {
            this.modal.show();
            this.paymentReceipt = result;       
            this.UploadDocPath = AppConsts.remoteServiceBaseUrl + this.paymentReceipt.eastimatePayReceiptPath;    
            this.eastimatePayDate = this.paymentReceipt.eastimatePayDate === undefined ? this.eastimatePayDate : this.paymentReceipt.eastimatePayDate;
            this.spinnerService.hide();
        });
    }
    handleFileInput(files: FileList) {
        this.fileToUpload = files.item(0);
    }
    savePaymentReceipt(): void {
        this.saving = true;
        this.spinnerService.show();
        const formData: FormData = new FormData();
        const file = this.fileToUpload;
        if (this.fileToUpload === null && this.paymentReceipt.eastimatePayReceiptPath == '') {
            this.notify.info(this.l('Plz Select any One File'));
            return
        }
        if (this.fileToUpload != null) {
            formData.append('file', this.fileToUpload, this.fileToUpload.name);
        }
        this.paymentReceipt.eastimatePayDate = this.eastimatePayDate;
        for (var key in this.paymentReceipt) {
            formData.append(key, this.paymentReceipt[key]);
        }
        formData.append('SectionId', "39");
        this._httpClient.post<any>(this.uploadUrl, formData)
            .subscribe((response) => {
                if (response.success) {
                    this.spinnerService.hide();
                    this.saving = false;
                    this.modal.hide();
                    this.notify.info(this.l('SavedSuccessfully'));
                } else if (response.error != null) {
                    this.notify.error(this.l('Failed'));
                }
            });
    }
    close(): void {
        this.active = false;
        this.modal.hide();
    }

    navigateToAppDetail(): void {
        this.ExpandedViewApp = !this.ExpandedViewApp;
        this.ExpandedViewApp = true;
    }
    expandApp() {
        this.ExpandedViewApp = false;
    }

}
