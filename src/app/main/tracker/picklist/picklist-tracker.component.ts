import { Component, ElementRef, HostListener, Injector, Input, OnInit, Output, ViewChild, ViewEncapsulation } from '@angular/core';
import { ViewMyLeadComponent } from '@app/main/leads/customerleads/view-mylead.component';
import { AddActivityModalComponent } from '@app/main/leads/leadactivity/add-activity-model.component';
import { DataTransferService } from '@app/shared/common/projectservice/data-transfer.service';
import { DateTimeService } from '@app/shared/common/timing/date-time.service';
import { AppComponentBase } from '@shared/common/app-component-base';
import { CommonLookupDto, CommonLookupServiceProxy, CreateOrEditJobRefundDto, JobRefundServiceProxy, OrganizationUnitDto, PickListTrackerServiceProxy, PickListTrackerSummaryCount, RefundTrackerServiceProxy, UserListDto } from '@shared/service-proxies/service-proxies';
import { AbpSessionService } from 'abp-ng2-module';
import { DateTime } from 'luxon';
import { LazyLoadEvent } from 'primeng/api';
import { Paginator } from 'primeng/paginator';
import { Table } from 'primeng/table';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppConsts } from '@shared/AppConsts';


@Component({
  selector: 'picklist-tracker',
  templateUrl: './picklist-tracker.component.html',
  styleUrls: ['./picklist-tracker.component.css'],
  encapsulation: ViewEncapsulation.None,
  animations: [appModuleAnimation()]
})
export class PickListTrackerComponent extends AppComponentBase implements OnInit {
  @ViewChild('dataTable', { static: true }) dataTable: Table;
  @ViewChild('paginator', { static: true }) paginator: Paginator;

  // @ViewChild('SampleDatePicker', { static: true }) sampleDatePicker: ElementRef;

  @ViewChild('viewLeadDetail', { static: true }) viewLeadDetail: ViewMyLeadComponent;
  @ViewChild('addActivityModal', { static: true }) addActivityModal: AddActivityModalComponent;
  // @Output() reloadLead = new EventEmitter();
  @Input() SelectedLeadId: number = 0;
  @Input() SectionName: string = "PickListTracker";
  advancedFiltersAreShown = false;
  FiltersData = false;
  active = false;
  saving = false;
  recordCount: any;
  leadId: any;
  filterText: string = '';
  shouldShow: boolean = false;
  //sampleDateRange: moment.Moment[] = [moment().add(0, 'days').endOf('day'), moment().add(0, 'days').endOf('day')];
  ExpandedView: boolean = true;
  leadName: string;
  flag = true;
  PageNo: number;
  allOrganizationUnits: OrganizationUnitDto[];
  allOrganizationUnitsList: OrganizationUnitDto[];
  organizationUnitlength: number = 0;
  organizationUnit = 0;
  applicationstatus: number = 0;
  leadstatuss: CommonLookupDto[];
  allStates: CommonLookupDto[];
  allapplicationStatus: CommonLookupDto[];
  allSolarType: CommonLookupDto[];
  allEmployeeList: UserListDto[];
  allDiscom: CommonLookupDto[];
  allDivision: CommonLookupDto[];
  allSubDivision: CommonLookupDto[];
  allTenderData: CommonLookupDto[];
  allCircleData: CommonLookupDto[];
  //filter variable
  leadStatusIDS = [];
  leadSourceIdFilter = [];
  leadApplicationStatusFilter = [];
  applicationstatusTrackerFilter: number;
  otpPendingFilter = '';
  queryRaisedFilter = '';
  employeeIdFilter = [];
  DiscomIdFilter = [];
  CircleIdFilter = [];
  DivisionIdFilter = [];
  SubDivisionIdFilter = [];
  solarTypeIdFilter = [];
  TenderIdFilter = [];
  FilterbyDate = '';
  date: Date = new Date('01/01/2022');
  StartDate: DateTime = this._dateTimeService.getStartOfDayForDate(this.date);
  EndDate: DateTime = this._dateTimeService.getDate();
  sectionId: number = AppConsts.SectionPage.PicklistJob;  
  public screenWidth: any;
  public screenHeight: any;
  testHeight = 200;
  contentHeight = 230;
  show: boolean = true;
  toggle: boolean = true;
  picklistTrackerSummaryCount :PickListTrackerSummaryCount;
  toggleBlock() {
    this.show = !this.show;
  };
  change() {
    this.toggle = !this.toggle;
  }
  constructor(
    injector: Injector,
    private _picklistTrackerServiceProxy: PickListTrackerServiceProxy,
    private _commonLookupService: CommonLookupServiceProxy,
    private _dateTimeService: DateTimeService,
    private _dataTransfer: DataTransferService,
    
  ) {
    super(injector);
    this.picklistTrackerSummaryCount  = new PickListTrackerSummaryCount();
  }

  ngOnInit(): void {
    this.screenHeight = window.innerHeight;
    //selected Project Application status
    this.applicationstatusTrackerFilter = 4;

    this._commonLookupService.getAllApplicationStatusForDropdown().subscribe(result => {
      this.allapplicationStatus = result;
    }, error => { this.spinnerService.hide() });
    this._commonLookupService.getAllLeadStatusDropdown().subscribe(result => {
      this.leadstatuss = result;
    }, error => { this.spinnerService.hide() });
    this._commonLookupService.getAllDiscomFilterDropdown(0).subscribe(result => {
      this.allDiscom = result;
    }, error => { this.spinnerService.hide() });

    this._commonLookupService.getAllSubdivisionWiseData('').subscribe(result => {
      this.allSubDivision = result;
    }, error => { this.spinnerService.hide() });
    this._commonLookupService.getAllSolarTypeForDropdown().subscribe(result => {
      this.allSolarType = result;
    }, error => { this.spinnerService.hide() });
    this._commonLookupService.getAllUserList().subscribe(result => {
      this.allEmployeeList = result;
    }, error => { this.spinnerService.hide() });
    this._commonLookupService.getOrganizationUnitUserWise().subscribe(output => {
      this.allOrganizationUnits = output;
      this.organizationUnit = this.allOrganizationUnits[0].id;
      this.organizationUnitlength = this.allOrganizationUnits.length;

      //change Organization
      this.allOrganizationUnitsList = this.allOrganizationUnits.filter(item => item.id != this.organizationUnit);
      this.getPickListList();
    }, error => { this.spinnerService.hide() })
  }
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.screenHeight = window.innerHeight;
  }
  testHeightSize() {
    if (this.FiltersData == true) {
      this.testHeight = this.testHeight + 81;
    }
    else {
      this.testHeight = this.testHeight - 81;
    }
  }
  expandGrid() {
    this.ExpandedView = true;
    this.getPickListList();
  }
  changeDiscom() {
    if (this.DiscomIdFilter.length > 0) {
      this._commonLookupService.getDiscomWiseCircleFilterForDropdown(this.DiscomIdFilter).subscribe(result => {
        this.allCircleData = result;
        if (this.allCircleData.length == 0) {
          this.allCircleData = undefined;
        }
      }, error => { this.spinnerService.hide() });
    }
    else {
      this.allCircleData = [];
      this.CircleIdFilter = [];
      this.allDivision = [];
      this.DivisionIdFilter = [];
      this.allSubDivision = [];
      this.SubDivisionIdFilter = [];
    }
  }
  changeCircle() {
    if (this.CircleIdFilter.length > 0) {
      this._commonLookupService.getCircleWiseDivisionFilterForDropdown(this.CircleIdFilter).subscribe(result => {
        this.allDivision = result;
        if (this.allDivision.length == 0) {
          this.allDivision = undefined
        }
      }, error => { this.spinnerService.hide() });
    }
    else {
      this.allDivision = [];
      this.DivisionIdFilter = [];
      this.allSubDivision = [];
      this.SubDivisionIdFilter = [];
    }
  }
  changeDivision() {
    if (this.CircleIdFilter.length > 0) {
      this._commonLookupService.getDivisionWiseSubDivisionFilterForDropdown(this.DivisionIdFilter).subscribe(result => {
        this.allSubDivision = result;
        if (this.allSubDivision.length == 0) {
          this.allSubDivision = undefined
        }
      }, error => { this.spinnerService.hide() });
    }
    else {
      this.allSubDivision = [];
      this.SubDivisionIdFilter = [];
    }
  }
  leadstatuschange() {
    if (this.leadStatusIDS.includes(6)) {
      //this.showJobStatus = true;
    } else {
      //this.showJobStatus = false;
      //this.jobStatusID = [];
    }
  }
  chaneSolarType() {
    this.allTenderData = undefined;
    this._commonLookupService.getSolarTypeWiseTenderForDropdown(this.solarTypeIdFilter).subscribe(result => {
      this.allTenderData = result;
      if (this.allTenderData.length == 0) {
        this.allTenderData = undefined;
      }
    }, error => { this.spinnerService.hide() });
  }
  getPickListList(event?: LazyLoadEvent) {
    this.picklistTrackerSummaryCount  = new PickListTrackerSummaryCount();
    this._dataTransfer.setOrgData(this.organizationUnit.toString());
    this.PageNo = this.primengTableHelper.getSkipCount(this.paginator, event);
    if (this.primengTableHelper.shouldResetPaging(event)) {
      this.paginator.changePage(0);
      //return;
    }
    this.primengTableHelper.showLoadingIndicator();

    //this.bindDocumentFilter.filter = this.leadId
    this._picklistTrackerServiceProxy.getAllPickListTracker(
      this.filterText,
      this.organizationUnit,
      this.leadStatusIDS,
      this.employeeIdFilter,
      this.leadApplicationStatusFilter,
      undefined,
      undefined,
      this.FilterbyDate,
      this.StartDate,
      this.EndDate,
      undefined,
      this.primengTableHelper.getSorting(this.dataTable),
      this.primengTableHelper.getSkipCount(this.paginator, event),
      this.primengTableHelper.getMaxResultCount(this.paginator, event)
    ).subscribe((result) => {
      this.primengTableHelper.totalRecordsCount = result.totalCount;
      this.primengTableHelper.records = result.items;
      this.primengTableHelper.hideLoadingIndicator();
      this.recordCount = result.totalCount;
      this.picklistTrackerSummaryCount = result.items[0].pickListTrackerSummaryCount;
      this.shouldShow = false;
      if (this.flag == false && result.totalCount != 0 && this.ExpandedView == false) {
        this.navigateToLeadDetail(this.SelectedLeadId)
      }
      else {
        if (this.ExpandedView == false && result.totalCount != 0) {
          this.navigateToLeadDetail(result.items[0].picklistTracker.leaddata.id)
        }
        else {
          this.ExpandedView = true;
        }
      }
    }, error => { this.spinnerService.hide() });
  }
  navigateToLeadDetail(leaddata): void {

    this.ExpandedView = !this.ExpandedView;
    this.ExpandedView = false;
    this.SelectedLeadId = leaddata.picklistTracker.leaddata.id;
    this.leadName = "PickListTracker"
    this.viewLeadDetail.showDetail(leaddata.picklistTracker.leaddata, leaddata.picklistTracker.applicationStatusdata, this.leadName, this.sectionId);
  }
  reloadPage(flafValue: boolean): void {
    this.ExpandedView = true;
    this.flag = flafValue;
    this.paginator.changePage(this.paginator.getPage());
    if (!flafValue) {
      this.navigateToLeadDetail(this.SelectedLeadId);
    }
    this.paginator.changePage(this.paginator.getPage());
  }

  exportToExcel() {

  }
  clear() {
    this.filterText = '';
    this.leadStatusIDS = [];
    this.leadApplicationStatusFilter = [];
    this.applicationstatusTrackerFilter = 4;
    this.otpPendingFilter = '';
    this.queryRaisedFilter = '';
    this.DiscomIdFilter = [];
    this.CircleIdFilter = [];
    this.DivisionIdFilter = [];
    this.SubDivisionIdFilter = [];
    this.solarTypeIdFilter = [];
    this.employeeIdFilter = [];
    this.FilterbyDate = '';
    this.StartDate = this._dateTimeService.getStartOfDayForDate(this.date);
    this.EndDate = this._dateTimeService.getDate();
    this.getPickListList();
  }
}
