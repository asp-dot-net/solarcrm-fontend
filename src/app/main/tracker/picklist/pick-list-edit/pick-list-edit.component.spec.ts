import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PickListEditComponent } from './pick-list-edit.component';

describe('PickListEditComponent', () => {
  let component: PickListEditComponent;
  let fixture: ComponentFixture<PickListEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PickListEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PickListEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
