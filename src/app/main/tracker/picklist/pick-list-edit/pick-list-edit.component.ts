import { Component, ElementRef, Injector, OnInit, ViewChild } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { AppConsts } from '@shared/AppConsts';
import { ActivityLogInput, CreateOrEditPicklistItemDto, GetPickListItemForEditOutput, PickListData, PickListTrackerServiceProxy } from '@shared/service-proxies/service-proxies';
import { NotifyService } from 'abp-ng2-module';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'picklistedit',
  templateUrl: './pick-list-edit.component.html',
  styleUrls: ['./pick-list-edit.component.css']
})
export class PickListEditComponent implements OnInit {
  @ViewChild('PickklistModel') public modal: ModalDirective;
  sectionId: number = AppConsts.SectionPage.PicklistJob;
  spinnerService: NgxSpinnerService;
  notify: NotifyService;
  PickListItemView: CreateOrEditPicklistItemDto[];
  PickListData: any[];
  constructor(injector: Injector,
    private _picklistTrackerServiceProxy: PickListTrackerServiceProxy,
    private spinner: NgxSpinnerService) {
    this.notify = injector.get(NotifyService);
  }

  ngOnInit(): void {
  }

  PicklistitemEditview(dispatchId: number): void {
    this._picklistTrackerServiceProxy.getPickLitsForView(dispatchId).subscribe(result => {      
      if (result.picklistItem.length > 0) {
        this.PickListItemView = result.picklistItem;
        this.PickListData = this.PickListItemView;
        this.modal.show();
      }
      else { 
        this.notify.info("Not PickList Found");
      }
    }, error => { this.spinnerService.hide() });
  }
  public editPicklist() {
    this.spinner.show();
    this._picklistTrackerServiceProxy.pickListItemForAddOrEdit(this.PickListData).subscribe(result => {
      this.notify.info(('SavedSuccessfully'));
      this.modal.hide();
      this.spinner.hide();
    }, error => { this.spinnerService.hide() });
  }
  close() {
    this.modal.hide();
  }
}
