import { Component, ElementRef, Injector, OnInit, ViewChild } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { AddActivityModalComponent } from '@app/main/leads/leadactivity/add-activity-model.component';
import { ActivityLogInput, CommonLookupDto, PdfTemplateServiceProxy, PickListQuotationDataDetailsDto, PickListTrackerServiceProxy, QuotationDataDetailsDto } from '@shared/service-proxies/service-proxies';
import * as _ from 'lodash';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { AppConsts } from '@shared/AppConsts';
import { DateTimeService } from '@app/shared/common/timing/date-time.service';
import { DataTransferService } from '@app/shared/common/projectservice/data-transfer.service';
import { NotifyService } from 'abp-ng2-module';
@Component({
  selector: 'picklistgenerate',
  templateUrl: './pick-list-generate.component.html',
  styleUrls: ['./pick-list-generate.component.css']
})
export class PickListGenerateComponent implements OnInit {
  @ViewChild("myNameElem") myNameElem: ElementRef;
  @ViewChild("area_select") area_select: ElementRef;
  @ViewChild("modal_body") modal_body: ElementRef;
  @ViewChild('pdfTemplateModel') public modal: ModalDirective;
  @ViewChild('btnEmail', { static: true }) btnEmail: ElementRef;
  @ViewChild('btnsmsnotify', { static: true }) btnsmsnotify: ElementRef;
  @ViewChild('SampleDatePicker', { static: true }) sampleDatePicker: ElementRef;
  @ViewChild('addActivityModal', { static: true }) addActivityModal: AddActivityModalComponent;


  saving = false;
  //sampleDate: moment.Moment = moment().add(1, 'days').startOf('day');

  activityLog: ActivityLogInput = new ActivityLogInput();
  activityType: number = 6;
  priorityType: number = 1;
  activityName = "";
  actionNote = "";
  id = 0;
  actionId: number = 12;
  sectionId: number = AppConsts.SectionPage.PicklistJob;  
  allEmailTemplates: CommonLookupDto[];
  allSMSTemplates: CommonLookupDto[];
  drpleadactivity: CommonLookupDto[];
  emailData = '';
  smsData = '';
  emailTemplate: number;
  uploadUrl: string;
  uploadedFiles: any[] = [];
  myDate = new Date();
  total = 0;
  credit = 0;
  customeTagsId = 0;
  role: string = '';
  subject = '';
  userName: string;
  leadCompanyName: any;
  pageid = 0;
  referanceId = 0;
  CurrentDate = new Date();
  SelectedJobNumber: string = '';
  emailfrom: string = '';
  emailto: string = '';
  fromemails: CommonLookupDto[];
  leadid: number;
  criteriaavaibaleornot = false;
  smsid = false;
  htmlstr: string = '';
  htmlContentbody: any;
  quoteNumber: string = '';
  teststr: string = '';
  item: PickListQuotationDataDetailsDto;
  spinnerService: NgxSpinnerService;
  notify: NotifyService;
  constructor(injector: Injector,
    private _pdfTemplateServiceProxy: PdfTemplateServiceProxy,
    private sanitizer: DomSanitizer,
    private _picklistTrackerServiceProxy: PickListTrackerServiceProxy,
    private spinner: NgxSpinnerService,
    private _dateTimeService: DateTimeService,
    private _dataTransfer: DataTransferService) {
      this.notify = injector.get(NotifyService);
      this.item = new PickListQuotationDataDetailsDto();
  }

  ngOnInit(): void {

  }
  GenereatePicklistFile(Picklistdata) {    
    this.SelectedJobNumber = Picklistdata.projectNumber;
    this._picklistTrackerServiceProxy.pickListQuotationData(Picklistdata.dispatchJobId,Picklistdata.id, parseInt(this._dataTransfer.getOrgData())).subscribe(result => {
      this.item = result;
    })
    this.show();
  }
  show(): void {
    this.spinner.show();
    this._pdfTemplateServiceProxy.getPdfTemplateForView(AppConsts.pdfGenerateDocument.Picklist,parseInt(this._dataTransfer.getOrgData())).subscribe(result => {
      if(result.pdfTemplates != undefined){
        this.htmlContentbody = result.pdfTemplates.viewHtml;
        setTimeout(() => {                        
          this.setHTML(this.htmlContentbody);
          this.spinner.hide();
        }, 1000);
      }else{
        this.spinner.hide();
        this.notify.info("Please First Create the PickList Template");
      }
     
      
    }, error => { this.spinnerService.hide() });
  }

  setHTML(pdfHTML) {
    let pdfTemplate = this.getPdfTemplate(pdfHTML);
    this.teststr = pdfHTML;
    this.htmlContentbody = pdfTemplate;
    this.htmlContentbody = this.sanitizer.bypassSecurityTrustHtml(this.htmlContentbody);

    if (pdfHTML != '') {
      this.modal.show();
    }
  }

  getPdfTemplate(smsHTML) {
    let myTemplateStr = smsHTML;
    let TableList = [];
    let BankList = [];
    if (this.item.jobProductItemList != null) {
      this.item.jobProductItemList.forEach(obj => {
        TableList.push("<tr class='highlight-row'><td>1</td><td class='bold' colspan='2'>" + obj.itemName + '<br>' + obj.itemDescription + '</td>' + '<td>' + obj.itemQty + "</td></tr>");
      })
    }
    var categoryName;
    if (this.item.jobPickList != null) {
      this.item.jobPickList.forEach(obj => {
        if (obj.productCategory != categoryName) {
          categoryName = obj.productCategory;
          TableList.push("<tr class='light'><th></th><th colspan='2'>" + obj.productCategory + "</th><th style='width:150px;'></th></tr>");
        }
        TableList.push("<tr><td>2</td><td colspan='2'>" + obj.productname + "</td><td>" + obj.productQty + "</td></tr>");
      })
    }
    let QrcodeImg;
    if (this.item.organizationbankDetails != null) {
      this.item.organizationbankDetails.forEach(obj => {
        QrcodeImg = '';
        BankList.push("<div class='payment-panel'><div class='pay-content'>Bank : " + obj.orgBankName + "</div><div class='pay-content'>A/C Name : " + obj.orgBankBrachName + "</div><div class='pay-content'>A/C No. : " + obj.orgBankAccountNumber + "</div><div class='pay-content'>IFSC : " + obj.orgBankIFSC_Code + "</div></div>");
        QrcodeImg = AppConsts.remoteServiceBaseUrl + obj.orgBankQrCode_path;
      });
      BankList.push("<div class='payment-panel-code'><img src='" + QrcodeImg + "'></div>");
    }
    let addressValue = this.item.address;// + " " + this.item.address2; //+ ", " + this.item.lead.state + "-" + this.item.lead.postCode;
     let installdate = this._dateTimeService.getStartOfDayForDate(this.item.installationDate)
    // let generatedate = this.item.quoteGenerateDate;
    let currentdate = this._dateTimeService.getUTCDate().day + '/' + this._dateTimeService.getUTCDate().month + '/' + this._dateTimeService.getUTCDate().year
    let myVariables = {
      Customer: {
        Name: this.item.customerName,
        Address: addressValue,
        Mobile: this.item.mobile,
        Email: this.item.email,
        Phone: this.item.altPhone,
        ProjectNo: this.item.projectNo,
        ApplicationNo: this.item.applicationNo,
        SystemSize: this.item.systemSize,
        StructureHeight: this.item.structureHeight,
        InstallationDate: installdate.day +"/"+installdate.month +"/"+ installdate.year ,
        // SalesRep: this.item.currentAssignUserName
      },
      Quote: {
        InstallerName: this.item.installerName,
        ChannelPartner: '',
        InstallerMobile: this.item.installerMobile,
        InstallerNote: this.item.installerNote,
        TotalSystemCost: this.item.totalSystemCost,
        PaidToDate: this.item.paidToDate,
        BalanceOwing: this.item.balanceOwing,
        PickedBy: this.item.pickedBy,
        PickedDate: currentdate,// this.item.pickedDate,

      },
      QuoteProduct: {
        QuoteTable: TableList.toString().replace(/,/g, ''),
      },
      Organization: {
        orgName: this.item.organizationDetails[0]['displayName'],
        orgEmail: this.item.organizationDetails[0]['organization_Email'],
        orgMobile: this.item.organizationDetails[0]['organization_Mobile'],
        orgLogo: AppConsts.remoteServiceBaseUrl + this.item.organizationDetails[0]['organization_LogoFilePath'],  //this.item.organizationDetails[0]['organization_GSTNumber']
        orgWebsite: this.item.organizationDetails[0]['organization_Website'],
        bankDetails: BankList.toString().replace(/,/g, ''),
      },
    }
    //let myVariables = myTemplateStr;
    // use custom delimiter {{ }}
    // _.templateSettings.interpolate = /{{([\s\S]+?)}}/g;
    // 
    // // interpolate
    // let compiled = _.template(myTemplateStr);
    // let myTemplateCompiled = compiled(myVariables);        
    _.templateSettings.interpolate = /{{([\s\S]+?)}}/g;

    // interpolate
    let compiled = _.template(myTemplateStr);
    let myTemplateCompiled = compiled(myVariables);
    return myTemplateCompiled;
  }

  GenereatePicklistFilePDf() {
    this.spinner.show();
    this.htmlstr = this.htmlContentbody.changingThisBreaksApplicationSecurity; // this.teststr.toString().replace('"',"'");// this.htmlContentbidy.changingThisBreaksApplicationSecurity;
    this._picklistTrackerServiceProxy.generatePickListPdf(this.SelectedJobNumber, this.htmlstr).subscribe(result => {
      let FileName = result;
      this.saving = false;
      this.spinner.hide();
      window.open(result, "_blank");
    }, error => { this.spinner.hide() });
  }
  close() {
    this.modal.hide();
  }
}
