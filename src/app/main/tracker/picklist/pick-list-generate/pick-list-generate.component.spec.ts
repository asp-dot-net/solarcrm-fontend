import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PickListGenerateComponent } from './pick-list-generate.component';

describe('PickListGenerateComponent', () => {
  let component: PickListGenerateComponent;
  let fixture: ComponentFixture<PickListGenerateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PickListGenerateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PickListGenerateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
