import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PickListTrackerComponent } from './picklist-tracker.component';

describe('PickListTrackerComponent', () => {
  let component: PickListTrackerComponent;
  let fixture: ComponentFixture<PickListTrackerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PickListTrackerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PickListTrackerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
