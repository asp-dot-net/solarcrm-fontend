import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { applicationListDetailModal } from './application/application-list-detail-modal.component';
import { ApplicationListComponent } from './application/application-list.componet';
import { EstimateListComponent } from './estimate/estimate-list.componet';
import { MeterConnectComponent } from './meter/meter-connect.componet';
import { PickListGenerateComponent } from './picklist/pick-list-generate/pick-list-generate.component';
import { PickListTrackerComponent } from './picklist/picklist-tracker.component';
import { RefundTrackerComponent } from './refundtracker/refund-tracker.component';
import { InstalltionTrackerComponent } from './installtion-tracker/installtion-tracker';
import { DepositTrackerComponent } from './deposit-tracker/deposit-tracker.component';
import { TransportTrackerComponent } from './transport-tracker/transport-tracker.component';
import { SelfCertificateTemplateModalComponent } from './meter/SelfCertificatel-template.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '',
                // children: [
                //     {
                //         path: 'manageleads',
                //         loadChildren: () => import('./manageleads/manageleads.module').then(m => m.ManageLeadsModule),
                //         //data: { permission: 'Pages.Tenant.DataVaults.Customer', features: 'App.DataVaults.Customer' },
                //     },
                //      {
                //          path: 'leadsactivity',
                //          loadChildren: () => import('../leads/leadactivity/leadsactivity.module').then(m => m.LeadsActivityModule),
                //     //     data: { permission: 'Pages.Tenant.DataVaults.Customer' },
                //      }
                // ]
            },          
            { path: 'application', component: ApplicationListComponent },
            { path: 'estimate', component: EstimateListComponent },
            { path: 'meter', component: MeterConnectComponent },
            { path: 'refundtracker', component: RefundTrackerComponent },  
            { path: 'picklist', component: PickListTrackerComponent },        
            { path : 'picklistgenerate', component: PickListGenerateComponent},
            { path: 'deposittracker', component: DepositTrackerComponent },  
            { path: 'installtiontracker', component: InstalltionTrackerComponent },  
            { path: 'transporttracker', component: TransportTrackerComponent } ,
            { path: 'selfCertificateTemplateModel', component: SelfCertificateTemplateModalComponent },      
        ]),
    ],
    exports: [RouterModule],
})
export class TrackerRoutingModule { }