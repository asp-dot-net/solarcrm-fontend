import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RefundVerifyComponent } from './refund-verify.component';

describe('JobRefundComponent', () => {
  let component: RefundVerifyComponent;
  let fixture: ComponentFixture<RefundVerifyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RefundVerifyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RefundVerifyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
