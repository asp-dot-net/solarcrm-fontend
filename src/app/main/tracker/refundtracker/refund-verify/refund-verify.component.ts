import { Component, EventEmitter, Injector, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { DataTransferService } from '@app/shared/common/projectservice/data-transfer.service';
import { DateTimeService } from '@app/shared/common/timing/date-time.service';
import { AppConsts } from '@shared/AppConsts';
import { AppComponentBase } from '@shared/common/app-component-base';
import { CommonLookupDto, CommonLookupServiceProxy, CreateOrEditJobRefundDto, GetJobRefundForEditOutput, GetJobRefundForViewDto, JobRefundServiceProxy, UserListDto } from '@shared/service-proxies/service-proxies';
import { AbpSessionService, MessageService, NotifyService } from 'abp-ng2-module';
import { DateTime } from 'luxon';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Paginator } from 'primeng/paginator';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'createOrEditRefundVerifyModal',
  templateUrl: './refund-verify.component.html',
  styleUrls: ['./refund-verify.component.css']
})
export class RefundVerifyComponent extends AppComponentBase implements OnInit {
  @ViewChild('paginator', { static: true }) paginator: Paginator;
  @Input() SelectedJobId: number = 0;
  @Input() SelectedLeadId: number = 0;
  // @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
  documentListname: any;
  leadDocumentList: any;
  //documentLibrary: CreateOrEditDocumentLibraryDto = new CreateOrEditDocumentLibraryDto();
  filesize: number = 1000000;
  active = false;
  saving = false;
  recordCount: any;
  leadId: any;
  filterText: string = '';
  NewPayment: boolean = false;
  RefundReason: CommonLookupDto[];
  RefundPaymentType: CommonLookupDto[];
  UserList: UserListDto[];
  RefundPaymentDetails: GetJobRefundForEditOutput;
  PaymentReceiptList: GetJobRefundForViewDto[];
  JobProducts: any[];
  paymentDate: DateTime;
  paymentRefund: CreateOrEditJobRefundDto = new CreateOrEditJobRefundDto();
  jobid: number = 0;
  UploadSignDate: DateTime;
  sectionId: number = AppConsts.SectionPage.JobRefund; 
  uploadUrl: string;
  notify: NotifyService;
  public totalfiles: Array<File> = [];
  public totalFileName = [];
  public lengthCheckToaddMore = 0;
  constructor(
    injector: Injector,
    private _paymentRefundServiceProxy: JobRefundServiceProxy,
    private _commonLookup: CommonLookupServiceProxy,
    private _sessionService: AbpSessionService,
    private formBuilder: FormBuilder,
    private _dateTimeService: DateTimeService,
    private _dataTransfer: DataTransferService,
  ) {
    super(injector);
    this.uploadUrl = AppConsts.remoteServiceBaseUrl + '/Payment/PaymentDetailsReceiptDocument';
  }

  ngOnInit(): void {
    //this.initializationpaymentReceipt();
    this.RefundReason = [];
    this.RefundPaymentType = [];

    this._commonLookup.getAllJobRefundForDropdown().subscribe((result) => {
      this.RefundReason = result;
    }, error => { this.spinnerService.hide() });

    this._commonLookup.getAllPaymentTypeDropdown().subscribe((result) => {
      this.RefundPaymentType = result;
    }, error => { this.spinnerService.hide() });
    this._commonLookup.getAllUserList().subscribe((result) => {
      this.UserList = result;
    }, error => { this.spinnerService.hide() });
    this.initializationpaymentReceipt();
    this.getRefundPaymentList();
  }
  @ViewChild('addRefundPaymentModal', { static: true }) modal: ModalDirective;
  public documentGrp: FormGroup;
  paymentRefundList: any;
  getRefundPaymentList() {
    this._paymentRefundServiceProxy.getAll(this.SelectedJobId).subscribe((result) => {
      //this.PaymentDetails = result;
      //this.PaymentDetails.paymentJobId = this.SelectedJobId;
      this.paymentRefundList = [];
      this.NewPayment = true;
      this.paymentRefundList = result;
      //this.items = this.formBuilder.array([this.createUploadDocuments()])

      //this.items.controls['items'].setFormValue(result.paymentReceiptItems[0].paymentPayDate);// insert(0,new FormArray(result.paymentReceiptItems.map(x => new FormArray([]))));
      // this.items.controls[0].controls['PaymentPayDate'].value = '';

    }, error => { this.spinnerService.hide() });
  }
  initializationpaymentReceipt() {
    this.documentGrp = this.formBuilder.group({
      Id: 0,
      RefundJobId: this.SelectedJobId,
      RefundReasonId: null,
      PaymentTypeId: 0,
      RefundPaidAmount: 0,
      RefundPaidDate: '',
      RefundPaidNotes: '',
      CreatorUserId: [this._sessionService.userId, { disabled: true }],
      IsRefundVerify: false,
      RefundVerifyDate: '',
      RefundVerifyNotes: '',
      RefundVerifyById: 0,
      BankAccountNumber: 0,
      BankAccountHolderName: '',
      BankBrachName: '',
      BankName: '',
      IFSC_Code: '',
      RefundRecPath: '',
      ReceiptNo: '',
      JobRefundEmailSend: 0,
      JobRefundEmailSendDate: '',
      JobRefundSmsSend: 0,
      JobRefundSmsSendDate: '',
      leadId: this.SelectedLeadId,
      OrgId: 1,
      // PaymentRecAddedById: [this._sessionService.userId, { disabled: true }],      
      documentFile: new FormControl(File),

    });
  }
  editrefundPayment(item) {
    this.documentGrp.reset();
    let date = item.refundPaidDate == null ? new Date : this._dateTimeService.formatDate(item.refundPaidDate, "DD/MM/YYYY"); //new Date(item.refundPaidDate).toLocaleDateString("DD/MM/YYYY");
    this.documentGrp.patchValue({
      Id: item.id,
      RefundJobId: item.refundJobId,
      RefundReasonId: item.refundReasonId,
      PaymentTypeId: item.paymentTypeId,
      RefundPaidAmount: item.refundPaidAmount,
      RefundPaidDate: date,
      RefundPaidNotes: item.refundPaidNotes,
      CreatorUserId: [this._sessionService.userId, { disabled: true }],
      IsRefundVerify: item.isRefundVerify,
      RefundVerifyDate: item.refundVerifyDate,
      RefundVerifyNotes: item.refundVerifyNotes,
      RefundVerifyById: this._sessionService.userId,
      BankAccountNumber: item.bankAccountNumber,
      BankAccountHolderName: item.bankAccountHolderName,
      BankBrachName: item.bankBrachName,
      BankName: item.bankName,
      IFSC_Code: item.iFSC_Code,
      RefundRecPath: item.refundRecPath,
      ReceiptNo: item.receiptNo,
      JobRefundEmailSend: 0,
      JobRefundEmailSendDate: null,
      JobRefundSmsSend: 0,
      JobRefundSmsSendDate: null,
      leadId: this.SelectedLeadId,
      OrgId: 1,
    });
    this.modal.show();
  }
  message: MessageService;

  addRefundPayment(item: any) {
    this.editrefundPayment(item);
    this.modal.show();
  }
  close(): void {
    this.active = false;
    this.modal.hide();
  }

  public singlefileSelectionEvent(fileInput: any,) {
    if (fileInput.target.files && fileInput.target.files[0]) {
      var reader = new FileReader();
      reader.onload = (event: any) => {
      }
      //if (oldIndex == 0) 
      {
        this.totalfiles.unshift((fileInput.target.files[0]))
        this.totalFileName.unshift(fileInput.target.files[0].name)
      }
      reader.readAsDataURL(fileInput.target.files[0]);
    }

    if (this.totalfiles.length == 1) {
      this.lengthCheckToaddMore = 1;
    }
  }
  downfile(filename): void {
    let FileName = AppConsts.remoteServiceBaseUrl + filename;//this.LeadDocument.filePath;
    window.open(FileName, "_blank");
  };
  OnSubmit(formValue: any): void {
    this.saving = true;
    this._paymentRefundServiceProxy.createOrEdit(formValue.value).pipe(
      finalize(() => { this.saving = false; })
    ).subscribe(() => {
      this.active = true;
      this.documentGrp.reset();
      this.notify.info(('SavedSuccessfully'));
      this.modal.hide();
      document.getElementById('btnRefresh').click();
    }, error => { this.spinnerService.hide() });
  }
}
