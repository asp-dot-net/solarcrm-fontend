import { Component, ElementRef, HostListener, Injector, Input, OnInit, Output, ViewChild } from '@angular/core';
import { ViewMyLeadComponent } from '@app/main/leads/customerleads/view-mylead.component';
import { AddActivityModalComponent } from '@app/main/leads/leadactivity/add-activity-model.component';
import { DataTransferService } from '@app/shared/common/projectservice/data-transfer.service';
import { DateTimeService } from '@app/shared/common/timing/date-time.service';
import { AppComponentBase } from '@shared/common/app-component-base';
import { CommonLookupDto, CommonLookupServiceProxy, CreateOrEditJobRefundDto, JobRefundServiceProxy, OrganizationUnitDto, RefundTrackerServiceProxy, RefundTrackerSummaryCount, UserListDto } from '@shared/service-proxies/service-proxies';
import { AbpSessionService } from 'abp-ng2-module';
import { stat } from 'fs';
import { DateTime } from 'luxon';
import { LazyLoadEvent } from 'primeng/api';
import { Paginator } from 'primeng/paginator';
import { Table } from 'primeng/table';
import { RefundVerifyComponent } from './refund-verify/refund-verify.component';
import { FileDownloadService } from '@shared/utils/file-download.service';
import { AppConsts } from '@shared/AppConsts';

@Component({
  selector: 'app-refund-tracker',
  templateUrl: './refund-tracker.component.html',
  styleUrls: ['./refund-tracker.component.css']
})
export class RefundTrackerComponent extends AppComponentBase implements OnInit {
  @ViewChild('dataTable', { static: true }) dataTable: Table;
  @ViewChild('paginator', { static: true }) paginator: Paginator;

  // @ViewChild('SampleDatePicker', { static: true }) sampleDatePicker: ElementRef;

  @ViewChild('viewLeadDetail', { static: true }) viewLeadDetail: ViewMyLeadComponent;
  @ViewChild('addActivityModal', { static: true }) addActivityModal: AddActivityModalComponent;
  @ViewChild('createOrEditRefundVerifyModal', { static: true }) createOrEditRefundVerifyModal: RefundVerifyComponent
  // @Output() reloadLead = new EventEmitter();
  @Input() SelectedLeadId: number = 0;
  @Input() SectionName: string = "ApplicationTracker";
  advancedFiltersAreShown = false;
  FiltersData = false;
  active = false;
  saving = false;
  recordCount: any;
  leadId: any;
  filterText: string = '';
  shouldShow: boolean = false;
  //leadStatusId = 0;
  //sampleDateRange: moment.Moment[] = [moment().add(0, 'days').endOf('day'), moment().add(0, 'days').endOf('day')];
  ExpandedView: boolean = true;
  leadName: string;
  flag = true;
  PageNo: number;
  allOrganizationUnits: OrganizationUnitDto[];
  allOrganizationUnitsList: OrganizationUnitDto[];
  organizationUnitlength: number = 0;
  organizationUnit = 0;
  applicationstatus: number = 0;
  leadstatuss: CommonLookupDto[];
  allStates: CommonLookupDto[];
  allapplicationStatus: CommonLookupDto[];
  // allLeadSources: CommonLookupDto[];
  allSolarType: CommonLookupDto[];
  allRefundReasonList: CommonLookupDto[];
  allDiscom: CommonLookupDto[];
  allDivision: CommonLookupDto[];
  allSubDivision: CommonLookupDto[];
  allTenderData: CommonLookupDto[];
  allCircleData: CommonLookupDto[];
  //filter variable
  leadStatusIDS = [];
  leadSourceIdFilter = [];
  leadApplicationStatusFilter = [];
  applicationstatusTrackerFilter: number;
  refundPenddingFilter: number = 0;
  refundVerifyFilter: number = 0;
  employeeIdFilter = [];
  DiscomIdFilter = [];
  CircleIdFilter = [];
  DivisionIdFilter = [];
  SubDivisionIdFilter = [];
  solarTypeIdFilter = [];
  TenderIdFilter = [];
  refundReasonFilter: number = 0;
  date: Date = new Date('01/01/2022');
  StartDate: DateTime = this._dateTimeService.getStartOfDayForDate(this.date);
  EndDate: DateTime = this._dateTimeService.getDate();
  sectionId: number = AppConsts.SectionPage.JobRefund;  
  public screenWidth: any;
  public screenHeight: any;
  testHeight = 290;
  contentHeight = 230;
  refundSummaryCount :RefundTrackerSummaryCount;
  show1: boolean = true;
    toggle: boolean = true;    
    toggleBlock() {
        this.show1 = !this.show1;
    };
    change() {
        this.toggle = !this.toggle;
    }
    
  constructor(
    injector: Injector,
    private _refundTrackerServiceProxy: RefundTrackerServiceProxy,
    private _commonLookupService: CommonLookupServiceProxy,
    private _dateTimeService: DateTimeService,
    private _dataTransfer: DataTransferService,
    private _paymentRefundServiceProxy: JobRefundServiceProxy,
    private _sessionService: AbpSessionService,
    private _fileDownloadService: FileDownloadService,
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this.screenHeight = window.innerHeight;
    this.refundSummaryCount = new RefundTrackerSummaryCount();
    //selected Project Application status
    this.applicationstatusTrackerFilter = 4;

    this._commonLookupService.getAllApplicationStatusForDropdown().subscribe(result => {
      this.allapplicationStatus = result;
    }, error => { this.spinnerService.hide() });
    this._commonLookupService.getAllLeadStatusDropdown().subscribe(result => {
      this.leadstatuss = result;
    }, error => { this.spinnerService.hide() });
    this._commonLookupService.getAllDiscomFilterDropdown(0).subscribe(result => {
      this.allDiscom = result;
    }, error => { this.spinnerService.hide() });

    this._commonLookupService.getAllSubdivisionWiseData('').subscribe(result => {
      this.allSubDivision = result;
    }, error => { this.spinnerService.hide() });
    this._commonLookupService.getAllSolarTypeForDropdown().subscribe(result => {
      this.allSolarType = result;
    }, error => { this.spinnerService.hide() });
    this._commonLookupService.getAllJobRefundForDropdown().subscribe(result => {
      this.allRefundReasonList = result;
    }, error => { this.spinnerService.hide() });
    this._commonLookupService.getOrganizationUnitUserWise().subscribe(output => {
      this.allOrganizationUnits = output;
      this.organizationUnit = this.allOrganizationUnits[0].id;
      this.organizationUnitlength = this.allOrganizationUnits.length;

      //change Organization
      this.allOrganizationUnitsList = this.allOrganizationUnits.filter(item => item.id != this.organizationUnit);
      this.getRefundPaymentList();
    }, error => { this.spinnerService.hide() })
  }
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.screenHeight = window.innerHeight;
  }
  testHeightSize() {
    if (this.FiltersData == true) {
      this.testHeight = this.testHeight + -82;
    }
    else {
      this.testHeight = this.testHeight - -82;
    }
  }
  expandGrid() {
    this.ExpandedView = true;
    this.getRefundPaymentList();
  }
  getRefundPaymentList(event?: LazyLoadEvent) {
    this._dataTransfer.setOrgData(this.organizationUnit.toString());
    this.PageNo = this.primengTableHelper.getSkipCount(this.paginator, event);
    if (this.primengTableHelper.shouldResetPaging(event)) {
      this.paginator.changePage(0);
      //return;
    }
    this.primengTableHelper.showLoadingIndicator();

    //this.bindDocumentFilter.filter = this.leadId
    this._refundTrackerServiceProxy.getAllRefundTracker(
      this.filterText,
      this.organizationUnit,
      this.leadStatusIDS,
      this.employeeIdFilter,
      this.leadApplicationStatusFilter,
      undefined,
      this.refundVerifyFilter,
      this.refundPenddingFilter,
      undefined,
      this.refundReasonFilter,
      undefined,
      undefined,
      this.StartDate,
      this.EndDate,
      undefined,
      this.primengTableHelper.getSorting(this.dataTable),
      this.primengTableHelper.getSkipCount(this.paginator, event),
      this.primengTableHelper.getMaxResultCount(this.paginator, event)
    ).subscribe((result) => {
      this.primengTableHelper.totalRecordsCount = result.totalCount;
      this.primengTableHelper.records = result.items;
      this.primengTableHelper.hideLoadingIndicator();
      this.recordCount = result.totalCount;
      this.refundSummaryCount = result.items[0].refundTrackerSummaryCount;
      this.shouldShow = false;
      if (this.flag == false && result.totalCount != 0 && this.ExpandedView == false) {
        this.navigateToLeadDetail(this.SelectedLeadId)
      }
      else {
        if (this.ExpandedView == false && result.totalCount != 0) {
          this.navigateToLeadDetail(result.items[0].refundTracker.leaddata.id)
        }
        else {
          this.ExpandedView = true;
        }
      }
    }, error => { this.spinnerService.hide() });
  }
  navigateToLeadDetail(leaddata): void {
    this.ExpandedView = !this.ExpandedView;
    this.ExpandedView = false;
    this.SelectedLeadId = leaddata.refundTracker.leaddata.id;
    this.leadName = "RefundTracker"
    this.viewLeadDetail.showDetail(leaddata.refundTracker.leaddata, leaddata.refundTracker.applicationStatusdata, this.leadName, this.sectionId);
  }
  reloadPage(flafValue: boolean): void {
    this.ExpandedView = true;
    this.flag = flafValue;
    this.paginator.changePage(this.paginator.getPage());
    if (!flafValue) {
      this.navigateToLeadDetail(this.SelectedLeadId);
    }
    this.paginator.changePage(this.paginator.getPage());
  }
  RefundVerify(refund: CreateOrEditJobRefundDto): void {
    this.message.confirm(
      this.l('', ''),
      this.l('AreYouSureYouWanttoVerify'),
      (isConfirmed) => {
        if (isConfirmed) {
          refund.isRefundVerify = true;
          refund.refundVerifyById = this._sessionService.userId;
          this._paymentRefundServiceProxy.createOrEdit(refund)
            .subscribe(() => {
              this.reloadPage;
              this.notify.success(this.l('SuccessfullyVerify'));
              document.getElementById('btnRefresh').click();
            }, error => { this.spinnerService.hide() });
        }
      }
    );
  }
  RefundNotVerify(refund: CreateOrEditJobRefundDto): void {
    this.message.confirm(
      this.l('', ''),
      this.l('AreYouSureYouWanttoVerify'),
      (isConfirmed) => {
        if (isConfirmed) {
          refund.isRefundVerify = false;
          refund.refundVerifyById = this._sessionService.userId;
          this._paymentRefundServiceProxy.createOrEdit(refund)
            .subscribe(() => {
              this.reloadPage;
              this.notify.success(this.l('SuccessfullyVerify'));
              document.getElementById('btnRefresh').click();
            }, error => { this.spinnerService.hide() });
        }
      }
    );
  }
  deleteRefundItem(index: any) {
    if (index.id != 0) {
      this.message.confirm(
        this.l('RefundPaymentDeleteWarningMessage', index.paymentRecTotalPaid),
        this.l('AreYouSure'),
        (isConfirmed) => {
          if (isConfirmed) {
            this._paymentRefundServiceProxy.delete(index.id)
              .subscribe(() => {
                this.getRefundPaymentList();
                this.notify.success(this.l('SuccessfullyDeleted'));
              }, error => { });
          }
        });
    }
  }
  exportToExcel() {
    this._refundTrackerServiceProxy.getRefundTrackerToExcel(
      this.filterText,
      this.organizationUnit,
      this.leadStatusIDS,
      this.employeeIdFilter,
      this.leadApplicationStatusFilter,
      undefined,
      this.refundVerifyFilter,
      this.refundPenddingFilter,
      undefined,
      this.refundReasonFilter,
      undefined,
      undefined,
      this.StartDate,
      this.EndDate,
      undefined,
      this.primengTableHelper.getSorting(this.dataTable),
      undefined,
      undefined
    )
      .subscribe(result => {
        this._fileDownloadService.downloadTempFile(result);
      });
  }
  clear() {
    this.filterText = '';
    this.leadStatusIDS = [];
    this.leadApplicationStatusFilter = [];
    this.applicationstatusTrackerFilter = 4;
    this.refundPenddingFilter = 0;
    this.refundVerifyFilter = 0;
    this.refundReasonFilter = 0;
    this.StartDate = this._dateTimeService.getStartOfDayForDate(this.date);
    this.EndDate = this._dateTimeService.getDate();
    this.getRefundPaymentList();
  }
}
