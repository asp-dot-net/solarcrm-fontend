import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RefundTrackerComponent } from './refund-tracker.component';

describe('RefundTrackerComponent', () => {
  let component: RefundTrackerComponent;
  let fixture: ComponentFixture<RefundTrackerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RefundTrackerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RefundTrackerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
