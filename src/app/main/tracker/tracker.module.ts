import { CommonModule, DatePipe } from '@angular/common';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppCommonModule } from '@app/shared/common/app-common.module';
import { UtilsModule } from '@shared/utils/utils.module';
import { CountoModule } from 'angular2-counto';
import { ModalModule } from 'ngx-bootstrap/modal';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { PopoverModule } from 'ngx-bootstrap/popover';
import { TrackerRoutingModule } from './tracker-routing.module';
import { BsDatepickerConfig, BsDaterangepickerConfig, BsLocaleService } from 'ngx-bootstrap/datepicker';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { NgxBootstrapDatePickerConfigService } from 'assets/ngx-bootstrap/ngx-bootstrap-datepicker-config.service';
import { SubheaderModule } from '@app/shared/common/sub-header/subheader.module';

import { CustomizableDashboardModule } from '@app/shared/common/customizable-dashboard/customizable-dashboard.module';

import { AgmCoreModule } from '@agm/core';
import { AdminSharedModule } from '@app/admin/shared/admin-shared.module';
import { AppSharedModule } from '@app/shared/app-shared.module';
import { EmailEditorModule } from 'angular-email-editor';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { PaginatorModule } from 'primeng/paginator';
import { EditorModule } from 'primeng/editor';
import { InputMaskModule } from 'primeng/inputmask'; import { FileUploadModule } from 'primeng/fileupload';
import { TableModule } from 'primeng/table';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';

import { applicationListDetailModal } from './application/application-list-detail-modal.component';
import { ApplicationListComponent } from './application/application-list.componet';
import { EstimateListComponent } from './estimate/estimate-list.componet';
import { estimateListDetailModal } from './estimate/estimate-list-detail-modal.component';
import { paymentRecieptModal } from './estimate/payment-reciept-modal.component';
import { applicationReadyStatusModal } from './application/application-ready-status-modal.component';
import { EditapplicationDetailModal } from './application/edit-application-detail-modal.component';
import { MeterConnectComponent } from './meter/meter-connect.componet';
import { SerialNoPhotosComponent } from '../installation/serial-photos/serial-photos.componet';

import { UploadSignApplication } from './application/UploadSignApplication-modal.component';
import { loadReduceDetailModal } from './application/load-reduce-modal.component';

import { LeadsModule } from './../leads/leads.module';
import { ViewMyLeadComponent } from '../leads/customerleads/view-mylead.component';
import { RejectLeadModelComponent } from '../leads/rejectleadmodal/reject-lead-modal.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { RefundTrackerComponent } from './refundtracker/refund-tracker.component';
import { RefundVerifyComponent } from './refundtracker/refund-verify/refund-verify.component';
import { PickListTrackerComponent } from './picklist/picklist-tracker.component';
import { PickListGenerateComponent } from './picklist/pick-list-generate/pick-list-generate.component';
import { PickListEditComponent } from './picklist/pick-list-edit/pick-list-edit.component';
import { InstalltionTrackerComponent } from './installtion-tracker/installtion-tracker';
import { DepositTrackerComponent } from './deposit-tracker/deposit-tracker.component';
import { TransportTrackerComponent } from './transport-tracker/transport-tracker.component';
import { InstallationListDetailModal } from './installtion-tracker/installtion-tracker-detail-modal.component';
import { TransportTrackerDetailModal } from './transport-tracker/transport-tracker-detail-modal.component';
import { ExtraMaterialModal } from './transport-tracker/extra-material-modal.component';
import { ReturnMaterialModal } from './transport-tracker/return-material-modal.component';
import { RevertDispatchModal } from './transport-tracker/revert-dispatch-modal.component';
import { MeterAppStatusModal } from './meter/meter-app-status-modal.component';
import { PaymentBillTemplateModalComponent } from '../accounts/payment-Bill/payment-bill-template.component';
import { SelfCertificateTemplateModalComponent } from './meter/SelfCertificatel-template.component';

NgxBootstrapDatePickerConfigService.registerNgxBootstrapDatePickerLocales();

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ModalModule,
        TabsModule,
        TooltipModule,
        AppCommonModule,
        UtilsModule,
        CountoModule,
        BsDatepickerModule.forRoot(),
        BsDropdownModule.forRoot(),
        PopoverModule.forRoot(),
        SubheaderModule,
        AdminSharedModule,
        AppSharedModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyALoDvKqgmTfpfPOEjqS2fI1kcabuxImMA',
            libraries: ['places']
        }),
        EmailEditorModule,
        AutoCompleteModule,
        PaginatorModule,
        EditorModule,
        InputMaskModule,
        TableModule,
        OwlDateTimeModule,
        OwlNativeDateTimeModule,
        TrackerRoutingModule,
        NgSelectModule,
        LeadsModule,
        //BrowserModule,
        //BrowserAnimationsModule        
    ],
    declarations: [ApplicationListComponent, applicationListDetailModal, EstimateListComponent, estimateListDetailModal, paymentRecieptModal, InstallationListDetailModal,
        TransportTrackerDetailModal, ExtraMaterialModal, ReturnMaterialModal, MeterAppStatusModal, RevertDispatchModal, applicationReadyStatusModal, EditapplicationDetailModal, MeterConnectComponent, SerialNoPhotosComponent,
        UploadSignApplication, loadReduceDetailModal, RefundTrackerComponent, RefundVerifyComponent, PickListTrackerComponent,
        InstalltionTrackerComponent, DepositTrackerComponent, TransportTrackerComponent,SelfCertificateTemplateModalComponent
    ],
    exports: [LeadsModule],
    schemas: [NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA],
    providers: [
        { provide: BsDatepickerConfig, useFactory: NgxBootstrapDatePickerConfigService.getDatepickerConfig },
        { provide: BsDaterangepickerConfig, useFactory: NgxBootstrapDatePickerConfigService.getDaterangepickerConfig },
        { provide: BsLocaleService, useFactory: NgxBootstrapDatePickerConfigService.getDatepickerLocale },
        { provide: DatePipe }
    ],
})
export class TrackerModule { }
