import { OnInit, Component, Injector, ViewEncapsulation, ViewChild, Input, Output, EventEmitter, HostListener } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ActivatedRoute, Router } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { LazyLoadEvent } from 'primeng/api';
import { CommonLookupServiceProxy, CommonLookupDto, UserListDto, OrganizationUnitDto, MeterConnectTrackerSummaryCount } from '@shared/service-proxies/service-proxies';
import { Paginator } from 'primeng/paginator';
import * as _ from 'lodash';
import { DataTransferService } from '@app/shared/common/projectservice/data-transfer.service';
//import { dispatchModal } from './dispatch-action-modal.component';
import { MeterAppStatusModal } from './meter-app-status-modal.component';
import { DateTime } from 'luxon';
import { DateTimeService } from '@app/shared/common/timing/date-time.service';
import { Table } from 'primeng/table';
import { MeterConnectTrackerServiceProxy } from '@shared/service-proxies/service-proxies';
import { ViewMyLeadComponent } from '@app/main/leads/customerleads/view-mylead.component';
import { AddActivityModalComponent } from '@app/main/leads/leadactivity/add-activity-model.component';
import { AppConsts } from '@shared/AppConsts';
import { HttpClient } from '@angular/common/http';
import { SelfCertificateTemplateModalComponent } from './SelfCertificatel-template.component';
import { FileDownloadService } from '@shared/utils/file-download.service';

@Component({
    selector: 'meter-connect',
    templateUrl: './meter-connect.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class MeterConnectComponent extends AppComponentBase implements OnInit {
    @ViewChild('MeterAppStatusModal', { static: true }) MeterAppStatusModal: MeterAppStatusModal;
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    @ViewChild('viewLeadDetail', { static: true }) viewLeadDetail: ViewMyLeadComponent;
    @ViewChild('addActivityModal', { static: true }) addActivityModal: AddActivityModalComponent;
    @ViewChild('selfCertificateTemplateModel', { static: true }) selfCertificateTemplateModel: SelfCertificateTemplateModalComponent;

    @Input() SectionName: string = "MeterConnectTracker";
    @Output() reloadLead = new EventEmitter();

    advancedFiltersAreShown = false;
    FiltersData = false;
    active = false;
    saving = false;
    recordCount: any;
    leadId: any;
    filterText: string = '';
    ExpandedView: boolean = true;
    leadName: string;
    flag = true;
    PageNo: number;
    allOrganizationUnits: OrganizationUnitDto[];
    allOrganizationUnitsList: OrganizationUnitDto[];
    organizationUnitlength: number = 0;
    organizationUnit = 0;
    billtypefilter: string = '';
    applicationstatus: number = 0;
    leadstatuss: CommonLookupDto[];
    allVehicalType: CommonLookupDto[];
    allapplicationStatus: CommonLookupDto[];
    allSolarType: CommonLookupDto[];
    allEmployeeList: UserListDto[];
    allDiscom: CommonLookupDto[];
    allDivision: CommonLookupDto[];
    allSubDivision: CommonLookupDto[];
    allTenderData: CommonLookupDto[];
    allCircleData: CommonLookupDto[];
    //filter variable
    leadStatusIDS = [];
    leadSourceIdFilter = [];
    leadApplicationStatusFilter = [];
    applicationstatusTrackerFilter: number;
    actiondUrl: string = '';
    employeeIdFilter = [];
    DiscomIdFilter = [];
    CircleIdFilter = [];
    DivisionIdFilter = [];
    SubDivisionIdFilter = [];
    solarTypeIdFilter = [];
    TenderIdFilter = [];
    FilterbyDate = '';
    date: Date = AppConsts.defaultDate;
    StartDate: DateTime = this._dateTimeService.getStartOfDayForDate(this.date);
    EndDate: DateTime = this._dateTimeService.getDate();
    sectionId: number = 37;
    @Input() SelectedLeadId: number = 0;
    meteapplicationReadyFilter: number = 0;
    shouldShow: boolean = false;
    public screenWidth: any;
    public screenHeight: any;
    testHeight = 280;
    contentHeight = 230;
    show1: boolean = true;
    toggle: boolean = true;
    @HostListener('window:resize', ['$event'])
    meterConnectTrackerSummaryCount: MeterConnectTrackerSummaryCount;
    toggleBlock() {
        this.show1 = !this.show1;
    };
    change() {
        this.toggle = !this.toggle;
    }

    constructor(
        injector: Injector,
        private _meterConnectServiceProxy: MeterConnectTrackerServiceProxy,
        private _commonLookupService: CommonLookupServiceProxy,
        private _fileDownloadService: FileDownloadService,
        private _dateTimeService: DateTimeService,
        private _dataTransfer: DataTransferService,
        private _httpClient: HttpClient,
    ) {
        super(injector);

    }

    ngOnInit(): void {
        this.meterConnectTrackerSummaryCount = new MeterConnectTrackerSummaryCount();
        this.screenHeight = window.innerHeight;
        this._commonLookupService.getAllApplicationStatusForDropdown().subscribe(result => {
            this.allapplicationStatus = result;
        }, error => { this.spinnerService.hide() });
        this._commonLookupService.getAllLeadStatusDropdown().subscribe(result => {
            this.leadstatuss = result;
        }, error => { this.spinnerService.hide() });
        this._commonLookupService.getAllDiscomFilterDropdown(0).subscribe(result => {
            this.allDiscom = result;
        }, error => { this.spinnerService.hide() });

        this._commonLookupService.getAllSubdivisionWiseData('').subscribe(result => {
            this.allSubDivision = result;
        }, error => { this.spinnerService.hide() });
        this._commonLookupService.getAllSolarTypeForDropdown().subscribe(result => {
            this.allSolarType = result;
        }, error => { this.spinnerService.hide() });
        this._commonLookupService.getAllUserList().subscribe(result => {
            this.allEmployeeList = result;
        }, error => { this.spinnerService.hide() });
        this._commonLookupService.getOrganizationUnitUserWise().subscribe(output => {
            this.allOrganizationUnits = output;
            this.organizationUnit = this.allOrganizationUnits[0].id;
            this.organizationUnitlength = this.allOrganizationUnits.length;
            //change Organization
            this.allOrganizationUnitsList = this.allOrganizationUnits.filter(item => item.id != this.organizationUnit);
            this.getMeterConnectList();
        }, error => { this.spinnerService.hide() })
    }


    onResize(event) {
        this.screenHeight = window.innerHeight;
    }
    testHeightSize() {
        if (this.FiltersData == true) {
            this.testHeight = this.testHeight + -82;
        }
        else {
            this.testHeight = this.testHeight - -82;
        }
    }

    expandGrid() {
        this.ExpandedView = true;
    }

    getMeterConnectList(event?: LazyLoadEvent) {
        this._dataTransfer.setOrgData(this.organizationUnit.toString());
        this.PageNo = this.primengTableHelper.getSkipCount(this.paginator, event);
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            //return;
        }
        this.primengTableHelper.showLoadingIndicator();

        //this.bindDocumentFilter.filter = this.leadId
        this._meterConnectServiceProxy.getAllMeterConnectTracker(
            this.filterText,
            this.organizationUnit,
            this.leadSourceIdFilter,
            this.employeeIdFilter,
            this.leadApplicationStatusFilter,
            this.applicationstatusTrackerFilter,
            this.meteapplicationReadyFilter,
            this.DiscomIdFilter,
            undefined,
            undefined,
            undefined,
            this.solarTypeIdFilter,
            this.TenderIdFilter,
            this.FilterbyDate,
            this.StartDate,
            this.EndDate,
            undefined,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe((result) => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            this.primengTableHelper.hideLoadingIndicator();
            this.recordCount = result.totalCount;
            this.meterConnectTrackerSummaryCount = result.items[0].meterConnectTrackerSummaryCount;
            this.shouldShow = false;
            if (this.flag == false && result.totalCount != 0 && this.ExpandedView == false) {
                this.navigateToLeadDetail(this.SelectedLeadId)
            }
            else {
                if (this.ExpandedView == false && result.totalCount != 0) {
                    this.navigateToLeadDetail(result.items[0].meterconnectTracker.leaddata.id)
                }
                else {
                    this.ExpandedView = true;
                }
            }
        }, error => { this.spinnerService.hide() });
    }

    navigateToLeadDetail(leaddata): void {
        this.ExpandedView = !this.ExpandedView;
        this.ExpandedView = false;
        this.SelectedLeadId = leaddata.meterconnectTracker.leaddata.id;
        this.leadName = "MeterConnectTracker"
        this.viewLeadDetail.showDetail(leaddata.meterconnectTracker.leaddata, leaddata.meterconnectTracker.applicationStatusdata, this.leadName, this.sectionId);
    }
    reloadPage(flafValue: boolean): void {
        this.ExpandedView = true;
        this.flag = flafValue;
        this.paginator.changePage(this.paginator.getPage());
        if (!flafValue) {
            this.navigateToLeadDetail(this.SelectedLeadId);
        }
        this.paginator.changePage(this.paginator.getPage());
    }
    exportToExcel(): void {
        this._meterConnectServiceProxy.getApplicationTrackerToExcel(
            this.filterText,
            this.organizationUnit,
            this.leadSourceIdFilter,
            this.employeeIdFilter,
            this.leadApplicationStatusFilter,
            this.applicationstatusTrackerFilter,
            this.meteapplicationReadyFilter,
            this.DiscomIdFilter,
            undefined,
            undefined,
            undefined,
            this.solarTypeIdFilter,
            this.TenderIdFilter,
            this.FilterbyDate,
            this.StartDate,
            this.EndDate,
            undefined,
            this.primengTableHelper.getSorting(this.dataTable),
            undefined,
            undefined
        ).subscribe((result) => {
            this._fileDownloadService.downloadTempFile(result);
        });
    }
    clear() {
        this.filterText = '';
        this.leadStatusIDS = [];
        this.leadApplicationStatusFilter = [];
        this.billtypefilter = '';
        this.allCircleData = [];
        this.DiscomIdFilter = [];
        this.solarTypeIdFilter = [];
        this.TenderIdFilter = [];
        this.employeeIdFilter = [];
        this.FilterbyDate = '';
        this.meteapplicationReadyFilter = 0;
        this.StartDate = this._dateTimeService.getStartOfDayForDate(this.date);
        this.EndDate = this._dateTimeService.getDate();
        this.getMeterConnectList();
    }
    changeDiscom() {
        if (this.DiscomIdFilter.length > 0) {
            this._commonLookupService.getDiscomWiseCircleFilterForDropdown(this.DiscomIdFilter).subscribe(result => {
                this.allCircleData = result;
                if (this.allCircleData.length == 0) {
                    this.allCircleData = undefined;
                }
            }, error => { this.spinnerService.hide() });
        }
        else {
            this.allCircleData = [];
            this.CircleIdFilter = [];
            this.allDivision = [];
            this.DivisionIdFilter = [];
            this.allSubDivision = [];
            this.SubDivisionIdFilter = [];
        }
    }
    changeCircle() {
        if (this.CircleIdFilter.length > 0) {
            this._commonLookupService.getCircleWiseDivisionFilterForDropdown(this.CircleIdFilter).subscribe(result => {
                this.allDivision = result;
                if (this.allDivision.length == 0) {
                    this.allDivision = undefined
                }
            }, error => { this.spinnerService.hide() });
        }
        else {
            this.allDivision = [];
            this.DivisionIdFilter = [];
            this.allSubDivision = [];
            this.SubDivisionIdFilter = [];
        }
    }
    changeDivision() {
        if (this.CircleIdFilter.length > 0) {
            this._commonLookupService.getDivisionWiseSubDivisionFilterForDropdown(this.DivisionIdFilter).subscribe(result => {
                this.allSubDivision = result;
                if (this.allSubDivision.length == 0) {
                    this.allSubDivision = undefined
                }
            }, error => { this.spinnerService.hide() });
        }
        else {
            this.allSubDivision = [];
            this.SubDivisionIdFilter = [];
        }
    }
    leadstatuschange() {
        if (this.leadStatusIDS.includes(6)) {
            //this.showJobStatus = true;
        } else {
            //this.showJobStatus = false;
            //this.jobStatusID = [];
        }
    }
    changeSolarType() {
        this.allTenderData = undefined;
        this._commonLookupService.getSolarTypeWiseTenderForDropdown(this.solarTypeIdFilter).subscribe(result => {
            this.allTenderData = result;
            if (this.allTenderData.length == 0) {
                this.allTenderData = undefined;
            }
        }, error => { this.spinnerService.hide() });
    }
    
   
    public DownloadDocument(leadid) {
        var orgName = this._dataTransfer.getOrgData();
        this.actiondUrl = AppConsts.remoteServiceBaseUrl; //+ '/LeadDocumentUpload/DownloadFiles?LeadId=' + leadid + '&OrgName=' + orgName ;
        return this._httpClient.get(`${this.actiondUrl + '/LeadDocumentUpload/DownloadFiles?LeadId=' + leadid + '&OrgName=' + orgName}`, {
            responseType: 'blob' as 'json'
        }
        ).subscribe(response => this.downLoadFile(response, "application/zip"));
    }
    downLoadFile(data: any, type: string) {
        let blob = new Blob([data], { type: type });
        let url = window.URL.createObjectURL(blob);
        let pwa = window.open(url);
        if (!pwa || pwa.closed || typeof pwa.closed == 'undefined') {
            alert('Please disable your Pop-up blocker and try again.');
        }
    }
    close() {
        //this.modal.hide();
    }
}
