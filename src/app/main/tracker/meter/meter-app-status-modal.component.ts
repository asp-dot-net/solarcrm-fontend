import { Component, Injector, ViewChild } from '@angular/core';
import { DateTimeService } from '@app/shared/common/timing/date-time.service';
import { AppComponentBase } from '@shared/common/app-component-base';
import { CreateOrEditJobDto, CreateOrEditMeterConnectDto, EstimatedTrackerQuickViewDto, GetMeterConnectDetailsForEditOutput, MeterConnectTrackerServiceProxy } from '@shared/service-proxies/service-proxies';
import { DateTime } from 'luxon';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { EstimatedPaidTrackerServiceProxy } from '@shared/service-proxies/service-proxies';
import { parse } from 'path';
import { DataTransferService } from '@app/shared/common/projectservice/data-transfer.service';
import { AppConsts } from '@shared/AppConsts';
@Component({
    selector: 'MeterAppStatusModal',
    templateUrl: './meter-app-status-modal.component.html',
})
export class MeterAppStatusModal extends AppComponentBase {
    @ViewChild('MeterAppStatusModal', { static: true }) modal: ModalDirective;
    QuickViewData: GetMeterConnectDetailsForEditOutput;
    ExpandedViewApp: boolean = true;
    active = false;
    customertitle: string = '';
    meterConnect: CreateOrEditMeterConnectDto = new CreateOrEditMeterConnectDto();
    jobid: number = 0;
    meterSubmittedDate: DateTime = this._dateTimeService.getDate();
    agreementsentdDate: DateTime = this._dateTimeService.getDate();
    //@Output() reloadLead = new EventEmitter<boolean>();
    constructor(injector: Injector, private _dateTimeService: DateTimeService,
        private _meterConnectTrackerServiceProxy: MeterConnectTrackerServiceProxy,
        private _dataTransfer: DataTransferService
    ) {
        super(injector);
    }

    ngOnInit(): void {
        this.meterConnect = new CreateOrEditMeterConnectDto()
        //this.QuickViewData = new EstimatedTrackerQuickViewDto;  
    }

    isMeterStatus: boolean;
    chnagestatus(event) {
        if (event.target.value == AppConsts.meterApplicationReadyStatus) {
            this.isMeterStatus = true
        }
        else {
            this.isMeterStatus = false
        }
    }

    GetMeterConnectView(meterconnectdata) {
        this.spinnerService.show();
        debugger
        this.meterConnect = new CreateOrEditMeterConnectDto();        
        this.meterConnect.installationId = meterconnectdata.installationId
        this.meterConnect.leadid = meterconnectdata.leaddata.id;
        this.meterConnect.orgID = parseInt(this._dataTransfer.getOrgData());
        this.customertitle = meterconnectdata.customerName + '(' + meterconnectdata.projectNumber + ')';
        this._meterConnectTrackerServiceProxy.getMeterConnectForEdit(meterconnectdata.installationId).subscribe((result) => {
            this.modal.show();
            if (result.meterConnectDto != undefined) {
                this.meterConnect = result.meterConnectDto;
                this.agreementsentdDate = this.meterConnect.meterAgreementSentDate;
                this.meterSubmittedDate = this.meterConnect.meterSubmissionDate;
                this.meterConnect.installationId = this.meterConnect.installationId
                this.meterConnect.leadid = meterconnectdata.leaddata.id;
                this.meterConnect.orgID = parseInt(this._dataTransfer.getOrgData());
                if (this.meterConnect.meterApplicationReadyStatus == AppConsts.meterApplicationReadyStatus) {
                    this.isMeterStatus = true;
                }
            }
            this.spinnerService.hide();
        }, error => { this.spinnerService.hide() });
    }
    saving = true;
    submit() {
        this.saving = true;
        if (this.saving) {
            //let meterConnectAddorEdit:  CreateOrEditMeterConnectDto = new CreateOrEditMeterConnectDto();
            //meterConnectAddorEdit.id =  this.QuickViewData.meterConnectDto.id;
            //meterConnectAddorEdit.installationId = this.QuickViewData.meterConnectDto.installationId;
            //meterConnectAddorEdit.meterAgreementGivenTo = this.QuickViewData.meterConnectDto.meterAgreementGivenTo;
            //meterConnectAddorEdit.meterAgreementNo = this.QuickViewData.meterConnectDto.meterAgreementNo;
            this.meterConnect.meterAgreementSentDate = this.agreementsentdDate;// this.QuickViewData.meterConnectDto.meterAgreementSentDate;
            //meterConnectAddorEdit.meterApplicationReadyStatus = this.QuickViewData.meterConnectDto.meterApplicationReadyStatus;
            this.meterConnect.meterSubmissionDate = this.meterSubmittedDate;// this.QuickViewData.meterConnectDto.meterSubmissionDate;

            this._meterConnectTrackerServiceProxy.createOrEdit(this.meterConnect)
                .pipe(finalize(() => { this.saving = false; }))
                .subscribe(() => {
                    this.saving = false;
                    this.modal.hide();
                    document.getElementById("btnRefrsh").click();
                    this.notify.info(this.l('Added Meter Connect Status Successfully'));
                }, error => { this.spinnerService.hide() });
        }
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }

    navigateToAppDetail(): void {
        this.ExpandedViewApp = !this.ExpandedViewApp;
        this.ExpandedViewApp = true;
    }
    expandApp() {
        this.ExpandedViewApp = false;
    }

}
