import { Component, Inject, Renderer2, ElementRef, EventEmitter, Injectable, Injector, OnInit, Optional, Output, ViewChild, Input } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
//import { LeadsServiceProxy, GetLeadForViewDto, LeadDto, ActivityLogInput, CommonLookupDto, EmailTemplateServiceProxy, SmsTemplatesServiceProxy, JobsServiceProxy, JobPromotionsServiceProxy, JobPromotionPromotionMasterLookupTableDto } from '@shared/service-proxies/service-proxies';
import { GetLeadsForViewDto, CommonLookupDto, UserListDto, LeadsDto, LeadsServiceProxy, CommonLookupServiceProxy, ActivityLogInput, LeadsActivityLogServiceProxy, PdfTemplateServiceProxy, QuotationsServiceProxy, QuotationDataDetailsDto, PaymentIssueServiceProxy, BillPaymentDto, MeterConnectTrackerServiceProxy, SelfCertificateDetailDto } from '@shared/service-proxies/service-proxies';
import * as _ from 'lodash';
import { DatePipe, DOCUMENT } from '@angular/common';
import * as moment from 'moment';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { Router } from '@angular/router';
import { EmailEditorComponent } from 'angular-email-editor';
import { AppConsts } from '@shared/AppConsts';
import { DEFAULTS, NgxSpinnerService } from 'ngx-spinner';
//import { event } from 'jquery';
//import {view}
declare var Quill: any;
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from "@angular/platform-browser";
import { DateTimeService } from '@app/shared/common/timing/date-time.service';
import { AddActivityModalComponent } from '@app/main/leads/leadactivity/add-activity-model.component';
import { NumberToWordsPipe } from '@shared/utils/number-to-words.pipe';
import { DataTransferService } from '@app/shared/common/projectservice/data-transfer.service';
@Component({
    selector: 'selfCertificateTemplateModel',
    templateUrl: './SelfCertificate-template.component.html'
})

export class SelfCertificateTemplateModalComponent extends AppComponentBase implements OnInit {
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @Output() reloadLead = new EventEmitter<boolean>();
    @Input() SelectedJobNumber: string = '';
    @ViewChild("myNameElem") myNameElem: ElementRef;
    @ViewChild("area_select") area_select: ElementRef;
    @ViewChild("modal_body") modal_body: ElementRef;
    @ViewChild('selfCertificateTemplateModel') public modal: ModalDirective;
    @ViewChild('btnEmail', { static: true }) btnEmail: ElementRef;
    @ViewChild('btnsmsnotify', { static: true }) btnsmsnotify: ElementRef;
    @ViewChild('SampleDatePicker', { static: true }) sampleDatePicker: ElementRef;
    @ViewChild('addActivityModal', { static: true }) addActivityModal: AddActivityModalComponent;
    @ViewChild(EmailEditorComponent)
    private emailEditor: EmailEditorComponent;
    saving = false;
    //sampleDate: moment.Moment = moment().add(1, 'days').startOf('day');
    item: SelfCertificateDetailDto;
    activityLog: ActivityLogInput = new ActivityLogInput();
    activityType: number = 6;
    priorityType: number = 1;
    activityName = "";
    actionNote = "";
    id = 0;
    actionId: number = 12;
    sectionId: number = 35;
    allEmailTemplates: CommonLookupDto[];
    allSMSTemplates: CommonLookupDto[];
    drpleadactivity: CommonLookupDto[];
    emailData = '';
    smsData = '';
    emailTemplate: number;
    uploadUrl: string;
    uploadedFiles: any[] = [];
    myDate = new Date();
    total = 0;
    credit = 0;
    customeTagsId = 0;
    role: string = '';
    subject = '';
    userName: string;
    leadCompanyName: any;
    pageid = 0;
    allUsers: UserListDto[];
    referanceId = 0;
    CurrentDate = new Date();
    emailfrom: string = '';
    emailto: string = '';
    fromemails: CommonLookupDto[];
    leadid: number;
    criteriaavaibaleornot = false;
    smsid = false;
    htmlstr: string = '';
    htmlContentbidy: any;
    quoteNumber: string = '';
    constructor(
        injector: Injector, private _http: HttpClient,
        private _quoteServiceProxy: QuotationsServiceProxy,
        private _commonLookupServiceProxy: CommonLookupServiceProxy,
        private _pdfTemplateServiceProxy: PdfTemplateServiceProxy,
        private _meterConnectServiceProxy: MeterConnectTrackerServiceProxy,
        @Inject(DOCUMENT) private document: Document,
        private spinner: NgxSpinnerService,
        private _dataTransfer: DataTransferService,
        private sanitizer: DomSanitizer,
        //private numtoword : NumberToWordsPipe,
        private _dateTimeService: DateTimeService,
    ) {
        super(injector);
        this.item = new SelfCertificateDetailDto();
        //this.item.leads = new LeadsDto();       
    }
    ngOnInit(): void {

    }

    show(leaddata): void {
        this.spinner.show();
        
        // let ttt = this.numtoword.transform(55);
        this._meterConnectServiceProxy.selfCertficateDetails(parseInt(this._dataTransfer.getOrgData()), leaddata.leaddata).subscribe(result => {
            this.item = result;
            debugger
            if (leaddata.discom != null){
                let discom = leaddata.discom.includes("Torrent");
                let tempid;
                if (discom) {
                    tempid = AppConsts.pdfGenerateDocument.SelfCertificateTorrent
                }
                else {
                    tempid = AppConsts.pdfGenerateDocument.SelfCertificateDiscom
                }
                this._pdfTemplateServiceProxy.getPdfTemplateForView(tempid, parseInt(this._dataTransfer.getOrgData())).subscribe(result => {
                    if (result.pdfTemplates != undefined) {
                        this.htmlContentbidy = result.pdfTemplates.viewHtml;
                        this.setHTML(this.htmlContentbidy);
                        this.spinner.hide();
                    } else {
                        this.spinner.hide();
                        this.notify.info("Please First Create Self Certificate Template");
                    }
                }, error => { this.spinner.hide(); });
            }
            else{
                this.notify.info("Please First Discom Selected ?");
            }
           
        });
    }

    close(): void {
        this.modal.hide();
    }
    teststr: string = '';
    setHTML(pdfHTML) {
        let pdfTemplate = this.getPdfTemplate(pdfHTML);
        this.teststr = pdfHTML;
        this.htmlContentbidy = pdfTemplate;
        this.htmlContentbidy = this.sanitizer.bypassSecurityTrustHtml(this.htmlContentbidy);

        if (pdfHTML != '') {
            this.modal.show();
        }

    }

    getPdfTemplate(smsHTML) {
        let myTemplateStr = smsHTML;
        let TableList = [];
        let BankList = [];

        let QrcodeImg;
        // if (this.item.organizationbankDetails != null) {
        //     this.item.organizationbankDetails.forEach(obj => {
        //         QrcodeImg = '';
        //         BankList.push("<div class='payment-panel'><div class='pay-content'>Bank : " + obj.orgBankName + "</div><div class='pay-content'>A/C Name : " + obj.orgBankBrachName + "</div><div class='pay-content'>A/C No. : " + obj.orgBankAccountNumber + "</div><div class='pay-content'>IFSC : " + obj.orgBankIFSC_Code + "</div></div>");
        //         QrcodeImg = AppConsts.remoteServiceBaseUrl + obj.orgBankQrCode_path;
        //     });
        //     BankList.push("<div class='payment-panel-code'><img src='" + QrcodeImg + "'></div>");
        // }
        // let addressValue = this.item.leaddata.address; // + " " + this.item.address2; //+ ", " + this.item.lead.state + "-" + this.item.lead.postCode;
        // // let signdate = this._dateTimeService.getStartOfDayForDate(this.item.quotateSignedDate)
        // // let generatedate = this.item.quoteGenerateDate;
        let currentdate = this._dateTimeService.getUTCDate().day + '/' + this._dateTimeService.getUTCDate().month + '/' + this._dateTimeService.getUTCDate().year
        let myVariables = {
            Customer: {
                Consumeno: this.item.consumerNumber,
                Discomname: this.item.discomName,
                Certificatedate: this.item.certificateDate,
                Agencyname: this.item.agencyName,
                GUVNLregnumber: this.item.guvnLnumber,
                Customername: this.item.customername,
                Address1: this.item.customeraddress1,
                Address2: this.item.customeraddress2,
                Circlename: this.item.circleName,
                Divisionname: this.item.divisionName,
                Subdivision: this.item.subDivisionName,
                Contractload: this.item.contractedLoadKW,
                Contractphase: this.item.contractPhase

                //Mobile: this.item.leaddata.mobileNumber,
                //Email: this.item.leaddata.emailId,

            },
            PVmodule: {
                PVmodulemake: this.item.solarPVModuleMake,
                PVmodelno: this.item.pvModelNo,
                Typeofpvmodel: this.item.typeOfPVModule,
                PVratedcapacity: this.item.ratedCapacityofSolarModuleinwattmorethan250wp,
                PVnoofmodule: this.item.pvNoofModules,
                Totalpvcapacity: this.item.totalPVCapacityinstalledinKwp
            },
            InverterModule: {                
                IVmodulemake: this.item.makeOfInverter,
                IVmodelno: this.item.ivModelNo,
                Typeofivmodel: this.item.typeOfInverter,
                IVratedcapacity: this.item.ratedACoutputofInverterKiloWatt,
                IVserialno: this.item.serialNoofInverter,
            },

        }

        // use custom delimiter {{ }}
        _.templateSettings.interpolate = /{{([\s\S]+?)}}/g;

        // interpolate
        let compiled = _.template(myTemplateStr);
        let myTemplateCompiled = compiled(myVariables);
        return myTemplateCompiled;
    }



    generatePdf() {
        this.spinner.show();
        this.htmlstr = this.htmlContentbidy.changingThisBreaksApplicationSecurity; // this.teststr.toString().replace('"',"'");// this.htmlContentbidy.changingThisBreaksApplicationSecurity;
        this._quoteServiceProxy.generatePdf(this.SelectedJobNumber, this.htmlstr).subscribe(result => {
            let FileName = result;
            this.saving = false;
            this.spinner.hide();
            window.open(result, "_blank");
        }, error => { this.spinner.hide(); })
    }
}