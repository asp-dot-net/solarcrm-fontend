import { OnInit, Component, Injector, ViewEncapsulation, ViewChild, Input, Output, EventEmitter, AfterViewInit, HostListener } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ActivatedRoute, Router } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { LazyLoadEvent } from 'primeng/api';
import { LeadsServiceProxy, LeadsDto, CommonLookupServiceProxy, CommonLookupDto, UserServiceProxy } from '@shared/service-proxies/service-proxies';
import { Paginator } from 'primeng/paginator';
import { Title } from '@angular/platform-browser';
import * as _ from 'lodash';
import * as moment from 'moment';
import { DataTransferService } from '@app/shared/common/projectservice/data-transfer.service';
import { InstallationListDetailModal } from './installtion-tracker-detail-modal.component';

@Component({
    selector: 'installtiontracker',
    templateUrl: './installtion-tracker.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class InstalltionTrackerComponent extends AppComponentBase implements OnInit {
    //@ViewChild('dispatchModal', { static: true }) dispatchModal: dispatchModal;    
    @ViewChild('InstallationListDetailModal', { static: true }) InstallationListDetailModal: InstallationListDetailModal;

    @Output() reloadLead = new EventEmitter();

    advancedFiltersAreShown = false;
    FiltersData = false;
    active = false;
    saving = false;
    recordCount: any;
    leadId: any;
    filterText: string = '';
    StartDate: moment.Moment;
    EndDate: moment.Moment;
    sampleDateRange: moment.Moment[] = [moment().add(0, 'days').endOf('day'), moment().add(0, 'days').endOf('day')];
    ExpandedView: boolean = true;
    shouldShow: boolean = false;
    public screenWidth: any;  
    public screenHeight: any;  
    testHeight = 200;

    constructor(
        injector: Injector,
        private _leadsServiceProxy: LeadsServiceProxy,
        private _commonLookupService: CommonLookupServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private titleService: Title,
        private _router: Router,
        private _dataTransfer: DataTransferService,
    ) {
        super(injector);

    }
    show(leadId?: number, param?: number, sectionId?: number): void {

        this.leadId = leadId;
        // this._leadsServiceProxy.leadDocumentList(this.bindDocumentFilter).subscribe(result => {
        //     console.log(result);
        //     this.primengTableHelper.totalRecordsCount = result.totalCount;
        //     this.primengTableHelper.records = result.items;
        //     this.primengTableHelper.hideLoadingIndicator();
        //     this.recordCount = result.totalCount;
        //     //this.leadDocumentList = result;            
        // });
        this.getInstallationList();
    }
    getInstallationList(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            //this.paginator.changePage(0);
            //return;
        }
        this.primengTableHelper.showLoadingIndicator();

        //this.bindDocumentFilter.filter = this.leadId
        // this._leadsServiceProxy.leadDocumentList(this.bindDocumentFilter).subscribe((result) => {
        //     this.primengTableHelper.totalRecordsCount = result.totalCount;
        //     this.primengTableHelper.records = result.items;
        //     this.primengTableHelper.hideLoadingIndicator();
        //     this.recordCount = result.totalCount;
        // }
        //);
    }

    navigateToLeadDetail(leadid): void {

        this.ExpandedView = !this.ExpandedView;
        this.ExpandedView = false;
        // this.SelectedLeadId = leadid;
        // this.leadName = "myleads"
        // this.viewLeadDetail.showDetail(leadid, this.leadName, this.sectionId);
    }

    exportToExcel(): void {
        // this._leadsServiceProxy.getLeadsToExcel(
        //     this.filterText,
        // )
        //     .subscribe(result => {
        //         this._fileDownloadService.downloadTempFile(result);
        //     });
    }

    showDispatchModal(): void {
        this.InstallationListDetailModal.show();
    }

    showJobTypeDetails(): void {
        //this.estimateListDetailModal.show();
    }

    ngOnInit(): void {
        this.screenHeight = window.innerHeight;
    }

    @HostListener('window:resize', ['$event'])  

    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }  
    testHeightSize () {
        if (this.FiltersData == true) {
            this.testHeight = this.testHeight + 80 ;
        }
        else {
            this.testHeight = this.testHeight - 80 ;
        }
    }

    expandGrid() {
        this.ExpandedView = true;
    }

    close() {
        //this.modal.hide();
    }
}
