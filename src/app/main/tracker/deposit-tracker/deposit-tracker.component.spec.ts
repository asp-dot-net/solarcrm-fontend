import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DepositTrackerComponent } from './deposit-tracker.component';

describe('DepositTrackerComponent', () => {
  let component: DepositTrackerComponent;
  let fixture: ComponentFixture<DepositTrackerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DepositTrackerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DepositTrackerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
