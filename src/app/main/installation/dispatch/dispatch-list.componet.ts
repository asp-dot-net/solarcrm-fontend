import { OnInit, Component, ElementRef, Injector, ViewEncapsulation, ViewChild, Input, Output, EventEmitter, AfterViewInit, HostListener } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { LazyLoadEvent } from 'primeng/api';
import { CommonLookupServiceProxy, CommonLookupDto, OrganizationUnitDto, DispatchTrackerServiceProxy, UserListDto, DispatchTrackerSummaryCount } from '@shared/service-proxies/service-proxies';
import { Paginator } from 'primeng/paginator';
import * as _ from 'lodash';
import * as moment from 'moment';
import { dispatchModal } from './dispatch-action-modal.component';
import { SelectMaterialModal } from './select-materials-modal.component';
import { SendExtraMaterialModal } from './send-extra-materials-modal.component';
import { StockTransferModal } from './stock-transfer-modal.component';
import { VerifyModal } from './verify-picklist-modal.component';
import { ViewMyLeadComponent } from '../../leads/customerleads/view-mylead.component';
import { DateTime } from 'luxon';
import { DateTimeService } from '@app/shared/common/timing/date-time.service';
import { Table } from 'primeng/table';
import { AddActivityModalComponent } from '@app/main/leads/leadactivity/add-activity-model.component';
import { DataTransferService } from '@app/shared/common/projectservice/data-transfer.service';
import { TransportDetails } from './TransportDetails.component';
import { PickListEditComponent } from '@app/main/tracker/picklist/pick-list-edit/pick-list-edit.component';
import { PickListGenerateComponent } from '@app/main/tracker/picklist/pick-list-generate/pick-list-generate.component';
import { ExtraMaterialApprovedModal } from './extra-materials-approved-modal.component';
import { FileDownloadService } from '@shared/utils/file-download.service';
import { AppConsts } from '@shared/AppConsts';

@Component({
    selector: 'dispatch-list',
    templateUrl: './dispatch-list.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class DispatchListComponent extends AppComponentBase implements OnInit {
    @ViewChild('dispatchModal', { static: true }) dispatchModal: dispatchModal;
    @ViewChild('SelectMaterialModal', { static: true }) SelectMaterialModal: SelectMaterialModal;
    @ViewChild('SendExtraMaterialModal', { static: true }) SendExtraMaterialModal: SendExtraMaterialModal;
    @ViewChild('StockTransferModal', { static: true }) StockTransferModal: StockTransferModal;
    @ViewChild('VerifyModal', { static: true }) VerifyModal: VerifyModal;
    @ViewChild('TransportDetails', { static: true }) TransportDetails: TransportDetails;
    @ViewChild('picklistgenerate', { static: true }) picklistgenerate: PickListGenerateComponent;
    @ViewChild('picklistedit', { static: true }) picklistedit: PickListEditComponent;
    @ViewChild('viewLeadDetail', { static: true }) viewLeadDetail: ViewMyLeadComponent;
    @ViewChild('addActivityModal', { static: true }) addActivityModal: AddActivityModalComponent;
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    @ViewChild('ExtraMaterialApprovedModal', { static: true }) ExtraMaterialApprovedModal: ExtraMaterialApprovedModal;
    @Output() reloadLead = new EventEmitter();
    @Input() SelectedLeadId: number = 0;
    @Input() SectionName: string = "DispatchTracker";
    advancedFiltersAreShown = false;
    FiltersData = false;
    active = false;
    saving = false;
    recordCount: any;
    leadId: any;
    filterText: string = '';
    shouldShow: boolean = false;
    leadStatusId = 0;
    //sampleDateRange: moment.Moment[] = [moment().add(0, 'days').endOf('day'), moment().add(0, 'days').endOf('day')];
    ExpandedView: boolean = true;
    leadName: string;
    flag = true;
    PageNo: number;
    allOrganizationUnits: OrganizationUnitDto[];
    allOrganizationUnitsList: OrganizationUnitDto[];
    organizationUnitlength: number = 0;
    organizationUnit = 0;
    applicationstatus: number = 0;
    leadstatuss: CommonLookupDto[];
    allStates: CommonLookupDto[];
    allapplicationStatus: CommonLookupDto[];
    allLeadSources: CommonLookupDto[];
    allSolarType: CommonLookupDto[];
    allDiscom: CommonLookupDto[];
    allDivision: CommonLookupDto[];
    allSubDivision: CommonLookupDto[];
    allTenderData: CommonLookupDto[];
    allCircleData: CommonLookupDto[];
    allEmployeeList: UserListDto[];
    //filter variable
    priorityFilter = '';
    leadStatusIDS = [];
    leadSourceIdFilter = [];
    leadApplicationStatusFilter = [];
    applicationstatusTrackerFilter: [];

    estimatedPaidFilter = '';
    estimatedPayResonseFilter = '';
    DiscomIdFilter = [];
    CircleIdFilter = [];
    DivisionIdFilter = [];
    SubDivisionIdFilter = [];
    solarTypeIdFilter = [];
    TenderIdFilter = [];
    employeeIdFilter = [];
    FilterbyDate = '';
    date: Date = new Date('01/01/2022');
    StartDate: DateTime = this._dateTimeService.getStartOfDayForDate(this.date);
    EndDate: DateTime = this._dateTimeService.getDate();
    tableRecords: any;
    sectionId: number = AppConsts.SectionPage.DispatchTracker;
    sampleDateRange: moment.Moment[] = [moment().add(0, 'days').endOf('day'), moment().add(0, 'days').endOf('day')];
    firstrowcount = 0;
    last = 0;
    paymentModeIdFilter: [0];
    PaymentTypeIdFilter: [0];
    public screenWidth: any;
    public screenHeight: any;
    testHeight = 280;
    contentHeight = 230;
    dispatchSummaryCount: DispatchTrackerSummaryCount;
    @HostListener('window:resize', ['$event'])
    FiltersData1 = false;
    show1: boolean = true;
    toggle: boolean = true;
    toggleBlock() {
        this.show1 = !this.show1;
    };
    change() {
        this.toggle = !this.toggle;
    }
    onResize(event) {
        this.screenHeight = window.innerHeight;
    }
    testHeightSize() {
        if (this.FiltersData == true) {
            this.testHeight = this.testHeight + -80;
        }
        else {
            this.testHeight = this.testHeight - -80;
        }
    }
    testHeightSize1() {
        if (this.FiltersData1 == true) {
            this.testHeight = this.testHeight + 87;
        }
        else {
            this.testHeight = this.testHeight - 87;
        }
    }
    constructor(
        injector: Injector,
        private _dateTimeService: DateTimeService,
        private _commonLookupService: CommonLookupServiceProxy,
        private _dispatchTrackerService: DispatchTrackerServiceProxy,
        private _dataTransfer: DataTransferService,
        private _fileDownloadService: FileDownloadService,
    ) {
        super(injector);
        this.dispatchSummaryCount = new DispatchTrackerSummaryCount();
    }

    ngOnInit(): void {

        this.screenHeight = window.innerHeight;
        this._commonLookupService.getAllLeadStatusDropdown().subscribe(result => {
            this.leadstatuss = result;
        }, error => { this.spinnerService.hide() });
        this._commonLookupService.getAllApplicationStatusForDropdown().subscribe(result => {
            this.allapplicationStatus = result;
        }, error => { this.spinnerService.hide() });
        this._commonLookupService.getAllDiscomFilterDropdown(0).subscribe(result => {
            this.allDiscom = result;
        }, error => { this.spinnerService.hide() });
        this._commonLookupService.getAllSubdivisionWiseData('').subscribe(result => {
            this.allSubDivision = result;
        }, error => { this.spinnerService.hide() });
        this._commonLookupService.getAllSolarTypeForDropdown().subscribe(result => {
            this.allSolarType = result;
        });
        this._commonLookupService.getAllUserList().subscribe(result => {
            this.allEmployeeList = result;
        }, error => { this.spinnerService.hide() });
        this._commonLookupService.getOrganizationUnitUserWise().subscribe(output => {
            this.allOrganizationUnits = output;
            this.organizationUnit = this.allOrganizationUnits[0].id;
            this.organizationUnitlength = this.allOrganizationUnits.length;

            //change Organization
            this.allOrganizationUnitsList = this.allOrganizationUnits.filter(item => item.id != this.organizationUnit);
            this.getDispatchList();
        }, error => { this.spinnerService.hide() })

    }

    OpenTransportDetail() {
        if (this.count > 0) {
            this.TransportDetails.addoreditTransport(this.tableRecords);
        }
        else {
            this.notify.info(this.l('Please Select Any One Row'));
        }
    }
    getDispatchList(event?: LazyLoadEvent) {
        this.dispatchSummaryCount = new DispatchTrackerSummaryCount();

        this._dataTransfer.setOrgData(this.organizationUnit.toString())
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            //return;
        }
        this.primengTableHelper.showLoadingIndicator();

        //this.bindDocumentFilter.filter = this.leadId
        this._dispatchTrackerService.getAllDispatch(
            this.filterText,
            this.organizationUnit,
            this.leadStatusIDS,
            this.leadApplicationStatusFilter,
            this.employeeIdFilter,
            this.DiscomIdFilter,
            this.CircleIdFilter,
            this.DivisionIdFilter,
            this.SubDivisionIdFilter,
            this.solarTypeIdFilter,
            '',
            this.paymentModeIdFilter,
            this.PaymentTypeIdFilter,
            0,
            0,
            // this.TenderIdFilter,
            // this.employeeIdFilter,
            this.FilterbyDate,
            this.StartDate,
            this.EndDate,
            undefined,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)

        ).subscribe((result) => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            this.primengTableHelper.hideLoadingIndicator();
            this.recordCount = result.totalCount;
            this.dispatchSummaryCount = result.items[0].dispatchTrackerSummaryCount;
            this.shouldShow = false;
            const totalrows = this.primengTableHelper.getSkipCount(this.paginator, event);
            this.firstrowcount = totalrows + 1;
            this.last = totalrows + result.items.length;
            this.tableRecords = result.items;
            if (this.flag == false && result.totalCount != 0 && this.ExpandedView == false) {
                this.navigateToLeadDetail(this.SelectedLeadId)
            }
            else {
                if (this.ExpandedView == false && result.totalCount != 0) {
                    this.navigateToLeadDetail(result.items[0].dispatchItem.leaddata.id)
                }
                else {
                    this.ExpandedView = true;
                }
            }
        }, error => { this.spinnerService.hide() });
    }
    show(leadId?: number, param?: number, sectionId?: number): void {

        this.leadId = leadId;
        // this._leadsServiceProxy.leadDocumentList(this.bindDocumentFilter).subscribe(result => {
        //     console.log(result);
        //     this.primengTableHelper.totalRecordsCount = result.totalCount;
        //     this.primengTableHelper.records = result.items;
        //     this.primengTableHelper.hideLoadingIndicator();
        //     this.recordCount = result.totalCount;
        //     //this.leadDocumentList = result;            
        // });
        this.getDispatchList();
    }

    navigateToLeadDetail(dispatchdata): void {

        this.ExpandedView = !this.ExpandedView;
        this.ExpandedView = false;
        this.SelectedLeadId = dispatchdata.dispatchItem.leaddata.id;
        this.leadName = "dispatchTracker"
        this.viewLeadDetail.showDetail(dispatchdata.dispatchItem.leaddata, dispatchdata.dispatchItem.applicationStatusdata, this.leadName, this.sectionId);
    }
    count: number;
    oncheckboxCheck() {
        this.count = 0;
        this.tableRecords.forEach(item => {
            if (item.dispatchItem.isSelected == true) {
                this.count = this.count + 1;
            }
        })
    }
    isAllChecked() {
        if (this.tableRecords)
            return this.tableRecords.every(_ => _.dispatchItem.isSelected);
    }
    checkAll(ev) {
        this.tableRecords.forEach(x => x.dispatchItem.isSelected = ev.target.checked);
        this.oncheckboxCheck();
        /// this.totalcount = this.tableRecords.forEach(x => x.lead.isSelected = ev.target.checked);
    }

    exportToExcel(): void {
        this._dispatchTrackerService.getDispatchTrackerToExcel(
            this.filterText,
            this.organizationUnit,
            this.leadStatusIDS,
            this.leadApplicationStatusFilter,
            this.employeeIdFilter,
            this.DiscomIdFilter,
            this.CircleIdFilter,
            this.DivisionIdFilter,
            this.SubDivisionIdFilter,
            this.solarTypeIdFilter,
            '',
            this.paymentModeIdFilter,
            this.PaymentTypeIdFilter,
            0,
            0,
            this.FilterbyDate,
            this.StartDate,
            this.EndDate,
            undefined,
            this.primengTableHelper.getSorting(this.dataTable),
            undefined,
            undefined
        )
            .subscribe(result => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }

    changeDiscom() {
        if (this.DiscomIdFilter.length > 0) {
            this._commonLookupService.getDiscomWiseCircleFilterForDropdown(this.DiscomIdFilter).subscribe(result => {
                this.allCircleData = result;
                if (this.allCircleData.length == 0) {
                    this.allCircleData = undefined;
                }
            }, error => { this.spinnerService.hide() });
        }
        else {
            this.allCircleData = [];
            this.CircleIdFilter = [];
            this.allDivision = [];
            this.DivisionIdFilter = [];
            this.allSubDivision = [];
            this.SubDivisionIdFilter = [];
        }
    }
    changeCircle() {
        if (this.CircleIdFilter.length > 0) {
            this._commonLookupService.getCircleWiseDivisionFilterForDropdown(this.CircleIdFilter).subscribe(result => {
                this.allDivision = result;
                if (this.allDivision.length == 0) {
                    this.allDivision = undefined
                }
            }, error => { this.spinnerService.hide() });
        }
        else {
            this.allDivision = [];
            this.DivisionIdFilter = [];
            this.allSubDivision = [];
            this.SubDivisionIdFilter = [];
        }
    }
    changeDivision() {
        if (this.CircleIdFilter.length > 0) {
            this._commonLookupService.getDivisionWiseSubDivisionFilterForDropdown(this.DivisionIdFilter).subscribe(result => {
                this.allSubDivision = result;
                if (this.allSubDivision.length == 0) {
                    this.allSubDivision = undefined
                }
            }, error => { this.spinnerService.hide() });
        }
        else {
            this.allSubDivision = [];
            this.SubDivisionIdFilter = [];
        }
    }
    leadstatuschange() {
        if (this.leadStatusIDS.includes(6)) {
            //this.showJobStatus = true;
        } else {
            //this.showJobStatus = false;
            //this.jobStatusID = [];
        }
    }
    chaneSolarType() {
        this.allTenderData = undefined;
        this._commonLookupService.getSolarTypeWiseTenderForDropdown(this.solarTypeIdFilter).subscribe(result => {
            this.allTenderData = result;
            if (this.allTenderData.length == 0) {
                this.allTenderData = undefined;
            }
        }, error => { this.spinnerService.hide() });
    }
    clear() {
        this.filterText = '';
        this.leadStatusIDS = [];
        this.leadApplicationStatusFilter = [];
        //this.applicationstatusTrackerFilter = 4;

        this.DiscomIdFilter = [];
        this.CircleIdFilter = [];
        this.DivisionIdFilter = [];
        this.SubDivisionIdFilter = [];
        this.solarTypeIdFilter = [];
        this.employeeIdFilter = [];
        this.FilterbyDate = '';
        this.StartDate = this._dateTimeService.getStartOfDayForDate(this.date);
        this.EndDate = this._dateTimeService.getDate();
        this.getDispatchList();
    }


    expandGrid() {
        this.ExpandedView = true;
    }


}
