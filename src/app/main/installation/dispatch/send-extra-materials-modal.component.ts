import { Component, Injector, ViewChild } from '@angular/core';
import { DataTransferService } from '@app/shared/common/projectservice/data-transfer.service';
import { DateTimeService } from '@app/shared/common/timing/date-time.service';
import { AppConsts } from '@shared/AppConsts';
import { AppComponentBase } from '@shared/common/app-component-base';
import { AuditLogListDto, CommonLookupDto, CommonLookupServiceProxy, DispatchExtraMaterialDto, DispatchTrackerServiceProxy, GetDispatchExtraMaterialForEditOutput, UserListDto } from '@shared/service-proxies/service-proxies';
import { DateTime } from 'luxon';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'SendExtraMaterialModal',
    templateUrl: './send-extra-materials-modal.html',
})
export class SendExtraMaterialModal extends AppComponentBase {
    @ViewChild('SendExtraMaterialModal', { static: true }) modal: ModalDirective;
    sectionId: number = AppConsts.SectionPage.DispatchTracker;
    ExpandedViewApp: boolean = true;
    active = false;
    //auditLog: AuditLogListDto;
    stockItemList: CommonLookupDto[];
    allManagerList: CommonLookupDto[];
    ExtraMaterialDetails: DispatchExtraMaterialDto[];
    extraComment: string = '';
    customertitle: string = '';
    ExtraMaterial: DispatchExtraMaterialDto[];
    selectedLeadId: number = 0;
    selectedDispatchId: number = 0;

    constructor(injector: Injector, private _dateTimeService: DateTimeService,
        private _commonLookupService: CommonLookupServiceProxy,
        private _dispatchtrackerProxy: DispatchTrackerServiceProxy,
        private _dataTransfer: DataTransferService) {
        super(injector);
        //this.ExtraMaterialDetails = new DispatchExtraMaterialDto[0];
    }
    ngOnInit(): void {
        this._commonLookupService.getAllStockItemForDropdown().subscribe(result => {
            this.stockItemList = result;
        }, error => { this.spinnerService.hide() });
    }
    sendExtraMaterial(dispatchdata) {
        this.spinnerService.show();
        this.selectedLeadId = dispatchdata.leaddata.id;
        this.selectedDispatchId = dispatchdata.id;
        this.customertitle = dispatchdata.customerName + ' ( ' + dispatchdata.projectNumber + ' ) ';

        this._commonLookupService.getSalesManagerUser(parseInt(this._dataTransfer.getOrgData())).subscribe(result => {
            this.allManagerList = result;
        }, error => { this.spinnerService.hide() });

        this._dispatchtrackerProxy.getAllDispatchExtraMaterial(dispatchdata.id).subscribe(result => {
            this.ExtraMaterial = result;
            if (this.ExtraMaterial.length == 0) {
                this.addExtraMaterial();
            }            
            else{
                this.extraComment = result[0].extraMaterialNotes
            }
            this.spinnerService.hide();
            this.modal.show();
        }, error => { this.spinnerService.hide() });
    }
    removeExtraMaterial(MaterialItem) {
        if (this.ExtraMaterial.length == 1)
            return;
        if (this.ExtraMaterial.indexOf(MaterialItem) === -1) {
        } else {
            this.ExtraMaterial.splice(this.ExtraMaterial.indexOf(MaterialItem), 1);
        }
    }
    addExtraMaterial() {
        let Item = new DispatchExtraMaterialDto()
        Item.leadid = this.selectedLeadId;
        Item.dispatchId = this.selectedDispatchId;
        Item.orgID = parseInt(this._dataTransfer.getOrgData());
        this.ExtraMaterial.push(Item);
    }
    saveDispatchExtraMaterial(): void {
        this.ExtraMaterial[0].leadid = this.selectedLeadId;
        this.ExtraMaterial[0].dispatchId = this.selectedDispatchId;
        this.ExtraMaterial[0].orgID = parseInt(this._dataTransfer.getOrgData());
        this.ExtraMaterial[0].extraMaterialNotes = this.extraComment;
        this._dispatchtrackerProxy.createOrEditExtraMaterial(this.ExtraMaterial).pipe(
            finalize(() => { this.active = false; })
        ).subscribe(() => {
            this.active = true;
            this.modal.hide();
            document.getElementById('btnRefresh').click();
            this.notify.info(this.l('SavedSuccessfully'));
        }, error => { this.spinnerService.hide() });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }

}
