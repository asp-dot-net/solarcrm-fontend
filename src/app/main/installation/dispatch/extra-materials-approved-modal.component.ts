import { Component, Injector, ViewChild } from '@angular/core';
import { DataTransferService } from '@app/shared/common/projectservice/data-transfer.service';
import { DateTimeService } from '@app/shared/common/timing/date-time.service';
import { AppComponentBase } from '@shared/common/app-component-base';
import { CommonLookupDto, CommonLookupServiceProxy, DispatchExtraMaterialDto, DispatchTrackerServiceProxy} from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'ExtraMaterialApprovedModal',
    templateUrl: './extra-materials-approved-modal.html',
})
export class ExtraMaterialApprovedModal extends AppComponentBase {
    @ViewChild('ExtraMaterialApprovedModal', { static: true }) modal: ModalDirective;

    ExpandedViewApp: boolean = true;
    active = false;
    stockItemList: CommonLookupDto[];
    allManagerList: CommonLookupDto[];
    ExtraMaterialDetails: DispatchExtraMaterialDto[];
    extraComment: string = '';
    customertitle: string = '';
    ExtraMaterial: DispatchExtraMaterialDto[];
    selectedLeadId: number = 0;
    selectedDispatchId: number = 0;

    constructor(injector: Injector, private _dateTimeService: DateTimeService,
        private _commonLookupService: CommonLookupServiceProxy,
        private _dispatchtrackerProxy: DispatchTrackerServiceProxy,
        private _dataTransfer: DataTransferService) {
        super(injector);
        //this.ExtraMaterialDetails = new DispatchExtraMaterialDto[0];
    }
    ngOnInit(): void {
        this._commonLookupService.getAllStockItemForDropdown().subscribe(result => {
            this.stockItemList = result;
        }, error => { this.spinnerService.hide() });
    }

    ExtraMaterialApproved(dispatchdata) {
        this.spinnerService.show();
        this.selectedLeadId = dispatchdata.leaddata.id;
        this.selectedDispatchId = dispatchdata.id;
        this.customertitle = dispatchdata.customerName + ' ( ' + dispatchdata.projectNumber + ' ) ';

        this._commonLookupService.getSalesManagerUser(parseInt(this._dataTransfer.getOrgData())).subscribe(result => {
            this.allManagerList = result;
        }, error => { this.spinnerService.hide() });

        this._dispatchtrackerProxy.getAllDispatchExtraMaterial(dispatchdata.id).subscribe(result => {
            this.ExtraMaterial = result;
            
            if (this.ExtraMaterial.length == 0) {
                this.addExtraMaterial();
            }
            else{
                this.extraComment = result[0].extraMaterialApprovedNotes
            }
            this.spinnerService.hide();
            this.modal.show();
        }, error => { this.spinnerService.hide() });
    }
    removeExtraMaterial(MaterialItem) {
        if (this.ExtraMaterial.length == 1)
            return;
        if (this.ExtraMaterial.indexOf(MaterialItem) === -1) {
        } else {
            this.ExtraMaterial.splice(this.ExtraMaterial.indexOf(MaterialItem), 1);
        }
    }
    addExtraMaterial() {
        let Item = new DispatchExtraMaterialDto()
        Item.leadid = this.selectedLeadId;
        Item.dispatchId = this.selectedDispatchId;
        Item.orgID = parseInt(this._dataTransfer.getOrgData());
        this.ExtraMaterial.push(Item);
    }
    saveDispatchExtraMaterialApprove(): void {
        this.ExtraMaterial[0].leadid = this.selectedLeadId;
        this.ExtraMaterial[0].dispatchId = this.selectedDispatchId;
        this.ExtraMaterial[0].orgID = parseInt(this._dataTransfer.getOrgData());
        this.ExtraMaterial[0].extraMaterialApprovedNotes = this.extraComment;
        this.ExtraMaterial[0].isExtraMaterialApproved = true;
        this._dispatchtrackerProxy.createOrEditExtraMaterial(this.ExtraMaterial).pipe(
            finalize(() => { this.active = false; })
        ).subscribe(() => {
            this.active = true;
            this.modal.hide();
            document.getElementById('btnRefresh').click();
            this.notify.info(this.l('SavedSuccessfully'));
        }, error => { this.spinnerService.hide() });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }

}
