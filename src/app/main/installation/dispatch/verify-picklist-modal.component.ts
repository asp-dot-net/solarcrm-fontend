import { Component, Injector, ViewChild } from '@angular/core';
import { DataTransferService } from '@app/shared/common/projectservice/data-transfer.service';
import { DateTimeService } from '@app/shared/common/timing/date-time.service';
import { AppConsts } from '@shared/AppConsts';
import { AppComponentBase } from '@shared/common/app-component-base';
import {  CreateOrEditDispatchDto, DispatchTrackerServiceProxy } from '@shared/service-proxies/service-proxies';

import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'VerifyModal',
    templateUrl: './verify-picklist-modal.html',
})
export class VerifyModal extends AppComponentBase {
    @ViewChild('VerifyModal', { static: true }) modal: ModalDirective;
    sectionId: number = AppConsts.SectionPage.PicklistJob;
    DispatchDetails: CreateOrEditDispatchDto;
    ExpandedViewApp: boolean = true;
    active = false;
    isVerify:boolean = false;
    //auditLog: AuditLogListDto;
   
    constructor(injector: Injector, private _dateTimeService: DateTimeService,
        private _dispatchtrackerProxy: DispatchTrackerServiceProxy,
        private _dataTransfer: DataTransferService
        ) {
        super(injector);
    }

    VerifyPickList(item) {        
        this.isVerify =item.isActive;
        this.DispatchDetails = new CreateOrEditDispatchDto();
        this.DispatchDetails.id = item.id;
        this.DispatchDetails.dispatchJobId = item.dispatchJobId;
        this.DispatchDetails.dispatchDate = item.dispatchDate;
        this.DispatchDetails.dispatchHeightStructureId = item.dispatchHeightStructureId;
        this.DispatchDetails.dispatchTypeId = item.dispatchTypeId;
        this.DispatchDetails.dispatchNotes = item.dispatchNotes;
        this.DispatchDetails.dispatchReturnDate = item.dispatchReturnDate;       
        this.DispatchDetails.isActive =  item.isActive;
        this.DispatchDetails.dispatchReturnNotes = item.dispatchReturnNotes;
        this.DispatchDetails.leadid = item.leaddata.id;
        this.modal.show();        
    }
    saveDispatchJob(): void {       
        this.DispatchDetails.isActive = this.isVerify;// item.isActive;
        this.DispatchDetails.orgID = parseInt(this._dataTransfer.getOrgData());
       
        this._dispatchtrackerProxy.verifypicklist(this.DispatchDetails).pipe(
          finalize(() => { this.active = false; })
        ).subscribe(() => {
          this.active = true;
          //this.dispatchAll();
          this.modal.hide();
          document.getElementById('btnRefresh').click();
          this.notify.info(this.l('SavedSuccessfully'));
        }, error => { this.spinnerService.hide() });
      }
    close(): void {
        this.active = false;
        this.modal.hide();
    }

    navigateToAppDetail(): void {
        this.ExpandedViewApp = !this.ExpandedViewApp;
        this.ExpandedViewApp = true;                
    }
    expandApp() {
        this.ExpandedViewApp = false;
    }

}
