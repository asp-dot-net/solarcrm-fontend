import { Component, Injector, ViewChild } from '@angular/core';
import { DataTransferService } from '@app/shared/common/projectservice/data-transfer.service';
import { DateTimeService } from '@app/shared/common/timing/date-time.service';
import { AppConsts } from '@shared/AppConsts';
import { AppComponentBase } from '@shared/common/app-component-base';
import { AuditLogListDto, CommonLookupDto, CommonLookupServiceProxy, DispatchMaterialTransferDto, DispatchTrackerServiceProxy } from '@shared/service-proxies/service-proxies';
import { DateTime } from 'luxon';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'StockTransferModal',
    templateUrl: './stock-transfer-action-modal.html',
})
export class StockTransferModal extends AppComponentBase {
    @ViewChild('StockTransferModal', { static: true }) modal: ModalDirective;
    sectionId: number = AppConsts.SectionPage.DispatchTracker;
    ExpandedViewApp: boolean = true;
    active = false;
    TransferMaterial: DispatchMaterialTransferDto;
    selectedLeadId: number = 0;
    selectedDispatchId: number = 0;
    customertitle: string = '';
    transferReasonList : CommonLookupDto[];
    constructor(injector: Injector, private _dateTimeService: DateTimeService,
        private _commonLookupService: CommonLookupServiceProxy,
        private _dispatchtrackerProxy: DispatchTrackerServiceProxy,
        private _dataTransfer: DataTransferService) {
        super(injector);
        
    }
    ngOnInit(): void { 
        this.TransferMaterial = new DispatchMaterialTransferDto();
        this._commonLookupService.getAllMaterialTransferReasonForDropdown().subscribe(result => {
            if(result != undefined){ 
                this.transferReasonList = result;
            }            
        }, error => { this.spinnerService.hide() });        
    }
    transferMaterial(dispatchdata) {
        this.spinnerService.show();        
        this.selectedLeadId = dispatchdata.leaddata.id;
        this.selectedDispatchId = dispatchdata.id;
        this.customertitle = dispatchdata.customerName + ' ( ' + dispatchdata.projectNumber + ' ) ';
       
       
        this._dispatchtrackerProxy.getDispatchMaterialTransferForEdit(dispatchdata.id).subscribe(result => {
            if(result.materialTransferDto != undefined){         
                this.TransferMaterial.id = result.materialTransferDto.id;     
                this.TransferMaterial.reason = result.materialTransferDto.reason;
                this.TransferMaterial.jobNumber = result.materialTransferDto.jobNumber;
            }           
            this.spinnerService.hide();
            this.modal.show();
        }, error => { this.spinnerService.hide() });     
       
    }
    saveTransferMaterial(){
        this.TransferMaterial.leadid =this.selectedLeadId; 
        this.TransferMaterial.dispatchId = this.selectedDispatchId;       
        this.TransferMaterial.orgID =parseInt(this._dataTransfer.getOrgData());
        this._dispatchtrackerProxy.createOrEditTransferMaterial(this.TransferMaterial).pipe(
            finalize(() => { this.active = false; })
        ).subscribe(() => {
            this.active = true;
            this.modal.hide();
            document.getElementById('btnRefresh').click();
            this.notify.info(this.l('SavedSuccessfully'));
        }, error => { this.spinnerService.hide() });
    }
    close(): void {
        this.active = false;
        this.modal.hide();
    }

}
