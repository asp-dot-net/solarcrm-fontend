import { Component, Injector, ViewChild } from '@angular/core';
import { DataTransferService } from '@app/shared/common/projectservice/data-transfer.service';
import { DateTimeService } from '@app/shared/common/timing/date-time.service';
import { AppConsts } from '@shared/AppConsts';
import { AppComponentBase } from '@shared/common/app-component-base';
import { CommonLookupDto, CommonLookupServiceProxy, CreateOrEditTransportDto, TransportTrackerServiceProxy } from '@shared/service-proxies/service-proxies';
import { DateTime } from 'luxon';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'TransportDetails',
    templateUrl: './TransportDetails.component.html',
})
export class TransportDetails extends AppComponentBase {
    @ViewChild('AddOrEditTransportModal', { static: true }) modal: ModalDirective;
    sectionId: number = AppConsts.SectionPage.TransportTracker;
    saving: boolean = false;
    active = false;
    dispatchDate: DateTime = this._dateTimeService.getDate();
    RegoNo: string = '';
    vehicalTypeList: CommonLookupDto[];
    TransportDetails: CreateOrEditTransportDto;
    constructor(injector: Injector, private _dateTimeService: DateTimeService,
        private _transporttrackerProxy: TransportTrackerServiceProxy,
        private _dataTransfer: DataTransferService,
        private _commonLookupService: CommonLookupServiceProxy) {
        super(injector);

    }
    ngOnInit(): void {
        this.TransportDetails = new CreateOrEditTransportDto();
        this._commonLookupService.getAllVehicalForDropdown().subscribe(result => {
            this.vehicalTypeList = result;
        }, error => { this.spinnerService.hide() });
    }
    changevehicalType(event) {
        
        this.RegoNo = '';
        if (event.target.value != undefined) {
            var result =this.vehicalTypeList.filter(x=>x.id == event.target.value);
            this.RegoNo = result[0].labeldata;
        }
    }
    ProjectNoList = [];
    DispatchJobList = [];
    addoreditTransport(tableRecords) {        
        this.ProjectNoList = [];        
        tableRecords.forEach(item => {
            if (item.dispatchItem.isSelected == true) {     
                this.DispatchJobList.push(item.dispatchItem.jobId);           
                this.ProjectNoList.push(item.dispatchItem.projectNumber);               
            }
        })
        this.modal.show();
    }

    saveTransportJob() {
        this.TransportDetails.dispatchDate = this.dispatchDate;
        this.TransportDetails.transportDate = this.dispatchDate;
        this.TransportDetails.dispatchJobList =this.DispatchJobList;
        this.TransportDetails.orgID = parseInt(this._dataTransfer.getOrgData());
        this.saving = true;
        this._transporttrackerProxy.createOrEdit(this.TransportDetails).pipe(
            finalize(() => { this.saving = false; })
        )
            .subscribe(() => {
                this.active = true;
                this.modal.hide();
                document.getElementById('btnRefresh').click();
                this.notify.info(this.l('SavedSuccessfully'));
            }, error => { this.spinnerService.hide() });
    }

    close() {
        this.modal.hide();
    }
}
