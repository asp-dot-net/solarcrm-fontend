import { Component, Injector, ViewChild } from '@angular/core';
import { DataTransferService } from '@app/shared/common/projectservice/data-transfer.service';
import { DateTimeService } from '@app/shared/common/timing/date-time.service';
import { AppConsts } from '@shared/AppConsts';
import { AppComponentBase } from '@shared/common/app-component-base';
import { CommonLookupDto, CommonLookupServiceProxy,  CreateOrEditDispatchDto, JobProductItemsServiceProxy, DispatchTrackerServiceProxy } from '@shared/service-proxies/service-proxies';
import { DateTime } from 'luxon';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'SelectMaterialModal',
    templateUrl: './select-materials-action-modal.html',
})
export class SelectMaterialModal extends AppComponentBase {
    @ViewChild('SelectMaterialModal', { static: true }) modal: ModalDirective;
    sectionId: number = AppConsts.SectionPage.DispatchTracker;
    heightofStructureList: CommonLookupDto[];
    billofmaterialList: CommonLookupDto[];
    dispatchTypeList: CommonLookupDto[];
    ExpandedViewApp: boolean = true;
    active = false;
    //auditLog: AuditLogListDto;
    JobProducts: any[];
    DispatchDetails: CreateOrEditDispatchDto; // GetDispatchForEditOutput = new GetDispatchForEditOutput();
    selectedjobId: number = 0;
    dispatchDate:DateTime = this._dateTimeService.getDate();
    constructor(injector: Injector, private _dateTimeService: DateTimeService,
        private _dispatchtrackerProxy: DispatchTrackerServiceProxy,
        private _jobProductItemProxy: JobProductItemsServiceProxy,
        private _commonLookupService: CommonLookupServiceProxy,
        private _dataTransfer: DataTransferService
    ) {
        super(injector);
    }
    ngOnInit(): void {
        this.DispatchDetails = new CreateOrEditDispatchDto();
    }    
    EditDispatchDetails(jobdata) {      
        this.modal.show();
        this.spinnerService.show();
        this.binddropdown();  
        this.DispatchDetails = new CreateOrEditDispatchDto();       
        this.dispatchDate = this._dateTimeService.getDate();
        //this.customertitle = jobdata.customerName +'('+ jobdata.projectNumber +')'
        this.selectedjobId = jobdata.jobId;
        this._dispatchtrackerProxy.getDispatchForEdit(jobdata.jobId).subscribe(result => {
            this.spinnerService.hide();
            if (result.dispatch == undefined) {
                this.DispatchDetails = this.DispatchDetails;               
            }
            else {               
                this.DispatchDetails = result.dispatch;                
                this.dispatchDate = this.DispatchDetails.dispatchDate == null ? this.dispatchDate : this.DispatchDetails.dispatchDate;
            }
            this.DispatchDetails.dispatchJobId = jobdata.jobId;
            this.DispatchDetails.leadid = jobdata.leaddata.id;
            this.DispatchDetails.orgID = parseInt(this._dataTransfer.getOrgData())
                     
        }, error => { this.spinnerService.hide() });
    }
    binddropdown() {        
        this._commonLookupService.getAllHeightStructureForDropdown().subscribe(result => {
            this.heightofStructureList = result;
        }, error => { this.spinnerService.hide() });

        this._commonLookupService.getBillOfMaterial().subscribe(result => {
            this.billofmaterialList = result;          
        }, error => { this.spinnerService.hide() });

        this._commonLookupService.getAllDispatchTypeForDropdown().subscribe(result => {
            this.dispatchTypeList = result;
        }, error => { this.spinnerService.hide() });
    }

    saving: boolean = false;
    saveDispatchJob(): void {
        this.DispatchDetails.dispatchDate = this.dispatchDate;
        this.saving = true;
        this._dispatchtrackerProxy.createOrEdit(this.DispatchDetails).pipe(
            finalize(() => { this.saving = false; })
        )
            .subscribe(() => {
                this.active = true;
                this.modal.hide();
                document.getElementById('btnRefresh').click();
                this.notify.info(this.l('SavedSuccessfully'));
            }, error => { this.spinnerService.hide() });
    }
    getjobProductList() {
        this._jobProductItemProxy.getJobProductItemByJobId(2).subscribe(result => {
            this.JobProducts = [];
            //this.PicklistProducts = [];

            result.map((item) => {

                var jobproductcreateOrEdit = { id: 0, productItemId: 0, productItemName: "", productTypeName: "", quantity: 0, jobId: 0, size: 0, model: "", productItems: [], productTypeId: 0 }
                jobproductcreateOrEdit.productTypeId = item.jobProductItem.productTypeId;
                jobproductcreateOrEdit.productItemId = item.jobProductItem.productItemId;
                jobproductcreateOrEdit.quantity = item.jobProductItem.quantity;
                jobproductcreateOrEdit.id = item.jobProductItem.id;
                jobproductcreateOrEdit.jobId = 1;
                jobproductcreateOrEdit.size = item.size;
                jobproductcreateOrEdit.model = item.model;
                jobproductcreateOrEdit.productTypeName = item.productTypeName;
                this.JobProducts.push(jobproductcreateOrEdit);
            })


        }, error => { this.spinnerService.hide() });
    }
    close(): void {
        this.active = false;
        this.modal.hide();
    }

    navigateToAppDetail(): void {
        this.ExpandedViewApp = !this.ExpandedViewApp;
        this.ExpandedViewApp = true;
    }
    expandApp() {
        this.ExpandedViewApp = false;
    }

}
