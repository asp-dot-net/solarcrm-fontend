import { Component, Injector, ViewChild } from '@angular/core';
import { DateTimeService } from '@app/shared/common/timing/date-time.service';
import { AppComponentBase } from '@shared/common/app-component-base';
import { DispatchTrackerServiceProxy, DispatchTrackerQuickViewDto } from '@shared/service-proxies/service-proxies';

import { ModalDirective } from 'ngx-bootstrap/modal';

@Component({
    selector: 'dispatchModal',
    templateUrl: './dispatch-action-modal.html',
})
export class dispatchModal extends AppComponentBase {
    @ViewChild('dispatchModal', { static: true }) modal: ModalDirective;
    customertitle:string = '';
    ExpandedViewApp: boolean = true;
    active = false;
    //auditLog: AuditLogListDto;
    QuickViewData:DispatchTrackerQuickViewDto;
    constructor(injector: Injector, private _dateTimeService: DateTimeService,
        private _dispatchtrackerProxy: DispatchTrackerServiceProxy,
        ) {
        super(injector);
    } 
    ngOnInit(): void {        
        this.QuickViewData = new DispatchTrackerQuickViewDto;  
    }  
    GetDispatchQuickView(dispatchdata){               
        this.spinnerService.show();
         this.customertitle = dispatchdata.customerName +' ( '+ dispatchdata.projectNumber +' ) '
         this._dispatchtrackerProxy.getDispatchQuickView(dispatchdata.id).subscribe((result) => {
             this.spinnerService.hide();            
             this.QuickViewData = result;            
             this.modal.show();   
         }, error => { this.spinnerService.hide() });
    }
    
    close(): void {
        this.active = false;
        this.modal.hide();
    }

    navigateToAppDetail(): void {
        this.ExpandedViewApp = !this.ExpandedViewApp;
        this.ExpandedViewApp = true;                
    }
    expandApp() {
        this.ExpandedViewApp = false;
    }

}
