import { OnInit, Component, Injector, ViewEncapsulation, ViewChild, Input, Output, EventEmitter, AfterViewInit } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ActivatedRoute, Router } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { LazyLoadEvent } from 'primeng/api';
import { LeadsServiceProxy, LeadsDto, CommonLookupServiceProxy, CommonLookupDto, UserServiceProxy, JobInstallationServiceProxy, InstallationCalendarDto, OrganizationUnitDto } from '@shared/service-proxies/service-proxies';
import { Paginator } from 'primeng/paginator';
import { Title } from '@angular/platform-browser';
import * as _ from 'lodash';
import * as moment from 'moment';
import { DateTime } from 'luxon';
import { DateTimeService } from '@app/shared/common/timing/date-time.service';

@Component({
    selector: 'installation-calendar',
    templateUrl: './installation-calendar.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class InstallationCalendarComponent extends AppComponentBase implements OnInit  {
    //@ViewChild('dispatchModal', { static: true }) dispatchModal: dispatchModal;    
    
    @Output() reloadLead = new EventEmitter();
    
    advancedFiltersAreShown = false;
    FiltersData = false;
    active = false;
    saving = false;
    recordCount:any;
    leadId:any;
    filterText : string = ''; 
    StartDate: moment.Moment;
    EndDate: moment.Moment;
    sampleDateRange: moment.Moment[] = [moment().add(0, 'days').endOf('day'), moment().add(0, 'days').endOf('day')];  
    ExpandedView: boolean = true;
    shouldShow: boolean = false;
    date = moment().startOf('isoWeek').toDate();
    recordStartDate =  moment(this.date, 'DD/MM/YYYY');
    InstallationList: InstallationCalendarDto[];
    InstallerList: CommonLookupDto[];
    allOrganizationUnits: OrganizationUnitDto[];
    organizationUnit = 0;
    organizationUnitlength: number = 0;
    areaNameFilter = '';
    stateNameFilter = '';
    //allStates: LeadStateLookupTableDto[];
    SelectedLeadId: number = 0;
    OpenRecordId: number = 0;
    cancelReasonNameFilter = '';
    installerId = 0;
    calendaruserid = 0;  
    Datetime1 :DateTime = this._dateTimeService.getStartOfDayForDate(this.date);
    SubDivisionIdFilter = [];
    drpInstallerUser:CommonLookupDto[];
    drpSubDivision:CommonLookupDto[];
    InstallerId: number = 3;
    FilterInstallerID:'';
    FilterSubDivisionID:'';   
    constructor(
        injector: Injector,
        private _leadsServiceProxy: LeadsServiceProxy,
        private _commonLookupService: CommonLookupServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private titleService: Title,
        private _jobInstallServiceProxy: JobInstallationServiceProxy,
        private _router: Router,
        private _dateTimeService: DateTimeService
    ) {
        super(injector);
    }
    show(leadId?: number, param?: number, sectionId?: number): void {
        
        this.leadId = leadId;
        // this._leadsServiceProxy.leadDocumentList(this.bindDocumentFilter).subscribe(result => {
        //     console.log(result);
        //     this.primengTableHelper.totalRecordsCount = result.totalCount;
        //     this.primengTableHelper.records = result.items;
        //     this.primengTableHelper.hideLoadingIndicator();
        //     this.recordCount = result.totalCount;
        //     //this.leadDocumentList = result;            
        // });
        this.getInstallationCalendar();
    }
    getInstallationCalendar(event?: LazyLoadEvent) {
        if(this.primengTableHelper.shouldResetPaging(event)){
            //this.paginator.changePage(0);
            //return;
        }
        this.primengTableHelper.showLoadingIndicator();
        this.shouldShow = false;
     
        //this.bindDocumentFilter.filter = this.leadId
        // this._leadsServiceProxy.leadDocumentList(this.bindDocumentFilter).subscribe((result) => {
        //     this.primengTableHelper.totalRecordsCount = result.totalCount;
        //     this.primengTableHelper.records = result.items;
        //     this.primengTableHelper.hideLoadingIndicator();
        //     this.recordCount = result.totalCount;
        // }
        //);
    }

    navigateToLeadDetail(leadid): void {

        this.ExpandedView = !this.ExpandedView;
        this.ExpandedView = false;
        // this.SelectedLeadId = leadid;
        // this.leadName = "myleads"
        // this.viewLeadDetail.showDetail(leadid, this.leadName, this.sectionId);
    }

    exportToExcel(): void {
        // this._leadsServiceProxy.getLeadsToExcel(
        //     this.filterText,
        // )
        //     .subscribe(result => {
        //         this._fileDownloadService.downloadTempFile(result);
        //     });
    }
    
    //InstallationList: Installation
    showJobTypeDetails(): void {
        //this.estimateListDetailModal.show();
    }
    //JobBookingData :GetJobInstallForViewDto[];
   
    ngOnInit(): void {  
        
        this._commonLookupService.getEmployees(this.InstallerId).subscribe(result => {
            this.drpInstallerUser = result;
          }, error => { });
          this._commonLookupService.getAllSubdivisionWiseData("").subscribe(result => {
            this.drpSubDivision = result;
          }, error => { });
        this._commonLookupService.getOrganizationUnit().subscribe(output => {
            this.allOrganizationUnits = output;
            this.organizationUnit = this.allOrganizationUnits[0].id;
            this.organizationUnitlength = this.allOrganizationUnits.length;
            this.getInstallations();
        });
    }
    
   
    getInstallations(): void {
        this.showMainSpinner();
        // if (this._permissionChecker.isGranted('Pages.Tenant.InstallerManager.InstallerCalendar')) {
        //   this.calendaruserid=this.installerId;
        // }
         this._jobInstallServiceProxy.getInstallationCalendar(this.calendaruserid, this.Datetime1, this.organizationUnit,this.areaNameFilter,this.stateNameFilter).subscribe(result => {
           this.InstallationList=result;
           console.log(this.InstallationList);
           this.hideMainSpinner();
         }, e => {
             this.hideMainSpinner();
         });
    }
    previousDate(): void {
        this.recordStartDate = moment(this.recordStartDate).add(-7, 'days');
        this.getInstallations();
    };
    getInstaller(): void {
    }
    nextDate(): void {
        this.recordStartDate = moment(this.recordStartDate).add(7, 'days');
        this.getInstallations();
    };

    setOpenRecord(id) {
        if (id === this.OpenRecordId) { this.OpenRecordId = 0; return; }
        this.OpenRecordId = id;
    }

    expandGrid() {
        this.ExpandedView = true;
    }
    
    close() { 
        //this.modal.hide();
    }
}
