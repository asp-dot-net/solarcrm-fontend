import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { DispatchListComponent } from '../installation/dispatch/dispatch-list.componet';
import { SerialNoPhotosComponent } from '../installation/serial-photos/serial-photos.componet';
import { MapComponent } from '../installation/map/map.componet';
import { InstallationCalendarComponent } from '../installation/installation-calendar/installation-calendar.componet';
import { TransportDetails } from './dispatch/TransportDetails.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '',
                // children: [
                //     {
                //         path: 'manageleads',
                //         loadChildren: () => import('./manageleads/manageleads.module').then(m => m.ManageLeadsModule),
                //         //data: { permission: 'Pages.Tenant.DataVaults.Customer', features: 'App.DataVaults.Customer' },
                //     },
                //      {
                //          path: 'leadsactivity',
                //          loadChildren: () => import('../leads/leadactivity/leadsactivity.module').then(m => m.LeadsActivityModule),
                //     //     data: { permission: 'Pages.Tenant.DataVaults.Customer' },
                //      }
                // ]
            },          
            { path: 'dispatch', component: DispatchListComponent },
            { path: 'serial-photos', component: SerialNoPhotosComponent },
            { path: 'map', component: MapComponent },            
            { path: 'installation-calendar', component: InstallationCalendarComponent }, 
            {path:'TransportDetails',component:TransportDetails}
        ]),
    ],
    exports: [RouterModule],
})
export class InstallationRoutingModule { }