import { CommonModule,DatePipe } from '@angular/common';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule ,NO_ERRORS_SCHEMA} from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppCommonModule } from '@app/shared/common/app-common.module';
import { UtilsModule } from '@shared/utils/utils.module';
import { CountoModule } from 'angular2-counto';
import { ModalModule } from 'ngx-bootstrap/modal';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { PopoverModule } from 'ngx-bootstrap/popover';
//import { TrackerRoutingModule } from './tracker-routing.module';
import { InstallationRoutingModule } from './installation-routing.module';
import { BsDatepickerConfig, BsDaterangepickerConfig, BsLocaleService } from 'ngx-bootstrap/datepicker';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { NgxBootstrapDatePickerConfigService } from 'assets/ngx-bootstrap/ngx-bootstrap-datepicker-config.service';
import { SubheaderModule } from '@app/shared/common/sub-header/subheader.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { AgmCoreModule } from '@agm/core';
import { AdminSharedModule } from '@app/admin/shared/admin-shared.module';
import { AppSharedModule } from '@app/shared/app-shared.module';
import { EmailEditorModule } from 'angular-email-editor';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { PaginatorModule } from 'primeng/paginator';
import { EditorModule } from 'primeng/editor';
import { InputMaskModule } from 'primeng/inputmask'; import { FileUploadModule } from 'primeng/fileupload';
import { TableModule } from 'primeng/table';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { DispatchListComponent } from './dispatch/dispatch-list.componet';
import { dispatchModal } from './dispatch/dispatch-action-modal.component';
import { SelectMaterialModal } from './dispatch/select-materials-modal.component';
import { SendExtraMaterialModal } from './dispatch/send-extra-materials-modal.component';
import { StockTransferModal } from './dispatch/stock-transfer-modal.component';
import { VerifyModal } from './dispatch/verify-picklist-modal.component';
import { InstallationCalendarComponent } from './installation-calendar/installation-calendar.componet';
import {LeadsModule} from './../leads/leads.module';
import { TransportDetails } from './dispatch/TransportDetails.component';
import { ExtraMaterialApprovedModal } from './dispatch/extra-materials-approved-modal.component';
NgxBootstrapDatePickerConfigService.registerNgxBootstrapDatePickerLocales();

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ModalModule,
        TabsModule,
        TooltipModule,
        AppCommonModule,
        UtilsModule,        
        CountoModule,
        BsDatepickerModule.forRoot(),
        BsDropdownModule.forRoot(),
        PopoverModule.forRoot(),
        SubheaderModule,
        AdminSharedModule,
        AppSharedModule,  
        NgSelectModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyALoDvKqgmTfpfPOEjqS2fI1kcabuxImMA',            
            libraries: ['places']
        }),    
        LeadsModule,    
        EmailEditorModule,
        AutoCompleteModule,
		PaginatorModule,
		EditorModule,
		InputMaskModule,
		TableModule,
        OwlDateTimeModule,
        OwlNativeDateTimeModule,    
        InstallationRoutingModule        
    ],
    declarations: [
        DispatchListComponent, dispatchModal, SelectMaterialModal, SendExtraMaterialModal, StockTransferModal,
             VerifyModal, InstallationCalendarComponent,TransportDetails, ExtraMaterialApprovedModal 
    ],
    schemas: [ NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA ],
    exports: [LeadsModule],
    providers: [ 
       { provide: BsDatepickerConfig, useFactory: NgxBootstrapDatePickerConfigService.getDatepickerConfig },
       { provide: BsDaterangepickerConfig, useFactory: NgxBootstrapDatePickerConfigService.getDaterangepickerConfig },
       { provide: BsLocaleService, useFactory: NgxBootstrapDatePickerConfigService.getDatepickerLocale },
       { provide: DatePipe }
    ],
})
export class InstallationModule {}
