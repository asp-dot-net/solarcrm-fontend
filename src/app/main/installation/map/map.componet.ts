import { OnInit, Component, Injector, ViewEncapsulation, ViewChild, Input, Output, EventEmitter, AfterViewInit } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ActivatedRoute, Router } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { LazyLoadEvent } from 'primeng/api';
import { LeadsServiceProxy, LeadsDto, CommonLookupServiceProxy, CommonLookupDto, UserServiceProxy, OrganizationUnitDto } from '@shared/service-proxies/service-proxies';
import { Paginator } from 'primeng/paginator';
import { Title } from '@angular/platform-browser';
import * as _ from 'lodash';
import * as moment from 'moment';

@Component({
    selector: 'map',
    templateUrl: './map.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class MapComponent extends AppComponentBase implements OnInit {
    // @ViewChild('dispatchModal', { static: true }) dispatchModal: dispatchModal;        

    @Output() reloadLead = new EventEmitter();

    advancedFiltersAreShown = false;
    FiltersData = false;
    active = false;
    saving = false;
    recordCount: any;
    leadId: any;
    filterText: string = '';
    StartDate: moment.Moment;
    EndDate: moment.Moment;
    sampleDateRange: moment.Moment[] = [moment().add(0, 'days').endOf('day'), moment().add(0, 'days').endOf('day')];
    ExpandedView: boolean = true;
    total = 0;

    housetypeId = 0;
    rooftypeid = 0;
    postalcode = "";
    jobstatusid = 0;
    panelmodel = "";
    invertmodel = "";
    paymenttypeid = 0;
    invertSuggestions: string[];
    panelSuggestions: string[];

    showchild: boolean = true;
    shouldShow: boolean = false;
    //   jobHouseTypes: JobHouseTypeLookupTableDto[];
    //   jobRoofTypes: JobRoofTypeLookupTableDto[];
    //   alljobstatus: JobStatusTableDto[];
    //   jobPaymentOptions: JobPaymentOptionLookupTableDto[];
    //   allStates: LeadStateLookupTableDto[];
    stateNameFilter = "";
    dateFilterType = 'All';
    areaNameFilter = "";
    steetAddressFilter = "";
    postalcodefrom = "";
    postalcodeTo = "";
    priority = 0;

    totalMapData = 0;
    totalDepRcvMapData = 0;
    totalActiveMapData = 0;
    totalJobBookedMapData = 0;
    totalJobInstallMapData = 0;
    MissingDatas = [];
    allOrganizationUnits: OrganizationUnitDto[];
    organizationUnit = 0;
    other = 0;
    organizationUnitlength: number = 0;
    
  icon: 'https://www.google.com/intl/en_us/mapfiles/ms/micons/blue-dot.png';
  //   icon = {
  //     url: 'https://www.google.com/intl/en_us/mapfiles/ms/micons/blue-dot.png',
  //     size: [100, 100],
  //     scaledSize: [50, 50],
  //     anchor: [25, 25]
  // };
  public customestyles = [{
    "featureType": "poi",
    "elementType": "labels",
    "stylers": [{
      visibility: "off",
    }]
  },];
//   markers: GetInstallerMapDto[
//     // {
//     //   lt: 21.1594627,
//     //   long: 72.6822083,
//     //   label: 'Surat'
//     // },
//     // {
//     //   lt: 23.0204978,
//     //   long: 72.4396548,
//     //   label: 'Ahmedabad'
//     // },
//     // {
//     //   lt: 22.2736308,
//     //   long: 70.7512555,
//     //   label: 'Rajkot'
//     // }
//   ];
    constructor(
        injector: Injector,
        private _leadsServiceProxy: LeadsServiceProxy,
        private _commonLookupService: CommonLookupServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private titleService: Title,
        private _router: Router,
    ) {
        super(injector);

    }
    show(leadId?: number, param?: number, sectionId?: number): void {
        alert();
        this.leadId = leadId;
        // this._leadsServiceProxy.leadDocumentList(this.bindDocumentFilter).subscribe(result => {
        //     console.log(result);
        //     this.primengTableHelper.totalRecordsCount = result.totalCount;
        //     this.primengTableHelper.records = result.items;
        //     this.primengTableHelper.hideLoadingIndicator();
        //     this.recordCount = result.totalCount;
        //     //this.leadDocumentList = result;            
        // });
        this.getDispatchList();
    }
    getDispatchList(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            //this.paginator.changePage(0);
            //return;
        }
        this.primengTableHelper.showLoadingIndicator();

        //this.bindDocumentFilter.filter = this.leadId
        // this._leadsServiceProxy.leadDocumentList(this.bindDocumentFilter).subscribe((result) => {
        //     this.primengTableHelper.totalRecordsCount = result.totalCount;
        //     this.primengTableHelper.records = result.items;
        //     this.primengTableHelper.hideLoadingIndicator();
        //     this.recordCount = result.totalCount;
        // }
        //);
    }

    navigateToLeadDetail(leadid): void {

        this.ExpandedView = !this.ExpandedView;
        this.ExpandedView = false;
        // this.SelectedLeadId = leadid;
        // this.leadName = "myleads"
        // this.viewLeadDetail.showDetail(leadid, this.leadName, this.sectionId);
    }

    exportToExcel(): void {
        // this._leadsServiceProxy.getLeadsToExcel(
        //     this.filterText,
        // )
        //     .subscribe(result => {
        //         this._fileDownloadService.downloadTempFile(result);
        //     });
    }
    showJobTypeDetails(): void {
        //this.estimateListDetailModal.show();
    }
    ngOnInit(): void {
    }

    expandGrid() {
        this.ExpandedView = true;
    }

    close() {
        //this.modal.hide();
    }
}
