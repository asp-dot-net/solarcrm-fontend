import { Component, EventEmitter, Injector, Input, Output, ViewChild, ViewEncapsulation } from '@angular/core';
import {
    LeadsServiceProxy,
    CommonLookupServiceProxy,
    JobServiceProxy,
    GetJobForEditOutput,
    CreateOrEditJobDto,
    CreateOrEditLocationsDto,
    CreateOrEditJobProductItemDto,
    JobProductItemsServiceProxy,
    CommonLookupDto,
    JobProductItemProductItemLookupTableDto, CreateOrEditStockItemDto,
    CreateOrEditJobVariationDto, JobVariationServiceProxy, JobVariationLookupTableDto,
    PriceServiceProxy, QuotationsServiceProxy, CreateOrEditQuotationDataDto,
    PriceDto,
    QuotationDataDto
} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { NgxSpinnerService } from 'ngx-spinner';
import { map as _map, filter as _filter } from 'lodash-es';
import { AppConsts } from '@shared/AppConsts';
import { finalize } from 'rxjs/operators';
import { Paginator } from 'primeng/paginator';
import { ActivityLogLeadsComponent } from '../leads/leadactivity/activity-log-leads.component';
import { Table } from 'primeng/table';
import { CreateOrEditPaymentModalComponent } from './create-or-edit-payment.component';
import { fail } from 'assert';
import { JobInstallerComponent } from './job-installer/job-installer.component';
import { JobQuotationComponent } from './job-quotation/job-quotation.component';
import { JobRefundComponent } from './job-refund/job-refund.component';

//import { PdftemplateshowmodalComponent } from './pdf-template-show-modal.component';


@Component({
    selector: 'jobDetails',
    templateUrl: './jobDetails.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()],
})
export class JobDetailsComponent extends AppComponentBase {
    @ViewChild('activityLog', { static: true }) activityLog: ActivityLogLeadsComponent;


    @ViewChild('createOrEditPaymentModal', { static: true }) createOrEditPaymentModal: CreateOrEditPaymentModalComponent;
    @ViewChild('createOrEditJobInstallerModal', { static: true }) createOrEditJobInstallerModal: JobInstallerComponent;
    @ViewChild('createOrEditJobQuotationModal', { static: true }) createOrEditJobQuotationModal: JobQuotationComponent;
    @ViewChild('createOrEditJobRefundModal', { static: true }) createOrEditJobRefundModal: JobRefundComponent;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @Output() reloadLead = new EventEmitter();
    QuoteData: QuotationDataDto[];
    PageNo: number;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    active = false;
    saving = false;
    recordCount: any;
    leadId: number = 0;
    actionId: number = 0;
    sectionId: number = 0;
    activeTabIndex: number = 0;
    active1 = false;
    jobid: number = 0;
    solarTypeList: any;
    job: CreateOrEditJobDto = new CreateOrEditJobDto();
    JobType: any;
    JobProducts: any[];
    jobDetails: GetJobForEditOutput = new GetJobForEditOutput();
    productTypes: CommonLookupDto[];
    EditproductItems: JobProductItemProductItemLookupTableDto[];
    //PickListtems: GetPicklistItemInput[];
    PicklistProducts: any[];
    productItemSuggestions: string[];
    variationItemSuggestions: string[];
    productItems: JobProductItemProductItemLookupTableDto[];
    @Input() SelectedJobId: number = 0;
    //Variation
    JobVariations: CreateOrEditJobVariationDto[];
    variations: JobVariationLookupTableDto[];

    //Subcidy40: any = 0.00;
    //Subcidy20: any = 0.00;
    SystemCapacity: any = 0.00;
    SystemRate: any = 0.00;
    SystemGST: any = 0.00;
    panelsize: any = 0.00;
    invertersize: any = 0.00;
    productSize: any[];
    isPDFshow: boolean = false;
    Price: PriceDto = new PriceDto();
    jobNumber: string = '';
    areaType: string = '';
    IsManualPriceSubcidy: boolean = false;
    leadActivityConst: any;
    jobproductcreateOrEdit: any;
    constructor(
        injector: Injector,
        private _commonLookup: CommonLookupServiceProxy,
        private _jobServiceProxy: JobServiceProxy,
        private _jobProductItemProxy: JobProductItemsServiceProxy,
        private _jobVariationServiceProxy: JobVariationServiceProxy,
        private _spinner: NgxSpinnerService,
        private _priceServiceProxy: PriceServiceProxy,
        //private _jobInstallServiceProxy: JobInstallationServiceProxy,
    ) {
        super(injector);
    }
    ngOnInit(): void {
        this.leadActivityConst = AppConsts.leadActivity;
        this.jobDetails = new GetJobForEditOutput();
        this.job = new CreateOrEditJobDto();
        try {

            this._commonLookup.getAllSolarTypeForDropdown().subscribe((result) => {
                this.solarTypeList = result;
            }, error => { this.spinnerService.hide() });

            this._commonLookup.getAllStockCategorForDropdown().subscribe((result) => {
                this.productTypes = result.filter(x => x.id <= AppConsts.StockItemBind.SalesStockDropDownBind);
            }, error => { this.spinnerService.hide() });

            this._jobProductItemProxy.getAllProductItemForTableDropdownForEdit().subscribe(result => {
                this.EditproductItems = result;
            }, error => { this.spinnerService.hide() });

            this._jobProductItemProxy.getAllProductItemForTableDropdown().subscribe(result => {
                this.productItems = result;
            }, error => { this.spinnerService.hide() });

            this._jobVariationServiceProxy.getAllVariationForTableDropdown(this.jobid).subscribe(result => {
                this.variations = result;
            }, error => { this.spinnerService.hide() });



            // this._jobProductItemProxy.getJobProductItemByJobId(this.jobid).subscribe(result => {
            //     this.JobProducts = [];              
            //     result.map((item) => {
            //    })
            // });
            var jobproductcreateOrEdit = { id: 0, productItemId: 0, productItemName: "", quantity: 0, jobId: this.jobid, size: 0, model: "", productItems: [], productTypeId: 0 }

        } catch (error) {
            console.log(error);
            this.spinnerService.hide();
        }

        // this.paymentModel.showDetail();
    }

    showJobDetails(leadId?: number, param?: number, sectionId?: number, Verified?: boolean): void {
        this.leadId = leadId;
        this.sectionId = sectionId;
        let that = this;
        this.active = true;
        this.jobDetails = new GetJobForEditOutput();
        this.job = new CreateOrEditJobDto();
        this.jobNumber = '';
        this.jobid = 0;
        this.areaType = '';
        this.IsManualPriceSubcidy = false;
        //this.createOrEditJobInstallerModal.showJobinstaller();
        if (leadId != 0 && Verified == true) {

            this._jobServiceProxy.getJobForEdit(leadId).subscribe((result) => {
                //Job Details
               
                if (result.job != undefined && result.job.id != 0) {
                    this.active1 = false;
                    this.jobDetails = result;
                    this.job = result.job;
                    this.jobid = this.job.id == null ? 0 : this.job.id;
                    this.jobNumber = this.job.jobNumber;
                    this.areaType = this.jobDetails.areaType;
                    this.IsManualPriceSubcidy = this.jobDetails.isManualPricingSubcidy;

                    if (this.jobid > 0 && this.job != undefined && this.jobDetails != undefined) {
                        //Solar Product Item
                        this._jobProductItemProxy.getJobProductItemByJobId(this.jobid).subscribe(result => {
                            this.JobProducts = [];
                            result.map((item) => {
                                if (item != null) {
                                    var jobproductcreateOrEdit = { id: 0, productItemId: 0, productItemName: "", quantity: 0, jobId: 0, size: 0, model: "", productItems: [], productTypeId: 0 }
                                    jobproductcreateOrEdit.productTypeId = item.jobProductItem.productTypeId;
                                    jobproductcreateOrEdit.productItemId = item.jobProductItem.productItemId;
                                    jobproductcreateOrEdit.quantity = item.jobProductItem.quantity;
                                    jobproductcreateOrEdit.id = item.jobProductItem.id;
                                    jobproductcreateOrEdit.jobId = this.jobid;
                                    jobproductcreateOrEdit.size = item.size;
                                    jobproductcreateOrEdit.model = item.model;
                                    this.JobProducts.push(jobproductcreateOrEdit);

                                    if (this.EditproductItems != undefined) {
                                        let productitemname = this.EditproductItems.find(x => x.id == item.jobProductItem.productItemId);
                                        jobproductcreateOrEdit.productItemName = productitemname.displayName;
                                    }
                                }
                            })
                            if (result.length == 0) {
                                var jobproductcreateOrEdit = { id: 0, productItemId: 0, productItemName: "", quantity: 0, jobId: 0, size: 0, model: "", productItems: [], productTypeId: 0 }
                                jobproductcreateOrEdit.productItems = this.EditproductItems;
                                this.JobProducts.push(jobproductcreateOrEdit);
                            }

                            // let that = this;
                            // setTimeout(function () {
                            //     that.calculateRates();
                            // }, 5000);
                            // this.saving = false;
                        }, error => { this.spinnerService.hide() });

                        //Solar Variation Item
                        this._jobVariationServiceProxy.getJobVariationByJobId(this.jobid).subscribe(result => {
                            this.JobVariations = [];
                            result.map((item) => {
                                var jobvariationcreateOrEdit = new CreateOrEditJobVariationDto()
                                jobvariationcreateOrEdit.jobId = this.jobid;
                                jobvariationcreateOrEdit.variationId = item.jobVariation.variationId;
                                jobvariationcreateOrEdit.cost = item.jobVariation.cost;
                                jobvariationcreateOrEdit.id = item.jobVariation.id;
                                if (item.jobVariation.id != undefined && item.jobVariation.jobId != undefined && item.jobVariation.variationId != undefined && item.jobVariation.cost != undefined) {
                                    this.JobVariations.push(jobvariationcreateOrEdit)
                                }
                            })
                            if (result.length == 0) {
                                this.JobVariations.push(new CreateOrEditJobVariationDto);
                            }

                        }, error => { this.spinnerService.hide() });
                        setTimeout(function () {
                            that.calculateSystemCapacity();
                        }, 2200);
                    }
                }
                else {
                    this.active1 = true;
                    this.jobid = 0;
                    this.jobDetails = new GetJobForEditOutput();
                    this.job = new CreateOrEditJobDto();
                    this.JobProducts = [];
                    this.JobProducts.push(new CreateOrEditJobProductItemDto);

                    this.JobVariations = [];
                    this.JobVariations.push(new CreateOrEditJobVariationDto);
                }
            });
        }
    }
    changeJobType(event) {
        if (event.target.value == undefined || event.target.value != 3) {
            this.IsManualPriceSubcidy = false;
        }
        else {
            this.IsManualPriceSubcidy = true;
        }
    }
    saveJob(): void {
        //this.job = new CreateOrEditJobDto();
        this.job.leadId = this.leadId;
        if (this.job.id == 0) {
            this.actionId = 10;
        }
        else {
            this.actionId = 11;
        }

        this.saving = true;
        this._jobServiceProxy.createOrEdit(this.sectionId, this.job).pipe(
            finalize(() => { this.saving = false; }))
            .subscribe(() => {
                this.active = true;
                this.active1 = false;
                this.jobDetails = new GetJobForEditOutput();
                this._jobServiceProxy.getJobForEdit(this.leadId).subscribe((result) => {
                    this.jobDetails = result;
                    this.job = result.job;
                    this.jobid = this.job.id;
                });
                //this.activityLog.getLeadActivityLog(this.leadId, 0);
                this.notify.info(this.l('SavedSuccessfully'));
            }, error => { this.spinnerService.hide() });
    }

    editJob(): void {
        this.saving = true;
        this.job.jobProductItems = [];
        this.JobProducts.map((item) => {
            let createOrEditJobProductItemDto = new CreateOrEditJobProductItemDto();
            createOrEditJobProductItemDto.productItemId = item.productItemId;
            createOrEditJobProductItemDto.quantity = item.quantity;
            createOrEditJobProductItemDto.jobId = this.jobid;
            createOrEditJobProductItemDto.id = item.id;
            if (item.productTypeId != undefined && item.productItemId != undefined && item.quantity != undefined && item.model != undefined) {
                this.job.jobProductItems.push(createOrEditJobProductItemDto);
            }
        });

        this.job.jobVariation = [];
        this.JobVariations.map((item) => {
            var jobvariationcreateOrEdit = new CreateOrEditJobVariationDto()
            jobvariationcreateOrEdit.jobId = this.jobid;
            jobvariationcreateOrEdit.variationId = item.variationId;
            jobvariationcreateOrEdit.cost = item.cost;
            jobvariationcreateOrEdit.id = item.id;
            if (item.variationId != undefined && item.cost != undefined) {
                this.job.jobVariation.push(jobvariationcreateOrEdit)
            }
        });

        this._jobServiceProxy.createOrEdit(this.sectionId, this.job).pipe(
            finalize(() => { this.saving = false; })
        )
            .subscribe(() => {
                this.active = true;
                // this.jobDetails = new GetJobForEditOutput();
                // this._jobServiceProxy.getJobForEdit(this.leadId).subscribe((result) => {
                //     this.jobDetails = result;
                //     this.job = result.job;
                //     this.jobid = this.job.id;
                // });

                this.notify.info(this.l('SavedSuccessfully'));
            }, error => { this.spinnerService.hide() });
    }
    // Job Install Save


    filterProductItem(item, i): void {
        this.JobProducts[i].productItems = this.productItems.filter(x => x.productTypeId == item.productTypeId);
    }

    filterProductIteams(event, i): void {
        let Id = 0;
        for (let j = 0; j < this.JobProducts.length; j++) {
            if (j == i) {
                Id = this.JobProducts[i].productTypeId
                this.JobProducts[i].model = '';
                // this.JobProducts[i].quantity= 0;
            }
        }

        this._jobServiceProxy.getProductItemList(Id, event.query).subscribe(output => {
            this.productItemSuggestions = output;
        }, error => { this.spinnerService.hide() });
    }

    filterVariationIteams(event, i): void {
        this._jobVariationServiceProxy.getAllVariationForTableDropdown(this.jobid).subscribe(result => {
            var data = result.filter(x => x.displayName.toLowerCase().indexOf(event.query.toLowerCase()) === 0)
            this.variations = data;
            this.variationItemSuggestions = ["123"]
        }, error => { this.spinnerService.hide() });
        let Id = 0;

        let that = this;
        if (this.JobVariations.length > 0) {

            let varobj;
            this.JobVariations.forEach(function (item) {
                varobj = that.variations.filter(x => x.id != item.variationId && x.displayName.includes(event.query));
            });
            this.variations = varobj;
            console.log(varobj);
        }

    }
    getVariationId(j) {

    }

    //add or Edit Solar Prodct Item
    addJobProduct(): void {
        //this.jobproductcreateOrEdit = { id: 0, productItemId: 0, productItemName: "", quantity: 0, jobId: this.jobid, size: 0, model: "", productItems: [], productTypeId: 0 }
        //this.JobProducts.push(new GetJobProductItemForEditOutput);
        this.JobProducts.push(new CreateOrEditJobProductItemDto());
    }

    removeJobProduct(JobProduct): void {
        // if (this.JobProducts.length == 1)
        //     return;        
        // if (this.JobProducts.indexOf(JobProduct) === -1) {                        
        // } else {
        //     this.JobProducts.splice(this.JobProducts.indexOf(JobProduct), index);
        // }
        if (this.JobProducts.length == 1)
            return;
        // this.JobProducts = this.JobProducts.filter(item => item.productItemId != JobProduct.productItemId);
        if (this.JobProducts.indexOf(JobProduct) === -1) {
            // this.JobProducts.push(JobProduct);
        } else {
            this.JobProducts.splice(this.JobProducts.indexOf(JobProduct), 1);
        }
        this.calculateRates();
    }

    getProductTypeId(i) {
        let pItem = this.productItems.find(x => x.displayName == this.JobProducts[i].productItemName)
        this.JobProducts[i].productItemId = pItem.id;
        this.JobProducts[i].model = pItem.model;
        this.JobProducts[i].size = pItem.size;
    }

    //Add or Remove Varation
    addJobVariation(): void {
        let that = this;
        // if(this.JobVariations.length > 0)
        // { 
        //     let varobj;
        //     this.JobVariations.forEach(function (item) {
        //         varobj = that.variations.filter(x => x.id != item.variationId);
        //     });
        //     this.variations = varobj;
        //     console.log(varobj);
        // }
        this.JobVariations.push(new CreateOrEditJobVariationDto);
    }

    removeJobVariation(rowitem): void {
        //;
        if (this.JobVariations.length == 1)
            return;
        if (this.JobVariations.indexOf(rowitem) === -1) {

        } else {
            this.JobVariations.splice(this.JobVariations.indexOf(rowitem), 1);
        }
        this.calculateRates();
    }
  
    calculateSystemCapacity(): void {
        let that = this;
        let panelsize: number = 0;
        let invertersize: number = 0;
        this.productSize = [];
        let SystemCapacity = 0; //this.SystemCapacity;
        this._spinner.show();
        try {
            
            if (this.JobProducts.length > 0) {
                
                this.JobProducts.forEach(function (item) {                    
                    //producttype = 1 for panel
                    if (item.productTypeId == 1) {
                        SystemCapacity = (parseFloat(item.quantity.toString()) * parseFloat(item.size.toString())) / 1000; //panel Formula
                        panelsize = SystemCapacity;
                    }
                    //producttype = 2 for inverter
                     if (item.productTypeId == 2) {
                         SystemCapacity = (parseFloat(item.quantity.toString()) * parseFloat(item.size.toString()));   //invertr Formula
                         invertersize = SystemCapacity;
                     }
                });
                
                // find the inverter and panel minimum size
                 if (panelsize != 0 && invertersize != 0) {
                     this.productSize.push(panelsize);
                     this.productSize.push(invertersize);

                     const productMinSize = Math.min.apply(null, this.productSize)
                     SystemCapacity = productMinSize;
                 }
                 
                this.SystemCapacity = parseFloat(SystemCapacity.toString());
                this.panelsize = parseFloat(panelsize.toString());
                this.invertersize = parseFloat(invertersize.toString());
                let minRate=[];
                if (this.SystemCapacity != 0) {
                    this.Price = new PriceDto();
                    this._priceServiceProxy.getPriceBySystemSize(Math.ceil(SystemCapacity), this.areaType).subscribe((result) => {
                        // this.Price = result.price;
                        if (result.productPriceDto != undefined) {
                            minRate.push(result.productPriceDto.stateRate);
                            minRate.push(result.productPriceDto.centralRate);
                            const systemMinPrice = Math.min.apply(null, minRate);
                            
                            this.SystemRate = result.productPriceDto.stateRate;
                            this.SystemGST = (result.productPriceDto.stateRate * 0.1384) + result.productPriceDto.stateRate;
                            const SystemCost = this.SystemGST * SystemCapacity; //result.productPriceDto.systemSize;
                            
                            //Actual System Cost Value calculation
                            if(SystemCost == this.jobDetails.job.actualSystemCost)
                            {                                
                                this.jobDetails.job.actualSystemCost =  this.jobDetails.job.actualSystemCost == 0 ? SystemCost : this.jobDetails.job.actualSystemCost;
                            }
                            else{
                                this.jobDetails.job.actualSystemCost = parseFloat(SystemCost.toFixed(2)) ;
                            }
                            
                            //Subsidy Price calculation
                            if(SystemCapacity >= 3){
                                this.jobDetails.job.jobSubsidy40 = 3 * systemMinPrice * 0.40 ;
                                if(SystemCapacity > 3){
                                    let subsidy20 = (SystemCapacity - 3)  *  result.productPriceDto.centralRate * 0.20;
                                    this.jobDetails.job.jobSubsidy20 = parseFloat(subsidy20.toFixed(2)) ; //result.productPriceDto.subcidy20;
                                }
                            }
                            else{
                                let subsidy40 =SystemCapacity * systemMinPrice * 0.40
                                this.jobDetails.job.jobSubsidy40 = parseFloat(subsidy40.toFixed(2))  ;                                
                            }
                            // this.jobDetails.job.actualSystemCost = this.jobDetails.job.actualSystemCost == 0 ? result.productPriceDto.actualPrice : this.jobDetails.job.actualSystemCost;
                            // this.jobDetails.job.jobSubsidy40 = result.productPriceDto.subcidy40;
                            // this.jobDetails.job.jobSubsidy20 = result.productPriceDto.subcidy20;
                            that.calculateRates();
                        }
                        else {                            
                            this.jobDetails.job.actualSystemCost = 0;
                            this.jobDetails.job.jobSubsidy40 = 0;
                            this.jobDetails.job.jobSubsidy20 = 0;

                            that.calculateRates();
                        }
                        //that.calculateRates();
                    }, error => { this.spinnerService.hide() });
                }
            }
            this._spinner.hide();
        } catch (error) {
            this._spinner.hide();
        }
    }
    calculateRates(): void {
        let that = this;
        let ActualSystemCost = this.job.actualSystemCost;
        //structure amount are not calculate the actualSystemCost
        if (this.JobVariations.length > 0) {
            this.JobVariations.forEach(function (item) {
                let varobj = that.variations.filter(x => x.id == item.variationId && x.id != 1);
                if (item.cost > 0 && varobj.length > 0) {
                    if (varobj[0].actionName == "Minus") {
                        ActualSystemCost = parseFloat(ActualSystemCost.toString()) - parseFloat(item.cost.toString());
                    } else {
                        ActualSystemCost = parseFloat(ActualSystemCost.toString()) + parseFloat(item.cost.toString());
                    }
                } else {
                    ActualSystemCost = parseFloat(ActualSystemCost.toString());
                }
            });

            this.job.netSystemCost = parseFloat(ActualSystemCost.toString());
            if (this.job.depositeAmount < 0) {
                this.job.depositeAmount = Math.round(this.job.netSystemCost * 10 / 100);
            }
            this.job.totalJobCost = (this.job.netSystemCost) - (parseFloat(this.jobDetails.job.jobSubsidy40.toString()) + parseFloat(this.jobDetails.job.jobSubsidy20.toString()));// - (this.job.depositeAmount);
            this.job.totalJobCost = parseFloat((this.job.totalJobCost).toFixed(2));
        }
    }
}
