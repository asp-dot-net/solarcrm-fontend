import { Component, Inject, Renderer2, ElementRef, EventEmitter, Injectable, Injector, OnInit, Optional, Output, ViewChild, Input } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
//import { LeadsServiceProxy, GetLeadForViewDto, LeadDto, ActivityLogInput, CommonLookupDto, EmailTemplateServiceProxy, SmsTemplatesServiceProxy, JobsServiceProxy, JobPromotionsServiceProxy, JobPromotionPromotionMasterLookupTableDto } from '@shared/service-proxies/service-proxies';
import { GetLeadsForViewDto, CommonLookupDto, UserListDto, LeadsDto, LeadsServiceProxy, CommonLookupServiceProxy, ActivityLogInput, LeadsActivityLogServiceProxy, PdfTemplateServiceProxy, QuotationsServiceProxy, QuotationDataDetailsDto } from '@shared/service-proxies/service-proxies';
import * as _ from 'lodash';
import { DatePipe, DOCUMENT } from '@angular/common';
import * as moment from 'moment';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { Router } from '@angular/router';
import { EmailEditorComponent } from 'angular-email-editor';
import { AppConsts } from '@shared/AppConsts';
//import { ViewMyLeadComponent } from './view-mylead.component';
//import { ViewMyLeadComponent } from '../manageleads/view-mylead.component';
import { isMoment } from 'moment';
import { Editor } from 'primeng/editor';
import { OWL_DATE_TIME_FORMATS } from 'ng-pick-datetime';
import { DEFAULTS, NgxSpinnerService } from 'ngx-spinner';
//import { event } from 'jquery';
//import {view}
declare var Quill: any;
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from "@angular/platform-browser";
import { AddActivityModalComponent } from '../leads/leadactivity/add-activity-model.component';
import { DateTimeService } from '@app/shared/common/timing/date-time.service';
import { DataTransferService } from '@app/shared/common/projectservice/data-transfer.service';
import { appModuleAnimation } from '@shared/animations/routerTransition';
@Component({
    selector: 'pdfTemplateModel',
    templateUrl: './pdf-template-show-model.component.html',
    animations: [appModuleAnimation()]
})

export class PdfTemplateShowModalComponent extends AppComponentBase implements OnInit {

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @Output() reloadLead = new EventEmitter<boolean>();
    @Input() SelectedJobNumber: string = '';
    @ViewChild("myNameElem") myNameElem: ElementRef;
    @ViewChild("area_select") area_select: ElementRef;
    @ViewChild("modal_body") modal_body: ElementRef;
    @ViewChild('pdfTemplateModel') public modal: ModalDirective;
    @ViewChild('btnEmail', { static: true }) btnEmail: ElementRef;
    @ViewChild('btnsmsnotify', { static: true }) btnsmsnotify: ElementRef;
    @ViewChild('SampleDatePicker', { static: true }) sampleDatePicker: ElementRef;
    @ViewChild('addActivityModal', { static: true }) addActivityModal: AddActivityModalComponent;
    @ViewChild(EmailEditorComponent)
    private emailEditor: EmailEditorComponent;
    saving = false;
    //sampleDate: moment.Moment = moment().add(1, 'days').startOf('day');
    item: QuotationDataDetailsDto;
    activityLog: ActivityLogInput = new ActivityLogInput();
    activityType: number = 6;
    priorityType: number = 1;
    activityName = "";
    actionNote = "";
    id = 0;
    actionId: number = 12;
    sectionId: number = AppConsts.SectionPage.PdfTemplateQuote;
    allEmailTemplates: CommonLookupDto[];
    allSMSTemplates: CommonLookupDto[];
    drpleadactivity: CommonLookupDto[];
    emailData = '';
    smsData = '';
    emailTemplate: number;
    uploadUrl: string;
    uploadedFiles: any[] = [];
    myDate = new Date();
    total = 0;
    credit = 0;
    customeTagsId = 0;
    role: string = '';
    subject = '';
    userName: string;
    leadCompanyName: any;
    pageid = 0;
    allUsers: UserListDto[];
    referanceId = 0;
    CurrentDate = new Date();
    //freebieTransport: JobPromotionPromotionMasterLookupTableDto[];
    emailfrom: string = '';
    emailto: string = '';
    fromemails: CommonLookupDto[];
    leadid: number;
    criteriaavaibaleornot = false;
    smsid = false;
    htmlstr: string = '';
    htmlContentbody: any;
    quoteNumber: string = '';
    constructor(
        injector: Injector, private _http: HttpClient,
        private _quoteServiceProxy: QuotationsServiceProxy,
        private _commonLookupServiceProxy: CommonLookupServiceProxy,
        private _pdfTemplateServiceProxy: PdfTemplateServiceProxy,
        private _router: Router,
        @Inject(DOCUMENT) private document: Document,
        private spinner: NgxSpinnerService,
        private elementRef: ElementRef,
        private _dataTransfer: DataTransferService,
        private sanitizer: DomSanitizer,
        private _dateTimeService: DateTimeService,
    ) {
        super(injector);
        this.item = new QuotationDataDetailsDto();
        //this.item.leads = new LeadsDto();       
    }
    ngOnInit(): void {

    }
    QuoteMode: number = 0;
    show(quoteId, quoteMode): void {
        this.QuoteMode = quoteMode;
        this.spinner.show();
        this._quoteServiceProxy.getQuoteForView(quoteId,parseInt(this._dataTransfer.getOrgData())).subscribe(result => {
            this.item = result;
            this.quoteNumber = result.quoteNumber
            // this.leadCompanyName = result.leads.firstName;
            // if (result.leads.emailId != null) {
            //     this.emailto = result.leads.emailId;
            // }    

            this._pdfTemplateServiceProxy.getPdfTemplateForView(AppConsts.pdfGenerateDocument.Quotation, parseInt(this._dataTransfer.getOrgData())).subscribe(result => {
                if (result.pdfTemplates != undefined) {
                    this.htmlContentbody = result.pdfTemplates.viewHtml;
                    this.setHTML(this.htmlContentbody);
                    this.spinner.hide();
                } else {
                    this.spinner.hide();
                    this.notify.info("Please First Create Quotation Template");
                }

            }, error => { this.spinnerService.hide() });
        }, error => { this.spinnerService.hide() });
    }

    close(): void {
        this.modal.hide();
    }

    setHTML(pdfHTML) {
        let pdfTemplate = this.getPdfTemplate(pdfHTML);

        this.htmlContentbody = pdfTemplate;
        this.htmlContentbody = this.sanitizer.bypassSecurityTrustHtml(this.htmlContentbody);

        if (pdfHTML != '') {
            this.modal.show();
        }

    }

    getPdfTemplate(smsHTML) {
        let myTemplateStr = smsHTML;
        let TableList = [];
        let BankList = [];
        if (this.item.jobProductItemList != null) {
            this.item.jobProductItemList.forEach(obj => {
               // TableList.push("<tr><td><div class='mb20'><div class='bold f16 mb10'>" + obj.quoteProductType + '</div><div>' + obj.quoteProductName + "(" + obj.quoteProductModelNumber + ")" + "</div></div></td><td>" + obj.quoteProductQuantity + "</td></tr>");
                TableList.push("<tr><td style='border: 1px solid #BFD5F5; border-width:0 1px 1px;'><div class='mb20'><div class='medium mb10 f16'>"+ obj.quoteProductType +"</div>"+ obj.quoteProductName +"</div></td><td style='border: 1px solid #BFD5F5; border-width:0 0 1px;' class='f16 medium'>₹ "+this.item.actualSystemCost+"</td></tr>");
            })
        }
        if (this.QuoteMode == 0) {
            if (this.item.actualSystemCost != null) {
                //TableList.push("<tr><td>" + 'Actual Amount' + "</td><td>" + this.item.actualSystemCost + "</td></tr>");
            }
            if (this.item.subcidy20 != null) {
                //TableList.push("<tr><td>" + 'subcidy20' + "</td><td>" + this.item.subcidy20 + "</td></tr>");
                TableList.push("<tr><td style='border: 1px solid #BFD5F5; border-width:0 1px 0;'>Subsidy 40%</td><td class=''>₹ "+ this.item.subcidy40 +"</td></tr>");
            }
            if (this.item.subcidy40 != null) {
                //TableList.push("<tr><td>" + 'subcidy40' + "</td><td>" + this.item.subcidy40 + "</td></tr>");
                TableList.push("<tr><td style='border: 1px solid #BFD5F5; border-width:0 1px 0;'>Subsidy 20%</td><td class=''>₹ "+ this.item.subcidy20 +"</td></tr>");
            }
        }

        if (this.item.jobVariationItemList != null) {
            //this.item.jobVariationItemList.forEach(obj => {
            //    TableList.push("<tr><td>" + obj.quoteVarationName + "</td><td>" + obj.quoteVarationCost + "</td></tr>");
            //})
        }
        let QrcodeImg;
        if (this.item.organizationbankDetails != null) {
            
            this.item.organizationbankDetails.forEach(obj => {
                QrcodeImg = '';
                //BankList.push("<div class='payment-panel'><div class='pay-content'>Bank : " + obj.orgBankName + "</div><div class='pay-content'>A/C Name : " + obj.orgBankBrachName + "</div><div class='pay-content'>A/C No. : " + obj.orgBankAccountNumber + "</div><div class='pay-content'>IFSC : " + obj.orgBankIFSC_Code + "</div></div>");
                BankList.push("<tr><td class='bold'>Bank :</td><td>" + obj.orgBankName + "</td></tr>");
                BankList.push("<tr><td class='bold'>A/C Name :</td><td>" + obj.orgBankBrachName + "</td></tr>")
                BankList.push("<tr><td class='bold'>A/C No. :</td><td>" + obj.orgBankAccountNumber + "</td></tr>")
                BankList.push("<tr><td class='bold'>IFSC :</td><td>" + obj.orgBankIFSC_Code + "</td></tr>")
                //BankList.push("<div class='payment-panel'><div class='pay-content'>Bank : " + obj.orgBankName + "</div><div class='pay-content'>A/C Name : " + obj.orgBankBrachName + "</div><div class='pay-content'>A/C No. : " + obj.orgBankAccountNumber + "</div><div class='pay-content'>IFSC : " + obj.orgBankIFSC_Code + "</div></div>");
                QrcodeImg = AppConsts.remoteServiceBaseUrl + obj.orgBankQrCode_path;
            });
            //BankList.push("<div class='payment-panel-code'><img src='" + QrcodeImg + "'></div>");
        }

        let addressValue = this.item.address1 + " " + this.item.address2; //+ ", " + this.item.lead.state + "-" + this.item.lead.postCode;
        let signdate = this._dateTimeService.getStartOfDayForDate(this.item.quotateSignedDate)
        let generatedate = this.item.quoteGenerateDate;
        let currentdate = this._dateTimeService.getUTCDate().day + '/' + this._dateTimeService.getUTCDate().month + '/' + this._dateTimeService.getUTCDate().year
        
        let myVariables = {
            Customer: {
                Name: this.item.customerName,
                Address: addressValue,
                Mobile: this.item.mobile,
                Email: this.item.email,
                // Phone: this.item.leads.alt_Phone,
                // SalesRep: this.item.currentAssignUserName
            },
            Quote: {
                QuoteNumber: this.item.quoteNumber,
                QuotaSigned: this.item.isQuotateSigned,
                QuoteSinedDate: this.item.quotateSignedDate == undefined ? currentdate : signdate.day + "/" + signdate.month + "/" + signdate.year,
                QuoteSignedImagePath: this.item.quotateSignedImagePath == undefined ? '' : '<img src=' + AppConsts.remoteServiceBaseUrl + this.item.quotateSignedImagePath + ' alt="img" >',
                SystemCapacity: this.item.pvCapacityKw != null ? this.item.pvCapacityKw + " " : this.item.pvCapacityKw,
                QuoteGenerateDate: generatedate.day + "/" + generatedate.month + "/" + generatedate.year,
                SolarAdvisorName: this.item.solarAdvisorName,
                SolarAdvisorMobile: this.item.solarAdvisorMobile,
                SolarAdvisorEmail: this.item.solarAdvisorEmail,
                DiscomName: this.item.discomName,
                HeightOfStructure: this.item.heightOfStructure,
                DivisionName: this.item.divisionName,
                QuoteNotes: this.item.quoteNotes,
                BillToPay: this.item.billToPay,
                DepositeRequired: this.item.depositeRequired,
                ActualSystemCost: this.item.actualSystemCost,
                subcidy20: this.item.subcidy20,
                subcidy40: this.item.subcidy40,
                PanelName: this.item.jobProductItemList.length == 0 ? "" : this.item.jobProductItemList[0]["quoteProductName"],                
                InverName: this.item.jobProductItemList.length == 0 ? "" : this.item.jobProductItemList[1]["quoteProductName"]
            },
            QuoteProduct: {
                QuoteTable: TableList.toString().replace(/,/g, ''),
            },
            Organization: {
                orgName: this.item.organizationDetails.length == 0 ? "" : this.item.organizationDetails[0]['displayName'],
                orgEmail: this.item.organizationDetails.length == 0 ? "" : this.item.organizationDetails[0]['organization_Email'],
                orgMobile: this.item.organizationDetails.length == 0 ? "" : this.item.organizationDetails[0]['organization_Mobile'],
                orgLogo: this.item.organizationDetails.length == 0 ? "" : AppConsts.remoteServiceBaseUrl + this.item.organizationDetails[0]['organization_LogoFilePath'],  //this.item.organizationDetails[0]['organization_GSTNumber']
                orgWebsite: this.item.organizationDetails.length == 0 ? "" : this.item.organizationDetails[0]['organization_Website'],
                bankDetails: BankList.toString().replace(/,/g, ''),
                QrCode:QrcodeImg
            },
        }

        // use custom delimiter {{ }}
        // _.templateSettings.interpolate = /{{([\s\S]+?)}}/g;
        // 
        // // interpolate
        // let compiled = _.template(myTemplateStr);
        // let myTemplateCompiled = compiled(myVariables);        
        _.templateSettings.interpolate = /{{([\s\S]+?)}}/g;

        // interpolate
        let compiled = _.template(myTemplateStr);
        let myTemplateCompiled = compiled(myVariables);
        return myTemplateCompiled;
    }
    generatePdf() {
        this.spinner.show();
        this.htmlstr = this.htmlContentbody.changingThisBreaksApplicationSecurity; // this.teststr.toString().replace('"',"'");// this.htmlContentbidy.changingThisBreaksApplicationSecurity;
        this._quoteServiceProxy.generatePdf(this.SelectedJobNumber, this.htmlstr).subscribe(result => {
            let FileName = result;
            this.saving = false;
            this.spinner.hide();
            window.open(result, "_blank");
        }, error => { this.spinnerService.hide() });
    }
}