import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, Inject, Renderer2, ElementRef, EventEmitter, Injectable, Injector, OnInit, Optional, Output, ViewChild, Input } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';

import { DateTimeService } from '@app/shared/common/timing/date-time.service';
import { CommonLookupDto, CommonLookupServiceProxy, CreateOrEditPaymentDto, CreateOrEditPaymentReceiptItemsDto, GetPaymentForEditOutput, JobProductItemProductItemLookupTableDto, JobProductItemsServiceProxy, PaymentModeDto, PaymentServiceProxy, PaymentsServiceProxy, PaymentTypeDto, UserListDto } from '@shared/service-proxies/service-proxies';
import { DateTime } from 'luxon';
import { AbpSessionService } from 'abp-ng2-module';
import { AppConsts } from '@shared/AppConsts';
import { finalize } from 'rxjs/operators';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { DataTransferService } from '@app/shared/common/projectservice/data-transfer.service';

import { ModalDirective } from 'ngx-bootstrap/modal';
import { Paginator } from 'primeng/paginator';
import { DecimalPipe } from '@angular/common';
import { appModuleAnimation } from '@shared/animations/routerTransition';

@Component({
    selector: 'createOrEditPaymentModal',
    templateUrl: './create-or-edit-payment.component.html',
    animations: [appModuleAnimation()]
})

export class CreateOrEditPaymentModalComponent extends AppComponentBase implements OnInit {
    //@ViewChild('addPaymentModal', { static: true }) addPaymentModal: addPaymentModal;
    @ViewChild('addPaymentModal', { static: true }) modal: ModalDirective;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    documentListname: any;
    leadDocumentList: any;
    filesize: number = 1000000;
    active = false;
    saving = false;
    recordCount: any;
    leadId: any;
    filterText: string = '';
    NewPayment: boolean = false;
    PaymentMode: CommonLookupDto[];
    PaymentType: CommonLookupDto[];
    UserList: UserListDto[];
    PaymentDetails: GetPaymentForEditOutput;
    PaymentReceiptList: CreateOrEditPaymentReceiptItemsDto[]
    @Input() SelectedJobId: number = 0;
    @Input() SelectedLeadId: number = 0;
    EditproductItems: JobProductItemProductItemLookupTableDto[];
    JobProducts: any[];
    paymentDate: DateTime;

    payment: CreateOrEditPaymentDto = new CreateOrEditPaymentDto();
    jobid: number = 0;
    UploadSignDate: DateTime;
    sectionId: number = AppConsts.SectionPage.paymentReceiptUpload;
    uploadUrl: string;
    public documentGrp: FormGroup;
    public totalfiles: Array<File> = [];
    public totalFileName = [];
    public lengthCheckToaddMore = 0;
    EnterDate: DateTime = this._dateTimeService.getDate();
    constructor(
        injector: Injector,
        private _paymentServiceProxy: PaymentsServiceProxy,
        private _commonLookup: CommonLookupServiceProxy,
        private _jobProductItemProxy: JobProductItemsServiceProxy,
        private _sessionService: AbpSessionService,
        private _httpClient: HttpClient,
        private formBuilder: FormBuilder,
        private _dataTransfer: DataTransferService,
        private _dateTimeService: DateTimeService,
    ) {
        super(injector);
        this.uploadUrl = AppConsts.remoteServiceBaseUrl + '/Payment/PaymentDetailsReceiptDocument';
    }
    ngOnInit(): void {
        this.initializationpaymentReceipt();
        this.PaymentMode = [];
        this.PaymentType = [];

        this._commonLookup.getAllPaymentModeDropdown().subscribe((result) => {
            this.PaymentMode = result;
        }, error => { this.spinnerService.hide() });

        this._commonLookup.getAllPaymentTypeDropdown().subscribe((result) => {
            this.PaymentType = result;
        }, error => { this.spinnerService.hide() });
        this._commonLookup.getAllUserList().subscribe((result) => {
            this.UserList = result;
        }, error => { this.spinnerService.hide() });
        this._jobProductItemProxy.getAllProductItemForTableDropdownForEdit().subscribe(result => {
            this.EditproductItems = result;
        }, error => { this.spinnerService.hide() });
        this.PaymentDetails = new GetPaymentForEditOutput();
        this.getPaymentDetailsForEdit();
        this.getjobProductList();
    }
    keyPressNumbersWithDecimal(event) {
        var charCode = (event.which) ? event.which : event.keyCode;
        if (charCode != 46 && charCode > 31
            && (charCode < 48 || charCode > 57)) {
            event.preventDefault();
            return false;
        }
        return true;
    }
    showPayment(): void {
        this.initializationpaymentReceipt();
        this.modal.show();
    }
    getjobProductList() {
        this._jobProductItemProxy.getJobProductItemByJobId(this.SelectedJobId).subscribe(result => {
            this.JobProducts = [];
            result.map((item) => {
                var jobproductcreateOrEdit = { id: 0, productItemId: 0, productItemName: "", productTypeName: "", quantity: 0, jobId: 0, size: 0, model: "", productItems: [], productTypeId: 0 }
                jobproductcreateOrEdit.productTypeId = item.jobProductItem.productTypeId;
                jobproductcreateOrEdit.productItemId = item.jobProductItem.productItemId;
                jobproductcreateOrEdit.quantity = item.jobProductItem.quantity;
                jobproductcreateOrEdit.id = item.jobProductItem.id;
                jobproductcreateOrEdit.jobId = this.SelectedJobId;
                jobproductcreateOrEdit.size = item.size;
                jobproductcreateOrEdit.model = item.model;
                jobproductcreateOrEdit.productTypeName = item.productTypeName;
                this.JobProducts.push(jobproductcreateOrEdit);
                if (this.EditproductItems != undefined) {
                    let pitemname = this.EditproductItems.find(x => x.id == item.jobProductItem.productItemId);
                    jobproductcreateOrEdit.productItemName = pitemname.displayName;
                }
            })
            if (result.length == 0) {
                var jobproductcreateOrEdit = { id: 0, productItemId: 0, productItemName: "", productTypeName: "", quantity: 0, jobId: 0, size: 0, model: "", productItems: [], productTypeId: 0 }
                jobproductcreateOrEdit.productItems = this.EditproductItems;
                this.JobProducts.push(jobproductcreateOrEdit);
            }

        }, error => { this.spinnerService.hide() });
    }
    paymentReceiptList: any;
    getPaymentDetailsForEdit() {
        this._paymentServiceProxy.getPaymentForEdit(this.SelectedJobId).subscribe((result) => {
            this.PaymentDetails = result;
            this.PaymentDetails.paymentJobId = this.SelectedJobId;
            this.PaymentReceiptList = [];
            if (result.paymentReceiptItems.length == 0) {
                //this.PaymentReceiptList = result.paymentReceiptItems
                // this.PaymentReceiptList.push(new CreateOrEditPaymentReceiptItemsDto);
                // this.PaymentReceiptList.forEach(element => {
                //     element.paymentAddedById = this._sessionService.userId
                // });
            }
            else {

                this.NewPayment = true;
                this.paymentReceiptList = result.paymentReceiptItems;
                //this.items = this.formBuilder.array([this.createUploadDocuments()])
                result.paymentReceiptItems.forEach((element, index) => {
                    //this.items.insert(index, this.SetUploadDocuments(element))
                });

                //this.items.controls['items'].setFormValue(result.paymentReceiptItems[0].paymentPayDate);// insert(0,new FormArray(result.paymentReceiptItems.map(x => new FormArray([]))));
                // this.items.controls[0].controls['PaymentPayDate'].value = '';
            }

        }, error => { this.spinnerService.hide() });
    }
    close(): void {
        this.active = false;
        this.modal.hide();
    }
    initializationpaymentReceipt() {
        this.documentGrp = this.formBuilder.group({
            Id: 0,
            PaymentRecEnterDate: this.EnterDate,
            PaymentRecRefNo: ['', Validators.required],
            PaymentRecTotalPaid: ['', [Validators.required, Validators.pattern("^[0-9]*$")]],
            PaymentRecTypeId: [0, Validators.required],
            PaymentRecModeId: [0, Validators.required],
            PaymentReceiptName: '',
            PaymentReceiptPath: '',
            PaymentRecAddedById: [this._sessionService.userId, { disabled: true }],
            IsPaymentRecVerified: false,
            documentFile: new FormControl(File),
            items: this.formBuilder.array([this.createUploadDocuments()])
        });
    }
    createUploadDocuments(): FormGroup {
        return this.formBuilder.group({
            Id: 0,
            PaymentRecEnterDate: '',
            PaymentRecRefNo: '',
            PaymentRecTotalPaid: '',
            PaymentRecTypeId: 0,
            PaymentRecModeId: 0,
            PaymentReceiptName: '',
            PaymentReceiptPath: '',
            PaymentRecAddedById: this._sessionService.userId,
            IsPaymentRecVerified: false,
            documentFile: File
        });
    }
    SetUploadDocuments(item): FormGroup {
        return this.formBuilder.group({
            Id: item['id'],
            PaymentRecEnterDate: new Date(item['paymentRecEnterDate']),
            PaymentRecRefNo: item['paymentRecRefNo'],
            PaymentRecTotalPaid: item['paymentRecTotalPaid'],
            PaymentRecTypeId: item['paymentRecTypeId'],
            PaymentRecModeId: item['paymentRecModeId'],
            PaymentReceiptName: item['paymentReceiptName'],
            PaymentReceiptPath: item['paymentReceiptPath'],
            PaymentRecAddedById: item['paymentRecAddedById'],
            IsPaymentRecVerified: item['isPaymentRecVerified'],
            documentFile: File
        });
    }
    get items(): FormArray {
        return this.documentGrp.get('items') as FormArray;
    };

    addItem(): void {

        if (this.totalfiles.length != 0)
            //if (this.items.value[0].PaymentPayDate != "" && ((this.lengthCheckToaddMore) === (this.totalfiles.length))) {

            this.items.insert(this.items.length + 1, this.createUploadDocuments());
        this.lengthCheckToaddMore = this.lengthCheckToaddMore + 1;
        //}
    }

    removeItem(index: any) {
        if (index.id != 0) {
            this.message.confirm(
                this.l('PaymentDeleteWarningMessage', index.paymentRecTotalPaid),
                this.l('AreYouSure'),
                (isConfirmed) => {
                    if (isConfirmed) {
                        this._paymentServiceProxy.deletePaymentReceipt(index.id)
                            .subscribe(() => {
                                this.totalfiles.splice(index);
                                this.totalFileName.splice(index);
                                this.items.removeAt(index);
                                this.lengthCheckToaddMore = this.lengthCheckToaddMore - 1;
                                this.getPaymentDetailsForEdit();
                                this.notify.success(this.l('SuccessfullyDeleted'));
                            }, error => { this.spinnerService.hide() });
                    }
                });
        }
        else {
            this.totalfiles.splice(index);
            this.totalFileName.splice(index);
            this.items.removeAt(index);
            this.lengthCheckToaddMore = this.lengthCheckToaddMore - 1;
        }

    }
    public fileSelectionEvent(fileInput: any, oldIndex) {

        //console.log("oldIndex is ", oldIndex);

        if (fileInput.target.files && fileInput.target.files[0]) {
            var reader = new FileReader();
            reader.onload = (event: any) => {
            }
            if (oldIndex == 0) {
                this.totalfiles.unshift((fileInput.target.files[0]))
                this.totalFileName.unshift(fileInput.target.files[0].name)
            }
            else {
                this.totalfiles[oldIndex] = (fileInput.target.files[0]);
                this.totalFileName[oldIndex] = fileInput.target.files[0].name
            }

            reader.readAsDataURL(fileInput.target.files[0]);
        }

        if (this.totalfiles.length == 1) {
            this.lengthCheckToaddMore = 1;
        }

    }
    public singlefileSelectionEvent(fileInput: any,) {


        if (fileInput.target.files && fileInput.target.files[0]) {
            var reader = new FileReader();
            reader.onload = (event: any) => {
            }
            //if (oldIndex == 0) 
            {
                this.totalfiles.unshift((fileInput.target.files[0]))
                this.totalFileName.unshift(fileInput.target.files[0].name)
            }
            // else {
            //     this.totalfiles[oldIndex] = (fileInput.target.files[0]);
            //     this.totalFileName[oldIndex] = fileInput.target.files[0].name
            // }

            reader.readAsDataURL(fileInput.target.files[0]);
        }

        if (this.totalfiles.length == 1) {
            this.lengthCheckToaddMore = 1;
        }

    }
    downfile(filename): void {
        let FileName = AppConsts.remoteServiceBaseUrl + filename;//this.LeadDocument.filePath;
        window.open(FileName, "_blank");
    };
    OnSubmit(formValue: any): void {
        //  this.job.leadId = this.leadId;
        // this.job.signApplicationDate =  this.signApplicationDate
        this.spinnerService.show();
        this.saving = true;
        let main_form: FormData = new FormData();

        for (let j = 0; j < this.totalfiles.length; j++) {
            formValue.value.PaymentReceiptName = this.totalFileName[j]
            main_form.append(this.totalFileName[j], <File>this.totalfiles[j])
        }
        let AllFilesObj = [];
        formValue.value.PaymentRecEnterDate = this.EnterDate;
        AllFilesObj.push(formValue.value);

        main_form.append("paymentDetails", JSON.stringify(this.PaymentDetails));
        main_form.append("paymentReceiptuploadList", JSON.stringify(AllFilesObj));
        main_form.append("sectionId", this.sectionId.toString());
        main_form.append("leadId", this.SelectedLeadId.toString());
        main_form.append("organizationId", this._dataTransfer.getOrgData());
        this._httpClient
            .post<any>(this.uploadUrl, main_form)
            .pipe(finalize(() => false))
            .subscribe((response) => {
                if (response.success) {
                    this.items.clear(); // = this.formBuilder.array([this.createUploadDocuments()])
                    this.initializationpaymentReceipt();
                    this.getPaymentDetailsForEdit();
                    this.modal.hide();
                    this.notify.info(this.l('SavedSuccessfully'));
                } else if (response.error != null) {
                    this.notify.error(this.l('Failed'));
                }
            }, error => { this.spinnerService.hide() });
        this.spinnerService.hide();
    }
}
