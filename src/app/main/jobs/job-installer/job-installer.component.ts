import { Component, Injector, Input, OnInit, ViewChild } from '@angular/core';
import { PickListEditComponent } from '@app/main/tracker/picklist/pick-list-edit/pick-list-edit.component';
import { PickListGenerateComponent } from '@app/main/tracker/picklist/pick-list-generate/pick-list-generate.component';
import { DataTransferService } from '@app/shared/common/projectservice/data-transfer.service';
import { DateTimeService } from '@app/shared/common/timing/date-time.service';
import { AppConsts } from '@shared/AppConsts';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { CreateOrEditDispatchProductItemDto } from '@shared/service-proxies/service-proxies';
import { CommonLookupDto, CommonLookupServiceProxy, CreateOrEditDispatchDto, CreateOrEditJobInstallDto, DispatchTrackerServiceProxy, GetJobInstallForEditOutput, GetPickListTrackerForViewDto, JobInstallationServiceProxy, JobProductItemProductItemLookupTableDto, JobProductItemsServiceProxy, JobServiceProxy, JobVariationServiceProxy, PickListTrackerServiceProxy } from '@shared/service-proxies/service-proxies';

import { DateTime } from 'luxon';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'createOrEditJobInstallerModal',
  templateUrl: './job-installer.component.html',
  styleUrls: ['./job-installer.component.css'],
  animations: [appModuleAnimation()]
})
export class JobInstallerComponent extends AppComponentBase implements OnInit {

  @ViewChild('picklistgenerate', { static: true }) picklistgenerate: PickListGenerateComponent;
  @ViewChild('picklistedit', { static: true }) picklistedit: PickListEditComponent;
  storeLocationList: CommonLookupDto[];
  InstallerId: number = 3;
  @Input() SelectedJobId: number = 0;
  @Input() SelectedLeadId: number = 0;
  @Input() jobproductItem: any[] = [];
  drpInstallerUser: CommonLookupDto[];
  heightofStructureList: CommonLookupDto[];
  dispatchTypeList: CommonLookupDto[];
  JobInstallData: GetJobInstallForEditOutput;
  jobInstall: CreateOrEditJobInstallDto = new CreateOrEditJobInstallDto();
  saving = false;
  active = false;
  JobProductsList: any[];
  productItemSuggestions: string[];
  DispatchDetails: CreateOrEditDispatchDto;
  installDate: DateTime = this._dateTimeService.getDate();
  endDate: DateTime;
  picklstList: GetPickListTrackerForViewDto[];
  EditproductItems: JobProductItemProductItemLookupTableDto[];
  productItems: JobProductItemProductItemLookupTableDto[];
  JobProducts: any[];
  productTypes: CommonLookupDto[];
  constructor(
    injector: Injector,
    private _commonLookup: CommonLookupServiceProxy,
    private _jobInstallServiceProxy: JobInstallationServiceProxy,
    private _dateTimeService: DateTimeService,
    private _dispatchtrackerProxy: DispatchTrackerServiceProxy,
    private _picklisttrackerProxy: PickListTrackerServiceProxy,
    private _dataTransfer: DataTransferService,
    private _jobServiceProxy: JobServiceProxy,
    private _jobProductItemProxy: JobProductItemsServiceProxy,
  ) { super(injector); }

  ngOnInit(): void {
    this.JobInstallData = new GetJobInstallForEditOutput();

    this._commonLookup.getAllLocationForDropdown().subscribe((result) => {
      this.storeLocationList = result;
    }, error => { this.spinnerService.hide() });

    this._commonLookup.getEmployees(this.InstallerId).subscribe(result => {
      this.drpInstallerUser = result;
    }, error => { this.spinnerService.hide() });

    this._commonLookup.getAllHeightStructureForDropdown().subscribe(result => {
      this.heightofStructureList = result;
    }, error => { this.spinnerService.hide() });

    this._commonLookup.getAllDispatchTypeForDropdown().subscribe(result => {
      this.dispatchTypeList = result;
    }, error => { this.spinnerService.hide() });
    this._commonLookup.getAllStockCategorForDropdown().subscribe((result) => {
      if(result != undefined){
        this.productTypes = result.filter(x => x.id <= AppConsts.StockItemBind.PicklistStockDropDownBind);
      }      
    }, error => { this.spinnerService.hide() });
    this._jobProductItemProxy.getAllProductItemForTableDropdownForEdit().subscribe(result => {
      this.EditproductItems = result;
    }, error => { this.spinnerService.hide() });
    this._jobProductItemProxy.getAllProductItemForTableDropdown().subscribe(result => {
      this.productItems = result;
    }, error => { this.spinnerService.hide() });

    this.AddorEditJobInstall(this.SelectedJobId);
    this.DispatchDetails = new CreateOrEditDispatchDto();
  }
  public dispatchAll() {
    if (this.JobInstallData.id != undefined) {

      this._picklisttrackerProxy.getAll(this.SelectedJobId).subscribe((result) => {
        this.picklstList = result;
      }, error => { this.spinnerService.hide() });
    }

  }
  public filterProductItem(item, i): void {
    this.JobProducts[i].productItems = this.productItems.filter(x => x.productTypeId == item.productTypeId);
  }

  public filterProductIteams(event, i): void {
    let Id = 0;
    for (let j = 0; j < this.JobProducts.length; j++) {
      if (j == i) {
        Id = this.JobProducts[i].productTypeId
        this.JobProducts[i].model = '';
        // this.JobProducts[i].quantity= 0;
      }
    }

    this._jobServiceProxy.getProductItemList(Id, event.query).subscribe(output => {
      this.productItemSuggestions = output;
    }, error => { this.spinnerService.hide() });
  }
  public getProductTypeId(i) {
    let pItem = this.productItems.find(x => x.displayName == this.JobProducts[i].productItemName)
    this.JobProducts[i].productItemId = pItem.id;
    this.JobProducts[i].model = pItem.model;
    this.JobProducts[i].size = pItem.size;
  }
  public generatePicklist(item) {
    item.dispatchJobId = this.SelectedJobId;
    this.picklistgenerate.GenereatePicklistFile(item);
  }
  editPicklist(item) {
    item.dispatchJobId = this.SelectedJobId;
    this.picklistedit.PicklistitemEditview(item.id);
  }
  AddorEditJobInstall(JobId) {
    this.JobProductsList = [];

    //this.startDate = this._dateTimeService.getDate();
    this._jobInstallServiceProxy.getJobInstallById(JobId).subscribe((result) => {
      this.JobInstallData = result;
      this.installDate = this.JobInstallData.installStartDate == null ? this.installDate : this.JobInstallData.installStartDate;
      this.JobProductsList = this.jobproductItem;

      this.dispatchAll();
      this.initializeStockItem();
    }, error => { this.spinnerService.hide() });


  }
  initializeStockItem() {
    this.JobProducts = [];
    var jobproductcreateOrEdit = { id: 0, productItemId: 0, productItemName: "", quantity: 0, size: 0, model: "", productItems: [], productTypeId: 0 } //productItems: [],
    jobproductcreateOrEdit.productItems = this.EditproductItems;
    this.JobProducts.push(jobproductcreateOrEdit);

  }
  saveJobInstall(): void {
    this.JobInstallData.installStartDate = this.installDate;
    this.JobInstallData.jobId = this.SelectedJobId;

    this.saving = true;
    this._jobInstallServiceProxy.createOrEdit(this.JobInstallData).pipe(
      finalize(() => { this.saving = false; })
    )
      .subscribe(() => {
        this.active = true;
        this.notify.info(('SavedSuccessfully'));
        this.AddorEditJobInstall(this.SelectedJobId);
      }, error => { this.spinnerService.hide() });
  }
  deletePicklistItem(picklstList) {
    this.spinnerService.show();
    this._dispatchtrackerProxy.deleteDispatch(picklstList.id, this.SelectedLeadId, parseInt(this._dataTransfer.getOrgData())).subscribe((result) => {
      picklstList = result;
      this.spinnerService.hide();
      this.notify.info("Successfuly Deleted Item");
      this.dispatchAll();
    }, error => { this.spinnerService.hide() });

  }
  public addJobProduct(): void {
    //this.jobproductcreateOrEdit = { id: 0, productItemId: 0, productItemName: "", quantity: 0, jobId: this.jobid, size: 0, model: "", productItems: [], productTypeId: 0 }
    //this.JobProducts.push(new GetJobProductItemForEditOutput);
    this.JobProducts.push(new CreateOrEditDispatchProductItemDto());
  }

  public removeJobProduct(JobProduct): void {
    if (this.JobProducts.length == 1)
      return;
    if (this.JobProducts.indexOf(JobProduct) === -1) {
      // this.JobProducts.push(JobProduct);
    } else {
      this.JobProducts.splice(this.JobProducts.indexOf(JobProduct), 1);
    }
  }

  public saveDispatchJob(): void {
    this.saving = true;
    this.DispatchDetails.dispatchJobId = this.SelectedJobId;
    this.DispatchDetails.orgID = parseInt(this._dataTransfer.getOrgData());
    this.DispatchDetails.leadid = this.SelectedLeadId;
    this.DispatchDetails.dispathproductlist = [];
    this.JobProducts.map((item) => {
      let createOrEditDispatchProductItemDto = new CreateOrEditDispatchProductItemDto();
      createOrEditDispatchProductItemDto.productItemId = item.productItemId;
      createOrEditDispatchProductItemDto.quantity = item.quantity;
      createOrEditDispatchProductItemDto.dispatchId = this.SelectedJobId;
      createOrEditDispatchProductItemDto.id = item.id;
      if (item.productTypeId != undefined && item.productItemId != undefined && item.quantity != undefined && item.model != undefined) {
        this.DispatchDetails.dispathproductlist.push(createOrEditDispatchProductItemDto);
      }
    });
    this._dispatchtrackerProxy.createOrEdit(this.DispatchDetails).pipe(
      finalize(() => { this.saving = false; })
    ).subscribe(() => {
      this.active = true;
      this.dispatchAll();      
      this.dispatchitemclear();
      this.initializeStockItem();
      //document.getElementById('btnRefresh').click();
      this.notify.info(this.l('SavedSuccessfully'));
    }, error => { this.spinnerService.hide() });
  }
  public dispatchitemclear() {
    this.DispatchDetails.dispatchHeightStructureId = undefined;
    this.JobProducts = [];
    this.DispatchDetails.dispatchNotes = '';
  }
}
