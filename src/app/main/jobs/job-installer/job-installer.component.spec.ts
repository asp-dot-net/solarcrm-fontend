import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JobInstallerComponent } from './job-installer.component';

describe('JobInstallerComponent', () => {
  let component: JobInstallerComponent;
  let fixture: ComponentFixture<JobInstallerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JobInstallerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(JobInstallerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
