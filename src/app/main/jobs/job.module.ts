import { CommonModule,DatePipe } from '@angular/common';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule ,NO_ERRORS_SCHEMA} from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppCommonModule } from '@app/shared/common/app-common.module';
import { UtilsModule } from '@shared/utils/utils.module';
import { CountoModule } from 'angular2-counto';
import { ModalModule } from 'ngx-bootstrap/modal';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { PopoverModule } from 'ngx-bootstrap/popover';
import { JobRoutingModule } from './job-routing.module';
import { BsDatepickerConfig, BsDaterangepickerConfig, BsLocaleService } from 'ngx-bootstrap/datepicker';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { NgxBootstrapDatePickerConfigService } from 'assets/ngx-bootstrap/ngx-bootstrap-datepicker-config.service';
import { SubheaderModule } from '@app/shared/common/sub-header/subheader.module';
import { CustomizableDashboardModule } from '@app/shared/common/customizable-dashboard/customizable-dashboard.module';
//import { ManageLeadsRoutingModule } from './manageleads/manageleads-routing.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { AgmCoreModule } from '@agm/core';
import { AdminSharedModule } from '@app/admin/shared/admin-shared.module';
import { AppSharedModule } from '@app/shared/app-shared.module';
import { EmailEditorModule } from 'angular-email-editor';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { PaginatorModule } from 'primeng/paginator';
import { EditorModule } from 'primeng/editor';
import { InputMaskModule } from 'primeng/inputmask'; import { FileUploadModule } from 'primeng/fileupload';
import { TableModule } from 'primeng/table';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { JobComponent } from './job.component';
// import { JobDetailsComponent } from './jobDetails.componet';
import { jobTypeDetailModal } from './job-type-detail-modal.component';
import { LeadsModule } from '../leads/leads.module';
// import { PaymentModeModule } from '../datavaults/paymentmode/paymentmode.module';
// import { JobInstallerComponent } from './job-installer/job-installer.component';
import { HttpClientModule } from '@angular/common/http';
//import { JobQuotationComponent } from './job-quotation/job-quotation.component';
//import { JobRefundComponent } from './job-refund/job-refund.component';
//import { PdftemplateshowmodalComponent } from './pdf-template-show-modal.component';

NgxBootstrapDatePickerConfigService.registerNgxBootstrapDatePickerLocales();

@NgModule({
    imports: [
        CommonModule,
        HttpClientModule,
        FormsModule,
        ModalModule,
        TabsModule,
        TooltipModule,
        AppCommonModule,
        UtilsModule,
        JobRoutingModule,
        CountoModule,
        BsDatepickerModule.forRoot(),
        BsDropdownModule.forRoot(),
        PopoverModule.forRoot(),
        SubheaderModule,
        AdminSharedModule,
        AppSharedModule,  
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyALoDvKqgmTfpfPOEjqS2fI1kcabuxImMA',            
            libraries: ['places']
        }),
        NgSelectModule,
        EmailEditorModule,
        AutoCompleteModule,
		PaginatorModule,
		EditorModule,
		InputMaskModule,
		TableModule,
        OwlDateTimeModule,
        OwlNativeDateTimeModule,  
        LeadsModule  
    ],
    declarations: [JobComponent, jobTypeDetailModal],
    exports: [LeadsModule],
    providers: [
         { provide: BsDatepickerConfig, useFactory: NgxBootstrapDatePickerConfigService.getDatepickerConfig },
         { provide: BsDaterangepickerConfig, useFactory: NgxBootstrapDatePickerConfigService.getDaterangepickerConfig },
         { provide: BsLocaleService, useFactory: NgxBootstrapDatePickerConfigService.getDatepickerLocale },
         { provide: DatePipe }
    ],
})
export class JobModule {}
