import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JobRefundComponent } from './job-refund.component';

describe('JobRefundComponent', () => {
  let component: JobRefundComponent;
  let fixture: ComponentFixture<JobRefundComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JobRefundComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(JobRefundComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
