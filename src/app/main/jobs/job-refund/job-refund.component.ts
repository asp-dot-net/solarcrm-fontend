import { Component, EventEmitter, Injector, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { DataTransferService } from '@app/shared/common/projectservice/data-transfer.service';
import { HttpClient } from '@microsoft/signalr';
import { AppConsts } from '@shared/AppConsts';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { CommonLookupDto, CommonLookupServiceProxy, CreateOrEditJobRefundDto, GetJobRefundForEditOutput, GetJobRefundForViewDto, JobRefundServiceProxy, UserListDto } from '@shared/service-proxies/service-proxies';
import { AbpSessionService, MessageService, NotifyService } from 'abp-ng2-module';
import { DateTime } from 'luxon';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Paginator } from 'primeng/paginator';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'createOrEditJobRefundModal',
  templateUrl: './job-refund.component.html',
  styleUrls: ['./job-refund.component.css'],
  animations: [appModuleAnimation()]
})
export class JobRefundComponent extends AppComponentBase implements OnInit {
  @ViewChild('paginator', { static: true }) paginator: Paginator;
  @Input() SelectedJobId: number = 0;
  @Input() SelectedLeadId: number = 0;
  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
  documentListname: any;
  leadDocumentList: any;
  //documentLibrary: CreateOrEditDocumentLibraryDto = new CreateOrEditDocumentLibraryDto();
  filesize: number = 1000000;
  active = false;
  saving = false;
  recordCount: any;
  leadId: any;
  filterText: string = '';
  NewPayment: boolean = false;
  RefundReason: CommonLookupDto[];
  RefundPaymentType: CommonLookupDto[];
  UserList: UserListDto[];
  RefundPaymentDetails: GetJobRefundForEditOutput;
  PaymentReceiptList: GetJobRefundForViewDto[];
  JobProducts: any[];
  paymentDate: DateTime;
  paymentRefund: CreateOrEditJobRefundDto = new CreateOrEditJobRefundDto();
  jobid: number = 0;
  UploadSignDate: DateTime;
  sectionId: number = 39;
  uploadUrl: string;
  notify: NotifyService;
  constructor(
    injector: Injector,
    private _paymentRefundServiceProxy: JobRefundServiceProxy,
    private _commonLookup: CommonLookupServiceProxy,
    private _sessionService: AbpSessionService,
    private formBuilder: FormBuilder,
    private _dataTransfer: DataTransferService,
  ) {
    super(injector);
    this.uploadUrl = AppConsts.remoteServiceBaseUrl + '/Payment/PaymentDetailsReceiptDocument';
  }

  ngOnInit(): void {
    this.RefundReason = [];
    this.RefundPaymentType = [];

    this._commonLookup.getAllJobRefundForDropdown().subscribe((result) => {
      this.RefundReason = result;
    }, error => { });

    this._commonLookup.getAllPaymentTypeDropdown().subscribe((result) => {
      this.RefundPaymentType = result;
    }, error => { });

    this._commonLookup.getAllUserList().subscribe((result) => {
      this.UserList = result;
    }, error => { });

    this.initializationpaymentReceipt();
    this.getRefundPaymentList();
  }
  @ViewChild('addRefundPaymentModal', { static: true }) modal: ModalDirective;
  public documentGrp: FormGroup;
  paymentRefundList: any;
  getRefundPaymentList() {
    this._paymentRefundServiceProxy.getAll(this.SelectedJobId).subscribe((result) => {
      this.paymentRefundList = [];
      this.NewPayment = true;
      this.paymentRefundList = result;
    }, error => { });
  }
  keyPressNumbersWithDecimal(event) {
    var charCode = (event.which) ? event.which : event.keyCode;
    if (charCode != 46 && charCode > 31
      && (charCode < 48 || charCode > 57)) {
      event.preventDefault();
      return false;
    }
    return true;
  }
  keyPressNumbers(event) {
    var charCode = (event.which) ? event.which : event.keyCode;
    // Only Numbers 0-9
    if ((charCode < 48 || charCode > 57)) {
      event.preventDefault();
      return false;
    } else {
      return true;
    }
  }
  initializationpaymentReceipt() {
    this.documentGrp = this.formBuilder.group({
      Id: 0,
      RefundJobId: this.SelectedJobId,
      RefundReasonId: [0, Validators.required],
      PaymentTypeId: [0, Validators.required],
      RefundPaidAmount: ['', [Validators.required, Validators.pattern("^[0-9]*$")]],
      RefundPaidDate: '',
      RefundPaidNotes: '',
      CreatorUserId: [this._sessionService.userId, { disabled: true }],
      IsRefundVerify: false,
      RefundVerifyDate: '',
      RefundVerifyNotes: '',
      RefundVerifyById: 0,
      BankAccountNumber: 0,
      BankAccountHolderName: '',
      BankBrachName: '',
      BankName: '',
      IFSC_Code: '',
      RefundRecPath: '',
      ReceiptNo: ['', Validators.required],
      JobRefundEmailSend: 0,
      JobRefundEmailSendDate: '',
      JobRefundSmsSend: 0,
      JobRefundSmsSendDate: '',
      leadId: this.SelectedLeadId,
      OrgId: this._dataTransfer.getOrgData(),
      // PaymentRecAddedById: [this._sessionService.userId, { disabled: true }],      
      documentFile: new FormControl(File),

    });
  }
  editrefundPayment(item) {
    this.documentGrp.patchValue({
      Id: item.id,
      RefundJobId: this.SelectedJobId,
      RefundReasonId: item.refundReasonId,
      PaymentTypeId: item.paymentTypeId,
      RefundPaidAmount: item.refundPaidAmount,
      RefundPaidDate: item.refundPaidDate,
      RefundPaidNotes: item.refundPaidNotes,
      CreatorUserId: [this._sessionService.userId, { disabled: true }],
      IsRefundVerify: item.isRefundVerify,
      RefundVerifyDate: item.refundVerifyDate,
      RefundVerifyNotes: item.refundVerifyNotes,
      RefundVerifyById: item.refundVerifyById,
      BankAccountNumber: item.bankAccountNumber,
      BankAccountHolderName: item.bankAccountHolderName,
      BankBrachName: item.bankBrachName,
      BankName: item.bankName,
      IFSC_Code: item.iFSC_Code,
      RefundRecPath: item.refundRecPath,
      ReceiptNo: item.receiptNo,
      JobRefundEmailSend: 0,
      JobRefundEmailSendDate: null,
      JobRefundSmsSend: 0,
      JobRefundSmsSendDate: null,
      leadId: this.SelectedLeadId,
      OrgId: 1,
    });
    this.modal.show();
  }

  deleteRefundItem(index: any) {
    if (index.id != 0) {
      this.message.confirm(
        this.l('PaymentDeleteWarningMessage', index.paymentRecTotalPaid),
        this.l('AreYouSure'),
        (isConfirmed) => {
          if (isConfirmed) {
            this._paymentRefundServiceProxy.delete(index.id)
              .subscribe(() => {
                // this.totalfiles.splice(index);
                // this.totalFileName.splice(index);
                // // this.items.removeAt(index);
                // this.lengthCheckToaddMore = this.lengthCheckToaddMore - 1;
                this.getRefundPaymentList();
                this.notify.success(this.l('SuccessfullyDeleted'));
              }, error => { this.spinnerService.hide() });
          }
        });
    }
  }
  addRefundPayment() {
    this.modal.show();
  }
  close(): void {
    this.active = false;
    this.modal.hide();
  }
  public totalfiles: Array<File> = [];
  public totalFileName = [];
  public lengthCheckToaddMore = 0;
  public singlefileSelectionEvent(fileInput: any,) {
    if (fileInput.target.files && fileInput.target.files[0]) {
      var reader = new FileReader();
      reader.onload = (event: any) => {
      }
      //if (oldIndex == 0) 
      {
        this.totalfiles.unshift((fileInput.target.files[0]))
        this.totalFileName.unshift(fileInput.target.files[0].name)
      }
      reader.readAsDataURL(fileInput.target.files[0]);
    }

    if (this.totalfiles.length == 1) {
      this.lengthCheckToaddMore = 1;
    }

  }
  downfile(filename): void {
    let FileName = AppConsts.remoteServiceBaseUrl + filename;//this.LeadDocument.filePath;
    window.open(FileName, "_blank");
  };
  OnSubmit(formValue: any): void {
    this.saving = true;

    this._paymentRefundServiceProxy.createOrEdit(formValue.value).pipe(
      finalize(() => { this.saving = false; })
    )
      .subscribe(() => {
        this.active = true;
        this.getRefundPaymentList();
        this.notify.info(('SavedSuccessfully'));
        this.modal.hide();
      }, error => { this.spinnerService.hide() });

  }
  OnSubmit1(formValue: any): void {
    //  this.job.leadId = this.leadId;
    // this.job.signApplicationDate =  this.signApplicationDate
    //this.spinnerService.show();
    this.saving = true;

    // const formData: FormData = new FormData();
    // const file = this.fileToUpload;

    let main_form: FormData = new FormData();

    for (let j = 0; j < this.totalfiles.length; j++) {
      //console.log("the values is ", <File>this.totalfiles[j]);
      //console.log("the name is ", this.totalFileName[j]);
      formValue.value.PaymentReceiptName = this.totalFileName[j]
      main_form.append(this.totalFileName[j], <File>this.totalfiles[j])
    }
    //console.log(formValue.items)

    let AllFilesObj = [];

    AllFilesObj.push(formValue.value);

    main_form.append("paymentDetails", JSON.stringify(this.RefundPaymentDetails));
    main_form.append("paymentReceiptuploadList", JSON.stringify(AllFilesObj));
    main_form.append("sectionId", this.sectionId.toString());
    main_form.append("leadId", this.SelectedLeadId.toString());
    main_form.append("organizationId", this._dataTransfer.getOrgData());
    // this._httpClient
    //     .post<any>(this.uploadUrl, main_form)
    //     .pipe(finalize(() => false))
    //     .subscribe((response) => {
    //         if (response.success) {
    //             this.items.clear(); // = this.formBuilder.array([this.createUploadDocuments()])
    //             this.initializationpaymentReceipt();
    //             this.getPaymentDetailsForEdit();
    //             this.modal.hide();
    //             this.notify.info(this.l('SavedSuccessfully'));
    //         } else if (response.error != null) {
    //             this.notify.error(this.l('Failed'));
    //         }
    //     }, error => { this.spinnerService.hide() });
    // this.spinnerService.hide();
  }
}
