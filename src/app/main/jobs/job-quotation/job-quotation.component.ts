import { Component, Injector, Input, OnInit, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { CreateOrEditJobDto, CreateOrEditQuotationDataDto, QuotationDataDto, QuotationsServiceProxy } from '@shared/service-proxies/service-proxies';
import { NgxSpinnerService } from 'ngx-spinner';
import { LazyLoadEvent } from 'primeng/api';
import { Paginator } from 'primeng/paginator';
import { Table } from 'primeng/table';
import { finalize } from 'rxjs/operators';
import { AppConsts } from '@shared/AppConsts';
import { PdfTemplateShowModalComponent } from '../pdf-template-show-model.component';
import { DataTransferService } from '@app/shared/common/projectservice/data-transfer.service';
import { appModuleAnimation } from '@shared/animations/routerTransition';
@Component({
  selector: 'createOrEditJobQuotationModal',
  templateUrl: './job-quotation.component.html',
  styleUrls: ['./job-quotation.component.css'],
  animations: [appModuleAnimation()]
})
export class JobQuotationComponent extends AppComponentBase {
  @Input() SelectedJobId: number = 0;
  @Input() SelectedLeadId:number = 0;
  @Input() selectedJobNo:number = 0;
  @Input() JobProducts: any[];
  @Input() JobVariations: any[];
  @Input() Subcidy40: 0;
  @Input() Subcidy20: 0;
  @Input() SelectedLoanOption:0;
  @Input()jobItem: CreateOrEditJobDto = new CreateOrEditJobDto();
  @ViewChild('paginator', { static: true }) paginator: Paginator;
  @ViewChild('dataTable', { static: true }) dataTable: Table;
  QuoteData: QuotationDataDto[];
  active = false;
  saving = false;
  recordCount: any;
  isPDFshow = false;
  @ViewChild('pdfTemplateModel', { static: true }) pdfTemplateModel: PdfTemplateShowModalComponent;
 
  constructor(
    injector: Injector,
    private _quotationServiceProxy: QuotationsServiceProxy,
    private _spinner: NgxSpinnerService,
    private _dataTransfer: DataTransferService
  ) {
    super(injector);
  }
  leadActivityConst: any;
  leadQuoteModeConst: any;
  ngOnInit(): void {    
    this.leadActivityConst = AppConsts.leadActivity;
    this.leadQuoteModeConst = AppConsts.QuoteModeType;
  }
  QuoteItemList: QuotationDataDto[];
  GetQuotationList(event?: LazyLoadEvent) {
    this._quotationServiceProxy.getAll(
      this.SelectedJobId
    ).subscribe((result) => {      
      this.primengTableHelper.totalRecordsCount = result.totalCount;
      this.primengTableHelper.records = result.items;
      this.primengTableHelper.hideLoadingIndicator();
      this.recordCount = result.totalCount;
    }, error => { this._spinner.hide() });
  }
  
   showpdfTemplate(quoteId, quoteMode) {
      this.isPDFshow = true;
      this.pdfTemplateModel.show(quoteId , quoteMode);
  }
  QuoteGenerate(quotemode) {    
    this.saving = true;
    this._spinner.show();
    try {
      if (this.saving) {
        let addquoteGenerate: CreateOrEditQuotationDataDto = new CreateOrEditQuotationDataDto();
        addquoteGenerate.jobId = this.SelectedJobId;
        addquoteGenerate.quoteMode = quotemode;
        addquoteGenerate.isSigned = false;
        addquoteGenerate.subcidy40 = this.Subcidy40;
        addquoteGenerate.subcidy20 = this.Subcidy20;
        addquoteGenerate.billToPay = quotemode == 0 ? this.jobItem["job"].totalJobCost : this.jobItem["job"].netSystemCost ;
        addquoteGenerate.jobProductItemList = this.JobProducts;
        addquoteGenerate.jobVariationItemList = this.JobVariations;
        addquoteGenerate.organizationUnitId = parseInt(this._dataTransfer.getOrgData());
        this._quotationServiceProxy.createOrEdit(addquoteGenerate).pipe(finalize(() => { this.saving = false; }))
          .subscribe(() => {
            this.saving = false;
            this._spinner.hide()
            this.GetQuotationList();
          }, error => { this._spinner.hide(); });
      }
    } catch (error) {
      this._spinner.hide();
    }

  }
}
