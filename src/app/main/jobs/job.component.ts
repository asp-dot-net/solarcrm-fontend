import { Component, Injector, Input, ViewEncapsulation, ViewChild, HostListener } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { CommonLookupDto, CommonLookupServiceProxy, DistrictLookupTableDto, JobSummaryCount, OrganizationUnitDto, TalukaLookupTableDto, TokenAuthServiceProxy, UserListDto } from '@shared/service-proxies/service-proxies';
import { Table } from 'primeng/table';
import { Paginator } from 'primeng/paginator';
import { FileDownloadService } from '@shared/utils/file-download.service';
import * as _ from 'lodash';
import { Title } from '@angular/platform-browser';
import { LazyLoadEvent } from 'primeng/api';
import { JobServiceProxy, JobDto, TransactionTypeDto } from '@shared/service-proxies/service-proxies';
import { jobTypeDetailModal } from './job-type-detail-modal.component';
import { ViewMyLeadComponent } from '../leads/customerleads/view-mylead.component';
import { AddActivityModalComponent } from '../leads/leadactivity/add-activity-model.component';
import { DateTime } from 'luxon';
import { DateTimeService } from '@app/shared/common/timing/date-time.service';
import { DataTransferService } from '@app/shared/common/projectservice/data-transfer.service';
import { AppConsts } from '@shared/AppConsts';

@Component({
    templateUrl: './job.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()],
})
export class JobComponent extends AppComponentBase {
    @ViewChild('jobTypeDetailModal', { static: true }) jobTypeDetailModal: jobTypeDetailModal;

    @ViewChild('viewLeadDetail', { static: true }) viewLeadDetail: ViewMyLeadComponent;
    @ViewChild('addActivityModal', { static: true }) addActivityModal: AddActivityModalComponent;
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    @Input() SectionName: string = AppConsts.SectionPage.ManageLead.toString();
    @Input() SelectedLeadId: number = 0;
    leadName: string;
    flag = true;
    shouldShow: boolean = false;
    advancedFiltersAreShown = false;
    FiltersData = false;
    active = false;
    saving = false;
    recordCount: any;
    leadId: any;
    filterText: string = '';
    date: Date = new Date('01/01/2022');
    PageNo: number;
    allOrganizationUnits: OrganizationUnitDto[];
    allOrganizationUnitsList: OrganizationUnitDto[];
    organizationUnitlength: number = 0;
    organizationUnit = 0;
    applicationstatus: number = 0;
    leadstatuss: CommonLookupDto[];
    allStates: CommonLookupDto[];
    allapplicationStatus: CommonLookupDto[];
    // allLeadSources: CommonLookupDto[];
    allSolarType: CommonLookupDto[];
    allEmployeeList: UserListDto[];
    allDiscom: CommonLookupDto[];
    allDivision: CommonLookupDto[];
    allSubDivision: CommonLookupDto[];
    allTenderData: CommonLookupDto[];
    allCircleData: CommonLookupDto[];
    //filter variable
    leadStatusIDS = [];
    leadSourceIdFilter = [];
    leadApplicationStatusFilter = [];
    employeeIdFilter = [];
    DiscomIdFilter = [];
    CircleIdFilter = [];
    DivisionIdFilter = [];
    SubDivisionIdFilter = [];
    solarTypeIdFilter = [];
    districtIdFilter = [];
    talukaIdFilter = [];
    cityIdFilter = [];
    TenderIdFilter = [];
    FilterbyDate = '';
    StartDate: DateTime = this._dateTimeService.getStartOfDayForDate(this.date);
    EndDate: DateTime = this._dateTimeService.getDate();
    sectionId: number = AppConsts.SectionPage.ManageLead;
    ExpandedView: boolean = true;
    drpdistrict: DistrictLookupTableDto[];
    drptaluka: TalukaLookupTableDto[];
    drpcity: CommonLookupDto[];
    jobTypeSummaryCount: JobSummaryCount;
    public screenWidth: any;
    public screenHeight: any;
    testHeight = 200;

    constructor(
        injector: Injector,
        private _dateTimeService: DateTimeService,
        private _jobsServiceProxy: JobServiceProxy,
        private _commonLookupService: CommonLookupServiceProxy,
        private _dataTransfer: DataTransferService,
        private _fileDownloadService: FileDownloadService,
    ) {
        super(injector);

    }
    show1: boolean = true;
    toggle: boolean = true;

    toggleBlock() {
        this.show1 = !this.show;
    };
    change() {
        this.toggle = !this.toggle;
    }
    @HostListener('window:resize', ['$event'])
    onResize(event) {
        this.screenHeight = window.innerHeight;
    }
    testHeightSize() {
        if (this.FiltersData == true) {
            this.testHeight = this.testHeight + 80;
        }
        else {
            this.testHeight = this.testHeight - 80;
        }
    }
    ngOnInit(): void {
        this.jobTypeSummaryCount = new JobSummaryCount;
        this.screenHeight = window.innerHeight;
        //this.isChangefalse = true;
        this._commonLookupService.getAllApplicationStatusForDropdown().subscribe(result => {
            this.allapplicationStatus = result;
        }, error => { this.spinnerService.hide() });
        this._commonLookupService.getAllLeadStatusDropdown().subscribe(result => {
            this.leadstatuss = result;
        }, error => { this.spinnerService.hide() });
        this._commonLookupService.getAllDiscomFilterDropdown(0).subscribe(result => {
            this.allDiscom = result;
        }, error => { this.spinnerService.hide() });

        this._commonLookupService.getAllSubdivisionWiseData('').subscribe(result => {
            this.allSubDivision = result;
        }, error => { this.spinnerService.hide() });
        this._commonLookupService.getAllSolarTypeForDropdown().subscribe(result => {
            this.allSolarType = result;
        }, error => { this.spinnerService.hide() });
        this._commonLookupService.getAllUserList().subscribe(result => {
            this.allEmployeeList = result;
        }, error => { this.spinnerService.hide() });
        this._commonLookupService.getOrganizationUnitUserWise().subscribe(output => {
            this.allOrganizationUnits = output;
            this.organizationUnit = this.allOrganizationUnits.length == 0 ? 0: this.allOrganizationUnits[0].id;
            this.organizationUnitlength = this.allOrganizationUnits.length;

            //change Organization
            this.allOrganizationUnitsList = this.allOrganizationUnits.filter(item => item.id != this.organizationUnit);
            this.getJobType();
        }, error => { this.spinnerService.hide() })
        this._commonLookupService.getAllDistrictForDropdown(0).subscribe(result => {
            if (result.length > 0) {
                this.drpdistrict = result;
            }
        }, error => { this.spinnerService.hide() })

    }
   
    // changeDistrict(event) {
    //     if (this.districtIdFilter != null) {
    //         this.gettaluka(this.districtIdFilter)
    //     }
    //     else {
    //         //alert("Please First Add the State Name");
    //         this.drptaluka = [];
    //         this.drpcity = [];
    //     }
    // }
    gettaluka() {
        this.drptaluka = [];
        this._commonLookupService.getAllTalukaForDropdown(this.districtIdFilter).subscribe(result => {
            this.drptaluka = result;
            //this.manageLeads.talukaId = result[0]['id'];
            //this.getDistrict(result[0]['value'])

        }, error => { this.spinnerService.hide() });
    }
    changeTaluka(event) {
        if (event.target.selectedIndex > 0) {
        }
    }
    chaneSolarType() {
        this.allTenderData = undefined;
        this._commonLookupService.getSolarTypeWiseTenderForDropdown(this.solarTypeIdFilter).subscribe(result => {
            this.allTenderData = result;
            if (this.allTenderData.length == 0) {
                this.allTenderData = undefined;
            }
        }, error => { this.spinnerService.hide() });
    }
    changeDiscom() {
        this.allCircleData = undefined;
        if (this.DiscomIdFilter.length > 0) {

            this._commonLookupService.getDiscomWiseCircleFilterForDropdown(this.DiscomIdFilter).subscribe(result => {
                this.allCircleData = result;
                if (this.allCircleData.length == 0) {
                    this.allCircleData = undefined;
                }
            }, error => { this.spinnerService.hide() });
        }
    }
    changeCircle() {
        this.allDivision = undefined;
        if (this.CircleIdFilter.length > 0) {
            this._commonLookupService.getCircleWiseDivisionFilterForDropdown(this.CircleIdFilter).subscribe(result => {
                this.allDivision = result;
                if (this.allDivision.length == 0) {
                    this.allDivision = undefined
                }
            }, error => { this.spinnerService.hide() });
        }
    }
    changeDivision() {
        this.allSubDivision = undefined;
        if (this.CircleIdFilter.length > 0) {
            this._commonLookupService.getDivisionWiseSubDivisionFilterForDropdown(this.DivisionIdFilter).subscribe(result => {
                this.allSubDivision = result;
                if (this.allSubDivision.length == 0) {
                    this.allSubDivision = undefined
                }
            }, error => { this.spinnerService.hide() });
        }
    }
    leadstatuschange() {
        if (this.leadStatusIDS.includes(6)) {
            //this.showJobStatus = true;
        } else {
            //this.showJobStatus = false;
            //this.jobStatusID = [];
        }
    }
    getJobType(event?: LazyLoadEvent) {
        this._dataTransfer.setOrgData(this.organizationUnit.toString());
        this.PageNo = this.primengTableHelper.getSkipCount(this.paginator, event);
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            //return;
        }
        this.primengTableHelper.showLoadingIndicator();
        this._jobsServiceProxy.getAll(
            this.filterText,
            this.organizationUnit,
            this.leadStatusIDS,
            this.employeeIdFilter,
            this.leadApplicationStatusFilter,
            this.DiscomIdFilter,
            this.CircleIdFilter,
            this.DivisionIdFilter,
            this.SubDivisionIdFilter,
            this.solarTypeIdFilter,
            this.TenderIdFilter,
            undefined,
            this.districtIdFilter,
            this.talukaIdFilter,
            this.cityIdFilter,
            undefined,
            this.FilterbyDate,
            this.StartDate,
            this.EndDate,
            undefined,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe((result) => {
            if (result.items.length > 0) {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items;
                this.primengTableHelper.hideLoadingIndicator();
                this.recordCount = result.totalCount;
                if(result.items.length > 0){
                    this.jobTypeSummaryCount =  result.items[0].jobSummaryCount;
                }                
                this.shouldShow = false;

                if (this.flag == false && result.totalCount != 0 && this.ExpandedView == false) {
                    this.navigateToLeadDetail(this.SelectedLeadId)
                }
                else {
                    if (this.ExpandedView == false && result.totalCount != 0 && result.items.length > 0) {
                        this.navigateToLeadDetail(result.items[0].job.leaddata)
                    }
                    else {
                        this.ExpandedView = true;
                    }
                }
            }
            this.spinnerService.hide();
        }, error => { this.spinnerService.hide(); });
    }

    reloadPage(flafValue: boolean): void {

        this.ExpandedView = true;
        this.flag = flafValue;
        this.paginator.changePage(this.paginator.getPage());
        if (!flafValue) {
            this.navigateToLeadDetail(this.SelectedLeadId);
        }
        this.paginator.changePage(this.paginator.getPage());
    }
    navigateToLeadDetail(jobdata): void {

        this.ExpandedView = !this.ExpandedView;
        this.ExpandedView = false;
        this.SelectedLeadId = jobdata.job.leaddata.id;
        this._dataTransfer.setLeadData(jobdata.job.leaddata);
        this.leadName = "jobtype"
        this.viewLeadDetail.showDetail(jobdata.job.leaddata, jobdata.job.applicationStatusdata, this.leadName, this.sectionId);
    }
    exportToExcel(): void {
        this._jobsServiceProxy.getJobsToExcel(
            this.filterText,
            this.organizationUnit,
            this.leadStatusIDS,
            this.employeeIdFilter,
            this.leadApplicationStatusFilter,
            this.DiscomIdFilter,
            this.CircleIdFilter,
            this.DivisionIdFilter,
            this.SubDivisionIdFilter,
            this.solarTypeIdFilter,
            this.TenderIdFilter,
            undefined,
            this.districtIdFilter,
            this.talukaIdFilter,
            this.cityIdFilter,
            undefined,
            this.FilterbyDate,
            this.StartDate,
            this.EndDate,
            undefined,
            this.primengTableHelper.getSorting(this.dataTable),
            undefined,
            undefined
        )
            .subscribe(result => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }
    show(leadId?: number, param?: number, sectionId?: number): void {
        this.leadId = leadId;
        // this._leadsServiceProxy.leadDocumentList(this.bindDocumentFilter).subscribe(result => {
        //     console.log(result);
        //     this.primengTableHelper.totalRecordsCount = result.totalCount;
        //     this.primengTableHelper.records = result.items;
        //     this.primengTableHelper.hideLoadingIndicator();
        //     this.recordCount = result.totalCount;
        //     //this.leadDocumentList = result;            
        // });
        //this.getJobType();
    }
    showJobTypeDetails(): void {
        this.jobTypeDetailModal.show();
    }
    clear() {
        this.filterText = '';
        this.leadStatusIDS = [];
        this.leadApplicationStatusFilter = [];
        this.DiscomIdFilter = [];
        this.CircleIdFilter = [];
        this.DivisionIdFilter = [];
        this.SubDivisionIdFilter = [];
        this.solarTypeIdFilter = [];
        this.employeeIdFilter = [];
        this.districtIdFilter = [];
        this.talukaIdFilter = [];
        this.cityIdFilter = [];        
        this.FilterbyDate = '';
        this.StartDate = this._dateTimeService.getStartOfDayForDate(this.date);
        this.EndDate = this._dateTimeService.getDate();
        this.getJobType();
    }
    expandGrid() {
        this.ExpandedView = true;
    }

}
