import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateOrEditPaymentModalComponent } from './create-or-edit-payment.component';
import { JobInstallerComponent } from './job-installer/job-installer.component';
import { JobRefundComponent } from './job-refund/job-refund.component';
import { JobComponent } from './job.component';
import { PdfTemplateShowModalComponent } from './pdf-template-show-model.component';

const routes: Routes = [
    {
        path: '',
        component: JobComponent,
        pathMatch: 'full',
    },
    { path: 'pdfTemplateModel', component: PdfTemplateShowModalComponent },
    { path: 'CreateOrEditPaymentModel', component: CreateOrEditPaymentModalComponent },
    { path: 'createOrEditJobInstallerModal', component: JobInstallerComponent },
    { path: 'createOrEditJobRefundModal', component: JobRefundComponent },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})

export class JobRoutingModule { }