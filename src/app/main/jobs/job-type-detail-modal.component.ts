import { Component, Injector, ViewChild } from '@angular/core';
import { DateTimeService } from '@app/shared/common/timing/date-time.service';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { AuditLogListDto } from '@shared/service-proxies/service-proxies';
import { DateTime } from 'luxon';
import { ModalDirective } from 'ngx-bootstrap/modal';

@Component({
    selector: 'jobTypeDetailModal',
    templateUrl: './job-type-detail-modal.component.html',
    animations: [appModuleAnimation()]
})
export class jobTypeDetailModal extends AppComponentBase {
    @ViewChild('jobTypeDetailModal', { static: true }) modal: ModalDirective;

    active = false;
    //auditLog: AuditLogListDto;

    constructor(injector: Injector, private _dateTimeService: DateTimeService) {
        super(injector);
    }

    show() {
        this.modal.show();        
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
