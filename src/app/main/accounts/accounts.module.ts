import { CommonModule,DatePipe } from '@angular/common';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule ,NO_ERRORS_SCHEMA} from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppCommonModule } from '@app/shared/common/app-common.module';
import { UtilsModule } from '@shared/utils/utils.module';
import { CountoModule } from 'angular2-counto';
import { ModalModule } from 'ngx-bootstrap/modal';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { PopoverModule } from 'ngx-bootstrap/popover';

import { AccountsRoutingModule } from './accounts-routing.module';
import { BsDatepickerConfig, BsDaterangepickerConfig, BsLocaleService } from 'ngx-bootstrap/datepicker';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { NgxBootstrapDatePickerConfigService } from 'assets/ngx-bootstrap/ngx-bootstrap-datepicker-config.service';
import { SubheaderModule } from '@app/shared/common/sub-header/subheader.module';

import { CustomizableDashboardModule } from '@app/shared/common/customizable-dashboard/customizable-dashboard.module';

import { AgmCoreModule } from '@agm/core';
import { AdminSharedModule } from '@app/admin/shared/admin-shared.module';
import { AppSharedModule } from '@app/shared/app-shared.module';
import { EmailEditorModule } from 'angular-email-editor';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { PaginatorModule } from 'primeng/paginator';
import { EditorModule } from 'primeng/editor';
import { InputMaskModule } from 'primeng/inputmask'; import { FileUploadModule } from 'primeng/fileupload';
import { TableModule } from 'primeng/table';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { LeadsModule } from '../leads/leads.module';
import { InvoiceIssuedComponent } from './payment-issued/invoice-issued-list.componet';
import { invoiceIssuedDetailModal } from './payment-issued/invoice-issued-detail-modal.component';
import { RemoveBillDoModal } from './payment-issued/remove-bill-modal.component';
import { DispatchStatusModal } from './payment-issued/dispatch-status-modal.component';
import { DeliveryModal } from './payment-issued/delivery-modal.component';
import { STCModal } from './payment-issued/stc-modal.component';
import { SSModal } from './payment-issued/ss-modal.component';
import { PaymentPaidComponent } from './payment-paid/payment-paid-list.componet';
import { PaymentPaidDetailModal } from './payment-paid/payment-paid-detail-modal.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { DataTransferService } from '@app/shared/common/projectservice/data-transfer.service';
import { PaymentBillTemplateModalComponent } from './payment-Bill/payment-bill-template.component';
import { NumberToWordsPipe } from '@shared/utils/number-to-words.pipe';
import { PaymentVerifiedModal } from './payment-paid/payment-verified-modal.component';
import { PaymentRefundModal } from './payment-paid/payment-refund-modal.component';
//import { ApplicationListComponent } from './application/application-list.componet';


NgxBootstrapDatePickerConfigService.registerNgxBootstrapDatePickerLocales();

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ModalModule,
        TabsModule,
        TooltipModule,
        AppCommonModule,
        UtilsModule,        
        CountoModule,
        BsDatepickerModule.forRoot(),
        BsDropdownModule.forRoot(),
        PopoverModule.forRoot(),
        SubheaderModule,
        AdminSharedModule,
        AppSharedModule,  
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyALoDvKqgmTfpfPOEjqS2fI1kcabuxImMA',            
            libraries: ['places']
        }),        
        EmailEditorModule,
        AutoCompleteModule,
		PaginatorModule,
		EditorModule,
		InputMaskModule,
		TableModule,
        OwlDateTimeModule,
        OwlNativeDateTimeModule,    
        AccountsRoutingModule,
        LeadsModule,
        NgSelectModule
        //BrowserModule,
        //BrowserAnimationsModule
    ],
    declarations: [
        InvoiceIssuedComponent, invoiceIssuedDetailModal, RemoveBillDoModal,
        DispatchStatusModal, DeliveryModal, STCModal, SSModal, PaymentPaidComponent,PaymentVerifiedModal,PaymentRefundModal,
        PaymentPaidDetailModal,PaymentBillTemplateModalComponent,NumberToWordsPipe
    ],
    exports: [LeadsModule],
    schemas: [ NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA ],
    providers: [
         { provide: BsDatepickerConfig, useFactory: NgxBootstrapDatePickerConfigService.getDatepickerConfig },
         { provide: BsDaterangepickerConfig, useFactory: NgxBootstrapDatePickerConfigService.getDaterangepickerConfig },
         { provide: BsLocaleService, useFactory: NgxBootstrapDatePickerConfigService.getDatepickerLocale },
         { provide: DatePipe }
    ],
})
export class AccountsModule {}
