import { Component, Injector, ViewChild } from '@angular/core';
import { DataTransferService } from '@app/shared/common/projectservice/data-transfer.service';
import { DateTimeService } from '@app/shared/common/timing/date-time.service';
import { AppComponentBase } from '@shared/common/app-component-base';
import { AuditLogListDto, CreateOrEditPaymentDto, PaymentIssueServiceProxy } from '@shared/service-proxies/service-proxies';
import { DateTime } from 'luxon';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'DeliveryModal',
    templateUrl: './delivery-modal.html',
})
export class DeliveryModal extends AppComponentBase {
    @ViewChild('DeliveryModal', { static: true }) modal: ModalDirective;
    active = false;
    saving = false;
    ExpandedViewApp: boolean = true;
    DeliveryPayment: CreateOrEditPaymentDto = new CreateOrEditPaymentDto();
    ISPaymentDeliveryOption: boolean = false;
    //auditLog: AuditLogListDto;
    sectionId: number = 50;
    constructor(injector: Injector, private _dateTimeService: DateTimeService,
        private _dataTransfer:DataTransferService,
        private spinner: NgxSpinnerService,
        private _paymentIssueServiceProxy: PaymentIssueServiceProxy) {
        super(injector);
    }
    customertitle: string = '';
    SelectedLeadId:number;
    GetDeliverymodelOpen(paymentIssue) {        
        this.modal.show();
        this.customertitle = paymentIssue.customerName + ' ( ' + paymentIssue.projectNumber + ' ) ';
        this.SelectedLeadId = paymentIssue.leaddata.id;
        this.ISPaymentDeliveryOption = paymentIssue.PaymentDeliveryOption;
        this.DeliveryPayment.id = paymentIssue.id;
        this.DeliveryPayment.paymentDeliveryOption = paymentIssue.PaymentDeliveryOption;
        this.DeliveryPayment.leadId = paymentIssue.leaddata.id;
        this.DeliveryPayment.organizationUnitId = parseInt(this._dataTransfer.getOrgData());
    }

    savePaymentDeliveryOption(): void {
        this.saving = true;
       // this.DeliveryPayment.leadId = this.SelectedLeadId;
        this._paymentIssueServiceProxy.paymentDeliveryForAddOrEdit(this.sectionId, this.DeliveryPayment).pipe(
            finalize(() => { this.saving = false; })
        )
            .subscribe(() => {
                this.saving = false;
                this.active = true;
                this.modal.hide();
                document.getElementById('btnRefresh').click();

                this.notify.info(this.l('SavedSuccessfully'));
            },error=>{this.spinner.hide();});
    }
    close(): void {
        this.active = false;
        this.modal.hide();
    }

    navigateToAppDetail(): void {
        this.ExpandedViewApp = !this.ExpandedViewApp;
        this.ExpandedViewApp = true;
    }
    expandApp() {
        this.ExpandedViewApp = false;
    }
}
