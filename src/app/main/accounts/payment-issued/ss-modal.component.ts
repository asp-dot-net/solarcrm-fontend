import { Component, Injector, ViewChild } from '@angular/core';
import { DataTransferService } from '@app/shared/common/projectservice/data-transfer.service';
import { DateTimeService } from '@app/shared/common/timing/date-time.service';
import { AppComponentBase } from '@shared/common/app-component-base';
import { AuditLogListDto, CommonLookupDto, CommonLookupServiceProxy, CreateOrEditSSPaymentDto, PaymentIssueServiceProxy } from '@shared/service-proxies/service-proxies';
import { DateTime } from 'luxon';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'SSModal',
    templateUrl: './ss-modal.html',
})
export class SSModal extends AppComponentBase {
    // @ViewChild('SSModal', { static: true }) modal: ModalDirective;

    ExpandedViewApp: boolean = true;
    active = false;
    saving = false;
    @ViewChild('SSModal', { static: true }) modal: ModalDirective;
    sectionId:number = 50;
    allUsers: CommonLookupDto[];
    //auditLog: AuditLogListDto;
    organizationUnit:number;  
    SSPayment: CreateOrEditSSPaymentDto = new CreateOrEditSSPaymentDto();
    constructor(injector: Injector,
        private _paymentIssueServiceProxy: PaymentIssueServiceProxy,
        private _commonLookupService: CommonLookupServiceProxy,
        private _dataTransfer:DataTransferService,
        private _dateTimeService: DateTimeService) {
        super(injector);       
    }
    sspaymentDate:DateTime;
    customertitle: string = '';
    SelectedLeadId:number;
    PaymentID:number;
    GetSSmodelOpen(paymentdata) {
        this.organizationUnit = parseInt(this._dataTransfer.getOrgData()); 
                 
        this._commonLookupService.getSalesManagerUser(this.organizationUnit).subscribe(result => {
            this.allUsers = result;
        }, error => { this.spinnerService.hide() });
        this.SelectedLeadId = paymentdata.leaddata.id;
        this.PaymentID = paymentdata.id;
        this.spinnerService.show();
        this.customertitle = paymentdata.customerName + ' ( ' + paymentdata.projectNumber + ' ) '
        this._paymentIssueServiceProxy.getSSPaymentForView(paymentdata.id).subscribe((result) => {
            this.spinnerService.hide();           
            this.SSPayment = result;           
            this.modal.show();
        }, error => { this.spinnerService.hide() });
    }

    saveSSPayment(): void { 
        this.SSPayment.leadId = this.SelectedLeadId;     
        this.SSPayment.ssPaymentId = this.PaymentID; 
         this.SSPayment.ssDate = this.sspaymentDate;
          this.saving = true;
          this._paymentIssueServiceProxy.sSPaymentForAddOrEdit(this.sectionId,this.SSPayment).pipe(
                  finalize(() => { this.saving = false; })
              )
              .subscribe(() => {
                  this.active = true;
                  this.modal.hide();
                  document.getElementById('btnRefresh').click();
         
                  this.notify.info(this.l('SavedSuccessfully'));
              }, error => { this.spinnerService.hide() });
     }
    close(): void {
        this.active = false;
        this.modal.hide();
    }

    navigateToAppDetail(): void {
        this.ExpandedViewApp = !this.ExpandedViewApp;
        this.ExpandedViewApp = true;
    }
    expandApp() {
        this.ExpandedViewApp = false;
    }

}
