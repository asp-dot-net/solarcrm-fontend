import { Component, Injector, ViewChild } from '@angular/core';
import { DataTransferService } from '@app/shared/common/projectservice/data-transfer.service';
import { DateTimeService } from '@app/shared/common/timing/date-time.service';
import { AppComponentBase } from '@shared/common/app-component-base';
import { AuditLogListDto, CommonLookupDto, CommonLookupServiceProxy, CreateOrEditSTCPaymentDto, PaymentIssueServiceProxy } from '@shared/service-proxies/service-proxies';
import { DateTime } from 'luxon';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'STCModal',
    templateUrl: './stc-modal.html',
})
export class STCModal extends AppComponentBase {
    @ViewChild('STCModal', { static: true }) modal: ModalDirective;
    active = false;
    saving = false;
    ExpandedViewApp: boolean = true;
   
    sectionId: number = 50;
    allUsers: CommonLookupDto[];
    customertitle: string = '';
    SelectedLeadId: number;
    PaymentID:number;
    organizationUnit: number;
    STCPayment: CreateOrEditSTCPaymentDto = new CreateOrEditSTCPaymentDto();
    constructor(injector: Injector, private _dateTimeService: DateTimeService,
        private _paymentIssueServiceProxy: PaymentIssueServiceProxy,
        private _dataTransfer:DataTransferService,
        private _commonLookupService: CommonLookupServiceProxy) {
        super(injector);
    }
   

    GetSTCmodelOpen(paymentdata) {
        this.organizationUnit = parseInt(this._dataTransfer.getOrgData());     
        this._commonLookupService.getSalesManagerUser(this.organizationUnit).subscribe(result => {
            this.allUsers = result;
        }, error => { this.spinnerService.hide() });
        this.SelectedLeadId = paymentdata.leaddata.id;
        this.PaymentID = paymentdata.id;
        this.spinnerService.show();
        this.customertitle = paymentdata.customerName + ' ( ' + paymentdata.projectNumber + ' ) '
        this._paymentIssueServiceProxy.getSTCPaymentForView(paymentdata.id).subscribe((result) => {
            this.spinnerService.hide();
            this.STCPayment = result;
            this.modal.show();
        }, error => { this.spinnerService.hide() });
    }
    stcpaymentDate:DateTime;
    saveSTCPayment(): void { 
        this.STCPayment.leadId = this.SelectedLeadId;  
        this.STCPayment.stcPaymentId = this.PaymentID;    
         this.STCPayment.stcDate = this.stcpaymentDate;
          this.saving = true;
          this._paymentIssueServiceProxy.sTCPaymentForAddOrEdit(this.sectionId,this.STCPayment).pipe(
                  finalize(() => { this.saving = false; })
              )
              .subscribe(() => {
                  this.active = true;
                  this.modal.hide();
                  document.getElementById('btnRefresh').click();
         
                  this.notify.info(this.l('SavedSuccessfully'));
              }, error => { this.spinnerService.hide() });
     }
    close(): void {
        this.active = false;
        this.modal.hide();
    }
  
    navigateToAppDetail(): void {
        this.ExpandedViewApp = !this.ExpandedViewApp;
        this.ExpandedViewApp = true;
    }
    expandApp() {
        this.ExpandedViewApp = false;
    }

}
