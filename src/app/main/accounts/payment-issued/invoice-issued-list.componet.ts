import { OnInit, Component, Injector, ViewEncapsulation, ViewChild, Input, Output, EventEmitter, AfterViewInit, ElementRef, HostListener } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { LazyLoadEvent } from 'primeng/api';
import { CommonLookupServiceProxy, CommonLookupDto, OrganizationUnitDto, UserListDto, PaymentIssueServiceProxy, PaymentIssueSummaryCount } from '@shared/service-proxies/service-proxies';
import { Paginator } from 'primeng/paginator';
import { Title } from '@angular/platform-browser';
import * as _ from 'lodash';
import { RemoveBillDoModal } from './remove-bill-modal.component';
import { SSModal } from './ss-modal.component';
import { Table } from 'primeng/table';
import { applicationListDetailModal } from '@app/main/tracker/application/application-list-detail-modal.component';
import { ViewMyLeadComponent } from '@app/main/leads/customerleads/view-mylead.component';
import { AddActivityModalComponent } from '@app/main/leads/leadactivity/add-activity-model.component';
import { DateTime } from 'luxon';
import { DateTimeService } from '@app/shared/common/timing/date-time.service';
import { PaymentBillTemplateModalComponent } from '../payment-Bill/payment-bill-template.component';
import { DataTransferService } from '@app/shared/common/projectservice/data-transfer.service';
import { FileDownloadService } from '@shared/utils/file-download.service';

@Component({
    selector: 'invoice-issued',
    templateUrl: './invoice-issued-list.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class InvoiceIssuedComponent extends AppComponentBase implements OnInit {
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    @ViewChild('applicationListDetailModal', { static: true }) applicationListDetailModal: applicationListDetailModal;
    @ViewChild('SampleDatePicker', { static: true }) sampleDatePicker: ElementRef;
    @ViewChild('SSModal', { static: true }) SSModal: SSModal;
    @ViewChild('STCModal', { static: true }) STCModal: SSModal;
    @ViewChild('DeliveryModal', { static: true }) DeliveryModal: SSModal;
    @ViewChild('DispatchStatusModal', { static: true }) DispatchStatusModal: SSModal;
    @ViewChild('RemoveBillDoModal', { static: true }) RemoveBillDoModal: RemoveBillDoModal;
    @ViewChild('viewLeadDetail', { static: true }) viewLeadDetail: ViewMyLeadComponent;
    @ViewChild('addActivityModal', { static: true }) addActivityModal: AddActivityModalComponent;
    @ViewChild('paymentBillTemplateModel', { static: true }) paymentBillTemplateModel: PaymentBillTemplateModalComponent;
    @Output() reloadLead = new EventEmitter();
    @Input() SelectedLeadId: number = 0;
    @Input() SectionName: string = "PaymentIssue";
    advancedFiltersAreShown = false;
    FiltersData = false;
    active = false;
    saving = false;
    recordCount: any;
    leadId: any;
    filterText: string = '';
    shouldShow: boolean = false;
    //leadStatusId = 0;
    //sampleDateRange: moment.Moment[] = [moment().add(0, 'days').endOf('day'), moment().add(0, 'days').endOf('day')];
    ExpandedView: boolean = true;
    leadName: string;
    flag = true;
    PageNo: number;
    allOrganizationUnits: OrganizationUnitDto[];
    allOrganizationUnitsList: OrganizationUnitDto[];
    organizationUnitlength: number = 0;
    organizationUnit = 0;
    applicationstatus: number = 0;
    leadstatuss: CommonLookupDto[];
    allStates: CommonLookupDto[];
    allapplicationStatus: CommonLookupDto[];
    // allLeadSources: CommonLookupDto[];
    allSolarType: CommonLookupDto[];
    allEmployeeList: UserListDto[];
    allDiscom: CommonLookupDto[];
    allDivision: CommonLookupDto[];
    allSubDivision: CommonLookupDto[];
    allTenderData: CommonLookupDto[];
    allCircleData: CommonLookupDto[];
    allPaymentTypeData: CommonLookupDto[];
    allPaymentModeData: CommonLookupDto[];
    //filter variable
    leadStatusIDS = [];
    leadSourceIdFilter = [];
    leadApplicationStatusFilter = [];
    applicationstatusTrackerFilter: number;
    otpPendingFilter = '';
    queryRaisedFilter = '';
    employeeIdFilter = [];
    DiscomIdFilter = [];
    CircleIdFilter = [];
    DivisionIdFilter = [];
    SubDivisionIdFilter = [];
    solarTypeIdFilter = [];
    BillTypeFilter = '';
    PaymentModeIdFilter = [];
    PaymentTypeIdFilter = [];
    OwingAmountFromFilter = 0;
    OwingAmountToFilter = 0;
    FilterbyDate = '';
    date: Date = new Date('01/01/2022');
    StartDate: DateTime = this._dateTimeService.getStartOfDayForDate(this.date);
    EndDate: DateTime = this._dateTimeService.getDate();
    sectionId: number = 37;
    paymentIssueSummaryCount: PaymentIssueSummaryCount;
    public screenWidth: any;
    public screenHeight: any;
    testHeight = 280;
    show1: boolean = true;
    toggle: boolean = true;

    toggleBlock() {
        this.show1 = !this.show1;
    };
    change() {
        this.toggle = !this.toggle;
    }

    constructor(
        injector: Injector,
        private _paymentIssueServiceProxy: PaymentIssueServiceProxy,
        private _commonLookupService: CommonLookupServiceProxy,
        private _dateTimeService: DateTimeService,
        private _dataTransfer: DataTransferService,
        private _fileDownloadService: FileDownloadService,
    ) {
        super(injector);
        this.paymentIssueSummaryCount = new PaymentIssueSummaryCount();
    }
    ngOnInit(): void {
        this.screenHeight = window.innerHeight;
        //selected Project Application status
        //this.applicationstatusTrackerFilter = 4;

        this._commonLookupService.getAllApplicationStatusForDropdown().subscribe(result => {
            this.allapplicationStatus = result;
        }, error => { this.spinnerService.hide() });
        this._commonLookupService.getAllLeadStatusDropdown().subscribe(result => {
            this.leadstatuss = result;
        }, error => { this.spinnerService.hide() });
        this._commonLookupService.getAllDiscomFilterDropdown(0).subscribe(result => {
            this.allDiscom = result;
        }, error => { this.spinnerService.hide() });
        this._commonLookupService.getAllSubdivisionWiseData('').subscribe(result => {
            this.allSubDivision = result;
        }, error => { this.spinnerService.hide() });
        this._commonLookupService.getAllSolarTypeForDropdown().subscribe(result => {
            this.allSolarType = result;
        }, error => { this.spinnerService.hide() });
        this._commonLookupService.getAllUserList().subscribe(result => {
            this.allEmployeeList = result;
        }, error => { this.spinnerService.hide() });
        this._commonLookupService.getAllPaymentModeDropdown().subscribe(result => {
            this.allPaymentModeData = result;
        }, error => { this.spinnerService.hide() });
        this._commonLookupService.getAllPaymentTypeDropdown().subscribe(result => {
            this.allPaymentTypeData = result;
        }, error => { this.spinnerService.hide() });
        this._commonLookupService.getOrganizationUnitUserWise().subscribe(output => {
            this.allOrganizationUnits = output;
            this.organizationUnit = this.allOrganizationUnits[0].id;
            this.organizationUnitlength = this.allOrganizationUnits.length;

            //change Organization
            this.allOrganizationUnitsList = this.allOrganizationUnits.filter(item => item.id != this.organizationUnit);
            this.getPaymentIssueList();
        }, error => { this.spinnerService.hide() })
    }

    @HostListener('window:resize', ['$event'])
    onResize(event) {
        this.screenHeight = window.innerHeight;
    }
    testHeightSize() {
        if (this.FiltersData == true) {
            this.testHeight = this.testHeight + -80;
        }
        else {
            this.testHeight = this.testHeight - -80;
        }
    }

    chaneSolarType() {
        this.allTenderData = undefined;
        this._commonLookupService.getSolarTypeWiseTenderForDropdown(this.solarTypeIdFilter).subscribe(result => {
            this.allTenderData = result;
            if (this.allTenderData.length == 0) {
                this.allTenderData = undefined;
            }
        }, error => { this.spinnerService.hide() });
    }
    changeDiscom() {
        this.allCircleData = undefined;
        if (this.DiscomIdFilter.length > 0) {
            this._commonLookupService.getDiscomWiseCircleFilterForDropdown(this.DiscomIdFilter).subscribe(result => {
                this.allCircleData = result;
                if (this.allCircleData.length == 0) {
                    this.allCircleData = undefined;
                }
            }, error => { this.spinnerService.hide() });
        }
    }
    changeCircle() {
        this.allDivision = undefined;
        if (this.CircleIdFilter.length > 0) {
            this._commonLookupService.getCircleWiseDivisionFilterForDropdown(this.CircleIdFilter).subscribe(result => {
                this.allDivision = result;
                if (this.allDivision.length == 0) {
                    this.allDivision = undefined
                }
            }, error => { this.spinnerService.hide() });
        }
    }
    changeDivision() {
        this.allSubDivision = undefined;
        if (this.CircleIdFilter.length > 0) {
            this._commonLookupService.getDivisionWiseSubDivisionFilterForDropdown(this.DivisionIdFilter).subscribe(result => {
                this.allSubDivision = result;
                if (this.allSubDivision.length == 0) {
                    this.allSubDivision = undefined
                }
            }, error => { this.spinnerService.hide() });
        }
    }
    leadstatuschange() {
        if (this.leadStatusIDS.includes(6)) {
            //this.showJobStatus = true;
        } else {
            //this.showJobStatus = false;
            //this.jobStatusID = [];
        }
    }
    show(leadId?: number, param?: number, sectionId?: number): void {
        this.leadId = leadId;
        this.getPaymentIssueList();
    }

    getPaymentIssueList(event?: LazyLoadEvent) {
        this.paymentIssueSummaryCount = new PaymentIssueSummaryCount();
        this._dataTransfer.setOrgData(this.organizationUnit.toString());
        this.PageNo = this.primengTableHelper.getSkipCount(this.paginator, event);
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            //return;
        }
        this.primengTableHelper.showLoadingIndicator();

        //this.bindDocumentFilter.filter = this.leadId
        this._paymentIssueServiceProxy.getAllPaymentIssue(
            this.filterText,
            this.organizationUnit,
            this.leadStatusIDS,
            this.leadApplicationStatusFilter,
            this.employeeIdFilter,
            this.DiscomIdFilter,
            this.CircleIdFilter,
            this.DivisionIdFilter,
            this.SubDivisionIdFilter,
            this.solarTypeIdFilter,
            this.BillTypeFilter,
            this.PaymentModeIdFilter,
            this.PaymentTypeIdFilter,
            this.OwingAmountFromFilter,
            this.OwingAmountToFilter,
            this.FilterbyDate,
            this.StartDate,
            this.EndDate,
            undefined,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe((result) => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            this.primengTableHelper.hideLoadingIndicator();
            this.recordCount = result.totalCount;
            this.paymentIssueSummaryCount = result.items[0].paymentIssueSummaryCount;
            this.shouldShow = false;

            if (this.flag == false && result.totalCount != 0 && this.ExpandedView == false) {
                this.navigateToLeadDetail(this.SelectedLeadId)
            }
            else {
                if (this.ExpandedView == false && result.totalCount != 0) {
                    this.navigateToLeadDetail(result.items[0].paymentIssue.leaddata.id)
                }
                else {
                    this.ExpandedView = true;
                }
            }
        }, error => { this.spinnerService.hide() });
    }

    navigateToLeadDetail(leaddata): void {
        this.ExpandedView = !this.ExpandedView;
        this.ExpandedView = false;
        this.SelectedLeadId = leaddata.paymentIssue.leaddata.id;
        this.leadName = "paymentIssue"
        this.viewLeadDetail.showDetail(leaddata.paymentIssue.leaddata, leaddata.paymentIssue.applicationStatusdata, this.leadName, this.sectionId);
    }
    reloadPage(flafValue: boolean): void {

        this.ExpandedView = true;
        this.flag = flafValue;
        this.paginator.changePage(this.paginator.getPage());
        if (!flafValue) {
            this.navigateToLeadDetail(this.SelectedLeadId);
        }
        this.paginator.changePage(this.paginator.getPage());
    }
    exportToExcel(): void {
        this._paymentIssueServiceProxy.getPaymentIssueToExcel(
            this.filterText,
            this.organizationUnit,
            this.leadStatusIDS,
            this.leadApplicationStatusFilter,
            this.employeeIdFilter,
            this.DiscomIdFilter,
            this.CircleIdFilter,
            this.DivisionIdFilter,
            this.SubDivisionIdFilter,
            this.solarTypeIdFilter,
            this.BillTypeFilter,
            this.PaymentModeIdFilter,
            this.PaymentTypeIdFilter,
            this.OwingAmountFromFilter,
            this.OwingAmountToFilter,
            this.FilterbyDate,
            this.StartDate,
            this.EndDate,
            undefined,
            this.primengTableHelper.getSorting(this.dataTable),
            undefined,
            undefined
        ).subscribe((result) => {
            this._fileDownloadService.downloadTempFile(result);
        });
    }
    clear() {
        this.filterText = '';
        this.leadStatusIDS = [];
        this.leadApplicationStatusFilter = [];
        this.applicationstatusTrackerFilter = 4;
        this.otpPendingFilter = '';
        this.queryRaisedFilter = '';
        this.DiscomIdFilter = [];
        this.CircleIdFilter = [];
        this.DivisionIdFilter = [];
        this.SubDivisionIdFilter = [];
        this.solarTypeIdFilter = [];
        this.employeeIdFilter = [];
        this.BillTypeFilter = '';
        this.PaymentModeIdFilter = [];
        this.PaymentTypeIdFilter = [];
        this.OwingAmountFromFilter = 0;
        this.OwingAmountToFilter = 0;
        this.FilterbyDate = '';
        this.StartDate = this._dateTimeService.getStartOfDayForDate(this.date);
        this.EndDate = this._dateTimeService.getDate();
        this.getPaymentIssueList();
    }



    expandGrid() {
        this.ExpandedView = true;
    }

    close() {
        //this.modal.hide();
    }
}
