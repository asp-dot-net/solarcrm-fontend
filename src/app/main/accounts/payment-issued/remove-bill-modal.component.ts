import { Component, Injector, ViewChild } from '@angular/core';
import { DataTransferService } from '@app/shared/common/projectservice/data-transfer.service';
import { DateTimeService } from '@app/shared/common/timing/date-time.service';
import { AppComponentBase } from '@shared/common/app-component-base';
import { AuditLogListDto, CreateOrEditPaymentDto, PaymentIssueServiceProxy } from '@shared/service-proxies/service-proxies';
import { DateTime } from 'luxon';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'RemoveBillDoModal',
    templateUrl: './remove-bill-modal.html',
})
export class RemoveBillDoModal extends AppComponentBase {
    @ViewChild('RemoveBillDoModal', { static: true }) modal: ModalDirective;

    ExpandedViewApp: boolean = true;
    active = false;
    //auditLog: AuditLogListDto;

    constructor(injector: Injector, private _dateTimeService: DateTimeService,
        private _paymentIssueServiceProxy: PaymentIssueServiceProxy,
        private _dataTransfer:DataTransferService) {
        super(injector);
    }
    customertitle:string = '';
    IS_SS_Payment:boolean;
    IS_STS_Payment:boolean;
    PaymentID:number;
    organizationUnit:number;
    SelectedLeadId:number;
    GetPaymentBillRemovemodelOpen(paymentIssue){ 
        this.organizationUnit = parseInt(this._dataTransfer.getOrgData());  
        this.PaymentID = paymentIssue.id;
        this.spinnerService.show();
        this.customertitle = paymentIssue.customerName +' ( '+ paymentIssue.projectNumber +' ) ';
        this.IS_SS_Payment = paymentIssue.IS_SS_Payment;
        this.IS_STS_Payment = paymentIssue.IS_STS_Payment;
        this.SelectedLeadId = paymentIssue.leaddata.id;
        this.modal.show();  
        this.spinnerService.hide();
    }
    stcpaymentDate:DateTime;
    STCorSRCPayment:CreateOrEditPaymentDto = new CreateOrEditPaymentDto();
    sectionId:number = 50;
    saving:boolean;
    RemoveSSorSTCPayment(): void { 
        this.STCorSRCPayment.id = this.PaymentID;  
        this.STCorSRCPayment.leadId = this.SelectedLeadId;     
        this.STCorSRCPayment.isSSPaymentDetails = this.IS_SS_Payment;    
         this.STCorSRCPayment.isSTCPaymentDetails = this.IS_STS_Payment;
         this.STCorSRCPayment.organizationUnitId = this.organizationUnit;
          this.saving = true;
          this._paymentIssueServiceProxy.sTCorSSPaymentDelete(this.sectionId,this.STCorSRCPayment).pipe(
                  finalize(() => { this.saving = false; })
              )
              .subscribe(() => {
                  this.active = true;
                  this.modal.hide();
                  document.getElementById('btnRefresh').click();
         
                  this.notify.info(this.l('SavedSuccessfully'));
              }, error => { this.spinnerService.hide() });
     }
    close(): void {
        this.active = false;
        this.modal.hide();
    }

    navigateToAppDetail(): void {
        this.ExpandedViewApp = !this.ExpandedViewApp;
        this.ExpandedViewApp = true;                
    }
    expandApp() {
        this.ExpandedViewApp = false;
    }

}
