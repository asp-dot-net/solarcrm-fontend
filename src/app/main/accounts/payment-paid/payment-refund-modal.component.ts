import { Component, Injector, ViewChild } from '@angular/core';
import { DataTransferService } from '@app/shared/common/projectservice/data-transfer.service';
import { DateTimeService } from '@app/shared/common/timing/date-time.service';
import { AppComponentBase } from '@shared/common/app-component-base';
import { AuditLogListDto, CommonLookupDto, CommonLookupServiceProxy, CreateOrEditPaymentReceiptItemsDto, CreateOrEditSTCPaymentDto, PaymentIssueServiceProxy, PaymentPaidServiceProxy } from '@shared/service-proxies/service-proxies';
import { DateTime } from 'luxon';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'PaymentRefundModal',
    templateUrl: './payment-refund-modal.component.html',
})
export class PaymentRefundModal extends AppComponentBase {
    @ViewChild('PaymentRefundModal', { static: true }) modal: ModalDirective;
    active = false;
    saving = false;
    ExpandedViewApp: boolean = true;
   
    sectionId: number = 50;
    allUsers: CommonLookupDto[];
    customertitle: string = '';
    SelectedLeadId: number;
    PaymentID:number;
    organizationUnit: number;
    PaymentMode: CommonLookupDto[];
    PaymentVerified: CreateOrEditPaymentReceiptItemsDto = new CreateOrEditPaymentReceiptItemsDto();
    constructor(injector: Injector, private _dateTimeService: DateTimeService,
        private _paymentPaidServiceProxy: PaymentPaidServiceProxy,
        private _commonLookup: CommonLookupServiceProxy,
        private _dataTransfer:DataTransferService) {
        super(injector);
    }
   
    Chequedrphideshow:boolean = true;
    GetPaymentRefundOpen(paymentdata) {
        this.PaymentMode = [];
        this._commonLookup.getAllPaymentModeDropdown().subscribe((result) => {
            this.PaymentMode = result;
        }, error => { this.spinnerService.hide() });
        this.organizationUnit = parseInt(this._dataTransfer.getOrgData());     
        this.Chequedrphideshow = false;
        if(paymentdata.paymentMode == 'Cheque'){
            this.Chequedrphideshow = true;
        }
        this.SelectedLeadId = paymentdata.leaddata.id;
        this.PaymentID = paymentdata.id;
        this.spinnerService.show();
        this.customertitle = paymentdata.customerName + ' ( ' + paymentdata.projectNumber + ' ) '
        this._paymentPaidServiceProxy.getPaymentRecVerifiedForView(paymentdata.id).subscribe((result) => {
            this.spinnerService.hide();
            
            this.PaymentVerified = result;
            //this.PaymentVerified.id = result.id;
            this.modal.show();
        }, error => { this.spinnerService.hide() });
    }
    
    paymentpaidDate:DateTime;
    savePaymentVerified(): void { 
        
         this.PaymentVerified.leadId = this.SelectedLeadId;  
         this.PaymentVerified.paymentId = this.PaymentID;    
          this.PaymentVerified.paymentRecPaidDate = this.paymentpaidDate;
          this.PaymentVerified.organizationUnitId = this.organizationUnit;
           this.saving = true;
           this._paymentPaidServiceProxy.paymentVerifiedForAddOrEdit(this.sectionId,this.PaymentVerified).pipe(
                   finalize(() => { this.saving = false; })
               ).subscribe(() => {
                   this.active = true;
                   this.modal.hide();
                   document.getElementById('btnRefresh').click();
    
                   this.notify.info(this.l('SavedSuccessfully'));
               }, error => { this.spinnerService.hide() });
     }
    close(): void {
        this.active = false;
        this.modal.hide();
    }
  
    navigateToAppDetail(): void {
        this.ExpandedViewApp = !this.ExpandedViewApp;
        this.ExpandedViewApp = true;
    }
    expandApp() {
        this.ExpandedViewApp = false;
    }
}
