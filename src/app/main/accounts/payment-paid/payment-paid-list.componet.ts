import { OnInit, Component, Injector, ViewEncapsulation, ViewChild, Input, Output, EventEmitter, HostListener } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ActivatedRoute, Router } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { finalize } from 'rxjs/operators';
import { LazyLoadEvent } from 'primeng/api';
import { LeadsServiceProxy, LeadsDto, CommonLookupServiceProxy, CommonLookupDto, PaymentPaidServiceProxy, UserListDto, OrganizationUnitDto, PaymentPaidSummaryCount } from '@shared/service-proxies/service-proxies';
import { Paginator } from 'primeng/paginator';
import { Title } from '@angular/platform-browser';
import * as _ from 'lodash';
import * as moment from 'moment';
import { PaymentPaidDetailModal } from './payment-paid-detail-modal.component';
import { DateTimeService } from '@app/shared/common/timing/date-time.service';
import { DataTransferService } from '@app/shared/common/projectservice/data-transfer.service';
import { DateTime } from 'luxon';
import { Table } from 'primeng/table';
import { ViewMyLeadComponent } from '@app/main/leads/customerleads/view-mylead.component';
import { AddActivityModalComponent } from '@app/main/leads/leadactivity/add-activity-model.component';
import { PaymentVerifiedModal } from './payment-verified-modal.component';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { TokenService } from 'abp-ng2-module';
import { AppConsts } from '@shared/AppConsts';
import { FileUpload } from 'primeng/fileupload';
import { PaymentRefundModal } from './payment-refund-modal.component';
import { FileDownloadService } from '@shared/utils/file-download.service';

@Component({
    selector: 'payment-paid',
    templateUrl: './payment-paid-list.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class PaymentPaidComponent extends AppComponentBase implements OnInit  {
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    @ViewChild('PaymentPaidDetailModal', { static: true }) PaymentPaidDetailModal: PaymentPaidDetailModal;
    @ViewChild('PaymentVerifiedModal', { static: true }) PaymentVerifiedModal: PaymentVerifiedModal;
    @ViewChild('PaymentRefundModal', { static: true }) PaymentRefundModal: PaymentRefundModal;
    @Input() SectionName: string = "PaymentPaid";
    @ViewChild('viewLeadDetail', { static: true }) viewLeadDetail: ViewMyLeadComponent;
    @ViewChild('addActivityModal', { static: true }) addActivityModal: AddActivityModalComponent;
    @Input() SelectedLeadId: number = 0;
    @Output() reloadLead = new EventEmitter();
    @ViewChild('ExcelFileUpload', { static: false }) excelFileUpload: FileUpload;
    advancedFiltersAreShown = false;
    FiltersData = false;
    active = false;
    saving = false;
    recordCount: any;
    leadId: any;
    filterText: string = '';
    shouldShow: boolean = false;  
    ExpandedView: boolean = true;
    leadName: string;
    flag = true;
    PageNo: number;
    allOrganizationUnits: OrganizationUnitDto[];
    allOrganizationUnitsList: OrganizationUnitDto[];
    organizationUnitlength: number = 0;
    organizationUnit = 0;
    applicationstatus: number = 0;
    leadstatuss: CommonLookupDto[];
    allStates: CommonLookupDto[];
    allapplicationStatus: CommonLookupDto[];
   // allLeadSources: CommonLookupDto[];
    allSolarType: CommonLookupDto[];
    allEmployeeList: UserListDto[];
    allDiscom: CommonLookupDto[];
    allDivision: CommonLookupDto[];
    allSubDivision: CommonLookupDto[];
    allTenderData: CommonLookupDto[];
    allCircleData: CommonLookupDto[];
    allPaymentTypeData: CommonLookupDto[];
    allPaymentModeData: CommonLookupDto[];
    //filter variable
    leadStatusIDS = [];
    leadSourceIdFilter = [];
    leadApplicationStatusFilter = [];
    applicationstatusTrackerFilter: number;
    otpPendingFilter = '';
    queryRaisedFilter = '';
    employeeIdFilter = [];
    DiscomIdFilter = [];
    CircleIdFilter = [];
    DivisionIdFilter = [];
    SubDivisionIdFilter = [];
    solarTypeIdFilter = [];
    TenderIdFilter = [];
    PaymentTypeIdFilter = [];
    PaymentModeIdFilter = [];
    BillFilter='';
    FilterbyDate = '';
    date: Date = new Date('01/01/2022');
    StartDate: DateTime = this._dateTimeService.getStartOfDayForDate(this.date);
    EndDate: DateTime = this._dateTimeService.getDate();
    sectionId: number = 38
    uploadUrl:string = '';  
    public screenWidth: any;  
    public screenHeight: any;  
    paymentPaidSummaryCount :PaymentPaidSummaryCount;
    testHeight = 280;
    contentHeight = 230;
    show1: boolean = true;
    toggle: boolean = true;
    PaymentStatusFilter:number = 0;
    toggleBlock() {
        this.show1 = !this.show1;
    };
    change() {
        this.toggle = !this.toggle;
    }

    constructor(
        injector: Injector,
        private _leadsServiceProxy: LeadsServiceProxy,
        private _commonLookupService: CommonLookupServiceProxy,       
        private _fileDownloadService: FileDownloadService,
        private _paymentPaidServiceProxy: PaymentPaidServiceProxy,
        private _dateTimeService: DateTimeService,
        private _httpClient: HttpClient,
        private _tokenService: TokenService,
        private _dataTransfer:DataTransferService,
    ) {
        super(injector);
        this.uploadUrl = AppConsts.remoteServiceBaseUrl + '/Users/ImportLeadFromExcel';
        this.paymentPaidSummaryCount = new PaymentPaidSummaryCount();
    }
    ngOnInit(): void {
        this.screenHeight = window.innerHeight;       
      
        this._commonLookupService.getAllApplicationStatusForDropdown().subscribe(result => {
            this.allapplicationStatus = result;
        }, error => { this.spinnerService.hide() });
        this._commonLookupService.getAllLeadStatusDropdown().subscribe(result => {
            this.leadstatuss = result;
        }, error => { this.spinnerService.hide() });
        this._commonLookupService.getAllDiscomFilterDropdown(0).subscribe(result => {
            this.allDiscom = result;
        }, error => { this.spinnerService.hide() });
        this._commonLookupService.getAllSubdivisionWiseData('').subscribe(result => {
            this.allSubDivision = result;
        }, error => { this.spinnerService.hide() });
        this._commonLookupService.getAllSolarTypeForDropdown().subscribe(result => {
            this.allSolarType = result;
        }, error => { this.spinnerService.hide() });
        this._commonLookupService.getAllUserList().subscribe(result => {
            this.allEmployeeList = result;
        }, error => { this.spinnerService.hide() });
        this._commonLookupService.getAllPaymentModeDropdown().subscribe(result => {
            this.allPaymentModeData = result;
        }, error => { this.spinnerService.hide() });
        this._commonLookupService.getAllPaymentTypeDropdown().subscribe(result => {
            this.allPaymentTypeData = result;
        }, error => { this.spinnerService.hide() });
        this._commonLookupService.getOrganizationUnitUserWise().subscribe(output => {
            this.allOrganizationUnits = output;
            this.organizationUnit = this.allOrganizationUnits[0].id;
            this.organizationUnitlength = this.allOrganizationUnits.length;

            //change Organization
            this.allOrganizationUnitsList = this.allOrganizationUnits.filter(item => item.id != this.organizationUnit);
            this.getPaymentPaidList();
        }, error => { this.spinnerService.hide() })
    }

    @HostListener('window:resize', ['$event'])  
    onResize(event) { 
        this.screenHeight = window.innerHeight;        
    }  
    testHeightSize () {
        if (this.FiltersData == true) {
            this.testHeight = this.testHeight + -80 ;
        }
        else {
            this.testHeight = this.testHeight - -80 ;
        }
    }
   
    chaneSolarType() {
        this.allTenderData = undefined;
        this._commonLookupService.getSolarTypeWiseTenderForDropdown(this.solarTypeIdFilter).subscribe(result => {
            this.allTenderData = result;
            if (this.allTenderData.length == 0) {
                this.allTenderData = undefined;
            }
        }, error => { this.spinnerService.hide() });
    }
    changeDiscom() {
        this.allCircleData = undefined;
        if (this.DiscomIdFilter.length > 0) {
            this._commonLookupService.getDiscomWiseCircleFilterForDropdown(this.DiscomIdFilter).subscribe(result => {
                this.allCircleData = result;
                if (this.allCircleData.length == 0) {
                    this.allCircleData = undefined;
                }
            }, error => { this.spinnerService.hide() });
        }
    }
    changeCircle() {
        this.allDivision = undefined;
        if (this.CircleIdFilter.length > 0) {
            this._commonLookupService.getCircleWiseDivisionFilterForDropdown(this.CircleIdFilter).subscribe(result => {
                this.allDivision = result;
                if (this.allDivision.length == 0) {
                    this.allDivision = undefined
                }
            }, error => { this.spinnerService.hide() });
        }
    }
    changeDivision() {
        this.allSubDivision = undefined;
        if (this.CircleIdFilter.length > 0) {
            this._commonLookupService.getDivisionWiseSubDivisionFilterForDropdown(this.DivisionIdFilter).subscribe(result => {
                this.allSubDivision = result;
                if (this.allSubDivision.length == 0) {
                    this.allSubDivision = undefined
                }
            }, error => { this.spinnerService.hide() });
        }
    }
    leadstatuschange() {
        if (this.leadStatusIDS.includes(6)) {
            //this.showJobStatus = true;
        } else {
            //this.showJobStatus = false;
            //this.jobStatusID = [];
        }
    }
    show(leadId?: number, param?: number, sectionId?: number): void {
        this.leadId = leadId;
        this.getPaymentPaidList();
    }
    getPaymentPaidList(event?: LazyLoadEvent) {   
        this.paymentPaidSummaryCount = new PaymentPaidSummaryCount();    
        this._dataTransfer.setOrgData(this.organizationUnit.toString());
        this.PageNo = this.primengTableHelper.getSkipCount(this.paginator, event);
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            //return;
        }
        this.primengTableHelper.showLoadingIndicator();        
        //this.bindDocumentFilter.filter = this.leadId
        this._paymentPaidServiceProxy.getAllPaymentPaid(
            this.filterText,           
            this.organizationUnit, 
            this.FilterbyDate,          
            this.leadStatusIDS,
            this.employeeIdFilter,
            this.leadApplicationStatusFilter,
            this.applicationstatusTrackerFilter,            
            this.DiscomIdFilter,
            this.CircleIdFilter,
            this.DivisionIdFilter,
            this.SubDivisionIdFilter,
            this.solarTypeIdFilter,
            this.TenderIdFilter,  
            "",
            [], 
            this.PaymentStatusFilter,        
            this.StartDate,
            this.EndDate,
            undefined,          
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe((result) => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            this.primengTableHelper.hideLoadingIndicator();
            this.recordCount = result.totalCount;                
            this.paymentPaidSummaryCount = result.items[0].paymentPaidSummaryCount;     
            this.shouldShow = false;          
          
            if (this.flag == false && result.totalCount != 0 && this.ExpandedView == false) {
                this.navigateToLeadDetail(this.SelectedLeadId)
            }
            else {
                if (this.ExpandedView == false && result.totalCount != 0) {
                    this.navigateToLeadDetail(result.items[0].paymentPaid.leaddata)
                }
                else {
                    this.ExpandedView = true;
                }
            }
        }, error => { this.spinnerService.hide() });
    }
  
    uploadExcel(data: { files: File }): void {        
        var headers_object = new HttpHeaders().set("Authorization", "Bearer " + this._tokenService.getToken());

        const httpOptions = {
            headers: headers_object
        };

        const formData: FormData = new FormData();
        const file = data.files[0];
        formData.append('file' + ',' + this.organizationUnit, file, file.name);

        this._httpClient
            .post<any>(this.uploadUrl, formData, httpOptions)
            .pipe(finalize(() => this.excelFileUpload.clear()))
            .subscribe(response => {
                if (response.success) {
                    this.notify.success(this.l('ImportLeadsProcessStart'));
                } else if (response.error != null) {
                    this.notify.error(this.l('ImportLeadUploadFailed'));
                }
            }, error => { this.spinnerService.hide() });
    }
    onUploadExcelError(): void {
        this.notify.error(this.l('ImportLeadUploadFailed'));
    }
  
    navigateToLeadDetail(leaddata): void {
        this.ExpandedView = !this.ExpandedView;
        this.ExpandedView = false;
        this.SelectedLeadId = leaddata.paymentPaid.leaddata.id;
        this.leadName = "paymentPaid"
        this.viewLeadDetail.showDetail(leaddata.paymentPaid.leaddata, leaddata.paymentPaid.applicationStatusdata, this.leadName, this.sectionId);
    }
    reloadPage(flafValue: boolean): void {        
        this.ExpandedView = true;
        this.flag = flafValue;
        this.paginator.changePage(this.paginator.getPage());
        if (!flafValue) {
            this.navigateToLeadDetail(this.SelectedLeadId);
        }
        this.paginator.changePage(this.paginator.getPage());
    }
    exportToExcel(): void {
        this._paymentPaidServiceProxy.getPaymentPaidToExcel(
            this.filterText,           
            this.organizationUnit, 
            this.FilterbyDate,          
            this.leadStatusIDS,
            this.employeeIdFilter,
            this.leadApplicationStatusFilter,
            this.applicationstatusTrackerFilter,            
            this.DiscomIdFilter,
            this.CircleIdFilter,
            this.DivisionIdFilter,
            this.SubDivisionIdFilter,
            this.solarTypeIdFilter,
            this.TenderIdFilter,  
            "",
            [], 
            this.PaymentStatusFilter,        
            this.StartDate,
            this.EndDate,
            undefined,          
            this.primengTableHelper.getSorting(this.dataTable),
            undefined,
            undefined
        ).subscribe((result) => {
                 this._fileDownloadService.downloadTempFile(result);
             });
    }  
    
    expandGrid() {
        this.ExpandedView = true;
    }
    clear() {
        this.filterText = '';        
        this.leadStatusIDS = [];
        this.leadApplicationStatusFilter = [];
        this.applicationstatusTrackerFilter = 4;
        this.otpPendingFilter ='';
        this.queryRaisedFilter = '';
        this.DiscomIdFilter = [];
        this.CircleIdFilter =[];
        this.DivisionIdFilter = [];
        this.SubDivisionIdFilter = [];
        this.solarTypeIdFilter=[];    
        this.organizationUnit=0; 
        this.employeeIdFilter = []; 
        this.BillFilter = '';
        this.PaymentModeIdFilter = [];
        this.FilterbyDate = '';
        this.PaymentStatusFilter = 0;
        this.StartDate = this._dateTimeService.getStartOfDayForDate(this.date);
        this.EndDate = this._dateTimeService.getDate();
        this.getPaymentPaidList();
    }
   
}
