import { Component, Injector, ViewChild } from '@angular/core';
import { DateTimeService } from '@app/shared/common/timing/date-time.service';
import { AppComponentBase } from '@shared/common/app-component-base';
import { AuditLogListDto, CommonLookupDto, CreateOrEditJobDto, CreateOrEditJobVariationDto, GetJobForEditOutput, JobProductItemProductItemLookupTableDto, JobProductItemsServiceProxy, JobServiceProxy, JobVariationLookupTableDto, JobVariationServiceProxy, PaymentIssueDto, PaymentPaidDto, PriceServiceProxy } from '@shared/service-proxies/service-proxies';
import { DateTime } from 'luxon';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'PaymentPaidDetailModal',
    templateUrl: './payment-paid-detail-modal.html',
})
export class PaymentPaidDetailModal extends AppComponentBase {
    @ViewChild('PaymentPaidDetailModal', { static: true }) modal: ModalDirective;

    ExpandedViewApp: boolean = true;
    active = false;    

    constructor(injector: Injector, private _dateTimeService: DateTimeService,
        private _jobProductItemProxy: JobProductItemsServiceProxy,
        private _jobVariationServiceProxy: JobVariationServiceProxy,
        private _jobServiceProxy: JobServiceProxy,
        private _spinner: NgxSpinnerService,
        private _priceServiceProxy: PriceServiceProxy,
    ) {
        super(injector);
    }
    job: CreateOrEditJobDto = new CreateOrEditJobDto();
    JobType: any;
    JobProducts: any[];

    jobDetails: GetJobForEditOutput = new GetJobForEditOutput();
    productTypes: CommonLookupDto[];
    // jobDetails: GetJobForEditOutput = new GetJobForEditOutput();
    // productTypes: CommonLookupDto[];

    EditproductItems: JobProductItemProductItemLookupTableDto[];
    //PickListtems: GetPicklistItemInput[];
    PicklistProducts: any[];
    productItemSuggestions: string[];
    productItems: JobProductItemProductItemLookupTableDto[];
    //Variation
    JobVariations: CreateOrEditJobVariationDto[];
    variations: JobVariationLookupTableDto[];
    ngOnInit(): void {
        this.PaymentPaidQuickView = new PaymentPaidDto();
        this.JobProducts = [];
    }
    customertitle: string = '';
    PaymentPaidQuickView: PaymentPaidDto;
    IsInitPage: boolean = false;
    GetPaymentQuickViewmodelOpen(paymentPaid) {
        this.IsInitPage = true
        //this.spinnerService.show();
        this.customertitle = paymentPaid.customerName + ' ( ' + paymentPaid.projectNumber + ' ) '
        this.PaymentPaidQuickView = paymentPaid;
        this.GetproductpriceList(paymentPaid.leaddata.id, paymentPaid.jobId)
        this.modal.show();
    }
    areaType: string = '';
    GetproductpriceList(leadId, jobid) {

        this.jobDetails = new GetJobForEditOutput();
        this.job = new CreateOrEditJobDto();
        this._jobServiceProxy.getJobForEdit(leadId).subscribe((result) => {
            this.jobDetails = result;
            if (result.job != undefined) {
                this.job = result.job;
               
                this.areaType = this.jobDetails.areaType;
            }
        }, error => { that._spinner.hide() });
        this._jobProductItemProxy.getJobProductItemByJobId(jobid).subscribe(result => {
            this.JobProducts = [];
            this.PicklistProducts = [];
            result.map((item) => {
                var jobproductcreateOrEdit = { id: 0, productItemId: 0, productItemName: "", quantity: 0, jobId: 0, size: 0, model: "", productItems: [], productTypeId: 0 }
                jobproductcreateOrEdit.productTypeId = item.jobProductItem.productTypeId;
                jobproductcreateOrEdit.productItemId = item.jobProductItem.productItemId;
                jobproductcreateOrEdit.quantity = item.jobProductItem.quantity;
                jobproductcreateOrEdit.id = item.jobProductItem.id;
                jobproductcreateOrEdit.jobId = jobid;
                jobproductcreateOrEdit.size = item.size;
                jobproductcreateOrEdit.model = item.model;
                jobproductcreateOrEdit.productItemName = item.productItemName;
                this.JobProducts.push(jobproductcreateOrEdit);
            }, error => { that._spinner.hide() })
            if (result.length == 0) {
                var jobproductcreateOrEdit = { id: 0, productItemId: 0, productItemName: "", quantity: 0, jobId: 0, size: 0, model: "", productItems: [], productTypeId: 0 }
                jobproductcreateOrEdit.productItems = this.EditproductItems;
                this.JobProducts.push(jobproductcreateOrEdit);
            }
          
        }, error => { that._spinner.hide() });

        this._jobVariationServiceProxy.getJobVariationByJobId(jobid).subscribe(result => {
            this.variations = [];
            

            result.map((item) => {
                var jobvariation = new JobVariationLookupTableDto()
                jobvariation.id = item.jobVariation.variationId;
                jobvariation.displayName = item.variationName;
                jobvariation.actionName = item.jobVariation.cost.toString();
                this.variations.push(jobvariation)
            }, error => { this.spinnerService.hide() })

        });
        let that = this;
        setTimeout(function () {
            that.calculateSystemCapacity();            
        }, 1500);
    }
    Subcidy40: any = 0.00;
    Subcidy20: any = 0.00;
    ActualSubcidy: any = 0.00;
    SystemCapacity: any = 0.00;
    productSize: any[];
    calculateSystemCapacity(): void {

        let that = this;
        let panelsize: number = 0;
        let invertersize: number = 0;
        this.productSize = [];
        
        let SystemCapacity = this.SystemCapacity;
        // this._spinner.show();
        if (this.JobProducts) {
            this.JobProducts.forEach(function (item) {
                //producttype =1 for inverter
                if (item.productTypeId == 1) {
                    SystemCapacity = (parseFloat(item.quantity.toString()) * parseFloat(item.size.toString()));   //invertr Formula
                    invertersize = SystemCapacity;
                }
                //producttype = 2 for panel
                else if (item.productTypeId == 2) {
                    SystemCapacity = (parseFloat(item.quantity.toString()) * parseFloat(item.size.toString())) / 1000; //invertr Formula
                    panelsize = SystemCapacity;
                }
            });
            // find the inverter and panel minimum size
            if (panelsize != 0 && invertersize != 0) {
                this.productSize.push(panelsize);
                this.productSize.push(invertersize);

                const productMinSize = Math.min.apply(null, this.productSize)
                SystemCapacity = productMinSize;
            }
            this.SystemCapacity = parseFloat(SystemCapacity.toString());

            if (SystemCapacity.toString() != "0") {

                this._priceServiceProxy.getPriceBySystemSize(SystemCapacity, this.areaType).subscribe((result) => {
                    // this.Price = result.price;
                    if (result.productPriceDto != undefined) {

                        this.jobDetails.job.actualSystemCost = result.productPriceDto.actualPrice;
                        this.Subcidy40 = result.productPriceDto.subcidy40;
                        this.Subcidy20 = result.productPriceDto.subcidy20;
                        this.ActualSubcidy = this.Subcidy40 + this.Subcidy20;
                    }
                }, error => { that._spinner.hide() });
            }
        }
        //this._spinner.hide();

    }
    close(): void {
        this.active = false;
        this.modal.hide();
    }

    navigateToAppDetail(): void {
        this.ExpandedViewApp = !this.ExpandedViewApp;
        this.ExpandedViewApp = true;
    }
    expandApp() {
        this.ExpandedViewApp = false;
    }
}
