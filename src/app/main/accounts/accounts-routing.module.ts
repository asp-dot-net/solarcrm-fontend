import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

//import { DispatchListComponent } from '../installation/dispatch/dispatch-list.componet';
import { InvoiceIssuedComponent } from '../accounts/payment-issued/invoice-issued-list.componet';
import { PaymentPaidComponent } from '../accounts/payment-paid/payment-paid-list.componet';
import { PaymentBillTemplateModalComponent } from './payment-Bill/payment-bill-template.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '',
                // children: [
                //     {
                //         path: 'manageleads',
                //         loadChildren: () => import('./manageleads/manageleads.module').then(m => m.ManageLeadsModule),
                //         //data: { permission: 'Pages.Tenant.DataVaults.Customer', features: 'App.DataVaults.Customer' },
                //     },
                //      {
                //          path: 'leadsactivity',
                //          loadChildren: () => import('../leads/leadactivity/leadsactivity.module').then(m => m.LeadsActivityModule),
                //     //     data: { permission: 'Pages.Tenant.DataVaults.Customer' },
                //      }
                // ]
            },            
            { path: 'payment-issued', component: InvoiceIssuedComponent },
            { path: 'payment-paid', component: PaymentPaidComponent },  
            { path: 'paymentBillTemplateModel', component: PaymentBillTemplateModalComponent },          
        ]),
    ],
    exports: [RouterModule],
})
export class AccountsRoutingModule { }