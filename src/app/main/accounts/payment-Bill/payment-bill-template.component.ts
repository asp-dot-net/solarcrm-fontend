import { Component, Inject, Renderer2, ElementRef, EventEmitter, Injectable, Injector, OnInit, Optional, Output, ViewChild, Input } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
//import { LeadsServiceProxy, GetLeadForViewDto, LeadDto, ActivityLogInput, CommonLookupDto, EmailTemplateServiceProxy, SmsTemplatesServiceProxy, JobsServiceProxy, JobPromotionsServiceProxy, JobPromotionPromotionMasterLookupTableDto } from '@shared/service-proxies/service-proxies';
import { GetLeadsForViewDto, CommonLookupDto, UserListDto, LeadsDto, LeadsServiceProxy, CommonLookupServiceProxy, ActivityLogInput, LeadsActivityLogServiceProxy, PdfTemplateServiceProxy, QuotationsServiceProxy, QuotationDataDetailsDto, PaymentIssueServiceProxy, BillPaymentDto } from '@shared/service-proxies/service-proxies';
import * as _ from 'lodash';
import { DatePipe, DOCUMENT } from '@angular/common';
import * as moment from 'moment';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { Router } from '@angular/router';
import { EmailEditorComponent } from 'angular-email-editor';
import { AppConsts } from '@shared/AppConsts';
//import { ViewMyLeadComponent } from './view-mylead.component';
//import { ViewMyLeadComponent } from '../manageleads/view-mylead.component';
import { isMoment } from 'moment';
import { Editor } from 'primeng/editor';
import { OWL_DATE_TIME_FORMATS } from 'ng-pick-datetime';
import { DEFAULTS, NgxSpinnerService } from 'ngx-spinner';
//import { event } from 'jquery';
//import {view}
declare var Quill: any;
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from "@angular/platform-browser";
import { DateTimeService } from '@app/shared/common/timing/date-time.service';
import { AddActivityModalComponent } from '@app/main/leads/leadactivity/add-activity-model.component';
import { NumberToWordsPipe } from '@shared/utils/number-to-words.pipe';
import { DataTransferService } from '@app/shared/common/projectservice/data-transfer.service';
@Component({
    selector: 'paymentBillTemplateModel',
    templateUrl: './payment-bill-template.component.html'
})

export class PaymentBillTemplateModalComponent extends AppComponentBase implements OnInit {

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @Output() reloadLead = new EventEmitter<boolean>();
    @Input() SelectedJobNumber: string = '';
    @ViewChild("myNameElem") myNameElem: ElementRef;
    @ViewChild("area_select") area_select: ElementRef;
    @ViewChild("modal_body") modal_body: ElementRef;
    @ViewChild('paymentBillTemplateModel') public modal: ModalDirective;
    @ViewChild('btnEmail', { static: true }) btnEmail: ElementRef;
    @ViewChild('btnsmsnotify', { static: true }) btnsmsnotify: ElementRef;
    @ViewChild('SampleDatePicker', { static: true }) sampleDatePicker: ElementRef;
    @ViewChild('addActivityModal', { static: true }) addActivityModal: AddActivityModalComponent;
    @ViewChild(EmailEditorComponent)
    private emailEditor: EmailEditorComponent;
    saving = false;
    //sampleDate: moment.Moment = moment().add(1, 'days').startOf('day');
    item: BillPaymentDto;
    activityLog: ActivityLogInput = new ActivityLogInput();
    activityType: number = 6;
    priorityType: number = 1;
    activityName = "";
    actionNote = "";
    id = 0;
    actionId: number = 12;
    sectionId: number = 35;
    allEmailTemplates: CommonLookupDto[];
    allSMSTemplates: CommonLookupDto[];
    drpleadactivity: CommonLookupDto[];
    emailData = '';
    smsData = '';
    emailTemplate: number;
    uploadUrl: string;
    uploadedFiles: any[] = [];
    myDate = new Date();
    total = 0;
    credit = 0;
    customeTagsId = 0;
    role: string = '';
    subject = '';
    userName: string;
    leadCompanyName: any;
    pageid = 0;
    allUsers: UserListDto[];
    referanceId = 0;
    CurrentDate = new Date();
    //freebieTransport: JobPromotionPromotionMasterLookupTableDto[];
    emailfrom: string = '';
    emailto: string = '';
    fromemails: CommonLookupDto[];
    leadid: number;
    criteriaavaibaleornot = false;
    smsid = false;
    htmlstr: string = '';
    htmlContentbidy: any;
    quoteNumber: string = '';
    constructor(
        injector: Injector, private _http: HttpClient,
        private _quoteServiceProxy: QuotationsServiceProxy,
        private _commonLookupServiceProxy: CommonLookupServiceProxy,
        private _pdfTemplateServiceProxy: PdfTemplateServiceProxy,
        private _paymentIssueServiceProxy: PaymentIssueServiceProxy,
        @Inject(DOCUMENT) private document: Document,
        private spinner: NgxSpinnerService,
        private _dataTransfer: DataTransferService,
        private sanitizer: DomSanitizer,
        //private numtoword : NumberToWordsPipe,
        private _dateTimeService: DateTimeService,
    ) {
        super(injector);
        this.item = new BillPaymentDto();
        //this.item.leads = new LeadsDto();       
    }



    ngOnInit(): void {

    }

    show(leaddata, billFlag): void {
        this.spinner.show();


        // let ttt = this.numtoword.transform(55);
        this._paymentIssueServiceProxy.billPaymentDetails(parseInt(this._dataTransfer.getOrgData()),leaddata).subscribe(result => {
            this.item = result;

            //this.quoteNumber = result.quoteNumber
            // this.leadCompanyName = result.leads.firstName;
            // if (result.leads.emailId != null) {
            //     this.emailto = result.leads.emailId;
            // }
            let tempid;
            if (billFlag == 'SS') {
                tempid = AppConsts.pdfGenerateDocument.SS
            }
            else {
                tempid = AppConsts.pdfGenerateDocument.STC
            }
            this._pdfTemplateServiceProxy.getPdfTemplateForView(tempid, parseInt(this._dataTransfer.getOrgData())).subscribe(result => {
                if (result.pdfTemplates != undefined) {
                    this.htmlContentbidy = result.pdfTemplates.viewHtml;
                    this.setHTML(this.htmlContentbidy);
                    this.spinner.hide();
                } else {
                    this.spinner.hide();                    
                    this.notify.info("Please First Create Quotation Template");
                }

            }, error => { this.spinner.hide(); });
        });
    }

    close(): void {
        this.modal.hide();
    }
    teststr: string = '';
    setHTML(pdfHTML) {
        let pdfTemplate = this.getPdfTemplate(pdfHTML);
        this.teststr = pdfHTML;
        this.htmlContentbidy = pdfTemplate;
        this.htmlContentbidy = this.sanitizer.bypassSecurityTrustHtml(this.htmlContentbidy);

        if (pdfHTML != '') {
            this.modal.show();
        }

    }

    getPdfTemplate(smsHTML) {
        let myTemplateStr = smsHTML;
        let TableList = [];
        let BankList = [];

        let QrcodeImg;
        if (this.item.organizationbankDetails != null) {
            this.item.organizationbankDetails.forEach(obj => {
                QrcodeImg = '';
                BankList.push("<div class='payment-panel'><div class='pay-content'>Bank : " + obj.orgBankName + "</div><div class='pay-content'>A/C Name : " + obj.orgBankBrachName + "</div><div class='pay-content'>A/C No. : " + obj.orgBankAccountNumber + "</div><div class='pay-content'>IFSC : " + obj.orgBankIFSC_Code + "</div></div>");
                QrcodeImg = AppConsts.remoteServiceBaseUrl + obj.orgBankQrCode_path;
            });
            BankList.push("<div class='payment-panel-code'><img src='" + QrcodeImg + "'></div>");
        }
        let addressValue = this.item.leaddata.address; // + " " + this.item.address2; //+ ", " + this.item.lead.state + "-" + this.item.lead.postCode;
        // let signdate = this._dateTimeService.getStartOfDayForDate(this.item.quotateSignedDate)
        // let generatedate = this.item.quoteGenerateDate;
        let currentdate = this._dateTimeService.getUTCDate().day + '/' + this._dateTimeService.getUTCDate().month + '/' + this._dateTimeService.getUTCDate().year
        let myVariables = {
            Customer: {
                Name: this.item.leaddata.customerName,
                Address: addressValue,
                Mobile: this.item.leaddata.mobileNumber,
                Email: this.item.leaddata.emailId,
                // Phone: this.item.leads.alt_Phone,
                // SalesRep: this.item.currentAssignUserName
            },
            Jobs: {
                JobNumber: this.item.jobdata.JobNumber,
                OrderNo: this.item.jobdata.JobNumber,
                DispatchNo: '04514',
                Through: '',
                Delivery: '',
                ApplicationSubmitedOn: this.item.jobdata.applicationDate,
                SSDate: '',
                STCDate: '',
                DeliveryNoteDate: '',
                Destination: '',
                HSN_SAC: 840000,
                Quantity: 1,
                Rate: 37500,
                Per: 'Pcs'
                // QuoteSinedDate: this.item.quotateSignedDate == undefined ? currentdate : signdate.day + "/" + signdate.month + "/" + signdate.year,
                // QuoteSignedImagePath: this.item.quotateSignedImagePath == undefined ? '' : '<img src=' + AppConsts.docUrl + "/" + this.item.quotateSignedImagePath + ' alt="img" >',
                // SystemCapacity: this.item.pvCapacityKw != null ? this.item.pvCapacityKw + " " : this.item.pvCapacityKw,
                // QuoteGenerateDate: generatedate.day +"/"+generatedate.month +"/"+ generatedate.year ,

            },
            Bill: {
                SolarGridAmount: Math.round(this.item.solarGridAmount),
                SystemSize: Math.round(this.item.systemSize),
                TotalSystemPrice: Math.round(this.item.totalSystemPrice),
                Subsidy20: Math.round(this.item.subsidy20),
                Subsidy40: Math.round(this.item.subsidy40),
                TotalSubSidy: Math.round(this.item.totalSubSidy),
                SystemSuplyCGST: Math.round(this.item.systemSuplyCGST),
                SystemSuplySGST: Math.round(this.item.systemSuplySGST),
                SolarServiceAmount: Math.round(this.item.solarServiceAmount),
                SolarServiceCGST: Math.round(this.item.solarServiceCGST),
                SolarServiceSGST: Math.round(this.item.solarServiceSGST),
                TotalAmount: Math.round(this.item.totalAmount),
                AmountChargeword: '',
                TaxableValue6: Math.round(this.item.solarGridAmount),
                TaxableValue9: Math.round(this.item.solarServiceAmount),
                CentralTaxAmount6: Math.round(this.item.systemSuplyCGST),
                CentralTaxAmount9: Math.round(this.item.systemSuplySGST),
                StateTaxAmount6: Math.round(this.item.solarServiceCGST),
                StateTaxAmount9: Math.round(this.item.solarServiceSGST),
                TotalTaxAmount6: Math.round(this.item.solarGridAmount) + Math.round(this.item.solarServiceCGST),
                TotalTaxAmount9: Math.round(this.item.solarServiceAmount) + Math.round(this.item.solarServiceSGST),
                TotalTaxableAmount: Math.round(this.item.solarGridAmount) + Math.round(this.item.solarServiceAmount),
                TotalCentralTaxAmount: Math.round(this.item.systemSuplyCGST) + Math.round(this.item.systemSuplySGST),
                TotalStateTaxAmount: Math.round(this.item.solarServiceCGST) + Math.round(this.item.solarServiceSGST),
                TotalTax: (this.item.solarGridAmount + this.item.solarServiceCGST) + (this.item.solarServiceAmount + this.item.solarServiceSGST),
                TaxAmountword: ''
            },
            Organization: {
                orgName: this.item.organizationDetails[0]['displayName'],
                orgEmail: this.item.organizationDetails[0]['organization_Email'],
                orgMobile: this.item.organizationDetails[0]['organization_Mobile'],
                orgLogo: AppConsts.docUrl + "/" + this.item.organizationDetails[0]['organization_LogoFilePath'],  //this.item.organizationDetails[0]['organization_GSTNumber']
                orgWebsite: this.item.organizationDetails[0]['organization_Website'],
                bankDetails: BankList.toString().replace(/,/g, ''),
            },
        }

        // use custom delimiter {{ }}
        _.templateSettings.interpolate = /{{([\s\S]+?)}}/g;

        // interpolate
        let compiled = _.template(myTemplateStr);
        let myTemplateCompiled = compiled(myVariables);
        return myTemplateCompiled;
    }



    generatePdf() {
        this.spinner.show();
        this.htmlstr = this.htmlContentbidy.changingThisBreaksApplicationSecurity; // this.teststr.toString().replace('"',"'");// this.htmlContentbidy.changingThisBreaksApplicationSecurity;
        this._quoteServiceProxy.generatePdf(this.SelectedJobNumber, this.htmlstr).subscribe(result => {
            let FileName = result;
            this.saving = false;
            this.spinner.hide();
            window.open(result, "_blank");
        }, error => { this.spinner.hide(); })
    }
}