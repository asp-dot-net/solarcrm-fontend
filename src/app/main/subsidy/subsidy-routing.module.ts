import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
//import { TransportTrackerComponent } from './transport-tracker/transport-tracker.component';
import { SubsidyTrackerComponent } from '../subsidy/subsidy-tracker/subsidy-tracker.componet';
import { SubsidyClaimComponent } from './subsidy-claim/subsidy-Claim.componet';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '',
                // children: [
                //     {
                //         path: 'manageleads',
                //         loadChildren: () => import('./manageleads/manageleads.module').then(m => m.ManageLeadsModule),
                //         //data: { permission: 'Pages.Tenant.DataVaults.Customer', features: 'App.DataVaults.Customer' },
                //     },
                //      {
                //          path: 'leadsactivity',
                //          loadChildren: () => import('../leads/leadactivity/leadsactivity.module').then(m => m.LeadsActivityModule),
                //     //     data: { permission: 'Pages.Tenant.DataVaults.Customer' },
                //      }
                // ]
            },

            { path: 'subsidy-tracker', component: SubsidyTrackerComponent },
            { path: "subsidy-claim", component: SubsidyClaimComponent }
        ]),
    ],
    exports: [RouterModule],
})
export class SubsidyRoutingModule { }