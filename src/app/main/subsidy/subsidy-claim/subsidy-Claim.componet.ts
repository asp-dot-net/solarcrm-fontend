import { OnInit, Component, Injector, ViewEncapsulation, ViewChild, Input, Output, EventEmitter, AfterViewInit, HostListener } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ActivatedRoute, Router } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { LazyLoadEvent } from 'primeng/api';
import { LeadsServiceProxy, LeadsDto, CommonLookupServiceProxy, CommonLookupDto, UserServiceProxy, OrganizationUnitDto, UserListDto, SubsidyTrackerServiceProxy, SubsidyTrackerSummaryCount, SubsidyClaimSummaryCount, SubsidyClaimServiceProxy } from '@shared/service-proxies/service-proxies';
import { Paginator } from 'primeng/paginator';
import { Title } from '@angular/platform-browser';
import * as _ from 'lodash';
import * as moment from 'moment';
import { DataTransferService } from '@app/shared/common/projectservice/data-transfer.service';
import { AppConsts } from '@shared/AppConsts';
import { HttpClient } from '@angular/common/http';
import { FileUpload } from 'primeng/fileupload';
import { Table } from 'primeng/table';
import { DateTime } from 'luxon';
import { DateTimeService } from '@app/shared/common/timing/date-time.service';
import { ViewMyLeadComponent } from '@app/main/leads/customerleads/view-mylead.component';
import { FileDownloadService } from '@shared/utils/file-download.service';

@Component({
    selector: 'SubsidyClaim',
    templateUrl: './subsidy-Claim.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class SubsidyClaimComponent extends AppComponentBase implements OnInit {
    @ViewChild('ExcelFileUpload', { static: false }) excelFileUpload: FileUpload;
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;   
    @ViewChild('viewLeadDetail', { static: true }) viewLeadDetail: ViewMyLeadComponent;
    @Input() SectionName: string = "SubsidyTracker";
    @Output() reloadLead = new EventEmitter();
    leadName: string;
    advancedFiltersAreShown = false;
    FiltersData = false;
    active = false;
    saving = false;
    recordCount: any;
    leadId: any;
    filterText: string = '';
    @Input() SelectedLeadId: number = 0;
    sampleDateRange: moment.Moment[] = [moment().add(0, 'days').endOf('day'), moment().add(0, 'days').endOf('day')];
    ExpandedView: boolean = true;
    shouldShow: boolean = false;
    public screenWidth: any;
    public screenHeight: any;
    PageNo: number;
    allOrganizationUnits: OrganizationUnitDto[];
    allOrganizationUnitsList: OrganizationUnitDto[];
    organizationUnitlength: number = 0;
    organizationUnit = 0;
    applicationstatus: number = 0;
    leadstatuss: CommonLookupDto[];
    allStates: CommonLookupDto[];
    allapplicationStatus: CommonLookupDto[];
    allSolarType: CommonLookupDto[];
    allEmployeeList: UserListDto[];
    allDiscom: CommonLookupDto[];
    allDivision: CommonLookupDto[];
    allSubDivision: CommonLookupDto[];
    allTenderData: CommonLookupDto[];
    allCircleData: CommonLookupDto[];
    leadStatusIDS = [];
    leadSourceIdFilter = [];
    leadApplicationStatusFilter = [];
    subsidyTrackerFilter: number;
    subsidyOcCopyFilter: number;
    subsidyReceivedFilter: number;
    subsidyFileVerifiedFilter = "";
    employeeIdFilter = [];
    DiscomIdFilter = [];
    CircleIdFilter = [];
    DivisionIdFilter = [];
    SubDivisionIdFilter = [];
    solarTypeIdFilter = [];
    TenderIdFilter = [];
    FilterbyDate = '';
    date: Date = new Date('01/01/2022');
    StartDate: DateTime = this._dateTimeService.getStartOfDayForDate(this.date);
    EndDate: DateTime = this._dateTimeService.getDate();
    subsidySummaryCount: SubsidyTrackerSummaryCount;
    sectionId: number = AppConsts.SectionPage.ApplicationTracker;
    flag = true;
    testHeight = 280;
    contentHeight = 230;
    show1: boolean = true;
    toggle: boolean = true;
    
    toggleBlock() {
        this.show1 = !this.show1;
    };
    change() {
        this.toggle = !this.toggle;
    }
    constructor(
        injector: Injector,
        private _leadsServiceProxy: LeadsServiceProxy,
        private _commonLookupService: CommonLookupServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private titleService: Title,
        private _router: Router,
        private _subsidyClaimServiceProxy: SubsidyClaimServiceProxy,
        private _dataTransfer: DataTransferService,
        private _dateTimeService: DateTimeService,
        private _fileDownloadService: FileDownloadService,
    ) {
        super(injector);
      
    }
    @HostListener('window:resize', ['$event'])
    onResize(event) {
        this.screenHeight = window.innerHeight;
    }
    testHeightSize() {
        if (this.FiltersData == true) {
            this.testHeight = this.testHeight + -82;
        }
        else {
            this.testHeight = this.testHeight - -82;
        }
    }
    ngOnInit(): void {
        this.screenHeight = window.innerHeight;
        this.subsidySummaryCount = new SubsidyTrackerSummaryCount();
        this.screenHeight = window.innerHeight;
        this._commonLookupService.getAllApplicationStatusForDropdown().subscribe(result => {
            this.allapplicationStatus = result;
        }, error => { this.spinnerService.hide() });
        this._commonLookupService.getAllLeadStatusDropdown().subscribe(result => {
            this.leadstatuss = result;
        }, error => { this.spinnerService.hide() });
        this._commonLookupService.getAllDiscomFilterDropdown(0).subscribe(result => {
            this.allDiscom = result;
        }, error => { this.spinnerService.hide() });

        this._commonLookupService.getAllSubdivisionWiseData('').subscribe(result => {
            this.allSubDivision = result;
        }, error => { this.spinnerService.hide() });
        this._commonLookupService.getAllSolarTypeForDropdown().subscribe(result => {
            this.allSolarType = result;
        }, error => { this.spinnerService.hide() });
        this._commonLookupService.getAllUserList().subscribe(result => {
            this.allEmployeeList = result;
        }, error => { this.spinnerService.hide() });
        this._commonLookupService.getOrganizationUnitUserWise().subscribe(output => {
            this.allOrganizationUnits = output;
            this.organizationUnit = this.allOrganizationUnits[0].id;
            this.organizationUnitlength = this.allOrganizationUnits.length;
            //change Organization
            this.allOrganizationUnitsList = this.allOrganizationUnits.filter(item => item.id != this.organizationUnit);
            this.getSubsidyClaimList();
        }, error => { this.spinnerService.hide() })
    }
    expandGrid() {
        this.ExpandedView = true;
        this.getSubsidyClaimList();
    }
    show(leadId?: number, param?: number, sectionId?: number): void {
        this.leadId = leadId;
        
    }
    getSubsidyClaimList(event?: LazyLoadEvent) {
        this._dataTransfer.setOrgData(this.organizationUnit.toString());
        this.PageNo = this.primengTableHelper.getSkipCount(this.paginator, event);
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            //return;
        }
        this.primengTableHelper.showLoadingIndicator();
        this.subsidySummaryCount = new SubsidyClaimSummaryCount();      
        this._subsidyClaimServiceProxy.getAllSubsidyClaim(
            this.filterText,
            this.organizationUnit,
            this.leadStatusIDS,
            this.employeeIdFilter,
            this.leadApplicationStatusFilter,
            this.subsidyTrackerFilter,
            this.subsidyOcCopyFilter,
            this.subsidyReceivedFilter,
            this.subsidyFileVerifiedFilter,
            this.DiscomIdFilter,
            //this.CircleIdFilter,
            this.DivisionIdFilter,
           // this.SubDivisionIdFilter,          
            this.solarTypeIdFilter,
            this.TenderIdFilter,
            this.FilterbyDate,
            this.StartDate,
            this.EndDate,
            undefined,
            this.primengTableHelper.getSorting(this.dataTable),
            this.primengTableHelper.getSkipCount(this.paginator, event),
            this.primengTableHelper.getMaxResultCount(this.paginator, event)
        ).subscribe((result) => {
            this.primengTableHelper.totalRecordsCount = result.totalCount;
            this.primengTableHelper.records = result.items;
            this.primengTableHelper.hideLoadingIndicator();
            this.recordCount = result.totalCount;
            this.subsidySummaryCount = result.items[0].subsidyClaimSummaryCount;
            this.shouldShow = false;
            if (this.flag == false && result.totalCount != 0 && this.ExpandedView == false) {
                this.navigateToLeadDetail(this.SelectedLeadId)
            }
            else {
                if (this.ExpandedView == false && result.totalCount != 0) {
                    this.navigateToLeadDetail(result.items[0].subsidyClaim.leaddata.id)
                }
                else {
                    this.ExpandedView = true;
                }
            }
        }, error => { this.spinnerService.hide() });
    }

    navigateToLeadDetail(leaddata): void {
        this.ExpandedView = !this.ExpandedView;
        this.ExpandedView = false;
        this.SelectedLeadId = leaddata.subsidyClaim.leaddata.id;
        this.leadName = "SubsidyClaim"
        debugger
        this.viewLeadDetail.showDetail(leaddata.subsidyClaim.leaddata, leaddata.subsidyClaim.applicationStatusdata, this.leadName, this.sectionId);
    }
    reloadPage(flafValue: boolean): void {
        this.ExpandedView = true;
        this.flag = flafValue;
        this.paginator.changePage(this.paginator.getPage());
        if (!flafValue) {
            this.navigateToLeadDetail(this.SelectedLeadId);
        }
        this.paginator.changePage(this.paginator.getPage());
    }
    changeDiscom() {
        if (this.DiscomIdFilter.length > 0) {
            this._commonLookupService.getDiscomWiseCircleFilterForDropdown(this.DiscomIdFilter).subscribe(result => {
                this.allCircleData = result;
                if (this.allCircleData.length == 0) {
                    this.allCircleData = undefined;
                }
            }, error => { this.spinnerService.hide() });
        }
        else {
            this.allCircleData = [];
            this.CircleIdFilter = [];
            this.allDivision = [];
            this.DivisionIdFilter = [];
            this.allSubDivision = [];
            this.SubDivisionIdFilter = [];
        }
    }
    changeCircle() {
        if (this.CircleIdFilter.length > 0) {
            this._commonLookupService.getCircleWiseDivisionFilterForDropdown(this.CircleIdFilter).subscribe(result => {
                this.allDivision = result;
                if (this.allDivision.length == 0) {
                    this.allDivision = undefined
                }
            }, error => { this.spinnerService.hide() });
        }
        else {
            this.allDivision = [];
            this.DivisionIdFilter = [];
            this.allSubDivision = [];
            this.SubDivisionIdFilter = [];
        }
    }
    changeDivision() {
        if (this.CircleIdFilter.length > 0) {
            this._commonLookupService.getDivisionWiseSubDivisionFilterForDropdown(this.DivisionIdFilter).subscribe(result => {
                this.allSubDivision = result;
                if (this.allSubDivision.length == 0) {
                    this.allSubDivision = undefined
                }
            }, error => { this.spinnerService.hide() });
        }
        else {
            this.allSubDivision = [];
            this.SubDivisionIdFilter = [];
        }
    }
    leadstatuschange() {
        if (this.leadStatusIDS.includes(6)) {
            //this.showJobStatus = true;
        } else {
            //this.showJobStatus = false;
            //this.jobStatusID = [];
        }
    }
    changeSolarType() {
        this.allTenderData = undefined;
        this._commonLookupService.getSolarTypeWiseTenderForDropdown(this.solarTypeIdFilter).subscribe(result => {
            this.allTenderData = result;
            if (this.allTenderData.length == 0) {
                this.allTenderData = undefined;
            }
        }, error => { this.spinnerService.hide() });
    }
    exportToExcel(): void {
        this._subsidyClaimServiceProxy.getSubsidyClaimToExcel(
            this.filterText,
            this.organizationUnit,
            this.leadStatusIDS,
            this.employeeIdFilter,
            this.leadApplicationStatusFilter,
            this.subsidyTrackerFilter,
            this.subsidyOcCopyFilter,
            this.subsidyReceivedFilter,
            this.subsidyFileVerifiedFilter,
            this.DiscomIdFilter,
            //this.CircleIdFilter,
            this.DivisionIdFilter,
           // this.SubDivisionIdFilter,          
            this.solarTypeIdFilter,
            this.TenderIdFilter,
            this.FilterbyDate,
            this.StartDate,
            this.EndDate,
            undefined,
            this.primengTableHelper.getSorting(this.dataTable),
            undefined,
            undefined
        )
            .subscribe(result => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }
    clear() {
        this.filterText = '';
        this.leadStatusIDS = [];
        this.leadApplicationStatusFilter = [];
        this.allCircleData = [];
        this.DiscomIdFilter = [];
        this.CircleIdFilter = [];
        this.solarTypeIdFilter = [];
        this.TenderIdFilter = [];
        this.employeeIdFilter = [];
        this.FilterbyDate = '';
        this.StartDate = this._dateTimeService.getStartOfDayForDate(this.date);
        this.EndDate = this._dateTimeService.getDate();
        this.getSubsidyClaimList();
    }
    downfile(filename): void {
        let FileName = AppConsts.remoteServiceBaseUrl + filename;//this.LeadDocument.filePath;
        window.open(FileName, "_blank");
    };
   
    close() {
        //this.modal.hide();
    }
}
