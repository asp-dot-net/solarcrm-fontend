import { AfterViewChecked, Component, ElementRef, EventEmitter, Injector, Output, ViewChild, ViewEncapsulation } from '@angular/core';
import { AppConsts } from '@shared/AppConsts';
import { AppComponentBase } from '@shared/common/app-component-base';
import {
    CreateOrUpdateUserInput,
    OrganizationUnitDto,
    PasswordComplexitySetting,
    ProfileServiceProxy,
    UserEditDto,
    UserRoleDto,
    UserServiceProxy,
    GetUserForEditOutput,
    CommonLookupServiceProxy,
    CommonLookupDto,
    StateServiceProxy,
    UserDetailsDto
} from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap/modal';
import {
    IOrganizationUnitsTreeComponentData,
    OrganizationUnitsTreeComponent,
} from '../shared/organization-unit-tree.component';
import { map as _map, filter as _filter } from 'lodash-es';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'createOrEditUserModal',
    templateUrl: './create-or-edit-user-modal.component.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['create-or-edit-user-modal.component.less']
})
export class CreateOrEditUserModalComponent extends AppComponentBase {
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @ViewChild('organizationUnitTree') organizationUnitTree: OrganizationUnitsTreeComponent;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;
    canChangeUserName = true;
    isTwoFactorEnabled: boolean = this.setting.getBoolean('Abp.Zero.UserManagement.TwoFactorLogin.IsEnabled');
    isLockoutEnabled: boolean = this.setting.getBoolean('Abp.Zero.UserManagement.UserLockOut.IsEnabled');
    passwordComplexitySetting: PasswordComplexitySetting = new PasswordComplexitySetting();

    user: UserEditDto = new UserEditDto();
    userDetails: UserDetailsDto = new UserDetailsDto();

    roles: UserRoleDto[];
    sendActivationEmail = true;
    setRandomPassword = true;
    passwordComplexityInfo = '';
    profilePicture: string;

    allOrganizationUnits: OrganizationUnitDto[];
    memberedOrganizationUnits: string[];
    userPasswordRepeat = '';
    stateList: any = [];
    userTypeList: any = [];
    districtList: any = [];
    talukaList: any = [];
    cityList: any = [];
    employeeList: any = [];
    discomList: any = [];
    departmentList: CommonLookupDto[];
    departmentIds: any = [];
    dateOfBirth: string;
    isEmployee = false;
    isChannelPartner = false;
    isInstaller = false;  

    constructor(
        injector: Injector,
        private _userService: UserServiceProxy,
        private _profileService: ProfileServiceProxy,
        private _commonLookupServiceProxy: CommonLookupServiceProxy,
        private _stateServiceProxy: StateServiceProxy
    ) {
        super(injector);
    }
    ngOnInit(): void {
        this._commonLookupServiceProxy.getAllUserTypes().subscribe((result) => {
            this.userTypeList = result;
        })

        this._stateServiceProxy.getAllStateForDropdown().subscribe(result => {
            this.stateList = result;
        });

        this._commonLookupServiceProxy.getEmployeesByRole('Manager').subscribe(result => {
            this.employeeList = result;
        });

        this._commonLookupServiceProxy.getDiscomForDropdown().subscribe(result => {
            this.discomList = result;
        });

        this._commonLookupServiceProxy.getDepartmentForDropdown().subscribe(result => {
            this.departmentList = result;
        });
    }
    show(userId?: number): void {
         this.user = new UserEditDto();
        if (!userId) {
            this.active = true;
            this.setRandomPassword = true;
            this.sendActivationEmail = true;
        }       

        // this.departmentIds = this.sa.split(',').map(function(item){
        //     return parseInt(item, 10);
        // });

        this._userService.getUserForEdit(userId).subscribe((userResult) => {
            this.user = userResult.user;
            this.roles = userResult.roles;
            this.userDetails = userResult.user.userDetails;

            this.canChangeUserName = this.user.userName !== AppConsts.userManagement.defaultAdminUserName;

            this.allOrganizationUnits = userResult.allOrganizationUnits;
            this.memberedOrganizationUnits = userResult.memberedOrganizationUnits;

            this.getProfilePicture(userId);

            if (userId) {
                this.active = true;

                setTimeout(() => {
                    this.setRandomPassword = false;
                }, 0);

                this.sendActivationEmail = false;
            }
            if (this.userDetails != undefined) {
                if (this.userDetails.state != null) {
                    setTimeout(() => {
                        this.BindDistrict(this.userDetails.state);
                    }, 500);
                }
                if (this.userDetails.district != null) {
                    setTimeout(() => {
                        this.BindTaluka(this.userDetails.district);
                    }, 500);
                }
                if (this.userDetails.taluko != null) {
                    setTimeout(() => {
                        this.BindCity(this.userDetails.taluko);
                    }, 300);
                }
            }
            
            if (this.userDetails.department == null || this.userDetails.department == "") {
                this.departmentIds = [];
            }
            else {
                this.departmentIds = this.userDetails.department.split(',').map(function (item) {
                    return parseInt(item, 10);
                });
            }
            this.changeUserType(this.userDetails.userType);

            this._profileService.getPasswordComplexitySetting().subscribe((passwordComplexityResult) => {
                this.passwordComplexitySetting = passwordComplexityResult.setting;
                this.setPasswordComplexityInfo();
                this.modal.show();
            });
        });
    }

    setPasswordComplexityInfo(): void {
        this.passwordComplexityInfo = '<ul>';

        if (this.passwordComplexitySetting.requireDigit) {
            this.passwordComplexityInfo += '<li>' + this.l('PasswordComplexity_RequireDigit_Hint') + '</li>';
        }

        if (this.passwordComplexitySetting.requireLowercase) {
            this.passwordComplexityInfo += '<li>' + this.l('PasswordComplexity_RequireLowercase_Hint') + '</li>';
        }

        if (this.passwordComplexitySetting.requireUppercase) {
            this.passwordComplexityInfo += '<li>' + this.l('PasswordComplexity_RequireUppercase_Hint') + '</li>';
        }

        if (this.passwordComplexitySetting.requireNonAlphanumeric) {
            this.passwordComplexityInfo += '<li>' + this.l('PasswordComplexity_RequireNonAlphanumeric_Hint') + '</li>';
        }

        if (this.passwordComplexitySetting.requiredLength) {
            this.passwordComplexityInfo +=
                '<li>' +
                this.l('PasswordComplexity_RequiredLength_Hint', this.passwordComplexitySetting.requiredLength) +
                '</li>';
        }

        this.passwordComplexityInfo += '</ul>';
    }

    getProfilePicture(userId: number): void {
        if (!userId) {
            this.profilePicture = this.appRootUrl() + 'assets/common/images/default-profile-picture.png';
            return;
        }

        this._profileService.getProfilePictureByUser(userId).subscribe((result) => {
            if (result && result.profilePicture) {
                this.profilePicture = 'data:image/jpeg;base64,' + result.profilePicture;
            } else {
                this.profilePicture = this.appRootUrl() + 'assets/common/images/default-profile-picture.png';
            }
        });
    }

    onShown(): void {
        this.organizationUnitTree.data = <IOrganizationUnitsTreeComponentData>{
            allOrganizationUnits: this.allOrganizationUnits,
            selectedOrganizationUnits: this.memberedOrganizationUnits,
        };

        document.getElementById('Name').focus();
    }

    save(): void {
        let input = new CreateOrUpdateUserInput();
        input.user = this.user;
        input.setRandomPassword = this.setRandomPassword;
        input.sendActivationEmail = this.sendActivationEmail;
        input.assignedRoleNames = _map(
            _filter(this.roles, { isAssigned: true, inheritedFromOrganizationUnit: false }),
            (role) => role.roleName
        );

        input.organizationUnits = this.organizationUnitTree.getSelectedOrganizations();
        input.user.userDetails.department = this.departmentIds.toString();

        this.saving = true;
        this._userService
            .createOrUpdateUser(input)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
            });

        //console.log(this.departmentIds.toString());
    }

    close(): void {
        this.active = false;
        this.userPasswordRepeat = '';
        this.modal.hide();
    }

    getAssignedRoleCount(): number {
        return _filter(this.roles, { isAssigned: true }).length;
    }

    isSMTPSettingsProvided(): boolean {
        return !(
            this.s('Abp.Net.Mail.DefaultFromAddress') === '' ||
            this.s('Abp.Net.Mail.Smtp.Host') === '' ||
            this.s('Abp.Net.Mail.Smtp.UserName') === '' ||
            this.s('Abp.Net.Mail.Smtp.Password') === ''
        );
    }

    BindDistrict(state): void {
        this.districtList = [];
        this._commonLookupServiceProxy.getDistrictByState(state).subscribe(result => {
            this.districtList = result;
        });
    }

    BindTaluka(district): void {
        this.talukaList = [];
        this._commonLookupServiceProxy.getTalukaByDistrict(district).subscribe(result => {
            this.talukaList = result;
        });
    }

    BindCity(taluka): void {
        this.cityList = [];
        this._commonLookupServiceProxy.getCityByTaluka(taluka).subscribe(result => {
            this.cityList = result;
        });
    }

    changeUserType(value): void {
        if (value == 1) {
            this.isEmployee = true;

            this.isChannelPartner = false;
            this.isInstaller = false;
        }
        else if (value == 2) {
            this.isChannelPartner = true;

            this.isEmployee = false;
            this.isInstaller = false;
        }
        else if (value == 3) {
            this.isInstaller = true;

            this.isEmployee = false;
            this.isChannelPartner = false;
        }
        else {
            this.isInstaller = false;
            this.isEmployee = false;
            this.isChannelPartner = false;
        }
    }
}
