import { ChangeDetectorRef, Component, ElementRef, EventEmitter, Injector, Output, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import {
    CreateOrEditOrgBankDetailsDto,
    CreateOrganizationUnitInput,
    OrganizationUnitDto,
    OrganizationUnitServiceProxy,
    UpdateOrganizationUnitInput,
} from '@shared/service-proxies/service-proxies';
import { AppConsts } from '@shared/AppConsts';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { IAjaxResponse, TokenService } from 'abp-ng2-module';
import { FileItem, FileUploader, FileUploaderOptions } from 'ng2-file-upload';
export interface IOrganizationUnitOnEdit {
    id?: number;
    parentId?: number;
    displayName?: string;
    organizationCode?: string;
    projectId?: string;
    defaultFromAddress?: string;
    defaultFromDisplayName?: string;
    email?: string;
    mobile?: string;
    filetoken?: string;
    logoFileName?: string;
    logoFilePath?: string;
    gstNumber?: string;
    panNumber?: string;
    cinNumber?: string;
    merchantId?: string;
    publishKey?: string;
    secrateKey?: string;
    address?: string;
    website?: string;
    contactNo?: string;
    sms_authorizationKey?: string;
    sms_clientId?: string;
    sms_senderId?: string;
    sms_msgType?: string;
    qrCodeFilePath?: string;
    OrgBankDetails?: CreateOrEditOrgBankDetailsDto[]
}

@Component({
    selector: 'createOrEditOrganizationUnitModal',
    templateUrl: './create-or-edit-unit-modal.component.html',
})
export class CreateOrEditUnitModalComponent extends AppComponentBase {
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @ViewChild('organizationUnitDisplayName', { static: true }) organizationUnitDisplayNameInput: ElementRef;

    @Output() unitCreated: EventEmitter<OrganizationUnitDto> = new EventEmitter<OrganizationUnitDto>();
    @Output() unitUpdated: EventEmitter<OrganizationUnitDto> = new EventEmitter<OrganizationUnitDto>();
    myInputVariable: ElementRef;
    saving = false;
    active = false;
    public uploader: FileUploader;
    public maxfileBytesUserFriendlyValue = 5;
    private _uploaderOptions: FileUploaderOptions = {};
    organizationUnit: IOrganizationUnitOnEdit = {};
    OrgBankDetails: CreateOrEditOrgBankDetailsDto[];
    newBank = false;
    activeTabIndex: number = (abp.clock.provider.supportsMultipleTimezone) ? 0 : 1;
    filetokenOrgImg: string;
    filetokenQrCodeIgm: string;
    OrganizationfileName: string;
    QrCodefileName: string;
    public temporaryPictureUrl: string;
    public fileupload: FileUploader;
    public temporaryFileUrl: string;
    private _fileuploadoption: FileUploaderOptions = {};
    constructor(
        injector: Injector,
        private _organizationUnitService: OrganizationUnitServiceProxy,
        private _changeDetector: ChangeDetectorRef,
        private _tokenService: TokenService

    ) {
        super(injector);
    }

    onShown(): void {
        document.getElementById('OrganizationUnitDisplayName').focus();

    }
    QrCodeImgPath:string = '';
    QrgImgPath : string = '';
    show(organizationUnit: IOrganizationUnitOnEdit): void {
        this.organizationUnit = organizationUnit;
        this.QrgImgPath = AppConsts.remoteServiceBaseUrl + this.organizationUnit['logoFilePath'];
        this.OrgBankDetails = [];

        var orgBankcreateOrEdit = new CreateOrEditOrgBankDetailsDto();
       
        if (this.organizationUnit.OrgBankDetails == null || this.organizationUnit.OrgBankDetails.length == 0) {
            this.OrgBankDetails.push(orgBankcreateOrEdit);
        }
        else {
            this.OrgBankDetails = this.organizationUnit.OrgBankDetails; //.push(banklist);
            this.QrCodeImgPath = AppConsts.remoteServiceBaseUrl + this.organizationUnit.OrgBankDetails[0]['orgBankQrCode_path'];
        }
       
        this.active = true;
        this.initializeModal();
        this.modal.show();
        this._changeDetector.detectChanges();
    }
    AddnewBank() {
        this.OrgBankDetails.push(new CreateOrEditOrgBankDetailsDto)
    }
    save(): void {
        if (!this.organizationUnit.id) {
            this.createUnit();
        } else {
            this.updateUnit();
        }
    }

    createUnit() {
        const createInput = new CreateOrganizationUnitInput();
        createInput.parentId = this.organizationUnit.parentId;
        createInput.displayName = this.organizationUnit.displayName;
        createInput.organizationCode = this.organizationUnit.organizationCode;
        createInput.projectId = this.organizationUnit.projectId;
        createInput.organization_Email = this.organizationUnit.email;
        createInput.organization_Mobile = this.organizationUnit.mobile;
        createInput.organization_Address = this.organizationUnit.address;
        createInput.fileTokenOrg = this.filetokenOrgImg;      
        createInput.organization_LogoFileName = this.OrganizationfileName; // organizationUnit.logoFileName;  
        createInput.organization_LogoFilePath = this.organizationUnit.logoFilePath;
        createInput.fileTokenQrCode = this.filetokenQrCodeIgm;
        createInput.organization_QrCodeFileName= this.QrCodefileName;
        createInput.organization_Website = this.organizationUnit.website;
       
        createInput.organization_defaultFromAddress = this.organizationUnit.defaultFromAddress;
        createInput.organization_defaultFromDisplayName = this.organizationUnit.defaultFromDisplayName;
        createInput.smS_authorizationKey = this.organizationUnit.sms_authorizationKey;
        createInput.sms_clientId = this.organizationUnit.sms_clientId;
        createInput.sms_senderId = this.organizationUnit.sms_senderId;
        createInput.sms_msgType = this.organizationUnit.sms_msgType;
        createInput.orgBankDetails = this.OrgBankDetails;
        createInput.organizationGSTNumber = this.organizationUnit.gstNumber;
        createInput.organizationPANNumber = this.organizationUnit.panNumber;
        createInput.organizationCINNumber = this.organizationUnit.cinNumber;
        this.saving = true;
        this._organizationUnitService
            .createOrganizationUnit(createInput)
            .pipe(finalize(() => (this.saving = false)))
            .subscribe((result: OrganizationUnitDto) => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.unitCreated.emit(null);
                this.myInputVariable.nativeElement.value = "";
            });
    }

    updateUnit() {
        const updateInput = new UpdateOrganizationUnitInput();
        updateInput.id = this.organizationUnit.id;
        updateInput.displayName = this.organizationUnit.displayName;
        updateInput.organizationCode = this.organizationUnit.organizationCode;
        updateInput.projectId = this.organizationUnit.projectId;
        updateInput.organization_Email = this.organizationUnit.email;
        updateInput.organization_Mobile = this.organizationUnit.mobile;
        updateInput.organization_Address = this.organizationUnit.address;
        updateInput.fileTokenOrg = this.filetokenOrgImg;
        updateInput.organization_LogoFileName = this.OrganizationfileName; //organizationUnit.logoFileName; // this.OrganizationfileName;
        updateInput.organization_LogoFilePath = this.organizationUnit.logoFilePath; // this.OrganizationfileName;
        updateInput.organization_Website = this.organizationUnit.website;
        updateInput.fileTokenQrCode = this.filetokenQrCodeIgm;
        updateInput.organization_QrCodeFileName = this.QrCodefileName;

        updateInput.organizationGSTNumber = this.organizationUnit.gstNumber;
        updateInput.organizationPANNumber = this.organizationUnit.panNumber;
        updateInput.organizationCINNumber = this.organizationUnit.cinNumber;
        updateInput.organization_defaultFromAddress = this.organizationUnit.defaultFromAddress;
        updateInput.organization_defaultFromDisplayName = this.organizationUnit.defaultFromDisplayName;
        updateInput.smS_authorizationKey = this.organizationUnit.sms_authorizationKey;
        updateInput.sms_clientId = this.organizationUnit.sms_clientId;
        updateInput.sms_senderId = this.organizationUnit.sms_senderId;
        updateInput.sms_msgType = this.organizationUnit.sms_msgType;
        updateInput.bankDetailsList = this.OrgBankDetails;
        
        this.saving = true;
        this._organizationUnitService
            .updateOrganizationUnit(updateInput)
            .pipe(finalize(() => (this.saving = false)))
            .subscribe((result: OrganizationUnitDto) => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.unitUpdated.emit(null);
                this.myInputVariable.nativeElement.value = "";
            });
    }
    initializeModal(): void {

        this.active = true;
        this.temporaryPictureUrl = '';
        this.initFileUploader();

    }
    initFileUploader(): void {

        this.uploader = new FileUploader({ url: AppConsts.remoteServiceBaseUrl + '/Profile/UploadFile' });
        this._uploaderOptions.autoUpload = false;
        this._uploaderOptions.authToken = 'Bearer ' + this._tokenService.getToken();
        this._uploaderOptions.removeAfterUpload = true;
        this.uploader.onAfterAddingFile = (file) => {
            file.withCredentials = false;
        };

        this.uploader.onBuildItemForm = (fileItem: FileItem, form: any) => {
            form.append('FileType', fileItem.file.type);
            form.append('FileName', fileItem.file.name);
            form.append('FileToken', this.guid());
        };

        this.uploader.onSuccessItem = (item, response, status) => {

            const resp = <IAjaxResponse>JSON.parse(response);
            if (resp.success) {
                if (this.fileFlag == 'orgimg') {
                    this.filetokenOrgImg = resp.result.fileToken;
                    this.OrganizationfileName = resp.result.fileName;
                }
                else {
                    this.filetokenQrCodeIgm = resp.result.fileToken;
                    this.QrCodefileName = resp.result.fileName;
                }

            } else {
                this.message.error(resp.error.message);
            }
        };
        this.uploader.setOptions(this._uploaderOptions);
    }

    fileFlag: string = '';
    guid(): string {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
    }

    fileChangeEvent(event: any): void {
        if (event.target.files[0].size > 5242880) { //5MB
            this.message.warn(this.l('OrganizationPicture_Warn_SizeLimit', this.maxfileBytesUserFriendlyValue));
            return;
        }
        this.fileFlag = 'orgimg';
        this.uploader.clearQueue();
        this.uploader.addToQueue([<File>event.target.files[0]]);
        this.uploader.uploadAll();
    }
    fileChangeQrCodeEvent(event: any): void {
        if (event.target.files[0].size > 5242880) { //5MB
            this.message.warn(this.l('QrCodePicture_Warn_SizeLimit', this.maxfileBytesUserFriendlyValue));
            return;
        }
        this.fileFlag = 'qrimg';
        this.uploader.clearQueue();
        this.uploader.addToQueue([<File>event.target.files[0]]);
        this.uploader.uploadAll();
    }
    close(): void {
        this.modal.hide();
        this.active = false;
    }
}
