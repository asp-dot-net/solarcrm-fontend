import { NgModule } from '@angular/core';
import {ChatRoutingModule} from './chat-routing.module'
import { AdminSharedModule } from '@app/admin/shared/admin-shared.module';
import { AppSharedModule } from '@app/shared/app-shared.module';
import {ChatComponent} from './chat.component'


@NgModule({
    declarations: [ChatComponent],
    imports: [AppSharedModule, AdminSharedModule, ChatRoutingModule],
})
export class RolesModule {}
