export class AppConsts {
    static readonly tenancyNamePlaceHolderInUrl = '{TENANCY_NAME}';

    static remoteServiceBaseUrl: string;
    static remoteServiceBaseUrlFormat: string;
    static appBaseUrl: string;
    static appBaseHref: string; // returns angular's base-href parameter value if used during the publish
    static appBaseUrlFormat: string;
    static recaptchaSiteKey: string;
    static subscriptionExpireNootifyDayCount: number;
    static meterApplicationReadyStatus : number = 3;
    static defaultDate:Date = new Date('01/01/2022');
    static localeMappings: any = [];

    static readonly userManagement = {
        defaultAdminUserName: 'admin',
    };

    static readonly localization = {
        defaultLocalizationSourceName: 'solarcrm',
    };

    static readonly authorization = {
        encrptedAuthTokenName: 'enc_auth_token',
    };

    static readonly grid = {
        defaultPageSize: 10,
    };
    static readonly EmailConfigure = {
        emailFrom: "info@solarbridge.com.au",
    };
    static readonly leadActivity = {
        SMS: 1,
        Email: 2,
        Reminder: 3,
        Notify: 4,
        Comment: 5,
        ToDo: 6
    };
    static readonly leadAction = {
        CreatedNewLead: 1,
        LeadModified: 2,
        LeadAssign: 3,
        LeadTransfer: 4,
        LeadStatusChanged: 5,
        SMSSent: 6,
        EmailSent: 7,
        ReminderSent: 8,
        Notify: 9,
        CreatedNewJob: 10,
        JobModified: 11,
        JobQuotation: 12,
        JobInstallation: 13,
        JobStatusChange: 14,
        JobRefund: 15,
        JobInvoice: 16,
        JobDocument: 17,
        JobSTCDetails: 18,
        JobPromotion: 19,
        SMSReply: 20,
        PromotionSMSSent: 21,
        PromotionSMSReply: 22,
        CancelRequest: 23,
        CommentAdded: 24,
        ToDo: 25,
        InstallerInvoice: 26,
        Service: 27,
        Review: 28,
        ServiceCreatd: 29,
        ServiceUpdate: 30,
        MyInstallerModified: 31
    };
    static readonly pdfGenerateDocument = {
        Quotation: 1,
        Picklist: 2,
        SS: 3,
        STC: 4,
        Email: 5,
        SelfCertificateDiscom:6,
        SelfCertificateTorrent:7
    }
    static readonly QuoteModeType = {
        RegularQuote: 0,
        LoanQuote: 1
    };
    static readonly SectionPage = {
        LeadSource:1,
        LeadType:2,
        ManageLead: 35,
        CancelLeads:17,
        RejectLeads:18,
        LeadStatus:30,
        LeadDocument:51,
        MISReport:45,
        ManageActivity:0,
        ManageJob: 65,
        JobCancel:19,
        HoldJob:20,
        JobInstallation:52,
        JobQuotation:53,
        JobRefund:54,       
        PaymentType:47,
        PaymentMode:48,
        paymentDetails:49,
        paymentReceiptUpload:50,
        ApplicationTracker:38,
        EstimatePaidTracker:39,
        DispatchTracker:40,
        PaymentIssuedTracker:41,
        PaymentPaidTracker:42,
        TransportTracker:67,
        MeterConnectTracker:43,
        SerialNoPhotosTracker:44,
        PdfTemplateQuote:0,
        PicklistJob:66,
    };
    static readonly MinimumUpgradePaymentAmount = 1;
    static readonly StockItemBind = {
        SalesStockDropDownBind : 6,
        PicklistStockDropDownBind : 2,
    }
    /// <summary>
    /// Gets current version of the application.
    /// It's also shown in the web page.
    /// </summary>
    static readonly WebAppGuiVersion = '11.1.0';
    //static readonly docUrl = "https://localhost:44301/"; //"http://documents.thesolarproduct.com";
    static readonly docUrl = "D:/APS"; //for Local
    //static readonly docUrl = "https://documents.solarcrm.co.in"; //for Live

}
