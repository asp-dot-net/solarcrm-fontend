export class AppEditionExpireAction {
    static DeactiveTenant = 'DeactiveTenant';
    static AssignToAnotherEdition = 'AssignToAnotherEdition';
    
    static DocumentFormateList = [
        {id: 'PNG', name: 'PNG'},
        {id: 'JPG', name: 'JPG'},
        {id: 'PDF', name: 'PDF'},
        {id: 'DOC', name: 'DOC'},
        {id: 'Docx', name: 'DOCX'}
    ];
}
