import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RequestDocumentComponent } from './request-document.component';
;

const routes: Routes = [
    {
        path: '',
        component: RequestDocumentComponent,
        pathMatch: 'full',
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class RequestDocumentRoutingModule {}