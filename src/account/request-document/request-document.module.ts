import { NgModule } from '@angular/core';
import { AppSharedModule } from '@app/shared/app-shared.module';
import { AccountSharedModule } from '@account/shared/account-shared.module';
import { RequestDocumentComponent } from './request-document.component';
import { RequestDocumentRoutingModule } from './request-document-routing.module';

@NgModule({
    declarations: [RequestDocumentComponent],
    imports: [AppSharedModule, AccountSharedModule, RequestDocumentRoutingModule],
})
export class RequestDocumentModule {}
