import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { AppConsts } from '@shared/AppConsts';
import { accountModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import {
    AccountServiceProxy,
    PasswordComplexitySetting,
    ProfileServiceProxy,
    
} from '@shared/service-proxies/service-proxies';

import { finalize, catchError } from 'rxjs/operators';
import { ReCaptchaV3Service } from 'ngx-captcha';

@Component({
    templateUrl: './signature.component.html',
    animations: [accountModuleAnimation()],
})
export class SignatureComponent extends AppComponentBase implements OnInit {
    
    saving = false;

    constructor(
        injector: Injector,
       
    ) {
        super(injector);
    }

    ngOnInit() {
        //Prevent to register new users in the host context
        // if (this.appSession.tenant == null) {
        //     this._router.navigate(['signature']);
        //     return;
        // }

        // this._profileService.getPasswordComplexitySetting().subscribe((result) => {
        //     //this.passwordComplexitySetting = result.setting;
        // });
    }

    get useCaptcha(): boolean {
        return this.setting.getBoolean('App.UserManagement.UseCaptchaOnRegistration');
    }

   
}
