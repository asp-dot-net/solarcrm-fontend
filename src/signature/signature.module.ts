import { CommonModule,DatePipe } from '@angular/common';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule ,NO_ERRORS_SCHEMA} from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppCommonModule } from '@app/shared/common/app-common.module';
import { UtilsModule } from '@shared/utils/utils.module';
import { CountoModule } from 'angular2-counto';
import { ModalModule } from 'ngx-bootstrap/modal';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { SignatureRoutingModule } from './signature-routing.module';
import { BsDatepickerConfig, BsDaterangepickerConfig, BsLocaleService } from 'ngx-bootstrap/datepicker';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { NgxBootstrapDatePickerConfigService } from 'assets/ngx-bootstrap/ngx-bootstrap-datepicker-config.service';
import { SubheaderModule } from '@app/shared/common/sub-header/subheader.module';
//import { ManageLeadsRoutingModule } from './manageleads/manageleads-routing.module';
import { NgSelectModule } from '@ng-select/ng-select';

import { AdminSharedModule } from '@app/admin/shared/admin-shared.module';
import { AppSharedModule } from '@app/shared/app-shared.module';
import { EmailEditorModule } from 'angular-email-editor';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { PaginatorModule } from 'primeng/paginator';
import { EditorModule } from 'primeng/editor';
import { InputMaskModule } from 'primeng/inputmask'; import { FileUploadModule } from 'primeng/fileupload';
import { TableModule } from 'primeng/table';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { CustomerSignatureComponent } from './customer-signature/customer-signature.component';
//import { AngularSignaturePadModule } from '@almothafar/angular-signature-pad';
import { SignaturePadModule } from '@ng-plus/signature-pad/src/lib/signature-pad.module';
import { DataTransferService } from '@app/shared/common/projectservice/data-transfer.service';
NgxBootstrapDatePickerConfigService.registerNgxBootstrapDatePickerLocales();

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ModalModule,
        TabsModule,
        TooltipModule,
        AppCommonModule,
        UtilsModule,
        SignatureRoutingModule,
        CountoModule,       
        SubheaderModule,
        AdminSharedModule,
        AppSharedModule,          
        NgSelectModule,
        EmailEditorModule,
        AutoCompleteModule,
		PaginatorModule,
		EditorModule,
		InputMaskModule,
		TableModule,
        OwlDateTimeModule,
        OwlNativeDateTimeModule,
        //SignaturePadModule 
       // LeadsModule  
    ],
    declarations: [CustomerSignatureComponent],
    //exports: [LeadsModule],
    providers: [DataTransferService,
         { provide: BsDatepickerConfig, useFactory: NgxBootstrapDatePickerConfigService.getDatepickerConfig },
         { provide: BsDaterangepickerConfig, useFactory: NgxBootstrapDatePickerConfigService.getDaterangepickerConfig },
         { provide: BsLocaleService, useFactory: NgxBootstrapDatePickerConfigService.getDatepickerLocale },
         { provide: DatePipe }
    ],
})
export class SignatureModule {}