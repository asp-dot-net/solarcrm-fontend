import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CustomerSignatureComponent } from './customer-signature/customer-signature.component';

const routes: Routes = [
    {
        path: '',
        component: CustomerSignatureComponent,
        pathMatch: 'full',
    },
    { path: 'customer-signature', component: CustomerSignatureComponent },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})

export class SignatureRoutingModule { }