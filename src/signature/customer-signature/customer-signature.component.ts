import { Component, HostListener, Injector, Input, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { accountModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { PdfTemplateServiceProxy, QuotationDataDetailsDto, QuotationsServiceProxy, SaveCustomerSignature, } from '@shared/service-proxies/service-proxies'; //SaveCustomerSignature, SignatureRequestDto
import { finalize } from 'rxjs/operators';
//import { NgSignaturePadOptions, SignaturePadComponent } from '@almothafar/angular-signature-pad';
import { NgxSpinnerService } from 'ngx-spinner';
import { AppConsts } from '@shared/AppConsts';
import { DomSanitizer } from '@angular/platform-browser';
import * as _ from 'lodash';
import { DateTimeService } from '@app/shared/common/timing/date-time.service';
import { SignaturePadComponent } from '@ng-plus/signature-pad/src/lib/signature-pad.component';
import SignaturePad from 'signature_pad';
import { DataTransferService } from '@app/shared/common/projectservice/data-transfer.service';

@Component({
    templateUrl: './customer-signature.component.html',
    styleUrls: ['./customer-signature.component.less'],
    animations: [accountModuleAnimation()]
})

export class CustomerSignatureComponent extends AppComponentBase implements OnInit {

    constructor(
        injector: Injector,
        private _httpClient: HttpClient,
        private _activatedRoute: ActivatedRoute,
        private _quotationsServiceProxy: QuotationsServiceProxy,        
        private _dateTimeService: DateTimeService,
        private _pdfTemplateServiceProxy: PdfTemplateServiceProxy,
        private _dataTransfer: DataTransferService,
        //@Inject(DOCUMENT) private document: Document,
        private sanitizer: DomSanitizer,
        private spinner: NgxSpinnerService) {
        super(injector);
        // lat:any;
        // lng:any;
    }
    item: QuotationDataDetailsDto;
    @Input() name: string;
    @ViewChild('sigPad') sigPad;
    @ViewChild('signature')
    //public signaturePad: SignaturePadComponent;


    sigPadElement;
    context;
    isDrawing = false;
    img;
    TD: number;
    JD: number;
    QD: number;
    STR: string;
    //quotationData: SignatureRequestDto = new SignatureRequestDto();
    expired: boolean = false;
    signed: boolean = false;
    isChange = false;
    ipAddress: any;
    lat: any;
    lng: any;
    lat1: any;
    lng1: any;
    saving = false;
    signatureImage;
    signatrefile;
    checkboxtrue = false;
    signaturePad;
    htmlContentbidy: any;

    ngOnInit(): void {
       
        this.STR = this._activatedRoute.snapshot.queryParams['STR'];
        this.spinner.show();
        this._quotationsServiceProxy.quotationData(this.STR).subscribe(result => {
            debugger
            if (result.isQuotateExpiry || result.isQuotateSigned) {
                this.expired = result.isQuotateExpiry;
                this.signed = result.isQuotateSigned;
                this.item = result;
                this.spinner.hide();
            }
            else {
                this.show(result);//this.quotationData = result;
            }

        });

        if (navigator) {
            ;
            navigator.geolocation.getCurrentPosition(pos => {
                this.lng = + pos.coords.longitude;
                this.lat = + pos.coords.latitude;
            });
        }
    }
    show(result): void {
        
        this.item = result;
        this._pdfTemplateServiceProxy.getPdfTemplateForView(AppConsts.pdfGenerateDocument.Quotation, this.item.organizationDetails[0]["id"]).subscribe(result => {
            if (result.pdfTemplates != undefined) {
                this.htmlContentbidy = result.pdfTemplates.viewHtml;
                this.setHTML(this.htmlContentbidy);
                this.spinner.hide();
            }else{
                this.spinner.hide();
                this.notify.info("Please First Create Quotation Template");
            }
        });
    }

    setHTML(quoteHTML) {
        let htmlTemplate = this.getQuoteTemplate(quoteHTML);
        this.htmlContentbidy = htmlTemplate;
        this.htmlContentbidy = this.sanitizer.bypassSecurityTrustHtml(this.htmlContentbidy);

    }
    //signatureImage:any;
    getQuoteTemplate(smsHTML) {
        let myTemplateStr = smsHTML;
        let TableList = [];
        let BankList = [];

        if (this.item.jobProductItemList != null) {
            this.item.jobProductItemList.forEach(obj => {
                TableList.push("<tr><td><div class='mb20'><div class='bold f16 mb10'>" + obj.quoteProductType + '</div><div>' + obj.quoteProductName + "(" + obj.quoteProductModelNumber + ")" + "</div></div></td><td>" + obj.quoteProductQuantity + "</td></tr>");
            })
        }
        if (this.item.subcidy20 != null) {
            TableList.push("<tr><td>" + 'subcidy20' + "</td><td>" + this.item.subcidy20 + "</td></tr>");
        }
        if (this.item.subcidy40 != null) {
            TableList.push("<tr><td>" + 'subcidy40' + "</td><td>" + this.item.subcidy40 + "</td></tr>");
        }
        if (this.item.jobVariationItemList != null) {
            this.item.jobVariationItemList.forEach(obj => {
                TableList.push("<tr><td>" + obj.quoteVarationName + "</td><td>" + obj.quoteVarationCost + "</td></tr>");
            })
        }
        // this.signatureImage = this.signaturePad.toDataURL();
        let QrcodeImg;
        if (this.item.organizationbankDetails != null) {
            this.item.organizationbankDetails.forEach(obj => {
                QrcodeImg = '';
                BankList.push("<div class='payment-panel'><div class='pay-content'>Bank : " + obj.orgBankName + "</div><div class='pay-content'>A/C Name : " + obj.orgBankBrachName + "</div><div class='pay-content'>A/C No. : " + obj.orgBankAccountNumber + "</div><div class='pay-content'>IFSC : " + obj.orgBankIFSC_Code + "</div></div>");
                QrcodeImg = AppConsts.remoteServiceBaseUrl + obj.orgBankQrCode_path;
            });
            BankList.push("<div class='payment-panel-code'><img src='" + QrcodeImg + "'></div>");
        }
        let addressValue = this.item.address1 + " " + this.item.address2; //+ ", " + this.item.lead.state + "-" + this.item.lead.postCode;
        let myVariables = {
            Customer: {
                Name: this.item.customerName,
                Address: addressValue,
                Mobile: this.item.mobile,
                Email: this.item.email,
                // Phone: this.item.leads.alt_Phone,
                // SalesRep: this.item.currentAssignUserName
            },
            Quote: {
                QuoteNumber: this.item.quoteNumber,
                QuotaSigned: this.item.isQuotateSigned,
                QuoteSinedDate: this.item.quotateSignedDate == undefined ? this._dateTimeService.getUTCDate() : this.item.quotateSignedDate,
                QuoteSignedImagePath: this.item.quotateSignedImagePath == '' ? '' : AppConsts.docUrl + "/" + this.item.organizationDetails[0]['organization_LogoFilePath'],
                SystemCapacity: this.item.pvCapacityKw != null ? this.item.pvCapacityKw + " " : this.item.pvCapacityKw,
                QuoteGenerateDate: this.item.quoteGenerateDate,
                SolarAdvisorName: this.item.solarAdvisorName,
                SolarAdvisorMobile: this.item.solarAdvisorMobile,
                SolarAdvisorEmail: this.item.solarAdvisorEmail,
                DiscomName: this.item.discomName,
                HeightOfStructure: this.item.heightOfStructure,
                DivisionName: this.item.divisionName,
                QuoteNotes: this.item.quoteNotes,
                BillToPay: this.item.billToPay
            },
            QuoteProduct: {
                QuoteTable: TableList.toString().replace(/,/g, ''),
                // TrackingNo: this.item.trackingNo,
                // TransportCompany: this.item.transportCompanyName,
                // TransportLink: this.item.transportLink,
                // PromoType: this.item.freebiesPromoType,
            },
            Organization: {
                orgName: this.item.organizationDetails[0]['displayName'],
                orgEmail: this.item.organizationDetails[0]['organization_Email'],
                orgMobile: this.item.organizationDetails[0]['organization_Mobile'],
                orgLogo: AppConsts.docUrl + "/" + this.item.organizationDetails[0]['organization_LogoFilePath'],  //this.item.organizationDetails[0]['organization_GSTNumber']
                orgWebsite: this.item.organizationDetails[0]['organization_Website'],
                bankDetails: BankList.toString().replace(/,/g, ''),
            },
        }

        // use custom delimiter {{ }}
        _.templateSettings.interpolate = /{{([\s\S]+?)}}/g;

        // interpolate
        let compiled = _.template(myTemplateStr);
        let myTemplateCompiled = compiled(myVariables);
        return myTemplateCompiled;
    }

    save() {
        this.saving = true;
        //this.spinner.show();
        this.signaturePad.toDataURL("image/jpeg")
        const input = new SaveCustomerSignature();
        input.encString = this.STR;
        input.imageData = this.signaturePad.toDataURL("image/jpeg");
        input.quoteSignLatitude = this.lat;
        input.quoteSignLongitude = this.lng;
        input.quoteSignIP = this.ipAddress;
        this.img = this.signaturePad.toDataURL("image/png");
        this._quotationsServiceProxy.saveSignature(input)
            .subscribe(() => {
                this.saving = false;
                this.spinner.hide();
                this.ngOnInit();
                this.notify.info(this.l('SavedSuccessfully'));
            });
        //  }
        //  else {
        //      this.message.warn("Please Accept term And Condition First");
        //  }
    }
    clear() {
        this.signaturePad.clear();
    }
    ngAfterViewInit() {
        const canvas = document.querySelector("canvas");

        this.signaturePad = new SignaturePad(canvas, {
            minWidth: 1,
            maxWidth: 2,
            penColor: "rgb(66, 133, 244)"
        });
        this.resizeCanvas();
        // this.signaturePad is now available
        //this.signaturePad.set('minWidth', 5); // set szimek/signature_pad options at runtime
        //this.signaturePad.clear(); // invoke functions from szimek/signature_pad API
    }
    resizeCanvas() {
        const ratio = Math.max(window.devicePixelRatio || 1, 1);
        this.signaturePad.width = this.signaturePad.offsetWidth * ratio;
        this.signaturePad.height = this.signaturePad.offsetHeight * ratio;
        // this.signaturePad.getContext("2d").scale(ratio, ratio);
        this.signaturePad.clear(); // otherwise isEmpty() might return incorrect value
    }

}